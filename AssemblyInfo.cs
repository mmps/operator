﻿// Assembly Operator, Version 1.22.24.2

[assembly: System.Reflection.AssemblyVersion("1.22.24.2")]
[assembly: System.Reflection.AssemblyTrademark("eKassir")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Runtime.InteropServices.Guid("eeffb677-6439-4883-a2bc-724b97ddbf47")]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 eKassir 2009")]
[assembly: System.Reflection.AssemblyProduct("eKassir PaySystem Operator")]
[assembly: System.Reflection.AssemblyFileVersion("1.22.24.2")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyTitle("PaySystem Operator")]
[assembly: System.Reflection.AssemblyDescription("Управление платежами в системе eKassir")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("eKassir")]

