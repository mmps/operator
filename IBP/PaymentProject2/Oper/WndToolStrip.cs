﻿namespace IBP.PaymentProject2.Oper
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    internal class WndToolStrip : ToolStrip
    {
        private ToolStripButton activeButton;
        private Form parent;

        public WndToolStrip(Form parent)
        {
            base.BackColor = SystemColors.Control;
            base.GripStyle = ToolStripGripStyle.Hidden;
            base.RenderMode = ToolStripRenderMode.System;
            base.LayoutStyle = ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.Dock = DockStyle.Bottom;
            this.parent = parent;
            base.Visible = true;
            parent.Controls.Add(this);
            parent.Controls.SetChildIndex(this, 0);
            this.parent.MdiChildActivate += new EventHandler(this.Parent_MdiChildActivate);
            base.ItemClicked += new ToolStripItemClickedEventHandler(this.ButtonClick);
            base.MouseDoubleClick += new MouseEventHandler(this.WndToolStrip_MouseDoubleClick);
        }

        private void ButtonClick(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem clickedItem = e.ClickedItem;
            foreach (Form form in this.parent.MdiChildren)
            {
                if (form.GetHashCode().Equals(clickedItem.Tag))
                {
                    if (this.parent.ActiveMdiChild != form)
                    {
                        form.Activate();
                    }
                    else
                    {
                        switch (form.WindowState)
                        {
                            case FormWindowState.Normal:
                                form.WindowState = FormWindowState.Maximized;
                                break;

                            case FormWindowState.Maximized:
                                form.WindowState = FormWindowState.Normal;
                                break;
                        }
                    }
                    break;
                }
            }
        }

        private void OnFormClosed(object sender, FormClosedEventArgs e)
        {
            int hashCode = sender.GetHashCode();
            ToolStripButton button = null;
            foreach (ToolStripItem item in this.Items)
            {
                if (item.Tag.Equals(hashCode))
                {
                    button = item as ToolStripButton;
                    break;
                }
            }
            if (button != null)
            {
                this.Items.Remove(button);
                button.Dispose();
            }
        }

        private void Parent_MdiChildActivate(object sender, EventArgs e)
        {
            if (this.parent.ActiveMdiChild != null)
            {
                string text = this.parent.ActiveMdiChild.Text;
                int hashCode = this.parent.ActiveMdiChild.GetHashCode();
                if (this.activeButton != null)
                {
                    this.activeButton.Checked = false;
                }
                ToolStripButton button = null;
                foreach (ToolStripItem item in this.Items)
                {
                    if (item.Tag.Equals(hashCode))
                    {
                        button = item as ToolStripButton;
                        break;
                    }
                }
                if (button != null)
                {
                    this.activeButton = button;
                    this.activeButton.Checked = true;
                }
                else
                {
                    this.activeButton = new ToolStripButton();
                    this.activeButton.AutoToolTip = false;
                    this.activeButton.Text = text;
                    this.activeButton.Tag = hashCode;
                    this.activeButton.Checked = true;
                    this.activeButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
                    this.activeButton.MergeIndex = 0;
                    this.activeButton.MergeAction = MergeAction.Insert;
                    this.Items.Add(this.activeButton);
                    this.parent.ActiveMdiChild.FormClosed += new FormClosedEventHandler(this.OnFormClosed);
                }
            }
        }

        private void WndToolStrip_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            foreach (ToolStripItem item in this.Items)
            {
                if (item.Bounds.Contains(pt))
                {
                    foreach (Form form in this.parent.MdiChildren)
                    {
                        if (form.GetHashCode().Equals(item.Tag))
                        {
                            form.Close();
                            break;
                        }
                    }
                }
            }
        }
    }
}

