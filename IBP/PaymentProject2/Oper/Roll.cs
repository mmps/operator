﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections;
    using System.IO;
    using System.Reflection;

    public abstract class Roll
    {
        protected string ext = "rol";
        protected string filename;
        private IBP.PaymentProject2.Common.GatewayType gatewayType;
        private static Hashtable rollsTable = new Hashtable();

        protected Roll()
        {
        }

        public abstract Stream CreateRoll(Payment[] payments);
        private static Roll CreateRoll(IBP.PaymentProject2.Common.GatewayType type)
        {
            Roll roll = FindType(type).InvokeMember(null, BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly, null, null, null) as Roll;
            roll.gatewayType = type;
            return roll;
        }

        private static Type FindType(IBP.PaymentProject2.Common.GatewayType type)
        {
            foreach (Type type2 in typeof(Roll).Assembly.GetTypes())
            {
                IEnumerator enumerator = type2.GetCustomAttributes(typeof(RollAttribute), false).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    RollAttribute current = enumerator.Current as RollAttribute;
                    if (current.Type == type)
                    {
                        return type2;
                    }
                }
            }
            throw new ArgumentOutOfRangeException("type", "Can't find suitable roll.");
        }

        private static Roll GetR(IBP.PaymentProject2.Common.GatewayType type)
        {
            if (rollsTable.ContainsKey(type))
            {
                return (rollsTable[type] as Roll);
            }
            Roll roll = null;
            try
            {
                roll = CreateRoll(type);
                rollsTable.Add(type, roll);
            }
            catch (ArgumentOutOfRangeException)
            {
                return GetR(IBP.PaymentProject2.Common.GatewayType.NoGateway);
            }
            return roll;
        }

        public static Roll GetRoll(IBP.PaymentProject2.Common.GatewayType type)
        {
            lock (rollsTable)
            {
                return GetR(type);
            }
        }

        public string Ext
        {
            get
            {
                return this.ext;
            }
        }

        public string FileName
        {
            get
            {
                return this.filename;
            }
        }

        public IBP.PaymentProject2.Common.GatewayType GatewayType
        {
            get
            {
                return this.gatewayType;
            }
        }
    }
}

