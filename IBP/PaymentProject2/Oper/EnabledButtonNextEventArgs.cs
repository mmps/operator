﻿namespace IBP.PaymentProject2.Oper
{
    using System;

    public class EnabledButtonNextEventArgs : EventArgs
    {
        private bool enabled;

        public EnabledButtonNextEventArgs(bool enabled)
        {
            this.enabled = enabled;
        }

        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }
    }
}

