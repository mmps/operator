﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common.Reports;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class ReportSelectForm : Form
    {
        private Report _selectedReport;
        private Button buttonCancel;
        private Button buttonOk;
        private IContainer components;
        private GroupBox groupBox1;
        private Label label1;
        private TextBox textBoxDescription;
        private TreeView treeViewReports;

        public ReportSelectForm(List<Report> list)
        {
            this.InitializeComponent();
            foreach (Report report in list)
            {
                this.AddNode(report);
            }
        }

        private void AddNode(Report report)
        {
            foreach (TreeNode node in this.treeViewReports.Nodes)
            {
                if (report.Category == node.Text)
                {
                    TreeNode node2 = new TreeNode(report.Name);
                    node2.Tag = report;
                    node.Nodes.Add(node2);
                    return;
                }
            }
            TreeNode node3 = new TreeNode(report.Category);
            TreeNode node4 = new TreeNode(report.Name);
            node4.Tag = report;
            node3.Nodes.Add(node4);
            this.treeViewReports.Nodes.Add(node3);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ReportSelectForm));
            this.label1 = new Label();
            this.textBoxDescription = new TextBox();
            this.buttonOk = new Button();
            this.buttonCancel = new Button();
            this.treeViewReports = new TreeView();
            this.groupBox1 = new GroupBox();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.textBoxDescription.BorderStyle = BorderStyle.None;
            manager.ApplyResources(this.textBoxDescription, "textBoxDescription");
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.buttonOk.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonCancel.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.treeViewReports.BackColor = SystemColors.Window;
            manager.ApplyResources(this.treeViewReports, "treeViewReports");
            this.treeViewReports.Name = "treeViewReports";
            this.treeViewReports.BeforeSelect += new TreeViewCancelEventHandler(this.treeViewReports_BeforeSelect);
            this.groupBox1.Controls.Add(this.textBoxDescription);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.treeViewReports);
            base.Controls.Add(this.buttonCancel);
            base.Controls.Add(this.buttonOk);
            base.Controls.Add(this.label1);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ReportSelectForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void treeViewReports_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                Report tag = (Report) e.Node.Tag;
                this._selectedReport = tag;
                this.textBoxDescription.Text = tag.Description;
                this.buttonOk.Enabled = true;
            }
            else
            {
                this.buttonOk.Enabled = false;
                this._selectedReport = null;
            }
        }

        public Report SelectedReport
        {
            get
            {
                return this._selectedReport;
            }
        }
    }
}

