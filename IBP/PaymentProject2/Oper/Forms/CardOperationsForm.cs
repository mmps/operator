﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [BindType(typeof(CardOperation)), FilterForm(TypeSearch.CardOperations)]
    internal class CardOperationsForm : ResultForm
    {
        private IContainer components;

        public CardOperationsForm()
        {
            this.InitializeComponent();
            base.mainMenuItem.Text = UserStrings.CardOperation;
            base.CreateMainMenuCommand(new ShowCardPayment(), UserStrings.ShowPays);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CardOperationsForm));
            base.SuspendLayout();
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "CardOperationsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            try
            {
                base.listViewItems.SuspendLayout();
                CardFilterOper oper = (CardFilterOper) filter;
                List<CardOperation> cardOperations = IBP.PaymentProject2.Oper.ServerFasad.Server.GetCardOperations(oper.CardFilter);
                base.listViewItems.Items.Clear();
                base.listViewItems.AddRange(cardOperations.ToArray());
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            CardOperation tag = (CardOperation) e.Tag;
            if (!tag.Success)
            {
                e.Item.BackColor = CardOperationsSettings.Default.FailedColor;
            }
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return CardOperationsSettings.Default.Helper;
            }
        }
    }
}

