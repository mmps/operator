﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Windows.Forms;

    internal class ClientTariffPlanHistoryForm : Form
    {
        protected ListViewEdit _listViewItems;
        private IContainer components;

        public ClientTariffPlanHistoryForm(string clientName, ClientTariffPlanHistoryItem[] items)
        {
            this.InitializeComponent();
            this._listViewItems.BindType = typeof(ClientTariffPlanHistoryItem);
            this._listViewItems.FormatProvider = MainSettings.Default.CultureInfo;
            this.Text = string.Format(UserStrings.HistoryTariffPlan, clientName);
            this.SetValues(items);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientTariffPlanHistoryForm));
            this._listViewItems = new ListViewEdit();
            base.SuspendLayout();
            this._listViewItems.AllowSort = true;
            this._listViewItems.BindType = null;
            this._listViewItems.ColumnsCollection = null;
            manager.ApplyResources(this._listViewItems, "_listViewItems");
            this._listViewItems.FormatProvider = new CultureInfo("");
            this._listViewItems.FullRowSelect = true;
            this._listViewItems.GridLines = true;
            this._listViewItems.HideSelection = false;
            this._listViewItems.Name = "_listViewItems";
            this._listViewItems.ReadOnly = true;
            this._listViewItems.UseCompatibleStateImageBehavior = false;
            this._listViewItems.View = View.Details;
            manager.ApplyResources(this, "$this");
            base.Controls.Add(this._listViewItems);
            base.Name = "ClientTariffPlanHistoryForm";
            base.ShowIcon = false;
            base.ResumeLayout(false);
        }

        private void SetValues(ClientTariffPlanHistoryItem[] items)
        {
            this._listViewItems.SuspendLayout();
            this._listViewItems.Items.Clear();
            this._listViewItems.AddRange(items);
            this._listViewItems.ResumeLayout();
        }
    }
}

