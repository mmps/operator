﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections.Generic;

    internal class OperMenuController
    {
        private List<OperMenuCommand> _arrayCmdItems = new List<OperMenuCommand>();
        private Command _mainCmd;
        private List<OperMenuCommand> _simpleCmdItems = new List<OperMenuCommand>();

        public void AddCmd(OperMenuCommand cmd)
        {
            if (cmd.CommandTagIsArray)
            {
                this._arrayCmdItems.Add(cmd);
            }
            else
            {
                this._simpleCmdItems.Add(cmd);
            }
        }

        public void AddMainCmd(OperMenuCommand cmd)
        {
            if (cmd.CommandTagIsArray)
            {
                this._arrayCmdItems.Add(cmd);
            }
            else
            {
                this._simpleCmdItems.Add(cmd);
            }
            this._mainCmd = cmd.Command;
        }

        public void TryExecuteMain()
        {
            if ((this._mainCmd != null) && this._mainCmd.CanExecute)
            {
                this._mainCmd.Execute();
            }
        }

        public object Tag
        {
            get
            {
                if (this._arrayCmdItems.Count > 0)
                {
                    return this._arrayCmdItems[0].Tag;
                }
                if (this._simpleCmdItems.Count > 0)
                {
                    return this._simpleCmdItems[0].Tag;
                }
                return null;
            }
            set
            {
                object obj2 = (value != null) ? ((object[]) value)[0] : null;
                foreach (OperMenuCommand command in this._simpleCmdItems)
                {
                    command.Tag = obj2;
                }
                foreach (OperMenuCommand command2 in this._arrayCmdItems)
                {
                    command2.Tag = value;
                }
            }
        }
    }
}

