﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterForm(TypeSearch.ExternalTransactions), BindType(typeof(ExternalTransaction))]
    internal class ExternalTransactionsForm : ResultForm
    {
        private IContainer components;

        public ExternalTransactionsForm()
        {
            this.InitializeComponent();
            base.mainMenuItem.Text = UserStrings.Receipt;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ExternalTransactionsForm));
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "ExternalTransactionsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            try
            {
                base.listViewItems.Items.Clear();
                foreach (ExternalTransaction transaction in IBP.PaymentProject2.Oper.ServerFasad.Server.GetExternalTransactions())
                {
                    base.listViewItems.Add(transaction);
                }
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            ExternalTransaction tag = (ExternalTransaction) e.Tag;
            e.Item.BackColor = tag.IsCallback ? ExtTransactionsSettings.Default.CallbackColor : ExtTransactionsSettings.Default.ExternalColor;
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return ExtTransactionsSettings.Default.Helper;
            }
        }
    }
}

