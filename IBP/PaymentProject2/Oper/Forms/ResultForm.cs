﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using Microsoft.Reporting.WinForms;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    [BindType(typeof(Payment))]
    internal class ResultForm : Form, IFormResultSearch
    {
        private readonly string _closeFormat;
        private bool _mainCmdExists;
        private System.Type bindType;
        private ToolStripMenuItem closeItemCM;
        private ToolStripMenuItem closeItemMM;
        private IContainer components;
        protected ContextMenuStrip contextMenu;
        private static bool enableResizeReaction;
        private FilterOper filter;
        protected ListViewEdit listViewItems;
        protected ToolStripMenuItem mainMenuItem;
        protected OperMenuController menuController;
        private bool report;
        private ToolStripMenuItem reportItemMM;
        private ReportViewer reportViewer;
        protected StatusStrip statusStripBottom;

        public ResultForm()
        {
            EventHandler handler = null;
            this._closeFormat = UserStrings.Close;
            this.menuController = new OperMenuController();
            this.mainMenuItem = new ToolStripMenuItem();
            enableResizeReaction = false;
            this.InitializeComponent();
            this.closeItemMM = new ToolStripMenuItem(this._closeFormat, null, new EventHandler(this.CloseWindow));
            this.closeItemCM = new ToolStripMenuItem(this._closeFormat, null, new EventHandler(this.CloseWindow));
            this.reportItemMM = new ToolStripMenuItem(UserStrings.Views, null, new EventHandler(this.SwitchView));
            this.contextMenu.Opening += new CancelEventHandler(this.UpdateCloseText);
            this.mainMenuItem.DropDownOpening += new EventHandler(this.UpdateCloseText);
            this.listViewItems.ContextMenuStrip = this.contextMenu;
            this.listViewItems.AllowColumnReorder = true;
            this.listViewItems.AllowSort = true;
            if (!base.DesignMode)
            {
                this.listViewItems.ItemAdded += new EventHandler<ItemEventArgs>(this.OnItemAdded);
                this.listViewItems.SelectedIndexChanged += new EventHandler(this.OnSelectedItemChanged);
                if (handler == null)
                {
                    handler = delegate {
                        this.menuController.TryExecuteMain();
                    };
                }
                this.listViewItems.DoubleClick += handler;
                this.listViewItems.BindType = this.BindType;
                if (((this.SelectedFieldsHelper != null) && this.listViewItems.IsBinding) && this.SelectedFieldsHelper.IsReady)
                {
                    this.listViewItems.ColumnsCollection = this.SelectedFieldsHelper.SelectedFields;
                    this.listViewItems.ColumnsCollectionChanged += new EventHandler(this.OnColumnsChanged);
                    this.SelectedFieldsHelper.SelectedFieldsChanged += new EventHandler(this.OnSelectedFieldsChanged);
                }
                this.listViewItems.FormatProvider = MainSettings.Default.CultureInfo;
                MainSettings.Default.CultureInfoChanged += new EventHandler(this.OnCultureChanged);
                this.listViewItems.ViewItemsChanged += new EventHandler<ViewItemsEventArgs>(this.listViewItems_ViewItemsChanged);
                this.listViewItems.ColumnsCollectionChanged += new EventHandler(this.ColumsSwitchView);
            }
        }

        private void CloseWindow(object sender, EventArgs e)
        {
            base.Close();
        }

        private void ColumsSwitchView(object sender, EventArgs e)
        {
            this.SwitchView(null, null);
        }

        protected void CreateCommand(IBP.PaymentProject2.Oper.Command cmd, string caption)
        {
            OperMenuCommand command = OperMenuCommand.GetMenuCommand(cmd, this.mainMenuItem, null, caption);
            this.menuController.AddCmd(command);
        }

        protected void CreateContextCommand(IBP.PaymentProject2.Oper.Command cmd, string caption)
        {
            OperMenuCommand command = OperMenuCommand.GetMenuCommand(cmd, null, this.contextMenu, caption);
            this.menuController.AddCmd(command);
        }

        protected void CreateMainMenuCommand(IBP.PaymentProject2.Oper.Command cmd, string caption)
        {
            if (this._mainCmdExists)
            {
                throw new InvalidOperationException("Дважды установлена основная комманда");
            }
            OperMenuCommand command = OperMenuCommand.GetMenuCommand(cmd, this.mainMenuItem, this.contextMenu, caption);
            this.menuController.AddMainCmd(command);
            this._mainCmdExists = true;
        }

        protected void CreateMenuCommand(IBP.PaymentProject2.Oper.Command cmd, string caption)
        {
            OperMenuCommand command = OperMenuCommand.GetMenuCommand(cmd, this.mainMenuItem, this.contextMenu, caption);
            this.menuController.AddCmd(command);
        }

        private void CreateReportView()
        {
            base.SuspendLayout();
            if (base.Controls.Contains(this.reportViewer))
            {
                base.Controls.Remove(this.reportViewer);
                this.reportViewer.Dispose();
            }
            this.reportViewer = new ReportViewer();
            this.reportViewer.Dock = DockStyle.Fill;
            this.reportViewer.Location = new Point(0, 0);
            this.reportViewer.Name = "reportViewer";
            this.reportViewer.Size = new Size(0x248, 0x10c);
            this.reportViewer.TabIndex = 2;
            this.reportViewer.Visible = this.report;
            base.Controls.Add(this.reportViewer);
            base.ResumeLayout();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ResultForm));
            this.statusStripBottom = new StatusStrip();
            this.contextMenu = new ContextMenuStrip(this.components);
            this.listViewItems = new ListViewEdit();
            base.SuspendLayout();
            manager.ApplyResources(this.statusStripBottom, "statusStripBottom");
            this.statusStripBottom.Name = "statusStripBottom";
            this.contextMenu.Name = "contextMenu";
            manager.ApplyResources(this.contextMenu, "contextMenu");
            this.listViewItems.BindType = null;
            this.listViewItems.ColumnsCollection = null;
            manager.ApplyResources(this.listViewItems, "listViewItems");
            this.listViewItems.FullRowSelect = true;
            this.listViewItems.GridLines = true;
            this.listViewItems.HideSelection = false;
            this.listViewItems.Name = "listViewItems";
            this.listViewItems.ReadOnly = true;
            this.listViewItems.UseCompatibleStateImageBehavior = false;
            this.listViewItems.View = View.Details;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.listViewItems);
            base.Controls.Add(this.statusStripBottom);
            base.Name = "ResultForm";
            base.ShowIcon = false;
            base.Shown += new EventHandler(this.ResultForm_Shown);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void listViewItems_ViewItemsChanged(object sender, ViewItemsEventArgs e)
        {
            try
            {
                this.reportViewer.LocalReport.DataSources.Clear();
                this.reportViewer.LocalReport.DataSources.Add(new ReportDataSource("mydata", e.Items));
            }
            catch (InvalidOperationException)
            {
            }
        }

        protected override void OnActivated(EventArgs e)
        {
            if (!base.DesignMode)
            {
                if (!this.mainMenuItem.DropDown.Items.Contains(this.closeItemMM))
                {
                    this.mainMenuItem.DropDown.Items.Add(new ToolStripSeparator());
                    this.mainMenuItem.DropDown.Items.Add(this.closeItemMM);
                }
                if (!this.mainMenuItem.DropDown.Items.Contains(this.reportItemMM))
                {
                    this.mainMenuItem.DropDown.Items.Add(new ToolStripSeparator());
                    this.reportItemMM.Text = this.report ? UserStrings.Table : UserStrings.Report;
                    this.mainMenuItem.DropDown.Items.Add(this.reportItemMM);
                }
                base.MdiParent.MainMenuStrip.Items.Insert(base.MdiParent.MainMenuStrip.Items.Count - 1, this.mainMenuItem);
            }
            base.OnActivated(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (this.SelectedFieldsHelper != null)
            {
                this.SelectedFieldsHelper.SelectedFieldsChanged -= new EventHandler(this.OnSelectedFieldsChanged);
            }
            base.OnClosed(e);
        }

        private void OnColumnsChanged(object sender, EventArgs e)
        {
            this.SelectedFieldsHelper.SelectedFields = this.listViewItems.ColumnsCollection;
        }

        private void OnCultureChanged(object sender, EventArgs e)
        {
            this.listViewItems.FormatProvider = MainSettings.Default.CultureInfo;
        }

        protected override void OnDeactivate(EventArgs e)
        {
            if (!base.DesignMode)
            {
                base.MdiParent.MainMenuStrip.Items.Remove(this.mainMenuItem);
            }
            base.OnDeactivate(e);
        }

        protected virtual void OnFilterChanged(FilterOper filter)
        {
        }

        protected virtual void OnItemAdded(object sender, ItemEventArgs e)
        {
        }

        private void OnSelectedFieldsChanged(object sender, EventArgs e)
        {
            this.listViewItems.ColumnsCollection = this.SelectedFieldsHelper.SelectedFields;
        }

        protected virtual void OnSelectedItemChanged(object sender, EventArgs e)
        {
            object obj2;
            ListView.SelectedListViewItemCollection selectedItems = this.listViewItems.SelectedItems;
            int count = selectedItems.Count;
            if (count == 0)
            {
                obj2 = null;
            }
            else
            {
                object[] objArray = new object[count];
                for (int i = 0; i < count; i++)
                {
                    objArray[i] = selectedItems[i].Tag;
                }
                obj2 = objArray;
            }
            this.menuController.Tag = obj2;
        }

        private void ResultForm_Shown(object sender, EventArgs e)
        {
            if (VisibilityFormSettings.Default.Maximized)
            {
                base.WindowState = FormWindowState.Maximized;
            }
            else
            {
                base.WindowState = FormWindowState.Normal;
            }
            enableResizeReaction = true;
            base.Resize += delegate (object senderII, EventArgs eII) {
                if (enableResizeReaction)
                {
                    if (base.WindowState != FormWindowState.Maximized)
                    {
                        VisibilityFormSettings.Default.Maximized = false;
                    }
                    else
                    {
                        VisibilityFormSettings.Default.Maximized = true;
                    }
                }
            };
        }

        private void SwitchView(object sender, EventArgs e)
        {
            if (sender != null)
            {
                this.report = !this.report;
                ToolStripMenuItem item = (ToolStripMenuItem) sender;
                item.Text = this.report ? UserStrings.Table : UserStrings.Report;
                foreach (Control control in base.Controls)
                {
                    if (control.Name == "reportViewer")
                    {
                        control.Visible = this.report;
                    }
                    else
                    {
                        control.Visible = !this.report;
                    }
                }
            }
            else
            {
                Stream stream = null;
                IEnumerable list = null;
                this.CreateReportView();
                this.listViewItems.GenerateDataReport(out stream, out list);
                this.reportViewer.LocalReport.LoadReportDefinition(stream);
                this.reportViewer.LocalReport.DataSources.Clear();
                ReportDataSource source = new ReportDataSource("mydata", list);
                this.reportViewer.LocalReport.DataSources.Add(source);
                this.reportViewer.Messages = new ReportMessages();
                stream.Flush();
                stream.Dispose();
            }
            if (this.report)
            {
                this.reportViewer.RefreshReport();
            }
        }

        private void UpdateCloseText(object sender, EventArgs e)
        {
            if (!this.contextMenu.Items.Contains(this.closeItemCM))
            {
                this.contextMenu.Items.Add(new ToolStripSeparator());
                this.contextMenu.Items.Add(this.closeItemCM);
            }
            string str = string.Format(this._closeFormat, this.Text);
            this.closeItemCM.Text = this.closeItemMM.Text = str;
        }

        [Browsable(false)]
        protected System.Type BindType
        {
            get
            {
                if (this.bindType == null)
                {
                    System.Type type = base.GetType();
                    object[] customAttributes = type.GetCustomAttributes(typeof(BindTypeAttribute), false);
                    if (customAttributes.Length == 0)
                    {
                        throw new ArgumentException(string.Format(UserStrings.TypeNotMarked, type, typeof(BindTypeAttribute)));
                    }
                    this.bindType = ((BindTypeAttribute) customAttributes[0]).BindType;
                }
                return this.bindType;
            }
        }

        public virtual FilterOper Filter
        {
            get
            {
                return this.filter;
            }
            set
            {
                this.filter = value;
                this.OnFilterChanged(value);
                base.BringToFront();
                this.SwitchView(null, null);
            }
        }

        protected virtual SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return null;
            }
        }
    }
}

