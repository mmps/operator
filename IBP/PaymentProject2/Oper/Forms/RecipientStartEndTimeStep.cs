﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class RecipientStartEndTimeStep : StepControl
    {
        private ComboBox comboBoxRecipient;
        private IContainer components;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private StepInfo info;

        public RecipientStartEndTimeStep(StepInfo info)
        {
            EventHandler handler = null;
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.info = info;
            this.InitializeComponent();
            this.comboBoxRecipient.Items.AddRange(this.info.AllowedRecipients);
            this.dateTimeStart.Value = this.info.Start;
            this.dateTimeEnd.Value = this.info.End;
            if (handler == null)
            {
                handler = delegate (object sender, EventArgs e) {
                    this.info.End = this.dateTimeEnd.Value;
                    this.info.Start = this.dateTimeStart.Value;
                    this.info.Recipient = this.SelectedRecipient;
                    if (this.info.Recipient == null)
                    {
                        this.comboBoxRecipient.Focus();
                        throw new StepException(UserStrings.ReceiverNotChoosen);
                    }
                };
            }
            base.BeforeLeaveStepForward += handler;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecipientStartEndTimeStep));
            this.comboBoxRecipient = new ComboBox();
            this.dateTimeEnd = new DateTimePicker();
            this.dateTimeStart = new DateTimePicker();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "labelRecipient");
            label.Name = "labelRecipient";
            manager.ApplyResources(label2, "label2");
            label2.Name = "label2";
            manager.ApplyResources(label3, "label1");
            label3.Name = "label1";
            manager.ApplyResources(this.comboBoxRecipient, "comboBoxRecipient");
            this.comboBoxRecipient.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxRecipient.DropDownWidth = 300;
            this.comboBoxRecipient.FormatString = "{0}";
            this.comboBoxRecipient.Name = "comboBoxRecipient";
            this.comboBoxRecipient.Sorted = true;
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Name = "dateTimeEnd";
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Name = "dateTimeStart";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(label);
            base.Controls.Add(label3);
            base.Controls.Add(this.comboBoxRecipient);
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(label2);
            base.Controls.Add(this.dateTimeStart);
            this.MinimumSize = new Size(370, 0xed);
            base.Name = "RecipientStartEndTimeStep";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string DateTimeCustomFormat
        {
            get
            {
                return this.dateTimeEnd.CustomFormat;
            }
            set
            {
                this.dateTimeEnd.CustomFormat = value;
                this.dateTimeStart.CustomFormat = value;
            }
        }

        public DateTimePickerFormat DateTimeFormat
        {
            get
            {
                return this.dateTimeEnd.Format;
            }
            set
            {
                this.dateTimeEnd.Format = value;
                this.dateTimeStart.Format = value;
            }
        }

        private Recipient SelectedRecipient
        {
            get
            {
                return (this.comboBoxRecipient.SelectedItem as Recipient);
            }
        }
    }
}

