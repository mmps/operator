﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;

    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
    internal class TariffPlansResource
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal TariffPlansResource()
        {
        }

        internal static string ClauseDeletion
        {
            get
            {
                return ResourceManager.GetString("ClauseDeletion", resourceCulture);
            }
        }

        internal static string Clauses
        {
            get
            {
                return ResourceManager.GetString("Clauses", resourceCulture);
            }
        }

        internal static string ClientTariffPlan
        {
            get
            {
                return ResourceManager.GetString("ClientTariffPlan", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string DoCancelChanges
        {
            get
            {
                return ResourceManager.GetString("DoCancelChanges", resourceCulture);
            }
        }

        internal static string DoDeleteSelectedClause
        {
            get
            {
                return ResourceManager.GetString("DoDeleteSelectedClause", resourceCulture);
            }
        }

        internal static string DoDeleteSelectedGroup
        {
            get
            {
                return ResourceManager.GetString("DoDeleteSelectedGroup", resourceCulture);
            }
        }

        internal static string DoSaveTariffPlan
        {
            get
            {
                return ResourceManager.GetString("DoSaveTariffPlan", resourceCulture);
            }
        }

        internal static string Error
        {
            get
            {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }

        internal static string GroupDeletion
        {
            get
            {
                return ResourceManager.GetString("GroupDeletion", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("IBP.PaymentProject2.Oper.Forms.TariffPlan.TariffPlansResource", typeof(TariffPlansResource).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static string Services
        {
            get
            {
                return ResourceManager.GetString("Services", resourceCulture);
            }
        }

        internal static string YouShouldSelectServices
        {
            get
            {
                return ResourceManager.GetString("YouShouldSelectServices", resourceCulture);
            }
        }
    }
}

