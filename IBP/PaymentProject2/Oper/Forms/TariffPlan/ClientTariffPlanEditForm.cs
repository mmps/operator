﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    [NoFilterAtribute]
    internal class ClientTariffPlanEditForm : Form
    {
        private Button _buttonApply;
        private Button _buttonEdit;
        private Button _buttonOK;
        private bool _closeWithOutAsk;
        private FormWindowState _lastNonMinimizedState;
        private Panel _panel;
        private TariffPlanControl _planControl;
        private TextBox _textBoxDescription;
        private TextBox _textBoxName;
        [CompilerGenerated]
        private IBP.PaymentProject2.Oper.IsolationCookie /*<IsolationCookie> */k__BackingField;
        private Button btnCancel;
        private IContainer components;
        private Label label1;
        private Label label2;
        private const string LOCK_TARIFF_PLAN = "LockTariffPlan";
        private const string RELEASE_TARIFF_PLAN = "ReleaseTariffPLan";
        private const string SAVE_TARIFF_PLAN = "SaveTariffPlan";

        public event EventHandler<TariffPlanEventArgs> LockTariffPlan
        {
            add
            {
                base.Events.AddHandler("LockTariffPlan", value);
            }
            remove
            {
                base.Events.RemoveHandler("LockTariffPlan", value);
            }
        }

        public event EventHandler<TariffPlanEventArgs> ReleaseTariffPlan
        {
            add
            {
                base.Events.AddHandler("ReleaseTariffPLan", value);
            }
            remove
            {
                base.Events.AddHandler("ReleaseTariffPLan", value);
            }
        }

        public event EventHandler<TariffPlanEventArgs> SaveTariffPlan
        {
            add
            {
                base.Events.AddHandler("SaveTariffPlan", value);
            }
            remove
            {
                base.Events.RemoveHandler("SaveTariffPlan", value);
            }
        }

        public ClientTariffPlanEditForm(ClientTariffPlan tariffPlan, ReconingMethod[] methods, bool newTariffPlan)
        {
            FormClosingEventHandler handler = null;
            this.InitializeComponent();
            this.Text = tariffPlan.Name;
            this._planControl = new TariffPlanControl(tariffPlan, methods);
            this._planControl.Dock = DockStyle.Fill;
            this.EditEnabled = newTariffPlan;
            this._panel.Controls.Add(this._planControl);
            this._textBoxDescription.Text = tariffPlan.Description;
            this._textBoxName.Text = tariffPlan.Name;
            if (handler == null)
            {
                handler = delegate (object sender, FormClosingEventArgs e) {
                    if (!this._closeWithOutAsk)
                    {
                        DialogResult result = MessageBox.Show(TariffPlansResource.DoCancelChanges, TariffPlansResource.ClientTariffPlan, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        e.Cancel = result == DialogResult.No;
                    }
                    if (!e.Cancel)
                    {
                        this.RaiseReleasePlan();
                    }
                };
            }
            base.FormClosing += handler;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (this.DoSaveTariffPlan())
            {
                this.SaveNameAndDescription();
                this.RaiseSaveTariffPlan();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.DoSaveTariffPlan())
            {
                this.SaveNameAndDescription();
                if (this.RaiseSaveTariffPlan())
                {
                    this.CloseForce();
                }
            }
            else
            {
                this.RaiseReleasePlan();
                this.CloseForce();
            }
        }

        public void CloseForce()
        {
            this._closeWithOutAsk = true;
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DoSaveTariffPlan()
        {
            return (MessageBox.Show(TariffPlansResource.DoSaveTariffPlan, TariffPlansResource.ClientTariffPlan, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
        }

        private void EnableEdit(object sender, EventArgs e)
        {
            if (this.RaiseLockTariffPlan())
            {
                this.EditEnabled = true;
            }
        }

        public static bool FindAndShowTariffPlanWindow(int planId)
        {
            foreach (Form form in Application.OpenForms)
            {
                ClientTariffPlanEditForm form2 = form as ClientTariffPlanEditForm;
                if ((form2 != null) && (form2.TariffPlan.Id == planId))
                {
                    form2.ShowOnTop();
                    return true;
                }
            }
            return false;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientTariffPlanEditForm));
            this.label1 = new Label();
            this._textBoxName = new TextBox();
            this.label2 = new Label();
            this._textBoxDescription = new TextBox();
            this._buttonOK = new Button();
            this.btnCancel = new Button();
            this._buttonApply = new Button();
            this._buttonEdit = new Button();
            this._panel = new Panel();
            base.SuspendLayout();
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this._textBoxName, "_textBoxName");
            this._textBoxName.Name = "_textBoxName";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this._textBoxDescription, "_textBoxDescription");
            this._textBoxDescription.Name = "_textBoxDescription";
            manager.ApplyResources(this._buttonOK, "_buttonOK");
            this._buttonOK.Name = "_buttonOK";
            this._buttonOK.UseVisualStyleBackColor = true;
            this._buttonOK.Click += new EventHandler(this.btnOk_Click);
            manager.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
            manager.ApplyResources(this._buttonApply, "_buttonApply");
            this._buttonApply.Name = "_buttonApply";
            this._buttonApply.UseVisualStyleBackColor = true;
            this._buttonApply.Click += new EventHandler(this.btnApply_Click);
            manager.ApplyResources(this._buttonEdit, "_buttonEdit");
            this._buttonEdit.Name = "_buttonEdit";
            this._buttonEdit.UseVisualStyleBackColor = true;
            this._buttonEdit.Click += new EventHandler(this.EnableEdit);
            manager.ApplyResources(this._panel, "_panel");
            this._panel.Name = "_panel";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._panel);
            base.Controls.Add(this._buttonApply);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this._buttonEdit);
            base.Controls.Add(this._buttonOK);
            base.Controls.Add(this.label2);
            base.Controls.Add(this._textBoxDescription);
            base.Controls.Add(this._textBoxName);
            base.Controls.Add(this.label1);
            base.Name = "ClientTariffPlanEditForm";
            base.ShowIcon = false;
            base.Layout += new LayoutEventHandler(this.OnLayout);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void OnLayout(object sender, LayoutEventArgs e)
        {
            if (base.WindowState != FormWindowState.Minimized)
            {
                this._lastNonMinimizedState = base.WindowState;
            }
        }

        private bool RaiseLockTariffPlan()
        {
            Delegate delegate2 = base.Events["LockTariffPlan"];
            if (delegate2 != null)
            {
                TariffPlanEventArgs e = new TariffPlanEventArgs(this._planControl.TariffPlan);
                ((EventHandler<TariffPlanEventArgs>) delegate2)(this, e);
                return e.Successful;
            }
            return false;
        }

        private bool RaiseReleasePlan()
        {
            Delegate delegate2 = base.Events["ReleaseTariffPLan"];
            if (delegate2 != null)
            {
                TariffPlanEventArgs e = new TariffPlanEventArgs(this._planControl.TariffPlan);
                ((EventHandler<TariffPlanEventArgs>) delegate2)(this, e);
                return e.Successful;
            }
            return false;
        }

        private bool RaiseSaveTariffPlan()
        {
            Delegate delegate2 = base.Events["SaveTariffPlan"];
            if (delegate2 != null)
            {
                TariffPlanEventArgs e = new TariffPlanEventArgs(this._planControl.TariffPlan);
                ((EventHandler<TariffPlanEventArgs>) delegate2)(this, e);
                return e.Successful;
            }
            return false;
        }

        private void SaveNameAndDescription()
        {
            this._planControl.TariffPlan.Name = this._textBoxName.Text;
            this._planControl.TariffPlan.Description = this._textBoxDescription.Text;
        }

        private void ShowOnTop()
        {
            base.WindowState = this._lastNonMinimizedState;
            base.Select();
            base.Show();
        }

        private bool EditEnabled
        {
            get
            {
                return this._textBoxDescription.Enabled;
            }
            set
            {
                this._textBoxDescription.Enabled = value;
                this._textBoxName.Enabled = value;
                this._planControl.Editable = value;
                this._buttonEdit.Enabled = !value;
                this._buttonOK.Enabled = value;
                this._buttonApply.Enabled = value;
                this._closeWithOutAsk = !value;
            }
        }

        public IBP.PaymentProject2.Oper.IsolationCookie IsolationCookie
        {
            [CompilerGenerated]
            get
            {
                return /*this.<IsolationCookie>*/k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                /*this.<IsolationCookie>*/
                k__BackingField = value;
                
            }
        }

        public ClientTariffPlan TariffPlan
        {
            get
            {
                return this._planControl.TariffPlan;
            }
            set
            {
                this._planControl.TariffPlan = value;
            }
        }

        public delegate void SavePlanEventHandler(ClientTariffPlan plan);

        public delegate void UpdateFormEventHandler();
    }
}

