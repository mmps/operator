﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class TariffPlanControl : UserControl
    {
        private TableCell _activeCell;
        private int _allCellsHeigth;
        private bool _alwaysShowScrolling;
        private ReconingMethod[] _availableReconingMethods;
        private int _bottomInvisibleArea;
        private double _columnRate;
        private int[] _columnWidth;
        private ContextMenuStrip _contextMenu;
        private int _count;
        private bool _editable;
        private int _maxCount;
        private double[] _opacity;
        private TableCell _selectedTablePanel;
        private List<TableMasterCell> _tableBody;
        private TablePanel[] _tableHeader;
        private ClientTariffPlan _tariffPlan;
        private Timer _timer;
        private ToolStripMenuItem _toolStripMenuItemAdd;
        private ToolStripMenuItem _toolStripMenuItemCopy;
        private ToolStripMenuItem _toolStripMenuItemDelete;
        private ToolStripMenuItem _toolStripMenuItemEdit;
        private IContainer components;
        private static string TARIFF_PLAN_UPDATED_KEY = "TariffPlanUpdated";
        private VScrollBar vScrollBar1;

        public event EventHandler TariffPlanUpdated
        {
            add
            {
                base.Events.AddHandler(TARIFF_PLAN_UPDATED_KEY, value);
            }
            remove
            {
                base.Events.RemoveHandler(TARIFF_PLAN_UPDATED_KEY, value);
            }
        }

        public TariffPlanControl(ClientTariffPlan tariffPlan, ReconingMethod[] reconingMethods)
        {
            EventHandler handler = null;
            this._editable = true;
            this._columnWidth = new int[2];
            this._columnRate = 0.5;
            this._tableHeader = new TablePanel[2];
            this._tableBody = new List<TableMasterCell>();
            this.InitializeComponent();
            this.DoubleBuffered = true;
            this._tariffPlan = tariffPlan;
            this._availableReconingMethods = reconingMethods;
            this.UpdateTariffPlanClauses(tariffPlan);
            this.InitControl();
            if (handler == null)
            {
                handler = delegate {
                    this.Redraw();
                };
            }
            base.SizeChanged += handler;
        }

        private void _contextMenu_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            this.DeactivateCell();
        }

        private TableCell ActivateCell(object sender)
        {
            TableCell cell = (TableCell) sender;
            cell.Selected = true;
            if ((this._selectedTablePanel != null) && (cell != this._selectedTablePanel))
            {
                this._selectedTablePanel.Selected = false;
                this._count = 0;
            }
            this._selectedTablePanel = cell;
            this._timer.Enabled = true;
            this._activeCell = cell;
            return cell;
        }

        private bool AddAction()
        {
            bool flag;
            if (this._selectedTablePanel is TableMasterCell)
            {
                ClientTariffPlanServiceGroup serviceGroup = this._tariffPlan.CreateAndAddServiceGroup();
                serviceGroup.CreateAndAddClause();
                flag = this.EditServiceGroup(serviceGroup);
                if (!flag)
                {
                    this._tariffPlan.RemoveGroup(serviceGroup);
                    return flag;
                }
                this.UpdateClausesInGroup(serviceGroup);
                return flag;
            }
            ClientTariffPlanServiceGroup tag = (ClientTariffPlanServiceGroup) this._selectedTablePanel.MasterCell.Tag;
            ReconingClause clause = tag.CreateAndAddClause();
            flag = this.EditReconingClause(clause);
            if (!flag)
            {
                tag.RemoveClause(clause);
            }
            return flag;
        }

        private void Build()
        {
            int y = this.CalculateHeaderSize();
            ClientTariffPlanServiceGroup[] serviceGroups = this._tariffPlan.ServiceGroups;
            this._tableBody.Clear();
            int num1 = this._columnWidth[0];
            int num3 = this._columnWidth[1];
            foreach (ClientTariffPlanServiceGroup group in serviceGroups)
            {
                y = this.BuildMasterCell(group, y);
            }
        }

        private int BuildMasterCell(ClientTariffPlanServiceGroup serviceGroup, int y)
        {
            int height = 50;
            int width = this._columnWidth[0];
            int num3 = this._columnWidth[1];
            TableMasterCell item = new TableMasterCell(this, new Point(0, y), new Size(width, height));
            item.Text = serviceGroup.ToString();
            item.Tag = serviceGroup;
            int num4 = 0;
            TableCell cell2 = null;
            foreach (ReconingClause clause in serviceGroup.Clauses)
            {
                cell2 = item.AddSlaveCell(new Point(width, y + num4), new Size(num3, height));
                cell2.Text = clause.Description;
                int num5 = (int) cell2.renderSizeText(num3).Height;
                cell2.Size = new Size(num3, num5);
                cell2.MouseClick += new MouseEventHandler(this.tp_Click);
                cell2.DoubleClick += new EventHandler(this.tp_DoubleClick);
                cell2.Tag = clause;
                num4 += num5;
            }
            item.MouseClick += new MouseEventHandler(this.tp_Click);
            item.DoubleClick += new EventHandler(this.tp_DoubleClick);
            y += item.Height;
            this._tableBody.Add(item);
            return y;
        }

        private void CalculateColumnSize(int h)
        {
            int count = this._tableBody.Count;
            TableCell cell2 = null;
            int width = this._columnWidth[0];
            int height = 0;
            int num5 = this._columnWidth[1];
            int num6 = 0;
            int num7 = this._allCellsHeigth;
            this._allCellsHeigth = h;
            if (this.vScrollBar1.Visible && (this._bottomInvisibleArea > 0))
            {
                double num8 = this.vScrollBar1.Maximum - this.vScrollBar1.Minimum;
                num6 = (int) Math.Floor((double) ((((double) ((this._bottomInvisibleArea + 20) * (this.vScrollBar1.Value - this.vScrollBar1.Minimum))) / num8) + 0.5));
                h -= num6;
            }
            for (int i = 0; i < count; i++)
            {
                TableMasterCell cell = this._tableBody[i];
                cell.Location = new Point(0, h);
                int num2 = (int) cell.renderSizeText(width).Height;
                TableCell[] slaveCells = cell.SlaveCells;
                int length = slaveCells.Length;
                int num11 = 0;
                for (int j = 0; j < length; j++)
                {
                    cell2 = slaveCells[j];
                    cell2.Location = new Point(width, h + num11);
                    height = (int) cell2.renderSizeText(num5).Height;
                    cell2.Size = new Size(num5, height);
                    num11 += height;
                }
                if (num11 >= num2)
                {
                    num2 = num11;
                }
                else if (cell2 != null)
                {
                    cell2.Size = new Size(num5, (num2 - num11) + height);
                }
                cell.Size = new Size(width, num2);
                h += num2;
                this._allCellsHeigth += num2;
            }
            this._bottomInvisibleArea = (num6 + h) - base.Height;
            if (!this.vScrollBar1.Visible && (this._bottomInvisibleArea > 0))
            {
                this.vScrollBar1.Visible = true;
                if (this.vScrollBar1.Visible)
                {
                    this.Redraw();
                }
            }
            if (this.vScrollBar1.Visible && (this._bottomInvisibleArea <= 0))
            {
                this.vScrollBar1.Visible = false;
                if (!this.vScrollBar1.Visible)
                {
                    this.Redraw();
                }
            }
            if ((num7 != this._allCellsHeigth) && (this._allCellsHeigth > base.Height))
            {
                double num13 = this.vScrollBar1.Maximum - this.vScrollBar1.Minimum;
                int maximum = (int) Math.Floor((double) ((this.vScrollBar1.Minimum + ((num6 * num13) / ((double) (this._bottomInvisibleArea + 20)))) + 0.5));
                if (maximum > this.vScrollBar1.Maximum)
                {
                    maximum = this.vScrollBar1.Maximum;
                }
                else if (maximum < this.vScrollBar1.Minimum)
                {
                    maximum = this.vScrollBar1.Minimum;
                }
                this.vScrollBar1.Value = maximum;
                this.Redraw();
            }
        }

        private int CalculateHeaderSize()
        {
            this._tableHeader[1].Location = new Point(this._columnWidth[0], 0);
            float height = this._tableHeader[0].renderSizeText(this._columnWidth[0]).Height;
            float num2 = this._tableHeader[1].renderSizeText(this._columnWidth[1]).Height;
            int num3 = (num2 > height) ? ((int) num2) : ((int) height);
            this._tableHeader[0].Size = new Size(this._columnWidth[0], num3);
            this._tableHeader[1].Size = new Size(this._columnWidth[1], num3);
            return num3;
        }

        private void CalculateWidth()
        {
            int num = this.vScrollBar1.Visible ? (base.Width - this.vScrollBar1.Width) : base.Width;
            int num2 = (int) ((num * this._columnRate) / (1.0 + this._columnRate));
            this._columnWidth[0] = num2;
            this._columnWidth[1] = num - num2;
        }

        private bool CanAddClause(ReconingClause clause)
        {
            return this.GetServiceGroupMethod(clause.Owner).Editor.CanAddClause(clause.Owner);
        }

        private bool CanDeleteClause(ReconingClause clause)
        {
            if (clause.Owner.Clauses.Length == 1)
            {
                return false;
            }
            return this.GetServiceGroupMethod(clause.Owner).Editor.CanDeleteClause(clause);
        }

        private bool CopyAction()
        {
            bool flag = false;
            if (this._selectedTablePanel is TableMasterCell)
            {
                ClientTariffPlanServiceGroup serviceGroup = this._tariffPlan.CreateAndAddServiceGroup();
                ((ClientTariffPlanServiceGroup) this._selectedTablePanel.Tag).CopyTo(serviceGroup);
                flag = this.EditServiceGroup(serviceGroup);
                if (!flag)
                {
                    this._tariffPlan.RemoveGroup(serviceGroup);
                }
            }
            return flag;
        }

        private void DeactivateCell()
        {
            if (this._activeCell != null)
            {
                this._activeCell.Selected = false;
                this._activeCell = null;
            }
        }

        private bool DeleteAction()
        {
            bool flag;
            if (this._selectedTablePanel is TableMasterCell)
            {
                ClientTariffPlanServiceGroup group = (ClientTariffPlanServiceGroup) this._selectedTablePanel.Tag;
                DialogResult result = MessageBox.Show(TariffPlansResource.DoDeleteSelectedGroup, TariffPlansResource.GroupDeletion, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                flag = DialogResult.Yes == result;
                if (flag)
                {
                    this._tariffPlan.RemoveGroup(group);
                }
                return flag;
            }
            ReconingClause tag = (ReconingClause) this._selectedTablePanel.Tag;
            DialogResult result2 = MessageBox.Show(TariffPlansResource.DoDeleteSelectedClause, TariffPlansResource.ClauseDeletion, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            flag = DialogResult.Yes == result2;
            if (flag)
            {
                tag.Owner.RemoveClause(tag);
                this.UpdateClausesInGroup(tag.Owner);
            }
            return flag;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void DoAction(Action action)
        {
            bool flag;
            this.ActivateCell(this._selectedTablePanel);
            switch (action)
            {
                case Action.Add:
                    flag = this.AddAction();
                    break;

                case Action.Copy:
                    flag = this.CopyAction();
                    break;

                case Action.Delete:
                    flag = this.DeleteAction();
                    break;

                case Action.Edit:
                    flag = this.EditAction();
                    break;

                default:
                    throw new NotImplementedException();
            }
            if (flag)
            {
                this.Rebuild();
            }
            else
            {
                this.DeactivateCell();
            }
        }

        private bool EditAction()
        {
            if (this._selectedTablePanel is TableMasterCell)
            {
                TableMasterCell cell = (TableMasterCell) this._selectedTablePanel;
                ClientTariffPlanServiceGroup serviceGroup = (ClientTariffPlanServiceGroup) cell.Tag;
                return this.EditServiceGroup(serviceGroup);
            }
            ReconingClause tag = (ReconingClause) this._selectedTablePanel.Tag;
            return this.EditReconingClause(tag);
        }

        private bool EditReconingClause(ReconingClause clause)
        {
            ReconingMethod serviceGroupMethod = this.GetServiceGroupMethod(clause.Owner);
            if (serviceGroupMethod.Editor.Edit(clause))
            {
                serviceGroupMethod.Editor.UpdateDescriptions(clause.Owner.Clauses);
                this.RaiseTariffPlanUpdated();
                return true;
            }
            return false;
        }

        private bool EditServiceGroup(ClientTariffPlanServiceGroup serviceGroup)
        {
            bool flag;
            try
            {
                using (TariffPlanServicesGroupDialog dialog = new TariffPlanServicesGroupDialog(serviceGroup, this._availableReconingMethods))
                {
                    if (dialog.ShowDialog() == DialogResult.Cancel)
                    {
                        return false;
                    }
                    this.UpdateClausesInGroup(serviceGroup);
                    this.RaiseTariffPlanUpdated();
                    flag = true;
                }
            }
            catch (ObjectDisposedException)
            {
                flag = false;
            }
            return flag;
        }

        private ReconingMethod GetServiceGroupMethod(ClientTariffPlanServiceGroup serviceGroup)
        {
            foreach (ReconingMethod method in this._availableReconingMethods)
            {
                if (method.CanEditServiceGroup(serviceGroup))
                {
                    return method;
                }
            }
            throw new InvalidOperationException("Не найден метод расчета группы услуг");
        }

        private void InitControl()
        {
            this._maxCount = 0x3e8 / this._timer.Interval;
            int height = 20;
            this._opacity = new double[this._maxCount];
            int index = 0;
            while (index < this._maxCount)
            {
                this._opacity[index] = (1.0 + Math.Cos(((((double) index++) / ((double) this._maxCount)) * 2.0) * 3.1415926535897931)) / 2.0;
            }
            this.CalculateWidth();
            this._tableHeader[0] = new TablePanel(this, new Point(0, 0), new Size(this._columnWidth[0], height));
            this._tableHeader[0].Text = TariffPlansResource.Services;
            this._tableHeader[0].BackGroundColor = Color.LightGray;
            this._tableHeader[1] = new TablePanel(this, new Point(this._columnWidth[0], 0), new Size(this._columnWidth[1], height));
            this._tableHeader[1].Text = TariffPlansResource.Clauses;
            this._tableHeader[1].BackGroundColor = Color.LightGray;
            this.Build();
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TariffPlanControl));
            this._timer = new Timer(this.components);
            this.vScrollBar1 = new VScrollBar();
            this._contextMenu = new ContextMenuStrip(this.components);
            this._toolStripMenuItemAdd = new ToolStripMenuItem();
            this._toolStripMenuItemCopy = new ToolStripMenuItem();
            this._toolStripMenuItemEdit = new ToolStripMenuItem();
            this._toolStripMenuItemDelete = new ToolStripMenuItem();
            this._contextMenu.SuspendLayout();
            base.SuspendLayout();
            this._timer.Interval = 20;
            this._timer.Tick += new EventHandler(this.timer1_Tick);
            manager.ApplyResources(this.vScrollBar1, "vScrollBar1");
            this.vScrollBar1.LargeChange = 1;
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.ValueChanged += new EventHandler(this.vScrollBar1_ValueChanged);
            this._contextMenu.Items.AddRange(new ToolStripItem[] { this._toolStripMenuItemAdd, this._toolStripMenuItemCopy, this._toolStripMenuItemEdit, this._toolStripMenuItemDelete });
            this._contextMenu.Name = "ctxMenu";
            manager.ApplyResources(this._contextMenu, "_contextMenu");
            this._contextMenu.Closed += new ToolStripDropDownClosedEventHandler(this._contextMenu_Closed);
            this._toolStripMenuItemAdd.Name = "_toolStripMenuItemAdd";
            manager.ApplyResources(this._toolStripMenuItemAdd, "_toolStripMenuItemAdd");
            this._toolStripMenuItemAdd.Click += new EventHandler(this.toolStripMenuItemAdd_Click);
            this._toolStripMenuItemCopy.Name = "_toolStripMenuItemCopy";
            manager.ApplyResources(this._toolStripMenuItemCopy, "_toolStripMenuItemCopy");
            this._toolStripMenuItemCopy.Click += new EventHandler(this.toolStripMenuItemCopy_Click);
            this._toolStripMenuItemEdit.Name = "_toolStripMenuItemEdit";
            manager.ApplyResources(this._toolStripMenuItemEdit, "_toolStripMenuItemEdit");
            this._toolStripMenuItemEdit.Click += new EventHandler(this.toolStripMenuItemEdit_Click);
            this._toolStripMenuItemDelete.Name = "_toolStripMenuItemDelete";
            manager.ApplyResources(this._toolStripMenuItemDelete, "_toolStripMenuItemDelete");
            this._toolStripMenuItemDelete.Click += new EventHandler(this.toolStripMenuItemDelete_Click);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.vScrollBar1);
            base.Name = "TariffPlanControl";
            base.ParentChanged += new EventHandler(this.TariffPlanControl_ParentChanged);
            this._contextMenu.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void Parent_MouseWheel(object sender, MouseEventArgs e)
        {
            int num = e.X - base.Location.X;
            int num2 = e.Y - base.Location.Y;
            int width = base.Size.Width;
            int height = base.Size.Height;
            if (((num >= 0) && (num < width)) && ((num2 >= 0) && (num2 < height)))
            {
                int num5 = this.vScrollBar1.Value - ((int) (((double) e.Delta) / 10.0));
                if (num5 < this.vScrollBar1.Minimum)
                {
                    this.vScrollBar1.Value = this.vScrollBar1.Minimum;
                }
                else if (num5 > this.vScrollBar1.Maximum)
                {
                    this.vScrollBar1.Value = this.vScrollBar1.Maximum;
                }
                else
                {
                    this.vScrollBar1.Value = num5;
                }
            }
        }

        private void RaiseTariffPlanUpdated()
        {
            Delegate delegate2 = base.Events[TARIFF_PLAN_UPDATED_KEY];
            if (delegate2 != null)
            {
                ((EventHandler) delegate2)(this, new EventArgs());
            }
        }

        private void Rebuild()
        {
            int count = this._tableBody.Count;
            for (int i = 0; i < count; i++)
            {
                TableMasterCell cell = this._tableBody[i];
                TableCell[] slaveCells = cell.SlaveCells;
                int length = slaveCells.Length;
                for (int j = 0; j < length; j++)
                {
                    base.Controls.Remove(slaveCells[j]);
                    slaveCells[j].Dispose();
                }
                base.Controls.Remove(cell);
                cell.Dispose();
            }
            this.Build();
            this.Redraw();
        }

        private void Redraw()
        {
            base.SuspendLayout();
            this.CalculateWidth();
            int h = this.CalculateHeaderSize();
            this.CalculateColumnSize(h);
            base.ResumeLayout();
        }

        private void TariffPlanControl_ParentChanged(object sender, EventArgs e)
        {
            base.ParentForm.MouseWheel += new MouseEventHandler(this.Parent_MouseWheel);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this._selectedTablePanel.SelectionOpacity = this._opacity[this._count++];
            if (this._count >= this._maxCount)
            {
                this._count = 0;
            }
        }

        private void toolStripMenuItemAdd_Click(object sender, EventArgs e)
        {
            this.DoAction(Action.Add);
        }

        private void toolStripMenuItemCopy_Click(object sender, EventArgs e)
        {
            this.DoAction(Action.Copy);
        }

        private void toolStripMenuItemDelete_Click(object sender, EventArgs e)
        {
            this.DoAction(Action.Delete);
        }

        private void toolStripMenuItemEdit_Click(object sender, EventArgs e)
        {
            this.DoAction(Action.Edit);
        }

        private void tp_Click(object sender, MouseEventArgs e)
        {
            if (base.Enabled && this._editable)
            {
                DateTime now = DateTime.Now;
                if (e.Button == MouseButtons.Right)
                {
                    TableCell cell = this.ActivateCell(sender);
                    if (cell is TableMasterCell)
                    {
                        ClientTariffPlanServiceGroup tag = (ClientTariffPlanServiceGroup) cell.Tag;
                        this._toolStripMenuItemDelete.Enabled = tag.Owner.ServiceGroups.Length > 1;
                        this._toolStripMenuItemAdd.Enabled = this._toolStripMenuItemCopy.Enabled = this.TariffPlan.AvailableServices.Length > 0;
                        this._toolStripMenuItemCopy.Visible = true;
                    }
                    else
                    {
                        ReconingClause clause = (ReconingClause) cell.Tag;
                        this._toolStripMenuItemDelete.Enabled = this.CanDeleteClause(clause);
                        this._toolStripMenuItemAdd.Enabled = this.CanAddClause(clause);
                        this._toolStripMenuItemCopy.Visible = false;
                    }
                    Point mousePosition = Control.MousePosition;
                    mousePosition.Offset(-10, -10);
                    this._contextMenu.Show(mousePosition);
                }
            }
        }

        private void tp_DoubleClick(object sender, EventArgs e)
        {
            this.ActivateCell(sender);
            if (this._editable)
            {
                this.toolStripMenuItemEdit_Click(null, null);
            }
        }

        private void UpdateClausesInGroup(ClientTariffPlanServiceGroup group)
        {
            this.GetServiceGroupMethod(group).Editor.UpdateDescriptions(group.Clauses);
        }

        private void UpdateTariffPlanClauses(ClientTariffPlan plan)
        {
            foreach (ClientTariffPlanServiceGroup group in plan.ServiceGroups)
            {
                this.UpdateClausesInGroup(group);
            }
        }

        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            if (base.Enabled)
            {
                this.Redraw();
            }
        }

        public bool AlwaysShowScrolling
        {
            get
            {
                return this._alwaysShowScrolling;
            }
            set
            {
                if (this._alwaysShowScrolling != value)
                {
                    this._alwaysShowScrolling = value;
                    this.vScrollBar1.Visible = value;
                }
            }
        }

        private double ColumnRate
        {
            get
            {
                return this._columnRate;
            }
            set
            {
                if (this._columnRate != value)
                {
                    this._columnRate = value;
                }
            }
        }

        public bool Editable
        {
            get
            {
                return this._editable;
            }
            set
            {
                this._editable = value;
            }
        }

        public int PlanId
        {
            get
            {
                return this._tariffPlan.Id;
            }
        }

        public ClientTariffPlan TariffPlan
        {
            get
            {
                return this._tariffPlan;
            }
            set
            {
                this._tariffPlan = value;
                this.UpdateTariffPlanClauses(value);
                this.Rebuild();
            }
        }

        private enum Action
        {
            Add,
            Copy,
            Delete,
            Edit
        }
    }
}

