﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class LevelsByServiceTurnoverEditor : ReconingClauseEditor
    {
        public override bool CanDeleteClause(ReconingClause clause)
        {
            return (clause.PreviouseClause != null);
        }

        public override bool Edit(ReconingClause clause)
        {
            using (LevelsByServiceTurnoverDialog dialog = new LevelsByServiceTurnoverDialog(clause))
            {
                return (dialog.ShowDialog() == DialogResult.OK);
            }
        }

        public override string ToString()
        {
            return EditorStrings.LevelsByServiceTurnover;
        }

        public override void UpdateDescriptions(IEnumerable<ReconingClause> clauses)
        {
            foreach (LevelsByServiceTurnoverClause clause in clauses)
            {
                clause.UpdateDescriptions();
            }
        }
    }
}

