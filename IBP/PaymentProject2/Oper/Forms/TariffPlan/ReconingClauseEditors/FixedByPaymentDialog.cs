﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class FixedByPaymentDialog : Form
    {
        private FixedByPaymentClause _clause;
        private NumericUpDown _numericUpDownAmount;
        private IContainer components;

        public FixedByPaymentDialog(FixedByPaymentClause clause)
        {
            this._clause = clause;
            this.InitializeComponent();
            this._numericUpDownAmount.Maximum = PaySystemParameters.MaxMoneyAmount;
            this._numericUpDownAmount.Value = clause.Amount;
        }

        private void _buttonOk_Click(object sender, EventArgs e)
        {
            this._clause.Amount = this._numericUpDownAmount.Value;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(FixedByPaymentDialog));
            this._numericUpDownAmount = new NumericUpDown();
            Button button = new Button();
            Button button2 = new Button();
            Label label = new Label();
            this._numericUpDownAmount.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(button, "_buttonOk");
            button.DialogResult = DialogResult.OK;
            button.Name = "_buttonOk";
            button.UseVisualStyleBackColor = true;
            button.Click += new EventHandler(this._buttonOk_Click);
            manager.ApplyResources(button2, "_buttonCancel");
            button2.DialogResult = DialogResult.Cancel;
            button2.Name = "_buttonCancel";
            button2.UseVisualStyleBackColor = true;
            manager.ApplyResources(label, "_label");
            label.Name = "_label";
            manager.ApplyResources(this._numericUpDownAmount, "_numericUpDownAmount");
            this._numericUpDownAmount.DecimalPlaces = 2;
            this._numericUpDownAmount.Name = "_numericUpDownAmount";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = button2;
            base.Controls.Add(this._numericUpDownAmount);
            base.Controls.Add(label);
            base.Controls.Add(button);
            base.Controls.Add(button2);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "FixedByPaymentDialog";
            base.ShowIcon = false;
            this._numericUpDownAmount.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}

