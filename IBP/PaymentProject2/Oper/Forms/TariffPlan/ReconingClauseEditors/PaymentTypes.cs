﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Component;
    using System;

    internal enum PaymentTypes
    {
        [DisplayName(typeof(EditorStrings), "PaymentTypesCash")]
        Cash = 2,
        [DisplayName(typeof(EditorStrings), "PaymentTypesCashless")]
        Cashless = 4,
        [DisplayName(typeof(EditorStrings), "PaymentTypesCashlessWithCommission")]
        CashlessWithCommission = 3,
        [DisplayName(typeof(EditorStrings), "PaymentTypesCashWithCommission")]
        CashWithCommission = 1,
        [DisplayName(typeof(EditorStrings), "PaymentTypesDigitalMoney")]
        DigitalMoney = 6,
        [DisplayName(typeof(EditorStrings), "PaymentTypesDigitalMoneyWithCommission")]
        DigitalMoneyWithCommission = 5,
        [DisplayName(typeof(EditorStrings), "PaymentTypesOtherCard")]
        OtherCard = 10,
        [DisplayName(typeof(EditorStrings), "PaymentTypesOtherCardWithCommission")]
        OtherCardWithCommission = 9,
        [DisplayName(typeof(EditorStrings), "PaymentTypesOwnCard")]
        OwnCard = 8,
        [DisplayName(typeof(EditorStrings), "PaymentTypesOwnCardWithCommission")]
        OwnCardWithCommission = 7
    }
}

