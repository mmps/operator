﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class LevelsByServiceTurnoverDialog : Form
    {
        private LevelsByServiceTurnoverClause _clause;
        private NumericUpDown _numericUpDownPercent;
        private NumericUpDown _numericUpDownStartTurnover;
        private TextBox _textBoxEndTurnover;
        private IContainer components;

        public LevelsByServiceTurnoverDialog(LevelsByServiceTurnoverClause clauseWrapper)
        {
            this.InitializeComponent();
            this._clause = clauseWrapper;
            this._numericUpDownStartTurnover.Minimum = this._clause.MinimumTurnover;
            if (this._clause.EndTurnoverIsInfinity)
            {
                this._textBoxEndTurnover.Text = "∞";
                this._numericUpDownStartTurnover.Maximum = PaySystemParameters.MaxMoneyAmount;
            }
            else
            {
                this._textBoxEndTurnover.Text = this._clause.EndTurnover.ToString();
                this._numericUpDownStartTurnover.Maximum = this._clause.EndTurnover;
            }
            this._numericUpDownStartTurnover.Value = this._clause.StartTurnover;
            this._numericUpDownPercent.Value = this._clause.Rate;
            if (this._clause.IsFirstClause)
            {
                this._numericUpDownStartTurnover.Enabled = false;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this._clause.Rate = this._numericUpDownPercent.Value;
            this._clause.StartTurnover = this._numericUpDownStartTurnover.Value;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(LevelsByServiceTurnoverDialog));
            this._numericUpDownStartTurnover = new NumericUpDown();
            this._textBoxEndTurnover = new TextBox();
            this._numericUpDownPercent = new NumericUpDown();
            Button button = new Button();
            Button button2 = new Button();
            GroupBox box = new GroupBox();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            box.SuspendLayout();
            this._numericUpDownStartTurnover.BeginInit();
            this._numericUpDownPercent.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(button, "buttonCancel");
            button.DialogResult = DialogResult.Cancel;
            button.Name = "buttonCancel";
            button.UseVisualStyleBackColor = true;
            manager.ApplyResources(button2, "buttonOk");
            button2.DialogResult = DialogResult.OK;
            button2.Name = "buttonOk";
            button2.UseVisualStyleBackColor = true;
            button2.Click += new EventHandler(this.buttonOk_Click);
            manager.ApplyResources(box, "groupBox1");
            box.Controls.Add(label);
            box.Controls.Add(label2);
            box.Controls.Add(this._numericUpDownStartTurnover);
            box.Controls.Add(this._textBoxEndTurnover);
            box.Name = "groupBox1";
            box.TabStop = false;
            manager.ApplyResources(label, "label2");
            label.Name = "label2";
            manager.ApplyResources(label2, "label1");
            label2.Name = "label1";
            manager.ApplyResources(this._numericUpDownStartTurnover, "_numericUpDownStartTurnover");
            this._numericUpDownStartTurnover.DecimalPlaces = 2;
            int[] bits = new int[4];
            bits[0] = 0x3e8;
            this._numericUpDownStartTurnover.Maximum = new decimal(bits);
            this._numericUpDownStartTurnover.Name = "_numericUpDownStartTurnover";
            manager.ApplyResources(this._textBoxEndTurnover, "_textBoxEndTurnover");
            this._textBoxEndTurnover.Name = "_textBoxEndTurnover";
            manager.ApplyResources(label3, "label3");
            label3.Name = "label3";
            manager.ApplyResources(this._numericUpDownPercent, "_numericUpDownPercent");
            this._numericUpDownPercent.DecimalPlaces = 3;
            this._numericUpDownPercent.Name = "_numericUpDownPercent";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(label3);
            base.Controls.Add(box);
            base.Controls.Add(button2);
            base.Controls.Add(button);
            base.Controls.Add(this._numericUpDownPercent);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "LevelsByServiceTurnoverDialog";
            base.ShowIcon = false;
            box.ResumeLayout(false);
            box.PerformLayout();
            this._numericUpDownStartTurnover.EndInit();
            this._numericUpDownPercent.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}

