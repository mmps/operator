﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class RuleByPaymentTypeEditor : ReconingClauseEditor
    {
        public override bool CanAddClause(ClientTariffPlanServiceGroup group)
        {
            EnumTypeInfo<PaymentTypes>[] info = EnumTypeInfo<PaymentTypes>.GetInfo();
            return (group.Clauses.Length < info.Length);
        }

        public override bool CanDeleteClause(ReconingClause clause)
        {
            return (clause.Owner.Clauses.Length > 1);
        }

        public override bool Edit(ReconingClause clause)
        {
            using (RuleByPaymentTypeDialog dialog = new RuleByPaymentTypeDialog(clause))
            {
                return (dialog.ShowDialog() == DialogResult.OK);
            }
        }

        public override string ToString()
        {
            return EditorStrings.RuleByPaymentType;
        }

        public override void UpdateDescriptions(IEnumerable<ReconingClause> clauses)
        {
            foreach (RuleByPaymentTypeClause clause in clauses)
            {
                clause.UpdateDescriptions();
            }
        }
    }
}

