﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;

    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), CompilerGenerated, DebuggerNonUserCode]
    internal class EditorStrings
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal EditorStrings()
        {
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string FixedByPayment
        {
            get
            {
                return ResourceManager.GetString("FixedByPayment", resourceCulture);
            }
        }

        internal static string FixedByPaymentClauseDescription
        {
            get
            {
                return ResourceManager.GetString("FixedByPaymentClauseDescription", resourceCulture);
            }
        }

        internal static string FixedByPaymentDescription
        {
            get
            {
                return ResourceManager.GetString("FixedByPaymentDescription", resourceCulture);
            }
        }

        internal static string LevelsByPaymentAmount
        {
            get
            {
                return ResourceManager.GetString("LevelsByPaymentAmount", resourceCulture);
            }
        }

        internal static string LevelsByPaymentAmountClauseDescription
        {
            get
            {
                return ResourceManager.GetString("LevelsByPaymentAmountClauseDescription", resourceCulture);
            }
        }

        internal static string LevelsByPaymentAmountDescription
        {
            get
            {
                return ResourceManager.GetString("LevelsByPaymentAmountDescription", resourceCulture);
            }
        }

        internal static string LevelsByServiceTurnover
        {
            get
            {
                return ResourceManager.GetString("LevelsByServiceTurnover", resourceCulture);
            }
        }

        internal static string LevelsByServiceTurnoverClauseDescription
        {
            get
            {
                return ResourceManager.GetString("LevelsByServiceTurnoverClauseDescription", resourceCulture);
            }
        }

        internal static string LevelsByServiceTurnoverDescription
        {
            get
            {
                return ResourceManager.GetString("LevelsByServiceTurnoverDescription", resourceCulture);
            }
        }

        internal static string PaymentTypesCash
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesCash", resourceCulture);
            }
        }

        internal static string PaymentTypesCashless
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesCashless", resourceCulture);
            }
        }

        internal static string PaymentTypesCashlessWithCommission
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesCashlessWithCommission", resourceCulture);
            }
        }

        internal static string PaymentTypesCashWithCommission
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesCashWithCommission", resourceCulture);
            }
        }

        internal static string PaymentTypesDigitalMoney
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesDigitalMoney", resourceCulture);
            }
        }

        internal static string PaymentTypesDigitalMoneyWithCommission
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesDigitalMoneyWithCommission", resourceCulture);
            }
        }

        internal static string PaymentTypesOtherCard
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesOtherCard", resourceCulture);
            }
        }

        internal static string PaymentTypesOtherCardWithCommission
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesOtherCardWithCommission", resourceCulture);
            }
        }

        internal static string PaymentTypesOwnCard
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesOwnCard", resourceCulture);
            }
        }

        internal static string PaymentTypesOwnCardWithCommission
        {
            get
            {
                return ResourceManager.GetString("PaymentTypesOwnCardWithCommission", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors.EditorStrings", typeof(EditorStrings).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static string RuleByPaymentType
        {
            get
            {
                return ResourceManager.GetString("RuleByPaymentType", resourceCulture);
            }
        }

        internal static string RuleByPaymentTypeClauseDescription
        {
            get
            {
                return ResourceManager.GetString("RuleByPaymentTypeClauseDescription", resourceCulture);
            }
        }

        internal static string RuleByPaymentTypeDescription
        {
            get
            {
                return ResourceManager.GetString("RuleByPaymentTypeDescription", resourceCulture);
            }
        }
    }
}

