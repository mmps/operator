﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;
    using System.Runtime.CompilerServices;

    internal class LevelsByServiceTurnoverClause
    {
        private ReconingClause _clause;
        private const string AMOUNT_FORMAT = "F2";
        internal const string INFINITY = "∞";
        [DecimalConstant(0, 0, uint.MaxValue, uint.MaxValue, uint.MaxValue)]
        private static readonly decimal INFINITY_VALUE = 79228162514264337593543950335M;
        [DecimalConstant(2, 0, (uint) 0, (uint) 0, (uint) 1)]
        private static readonly decimal MINIMUM_AMOUNT = 0.01M;
        private const string START_TURNOVER = "StartTurnover";

        public LevelsByServiceTurnoverClause(ReconingClause clause)
        {
            this._clause = clause;
        }

        public static implicit operator LevelsByServiceTurnoverClause(ReconingClause clause)
        {
            return new LevelsByServiceTurnoverClause(clause);
        }

        public static implicit operator ReconingClause(LevelsByServiceTurnoverClause clause)
        {
            return clause._clause;
        }

        internal void UpdateDescriptions()
        {
            string str;
            if (this.EndTurnoverIsInfinity)
            {
                str = "∞";
            }
            else
            {
                str = this.EndTurnover.ToString();
            }
            this._clause.ClauseDescription = string.Format(EditorStrings.LevelsByServiceTurnoverClauseDescription, this.StartTurnover, str);
            this._clause.Description = string.Format(EditorStrings.LevelsByServiceTurnoverDescription, this._clause.ClauseDescription, Environment.NewLine, this.Rate);
        }

        public decimal EndTurnover
        {
            get
            {
                ReconingClause nextClause = this._clause.NextClause;
                if (nextClause == null)
                {
                    return 79228162514264337593543950335M;
                }
                LevelsByServiceTurnoverClause clause2 = nextClause;
                return (clause2.StartTurnover - 0.01M);
            }
        }

        public bool EndTurnoverIsInfinity
        {
            get
            {
                return (this._clause.NextClause == null);
            }
        }

        public bool IsFirstClause
        {
            get
            {
                return (this._clause.PreviouseClause == null);
            }
        }

        public decimal MinimumTurnover
        {
            get
            {
                ReconingClause previouseClause = this._clause.PreviouseClause;
                if (this._clause.PreviouseClause == null)
                {
                    return 0M;
                }
                
                return (((LevelsByServiceTurnoverClause)this._clause.PreviouseClause).StartTurnover + 0.01M);
                
            }
        }

        public decimal Rate
        {
            get
            {
                return this._clause.MainRate;
            }
            set
            {
                this._clause.MainRate = value;
            }
        }

        public decimal StartTurnover
        {
            get
            {
                decimal @decimal = this._clause.GetDecimal("StartTurnover");
                if (@decimal < this.MinimumTurnover)
                {
                    @decimal = this.MinimumTurnover;
                }
                this.StartTurnover = @decimal;
                return @decimal;
            }
            set
            {
                this._clause.SetDecimal(value, "F2", "StartTurnover");
            }
        }
    }
}

