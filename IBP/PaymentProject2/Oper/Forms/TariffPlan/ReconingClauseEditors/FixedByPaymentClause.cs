﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;

    internal class FixedByPaymentClause
    {
        private ReconingClause _clause;
        private const string AMOUNT_FORMAT = "F2";
        private const string AMOUNT_KEY = "Amount";

        public FixedByPaymentClause(ReconingClause clause)
        {
            this._clause = clause;
        }

        public static implicit operator FixedByPaymentClause(ReconingClause clause)
        {
            return new FixedByPaymentClause(clause);
        }

        public static implicit operator ReconingClause(FixedByPaymentClause clause)
        {
            return clause._clause;
        }

        public void UpdateDescriptions()
        {
            this._clause.ClauseDescription = EditorStrings.FixedByPaymentClauseDescription;
            this._clause.Description = string.Format(EditorStrings.FixedByPaymentDescription, this._clause.ClauseDescription, Environment.NewLine, this.Amount);
        }

        public decimal Amount
        {
            get
            {
                return this._clause.GetDecimal("Amount");
            }
            set
            {
                this._clause.SetDecimal(value, "F2", "Amount");
            }
        }
    }
}

