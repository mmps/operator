﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using System;
    using System.Collections.Generic;

    internal class RuleByPaymentTypeClause
    {
        private ReconingClause _clause;
        private const string PAYMENT_TYPE = "PaymentType";

        public RuleByPaymentTypeClause(ReconingClause clause)
        {
            this._clause = clause;
        }

        private PaymentTypes GetFreePaymentType(ReconingClause clause)
        {
            List<PaymentTypes> list = new List<PaymentTypes>();
            foreach (EnumTypeInfo<PaymentTypes> info in EnumTypeInfo<PaymentTypes>.GetInfo())
            {
                list.Add(info.EnumValue);
            }
            foreach (RuleByPaymentTypeClause clause2 in this.GetOtherClauses())
            {
                list.Remove(clause2.PaymentType);
            }
            return list[0];
        }

        public RuleByPaymentTypeClause[] GetOtherClauses()
        {
            List<RuleByPaymentTypeClause> list = new List<RuleByPaymentTypeClause>();
            foreach (ReconingClause clause in this._clause.Owner.Clauses)
            {
                if (!object.ReferenceEquals(clause, this._clause))
                {
                    list.Add(clause);
                }
            }
            return list.ToArray();
        }

        public static implicit operator RuleByPaymentTypeClause(ReconingClause clause)
        {
            return new RuleByPaymentTypeClause(clause);
        }

        public static implicit operator ReconingClause(RuleByPaymentTypeClause clause)
        {
            return clause._clause;
        }

        public void UpdateDescriptions()
        {
            EnumTypeInfo<PaymentTypes> info = new EnumTypeInfo<PaymentTypes>(this.PaymentType);
            this._clause.ClauseDescription = string.Format(EditorStrings.RuleByPaymentTypeClauseDescription, info);
            this._clause.Description = string.Format(EditorStrings.RuleByPaymentTypeDescription, this._clause.ClauseDescription, Environment.NewLine, this.Rate);
        }

        public PaymentTypes PaymentType
        {
            get
            {
                PaymentTypes freePaymentType;
                try
                {
                    string str = this._clause["PaymentType"];
                    freePaymentType = (PaymentTypes) Enum.Parse(typeof(PaymentTypes), str);
                }
                catch
                {
                    freePaymentType = this.GetFreePaymentType(this._clause);
                    this.PaymentType = freePaymentType;
                }
                return freePaymentType;
            }
            set
            {
                this._clause["PaymentType"] = value.ToString("d");
            }
        }

        public decimal Rate
        {
            get
            {
                return this._clause.MainRate;
            }
            set
            {
                this._clause.MainRate = value;
            }
        }
    }
}

