﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class RuleByPaymentTypeDialog : Form
    {
        private RuleByPaymentTypeClause _clause;
        private ListBox _listBoxPaymentTypes;
        private NumericUpDown _numericUpDownRate;
        private IContainer components;

        public RuleByPaymentTypeDialog(RuleByPaymentTypeClause clause)
        {
            this.InitializeComponent();
            this._clause = clause;
            this._listBoxPaymentTypes.Items.AddRange(EnumTypeInfo<PaymentTypes>.GetInfo());
            foreach (RuleByPaymentTypeClause clause2 in clause.GetOtherClauses())
            {
                EnumTypeInfo<PaymentTypes> info = new EnumTypeInfo<PaymentTypes>(clause2.PaymentType);
                this._listBoxPaymentTypes.Items.Remove(info);
            }
            this.SelectedType = clause.PaymentType;
            this._numericUpDownRate.Value = clause.Rate;
        }

        private void _buttonOK_Click(object sender, EventArgs e)
        {
            this._clause.Rate = this._numericUpDownRate.Value;
            this._clause.PaymentType = this.SelectedType;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RuleByPaymentTypeDialog));
            this._numericUpDownRate = new NumericUpDown();
            this._listBoxPaymentTypes = new ListBox();
            Label label = new Label();
            Label label2 = new Label();
            Button button = new Button();
            Button button2 = new Button();
            this._numericUpDownRate.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "label1");
            label.Name = "label1";
            manager.ApplyResources(label2, "label2");
            label2.Name = "label2";
            manager.ApplyResources(button, "_buttonCancel");
            button.DialogResult = DialogResult.Cancel;
            button.Name = "_buttonCancel";
            button.UseVisualStyleBackColor = true;
            manager.ApplyResources(button2, "_buttonOK");
            button2.DialogResult = DialogResult.OK;
            button2.Name = "_buttonOK";
            button2.UseVisualStyleBackColor = true;
            button2.Click += new EventHandler(this._buttonOK_Click);
            manager.ApplyResources(this._numericUpDownRate, "_numericUpDownRate");
            this._numericUpDownRate.DecimalPlaces = 3;
            this._numericUpDownRate.Name = "_numericUpDownRate";
            manager.ApplyResources(this._listBoxPaymentTypes, "_listBoxPaymentTypes");
            this._listBoxPaymentTypes.FormattingEnabled = true;
            this._listBoxPaymentTypes.Name = "_listBoxPaymentTypes";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._listBoxPaymentTypes);
            base.Controls.Add(button2);
            base.Controls.Add(button);
            base.Controls.Add(label2);
            base.Controls.Add(label);
            base.Controls.Add(this._numericUpDownRate);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "RuleByPaymentTypeDialog";
            base.ShowIcon = false;
            this._numericUpDownRate.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private PaymentTypes SelectedType
        {
            get
            {
                EnumTypeInfo<PaymentTypes> selectedItem = (EnumTypeInfo<PaymentTypes>) this._listBoxPaymentTypes.SelectedItem;
                return selectedItem.EnumValue;
            }
            set
            {
                this._listBoxPaymentTypes.SelectedItem = new EnumTypeInfo<PaymentTypes>(value);
            }
        }
    }
}

