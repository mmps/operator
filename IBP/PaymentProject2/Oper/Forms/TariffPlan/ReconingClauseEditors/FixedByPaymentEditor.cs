﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class FixedByPaymentEditor : ReconingClauseEditor
    {
        public override bool CanAddClause(ClientTariffPlanServiceGroup group)
        {
            return false;
        }

        public override bool CanDeleteClause(ReconingClause clause)
        {
            return false;
        }

        public override bool Edit(ReconingClause clause)
        {
            using (FixedByPaymentDialog dialog = new FixedByPaymentDialog(clause))
            {
                return (dialog.ShowDialog() == DialogResult.OK);
            }
        }

        public override string ToString()
        {
            return EditorStrings.FixedByPayment;
        }

        public override void UpdateDescriptions(IEnumerable<ReconingClause> clauses)
        {
            foreach (FixedByPaymentClause clause in clauses)
            {
                clause.UpdateDescriptions();
            }
        }
    }
}

