﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;
    using System.Runtime.CompilerServices;

    internal class LevelsByPaymentAmountClause
    {
        private ReconingClause _clause;
        private const string AMOUNT_FORMAT = "F2";
        internal const string INFINITY = "∞";
        [DecimalConstant(0, 0, uint.MaxValue, uint.MaxValue, uint.MaxValue)]
        private static readonly decimal INFINITY_VALUE = 79228162514264337593543950335M;
        [DecimalConstant(2, 0, (uint) 0, (uint) 0, (uint) 1)]
        private static readonly decimal MINIMUM_AMOUNT = 0.01M;
        private const string START_AMOUNT = "StartAmount";

        public LevelsByPaymentAmountClause(ReconingClause clause)
        {
            this._clause = clause;
        }

        public static implicit operator LevelsByPaymentAmountClause(ReconingClause clause)
        {
            return new LevelsByPaymentAmountClause(clause);
        }

        public static implicit operator ReconingClause(LevelsByPaymentAmountClause clause)
        {
            return clause._clause;
        }

        internal void UpdateDescriptions()
        {
            string str;
            if (this.EndAmountIsInfinity)
            {
                str = "∞";
            }
            else
            {
                str = this.EndAmount.ToString();
            }
            this._clause.ClauseDescription = string.Format(EditorStrings.LevelsByPaymentAmountClauseDescription, this.StartAmount, str);
            this._clause.Description = string.Format(EditorStrings.LevelsByPaymentAmountDescription, this._clause.ClauseDescription, Environment.NewLine, this.Rate);
        }

        public decimal EndAmount
        {
            get
            {
                ReconingClause nextClause = this._clause.NextClause;
                if (nextClause == null)
                {
                    return 79228162514264337593543950335M;
                }
                LevelsByPaymentAmountClause clause2 = nextClause;
                return (clause2.StartAmount - 0.01M);
            }
        }

        public bool EndAmountIsInfinity
        {
            get
            {
                return (this._clause.NextClause == null);
            }
        }

        public bool IsFirstClause
        {
            get
            {
                return (this._clause.PreviouseClause == null);
            }
        }

        public decimal MinimumAmount
        {
            get
            {
                ReconingClause previouseClause = this._clause.PreviouseClause;
                if (this._clause.PreviouseClause == null)
                {
                    return 0M;
                }
                return (/*this._clause.PreviouseClause.StartAmount + */0.01M);
            }
        }

        public decimal Rate
        {
            get
            {
                return this._clause.MainRate;
            }
            set
            {
                this._clause.MainRate = value;
            }
        }

        public decimal StartAmount
        {
            get
            {
                decimal @decimal = this._clause.GetDecimal("StartAmount");
                if (@decimal < this.MinimumAmount)
                {
                    @decimal = this.MinimumAmount;
                }
                this.StartAmount = @decimal;
                return @decimal;
            }
            set
            {
                this._clause.SetDecimal(value, "F2", "StartAmount");
            }
        }
    }
}

