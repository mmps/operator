﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class LevelsByPaymentAmountEditor : ReconingClauseEditor
    {
        public override bool CanDeleteClause(ReconingClause clause)
        {
            return (clause.PreviouseClause != null);
        }

        public override bool Edit(ReconingClause clause)
        {
            using (LevelsByPaymentAmountDialog dialog = new LevelsByPaymentAmountDialog(clause))
            {
                return (dialog.ShowDialog() == DialogResult.OK);
            }
        }

        public override string ToString()
        {
            return EditorStrings.LevelsByPaymentAmount;
        }

        public override void UpdateDescriptions(IEnumerable<ReconingClause> clauses)
        {
            foreach (LevelsByPaymentAmountClause clause in clauses)
            {
                clause.UpdateDescriptions();
            }
        }
    }
}

