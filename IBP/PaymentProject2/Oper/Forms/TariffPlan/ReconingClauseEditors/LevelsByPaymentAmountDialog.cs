﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class LevelsByPaymentAmountDialog : Form
    {
        private LevelsByPaymentAmountClause _clause;
        private NumericUpDown _numericUpDownPercent;
        private NumericUpDown _numericUpDownStartAmount;
        private TextBox _textBoxEndAmount;
        private IContainer components;

        public LevelsByPaymentAmountDialog(LevelsByPaymentAmountClause clause)
        {
            this.InitializeComponent();
            this._clause = clause;
            this._numericUpDownStartAmount.Minimum = this._clause.MinimumAmount;
            if (this._clause.EndAmountIsInfinity)
            {
                this._textBoxEndAmount.Text = "∞";
                this._numericUpDownStartAmount.Maximum = PaySystemParameters.MaxMoneyAmount;
            }
            else
            {
                this._textBoxEndAmount.Text = this._clause.EndAmount.ToString();
                this._numericUpDownStartAmount.Maximum = this._clause.EndAmount;
            }
            this._numericUpDownStartAmount.Value = this._clause.StartAmount;
            this._numericUpDownPercent.Value = this._clause.Rate;
            if (this._clause.IsFirstClause)
            {
                this._numericUpDownStartAmount.Enabled = false;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this._clause.Rate = this._numericUpDownPercent.Value;
            this._clause.StartAmount = this._numericUpDownStartAmount.Value;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(LevelsByPaymentAmountDialog));
            this._numericUpDownStartAmount = new NumericUpDown();
            this._textBoxEndAmount = new TextBox();
            this._numericUpDownPercent = new NumericUpDown();
            Label label = new Label();
            GroupBox box = new GroupBox();
            Label label2 = new Label();
            Label label3 = new Label();
            Button button = new Button();
            Button button2 = new Button();
            box.SuspendLayout();
            this._numericUpDownStartAmount.BeginInit();
            this._numericUpDownPercent.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "label3");
            label.Name = "label3";
            manager.ApplyResources(box, "groupBox1");
            box.Controls.Add(label2);
            box.Controls.Add(label3);
            box.Controls.Add(this._numericUpDownStartAmount);
            box.Controls.Add(this._textBoxEndAmount);
            box.Name = "groupBox1";
            box.TabStop = false;
            manager.ApplyResources(label2, "label2");
            label2.Name = "label2";
            manager.ApplyResources(label3, "label1");
            label3.Name = "label1";
            manager.ApplyResources(this._numericUpDownStartAmount, "_numericUpDownStartAmount");
            this._numericUpDownStartAmount.DecimalPlaces = 2;
            int[] bits = new int[4];
            bits[0] = 0x3e8;
            this._numericUpDownStartAmount.Maximum = new decimal(bits);
            this._numericUpDownStartAmount.Name = "_numericUpDownStartAmount";
            manager.ApplyResources(this._textBoxEndAmount, "_textBoxEndAmount");
            this._textBoxEndAmount.Name = "_textBoxEndAmount";
            manager.ApplyResources(button, "_buttonOk");
            button.DialogResult = DialogResult.OK;
            button.Name = "_buttonOk";
            button.UseVisualStyleBackColor = true;
            button.Click += new EventHandler(this.buttonOk_Click);
            manager.ApplyResources(button2, "_buttonCancel");
            button2.DialogResult = DialogResult.Cancel;
            button2.Name = "_buttonCancel";
            button2.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._numericUpDownPercent, "_numericUpDownPercent");
            this._numericUpDownPercent.DecimalPlaces = 3;
            this._numericUpDownPercent.Name = "_numericUpDownPercent";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(label);
            base.Controls.Add(box);
            base.Controls.Add(button);
            base.Controls.Add(button2);
            base.Controls.Add(this._numericUpDownPercent);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "LevelsByPaymentAmountDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            box.ResumeLayout(false);
            box.PerformLayout();
            this._numericUpDownStartAmount.EndInit();
            this._numericUpDownPercent.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}

