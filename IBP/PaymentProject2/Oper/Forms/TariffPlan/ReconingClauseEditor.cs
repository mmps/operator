﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;
    using System.Collections.Generic;

    internal abstract class ReconingClauseEditor
    {
        protected ReconingClauseEditor()
        {
        }

        public virtual bool CanAddClause(ClientTariffPlanServiceGroup group)
        {
            return true;
        }

        public abstract bool CanDeleteClause(ReconingClause clause);
        public abstract bool Edit(ReconingClause clause);
        public abstract void UpdateDescriptions(IEnumerable<ReconingClause> clauses);
    }
}

