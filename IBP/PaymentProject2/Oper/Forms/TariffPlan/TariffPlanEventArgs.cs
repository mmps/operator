﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;

    internal class TariffPlanEventArgs : EventArgs
    {
        public bool Successful = true;
        public readonly ClientTariffPlan TariffPlan;

        public TariffPlanEventArgs(ClientTariffPlan tariffPlan)
        {
            this.TariffPlan = tariffPlan;
        }
    }
}

