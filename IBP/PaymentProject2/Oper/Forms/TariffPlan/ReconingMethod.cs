﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingClauseEditors;
    using System;
    using System.Collections.Generic;

    internal class ReconingMethod
    {
        private static List<ReconingMethod> _buildInMethods = new List<ReconingMethod>();
        private ReconingClauseEditor _editor;
        public readonly ReconingMethods ServiceGroupType;

        static ReconingMethod()
        {
            _buildInMethods.Add(new ReconingMethod(new LevelsByServiceTurnoverEditor(), ReconingMethods.LevelsByServiceTurnover));
            _buildInMethods.Add(new ReconingMethod(new LevelsByPaymentAmountEditor(), ReconingMethods.LevelsByPaymentAmount));
            _buildInMethods.Add(new ReconingMethod(new RuleByPaymentTypeEditor(), ReconingMethods.RuleByPaymentType));
            _buildInMethods.Add(new ReconingMethod(new FixedByPaymentEditor(), ReconingMethods.FixedByPayment));
        }

        internal ReconingMethod(ReconingClauseEditor editor, ReconingMethods serviceGroupType)
        {
            this._editor = editor;
            this.ServiceGroupType = serviceGroupType;
        }

        public bool CanEditServiceGroup(ClientTariffPlanServiceGroup serviceGroup)
        {
            return (serviceGroup.GroupType == this.ServiceGroupType);
        }

        public static ReconingMethod[] GetAvailableMethods()
        {
            return _buildInMethods.ToArray();
        }

        public override string ToString()
        {
            return this._editor.ToString();
        }

        public ReconingClauseEditor Editor
        {
            get
            {
                return this._editor;
            }
        }
    }
}

