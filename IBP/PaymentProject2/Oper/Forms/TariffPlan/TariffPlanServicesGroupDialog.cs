﻿namespace IBP.PaymentProject2.Oper.Forms.TariffPlan
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class TariffPlanServicesGroupDialog : Form
    {
        private List<Service> _currentAvailableServices;
        private List<Service> _currentSelectedServices;
        private ClientTariffPlanServiceGroup _editedServiceGroup;
        private RadioButton _rbDealerToPaySystem;
        private RadioButton _rbPaySystemToDealer;
        private ComboBox _reconingMethod;
        private Button btnCancel;
        private Button btnDeselect;
        private Button btnDeselectAll;
        private Button btnOk;
        private Button btnSelect;
        private Button btnSelectAll;
        private IContainer components;
        private GroupBox groupBox1;
        private Label label1;
        private Label label2;
        private Label label4;
        private ListBox lstbAvailable;
        private ListBox lstbSelected;
        private ComboBox serviceType;

        public TariffPlanServicesGroupDialog(ClientTariffPlanServiceGroup serviceGroup, IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingMethod[] availableMethods)
        {
            this.InitializeComponent();
            this.InitData(serviceGroup);
            this._reconingMethod.Items.Clear();
            this._reconingMethod.Items.AddRange(availableMethods);
            foreach (IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingMethod method in availableMethods)
            {
                if (method.CanEditServiceGroup(serviceGroup))
                {
                    this.ReconingMethod = method;
                    break;
                }
            }
            this.MoneyFlowDirection = this._editedServiceGroup.MoneyFlowDirection;
        }

        private void btnDeselect_Click(object sender, EventArgs e)
        {
            this.DeselectServices();
        }

        private void btnDeselectAll_Click(object sender, EventArgs e)
        {
            ListBox.ObjectCollection items = this.lstbSelected.Items;
            if (items.Count > 0)
            {
                this.MoveAll(items, this._currentSelectedServices, this._currentAvailableServices);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this._currentSelectedServices.Count == 0)
            {
                base.DialogResult = DialogResult.None;
                MessageBox.Show(TariffPlansResource.YouShouldSelectServices, TariffPlansResource.Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                this._editedServiceGroup.MoneyFlowDirection = this.MoneyFlowDirection;
                this._editedServiceGroup.GroupType = this.ReconingMethod.ServiceGroupType;
                this._editedServiceGroup.Services = this._currentSelectedServices.ToArray();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            this.SelectServices();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            ListBox.ObjectCollection items = this.lstbAvailable.Items;
            if (items.Count > 0)
            {
                this.MoveAll(items, this._currentAvailableServices, this._currentSelectedServices);
            }
        }

        private void DeselectServices()
        {
            ListBox.SelectedObjectCollection selectedItems = this.lstbSelected.SelectedItems;
            if (selectedItems.Count > 0)
            {
                this.MoveAll(selectedItems, this._currentSelectedServices, this._currentAvailableServices);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillListBox(ListBox listBox, ServiceType selectedType, List<Service> sourceList)
        {
            listBox.Items.Clear();
            foreach (Service service in sourceList)
            {
                if ((selectedType == null) || (selectedType.Id == service.ServiceTypeID))
                {
                    listBox.Items.Add(service);
                }
            }
        }

        private void FillListBoxes()
        {
            ByNameComparer comparer = new ByNameComparer();
            this._currentAvailableServices.Sort(comparer);
            this._currentSelectedServices.Sort(comparer);
            base.SuspendLayout();
            ServiceType selectedServiceType = this.SelectedServiceType;
            this.FillListBox(this.lstbAvailable, selectedServiceType, this._currentAvailableServices);
            this.FillListBox(this.lstbSelected, selectedServiceType, this._currentSelectedServices);
            base.ResumeLayout();
        }

        private void InitData(ClientTariffPlanServiceGroup serviceGroup)
        {
            this._currentAvailableServices = new List<Service>(serviceGroup.Owner.AvailableServices);
            this._currentSelectedServices = new List<Service>(serviceGroup.Services);
            this._editedServiceGroup = serviceGroup;
            ServiceType[] items = ServiceTypesContainer.Instance.ToArray();
            this.serviceType.Items.Add(string.Empty);
            this.serviceType.Items.AddRange(items);
            bool flag = serviceGroup.Owner.AvailableServices.Length > 0;
            bool flag2 = serviceGroup.Services.Length > 0;
            if (!flag && !flag2)
            {
                MessageBox.Show(UserStrings.NoAvailableServices, UserStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                base.Close();
            }
            else
            {
                this.FillListBoxes();
            }
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TariffPlanServicesGroupDialog));
            this.groupBox1 = new GroupBox();
            this.serviceType = new ComboBox();
            this.label4 = new Label();
            this.lstbAvailable = new ListBox();
            this.label1 = new Label();
            this.lstbSelected = new ListBox();
            this.label2 = new Label();
            this.btnSelect = new Button();
            this.btnDeselectAll = new Button();
            this.btnSelectAll = new Button();
            this.btnDeselect = new Button();
            this.btnOk = new Button();
            this.btnCancel = new Button();
            this._rbDealerToPaySystem = new RadioButton();
            this._rbPaySystemToDealer = new RadioButton();
            this._reconingMethod = new ComboBox();
            Panel panel = new Panel();
            Panel panel2 = new Panel();
            Panel panel3 = new Panel();
            Label label = new Label();
            Label label2 = new Label();
            panel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            panel2.SuspendLayout();
            panel3.SuspendLayout();
            base.SuspendLayout();
            panel.Controls.Add(this.groupBox1);
            panel.Controls.Add(this.lstbAvailable);
            panel.Controls.Add(this.label1);
            panel.Controls.Add(this.lstbSelected);
            panel.Controls.Add(this.label2);
            panel.Controls.Add(this.btnSelect);
            panel.Controls.Add(this.btnDeselectAll);
            panel.Controls.Add(this.btnSelectAll);
            panel.Controls.Add(this.btnDeselect);
            manager.ApplyResources(panel, "panel1");
            panel.Name = "panel1";
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.serviceType);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            this.serviceType.DropDownStyle = ComboBoxStyle.DropDownList;
            this.serviceType.FormattingEnabled = true;
            manager.ApplyResources(this.serviceType, "serviceType");
            this.serviceType.Name = "serviceType";
            this.serviceType.SelectedIndexChanged += new EventHandler(this.serviceType_SelectedIndexChanged);
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            manager.ApplyResources(this.lstbAvailable, "lstbAvailable");
            this.lstbAvailable.FormattingEnabled = true;
            this.lstbAvailable.Name = "lstbAvailable";
            this.lstbAvailable.SelectionMode = SelectionMode.MultiExtended;
            this.lstbAvailable.DoubleClick += new EventHandler(this.lstbAvailable_DoubleClick);
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.lstbSelected, "lstbSelected");
            this.lstbSelected.FormattingEnabled = true;
            this.lstbSelected.Name = "lstbSelected";
            this.lstbSelected.SelectionMode = SelectionMode.MultiExtended;
            this.lstbSelected.DoubleClick += new EventHandler(this.lstbSelected_DoubleClick);
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.btnSelect, "btnSelect");
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new EventHandler(this.btnSelect_Click);
            manager.ApplyResources(this.btnDeselectAll, "btnDeselectAll");
            this.btnDeselectAll.Name = "btnDeselectAll";
            this.btnDeselectAll.UseVisualStyleBackColor = true;
            this.btnDeselectAll.Click += new EventHandler(this.btnDeselectAll_Click);
            manager.ApplyResources(this.btnSelectAll, "btnSelectAll");
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new EventHandler(this.btnSelectAll_Click);
            manager.ApplyResources(this.btnDeselect, "btnDeselect");
            this.btnDeselect.Name = "btnDeselect";
            this.btnDeselect.UseVisualStyleBackColor = true;
            this.btnDeselect.Click += new EventHandler(this.btnDeselect_Click);
            panel2.Controls.Add(this.btnOk);
            panel2.Controls.Add(this.btnCancel);
            manager.ApplyResources(panel2, "panel2");
            panel2.Name = "panel2";
            manager.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.DialogResult = DialogResult.OK;
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new EventHandler(this.btnOk_Click);
            manager.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            panel3.Controls.Add(label);
            panel3.Controls.Add(this._rbDealerToPaySystem);
            panel3.Controls.Add(this._rbPaySystemToDealer);
            panel3.Controls.Add(label2);
            panel3.Controls.Add(this._reconingMethod);
            manager.ApplyResources(panel3, "panel3");
            panel3.Name = "panel3";
            manager.ApplyResources(label, "label5");
            label.Name = "label5";
            manager.ApplyResources(this._rbDealerToPaySystem, "_rbDealerToPaySystem");
            this._rbDealerToPaySystem.Name = "_rbDealerToPaySystem";
            this._rbDealerToPaySystem.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._rbPaySystemToDealer, "_rbPaySystemToDealer");
            this._rbPaySystemToDealer.Checked = true;
            this._rbPaySystemToDealer.Name = "_rbPaySystemToDealer";
            this._rbPaySystemToDealer.TabStop = true;
            this._rbPaySystemToDealer.UseVisualStyleBackColor = true;
            manager.ApplyResources(label2, "label3");
            label2.Name = "label3";
            this._reconingMethod.DropDownStyle = ComboBoxStyle.DropDownList;
            this._reconingMethod.FormattingEnabled = true;
            manager.ApplyResources(this._reconingMethod, "_reconingMethod");
            this._reconingMethod.Name = "_reconingMethod";
            base.AcceptButton = this.btnOk;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this.btnCancel;
            base.Controls.Add(panel);
            base.Controls.Add(panel2);
            base.Controls.Add(panel3);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "TariffPlanServicesGroupDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            panel.ResumeLayout(false);
            panel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            panel2.ResumeLayout(false);
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            base.ResumeLayout(false);
        }

        private void lstbAvailable_DoubleClick(object sender, EventArgs e)
        {
            if (this.lstbAvailable.SelectedItems.Count > 0)
            {
                this.SelectServices();
            }
        }

        private void lstbSelected_DoubleClick(object sender, EventArgs e)
        {
            if (this.lstbSelected.SelectedItems.Count > 0)
            {
                this.DeselectServices();
            }
        }

        private void MoveAll(IEnumerable moveList, List<Service> sourceList, List<Service> destinationList)
        {
            foreach (Service service in moveList)
            {
                sourceList.Remove(service);
                destinationList.Add(service);
            }
            this.FillListBoxes();
            foreach (Service service2 in moveList)
            {
                if (this.lstbAvailable.Items.Contains(service2))
                {
                    this.lstbAvailable.SelectedItems.Add(service2);
                }
                else
                {
                    this.lstbSelected.SelectedItems.Add(service2);
                }
            }
        }

        private void SelectServices()
        {
            ListBox.SelectedObjectCollection selectedItems = this.lstbAvailable.SelectedItems;
            if (selectedItems.Count > 0)
            {
                this.MoveAll(selectedItems, this._currentAvailableServices, this._currentSelectedServices);
            }
        }

        private void serviceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillListBoxes();
        }

        private string SrvToString(Service srv)
        {
            return string.Format("{0}-{1}", srv.Name, srv.Id);
        }

        private IBP.PaymentProject2.Common.Reconing.MoneyFlowDirection MoneyFlowDirection
        {
            get
            {
                if (this._rbDealerToPaySystem.Checked)
                {
                    return IBP.PaymentProject2.Common.Reconing.MoneyFlowDirection.DealerToPaySystem;
                }
                return IBP.PaymentProject2.Common.Reconing.MoneyFlowDirection.PaySystemToDealer;
            }
            set
            {
                if (value == IBP.PaymentProject2.Common.Reconing.MoneyFlowDirection.DealerToPaySystem)
                {
                    this._rbDealerToPaySystem.Checked = true;
                }
                else
                {
                    this._rbPaySystemToDealer.Checked = true;
                }
            }
        }

        private IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingMethod ReconingMethod
        {
            get
            {
                return (IBP.PaymentProject2.Oper.Forms.TariffPlan.ReconingMethod) this._reconingMethod.SelectedItem;
            }
            set
            {
                this._reconingMethod.SelectedItem = value;
            }
        }

        private ServiceType SelectedServiceType
        {
            get
            {
                return (this.serviceType.SelectedItem as ServiceType);
            }
        }

        private class ByNameComparer : IComparer<Service>
        {
            public int Compare(Service x, Service y)
            {
                return x.Name.CompareTo(y.Name);
            }
        }
    }
}

