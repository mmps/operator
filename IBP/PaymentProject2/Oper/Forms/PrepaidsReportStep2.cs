﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Windows.Forms;

    internal class PrepaidsReportStep2 : StepControl
    {
        private ColumnHeader columnHeaderComission;
        private ColumnHeader columnHeaderCount;
        private ColumnHeader columnHeaderLeft;
        private ColumnHeader columnHeaderNominal;
        private ColumnHeader columnHeaderTotal;
        private IContainer components;
        private PrepaidStatisticInfo[] info;
        private Label labelInfo;
        private ListViewEdit listViewInfo;
        private StepInfo stepInfo;

        public PrepaidsReportStep2(StepInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.InitializeComponent();
            this.stepInfo = info;
            this.MinimumSize = base.Size;
            base.HeaderText = UserStrings.ReportOnCards02;
            this.labelInfo.BringToFront();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void GetData()
        {
            try
            {
                this.info = new PrepaidStatisticInfo[0];
                Application.UseWaitCursor = true;
                foreach (PrepaidNominal nominal in IBP.PaymentProject2.Oper.ServerFasad.Server.GetPrepaidNominals())
                {
                    PrepaidNominalsContainer.Instance[nominal.Id] = nominal;
                }
                if (PrepaidNominalsContainer.Instance.GetNominals(this.stepInfo.Recipient).Length != 0)
                {
                    PrepaidsFilter filter = new PrepaidsFilter();
                    filter.EndTime = this.stepInfo.End;
                    filter.StartTime = this.stepInfo.Start;
                    filter.RecipientID = this.stepInfo.Recipient.Id;
                    this.info = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPrepaidStatisticInfo(filter).ToArray();
                    if (this.info.Length == 0)
                    {
                    }
                }
            }
            catch (SrvException exception)
            {
                throw new StepException(exception.Message, exception);
            }
            finally
            {
                Application.UseWaitCursor = false;
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(PrepaidsReportStep2));
            this.listViewInfo = new ListViewEdit();
            this.columnHeaderNominal = new ColumnHeader();
            this.columnHeaderCount = new ColumnHeader();
            this.columnHeaderLeft = new ColumnHeader();
            this.columnHeaderTotal = new ColumnHeader();
            this.columnHeaderComission = new ColumnHeader();
            this.labelInfo = new Label();
            base.SuspendLayout();
            this.listViewInfo.AllowSort = false;
            this.listViewInfo.BindType = null;
            this.listViewInfo.Columns.AddRange(new ColumnHeader[] { this.columnHeaderNominal, this.columnHeaderCount, this.columnHeaderLeft, this.columnHeaderTotal, this.columnHeaderComission });
            this.listViewInfo.ColumnsCollection = null;
            manager.ApplyResources(this.listViewInfo, "listViewInfo");
            this.listViewInfo.FullRowSelect = true;
            this.listViewInfo.GridLines = true;
            this.listViewInfo.Name = "listViewInfo";
            this.listViewInfo.ReadOnly = true;
            this.listViewInfo.UseCompatibleStateImageBehavior = false;
            this.listViewInfo.View = View.Details;
            manager.ApplyResources(this.columnHeaderNominal, "columnHeaderNominal");
            manager.ApplyResources(this.columnHeaderCount, "columnHeaderCount");
            manager.ApplyResources(this.columnHeaderLeft, "columnHeaderLeft");
            manager.ApplyResources(this.columnHeaderTotal, "columnHeaderTotal");
            manager.ApplyResources(this.columnHeaderComission, "columnHeaderComission");
            manager.ApplyResources(this.labelInfo, "labelInfo");
            this.labelInfo.Name = "labelInfo";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.labelInfo);
            base.Controls.Add(this.listViewInfo);
            base.Name = "PrepaidsReportStep2";
            base.ResumeLayout(false);
        }

        protected override void OnBeforeEnterStepForward()
        {
            base.OnBeforeEnterStepForward();
            this.GetData();
            this.SetData();
        }

        protected override void OnBeforeLeaveStepForward()
        {
            base.OnBeforeLeaveStepForward();
            string path = null;
            string extension = null;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.CheckFileExists = true;
                dialog.Multiselect = false;
                dialog.Title = UserStrings.ChooseTmpl;
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    throw new StepException("", true);
                }
                path = dialog.FileName;
                extension = Path.GetExtension(path);
            }
            PrepaidActTemplate template = null;
            try
            {
                template = new PrepaidActTemplate(path, null, this.info);
            }
            catch (Exception exception)
            {
                throw new StepException(exception.Message, exception);
            }
            string fileName = null;
            using (SaveFileDialog dialog2 = new SaveFileDialog())
            {
                dialog2.CheckFileExists = false;
                dialog2.DefaultExt = extension;
                if (dialog2.ShowDialog() != DialogResult.OK)
                {
                    throw new StepException("", true);
                }
                fileName = dialog2.FileName;
            }
            try
            {
                template.Save(fileName);
            }
            catch (Exception exception2)
            {
                throw new StepException(exception2.Message, exception2);
            }
        }

        private void SetData()
        {
            this.ShowInfo();
        }

        private void ShowInfo()
        {
            try
            {
                base.SuspendLayout();
                this.labelInfo.Text = string.Format(UserStrings.ReceiverReport, new object[] { Environment.NewLine, this.stepInfo.Recipient.Name, this.stepInfo.Start.ToString("D", Application.CurrentCulture), this.stepInfo.End.ToString("D", Application.CurrentCulture) });
                this.listViewInfo.Items.Clear();
                foreach (PrepaidStatisticInfo info in this.info)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = item.Name = info.PrepaidNominal.Nominal;
                    item.SubItems[0].Tag = item.Text;
                    item.SubItems.Add(info.Count.ToString());
                    item.SubItems[1].Tag = info.Count;
                    item.SubItems.Add(info.CountLeft.ToString());
                    item.SubItems[2].Tag = info.CountLeft;
                    item.SubItems.Add(info.Total.ToString("F", Application.CurrentCulture));
                    item.SubItems[3].Tag = info.Total;
                    item.SubItems.Add(info.Comission.ToString("F", Application.CurrentCulture));
                    item.SubItems[4].Tag = info.Comission;
                    this.listViewInfo.Items.Add(item);
                }
            }
            finally
            {
                base.ResumeLayout();
            }
        }
    }
}

