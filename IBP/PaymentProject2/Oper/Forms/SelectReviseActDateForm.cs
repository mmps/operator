﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class SelectReviseActDateForm : Form
    {
        private Button cancel;
        private IContainer components;
        private Button enter;
        private Label label1;
        private DateTimePicker startTime;

        public SelectReviseActDateForm()
        {
            this.InitializeComponent();
            DateTime today = DateTime.Today;
            DateTime time2 = today.AddDays((double) (1 - today.Day)).AddMonths(-1);
            this.startTime.Value = time2;
            this.startTime.MaxDate = time2;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(SelectReviseActDateForm));
            this.startTime = new DateTimePicker();
            this.enter = new Button();
            this.cancel = new Button();
            this.label1 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.startTime, "startTime");
            this.startTime.Format = DateTimePickerFormat.Custom;
            this.startTime.Name = "startTime";
            this.startTime.ShowUpDown = true;
            this.startTime.Value = new DateTime(0x7d9, 3, 1, 0, 0, 0, 0);
            this.enter.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.enter, "enter");
            this.enter.Name = "enter";
            this.enter.UseVisualStyleBackColor = true;
            this.cancel.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.cancel, "cancel");
            this.cancel.Name = "cancel";
            this.cancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.label1);
            base.Controls.Add(this.cancel);
            base.Controls.Add(this.enter);
            base.Controls.Add(this.startTime);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "SelectReviseActDateForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public DateTime StartDate
        {
            get
            {
                return this.startTime.Value;
            }
        }
    }
}

