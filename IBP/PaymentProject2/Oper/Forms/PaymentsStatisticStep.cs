﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class PaymentsStatisticStep : StepControl
    {
        private IContainer components;
        private StepInfo info;
        private Label label1;
        private Label label2;
        private Label labelCount;
        private Label labelTotal;

        public PaymentsStatisticStep(StepInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.info = info;
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(PaymentsStatisticStep));
            this.labelTotal = new Label();
            this.labelCount = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.labelTotal, "labelTotal");
            this.labelTotal.ForeColor = Color.Red;
            this.labelTotal.Name = "labelTotal";
            manager.ApplyResources(this.labelCount, "labelCount");
            this.labelCount.ForeColor = Color.Red;
            this.labelCount.Name = "labelCount";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.labelTotal);
            base.Controls.Add(this.labelCount);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            this.MinimumSize = new Size(0x151, 0x92);
            base.Name = "PaymentsStatisticStep";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnBeforeEnterStepForward()
        {
            base.OnBeforeEnterStepForward();
            try
            {
                this.info.StatisticInfo = IBP.PaymentProject2.Oper.ServerFasad.Server.GetStatisticInfo((PaymentsFilter) this.info.Filter)[0];
            }
            catch (SrvException exception)
            {
                throw new StepException(exception.Message, exception);
            }
            this.labelCount.Text = this.info.StatisticInfo.Count.ToString(Application.CurrentCulture);
            this.labelTotal.Text = this.info.Value.ToString("C", Application.CurrentCulture);
        }
    }
}

