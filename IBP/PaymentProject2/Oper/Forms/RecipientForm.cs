﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterForm(TypeSearch.Recipients), BindType(typeof(Recipient))]
    internal class RecipientForm : ResultForm
    {
        private IContainer components;

        public RecipientForm()
        {
            this.InitializeComponent();
            base.mainMenuItem.Text = UserStrings.Recipients;
            base.CreateMenuCommand(new ShowCommissionLimitation(), UserStrings.SetCommissionLimitation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecipientForm));
            base.SuspendLayout();
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "RecipientForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            RecipientFilter filter2 = (RecipientFilter) filter.Filter;
            try
            {
                string str = filter2.Part.ToLowerInvariant();
                bool isInnPart = filter2.IsInnPart;
                int recipientID = filter2.RecipientID;
                List<Recipient> list = new List<Recipient>();
                base.listViewItems.SuspendLayout();
                base.listViewItems.Items.Clear();
                foreach (Recipient recipient in RecipientsContainer.Instance.Collection)
                {
                    if (((isInnPart && recipient.Requisites.TaxpayerIdNumber.Contains(str)) || (!isInnPart && recipient.Name.ToLowerInvariant().Contains(str))) || string.IsNullOrEmpty(str))
                    {
                        if (recipientID != 0)
                        {
                            if (recipient.Serial == recipientID)
                            {
                                list.Add(recipient);
                            }
                        }
                        else
                        {
                            list.Add(recipient);
                        }
                    }
                }
                base.listViewItems.AddRange(list.ToArray());
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }
    }
}

