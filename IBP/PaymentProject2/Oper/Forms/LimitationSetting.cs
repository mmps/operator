﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common.CommissionLimitation;
    using System;

    internal class LimitationSetting : IComparable
    {
        private IBP.PaymentProject2.Common.CommissionLimitation.Limitation _limitation;
        private TimeLabel _mark;

        public LimitationSetting()
        {
        }

        public LimitationSetting(IBP.PaymentProject2.Common.CommissionLimitation.Limitation limitation)
        {
            this._limitation = limitation;
        }

        public int CompareTo(object obj)
        {
            LimitationSetting setting = (LimitationSetting) obj;
            return DateTime.Compare(this._limitation.AssignDate, setting.Limitation.AssignDate);
        }

        public IBP.PaymentProject2.Common.CommissionLimitation.Limitation Limitation
        {
            get
            {
                return this._limitation;
            }
            set
            {
                this._limitation = value;
            }
        }

        public TimeLabel Mark
        {
            get
            {
                return this._mark;
            }
            set
            {
                this._mark = value;
            }
        }
    }
}

