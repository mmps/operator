﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class StepControl : UserControl
    {
        private object BEFORE_ENTER_BW = new object();
        private object BEFORE_ENTER_FW = new object();
        private object BEFORE_LEAVE_BW = new object();
        private object BEFORE_LEAVE_FW = new object();
        private IContainer components;
        private Label labelHeadStep1;
        private StepControl nextStep;
        private StepControl previousStep;

        public event EventHandler BeforeEnterStepBackward
        {
            add
            {
                base.Events.AddHandler(this.BEFORE_ENTER_BW, value);
            }
            remove
            {
                base.Events.RemoveHandler(this.BEFORE_ENTER_BW, value);
            }
        }

        public event EventHandler BeforeEnterStepForward
        {
            add
            {
                base.Events.AddHandler(this.BEFORE_ENTER_FW, value);
            }
            remove
            {
                base.Events.RemoveHandler(this.BEFORE_ENTER_FW, value);
            }
        }

        public event EventHandler BeforeLeaveStepBackward
        {
            add
            {
                base.Events.AddHandler(this.BEFORE_LEAVE_BW, value);
            }
            remove
            {
                base.Events.RemoveHandler(this.BEFORE_LEAVE_BW, value);
            }
        }

        public event EventHandler BeforeLeaveStepForward
        {
            add
            {
                base.Events.AddHandler(this.BEFORE_LEAVE_FW, value);
            }
            remove
            {
                base.Events.RemoveHandler(this.BEFORE_LEAVE_FW, value);
            }
        }

        public StepControl()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StepControl));
            this.labelHeadStep1 = new Label();
            Panel panel = new Panel();
            panel.SuspendLayout();
            base.SuspendLayout();
            panel.BackColor = Color.White;
            panel.Controls.Add(this.labelHeadStep1);
            manager.ApplyResources(panel, "panel1");
            panel.Name = "panel1";
            manager.ApplyResources(this.labelHeadStep1, "labelHeadStep1");
            this.labelHeadStep1.Name = "labelHeadStep1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(panel);
            base.Name = "StepControl";
            panel.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        public StepControl MoveBack()
        {
            this.OnBeforeLeaveStepBackward();
            if (this.previousStep != null)
            {
                this.previousStep.OnBeforeEnterStepBackward();
            }
            base.Hide();
            if (this.previousStep != null)
            {
                this.previousStep.BringToFront();
                this.previousStep.Show();
            }
            return this.previousStep;
        }

        public StepControl MoveNext()
        {
            this.OnBeforeLeaveStepForward();
            if (this.nextStep != null)
            {
                this.nextStep.OnBeforeEnterStepForward();
            }
            base.Hide();
            if (this.nextStep != null)
            {
                this.nextStep.BringToFront();
                this.nextStep.Show();
            }
            return this.nextStep;
        }

        protected virtual void OnBeforeEnterStepBackward()
        {
            this.RaiseEvent(this.BEFORE_ENTER_BW);
        }

        protected virtual void OnBeforeEnterStepForward()
        {
            this.RaiseEvent(this.BEFORE_ENTER_FW);
        }

        protected virtual void OnBeforeLeaveStepBackward()
        {
            this.RaiseEvent(this.BEFORE_LEAVE_BW);
        }

        protected virtual void OnBeforeLeaveStepForward()
        {
            this.RaiseEvent(this.BEFORE_LEAVE_FW);
        }

        private void RaiseEvent(object key)
        {
            EventHandler handler = base.Events[key] as EventHandler;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public bool CanMoveBack
        {
            get
            {
                return (this.previousStep != null);
            }
        }

        public bool CanMoveNext
        {
            get
            {
                return (this.nextStep != null);
            }
        }

        public string HeaderText
        {
            get
            {
                return this.labelHeadStep1.Text;
            }
            set
            {
                this.labelHeadStep1.Text = value;
            }
        }

        public StepControl NextStep
        {
            get
            {
                return this.nextStep;
            }
            set
            {
                if (this.nextStep != null)
                {
                    if (this.nextStep.Equals(value))
                    {
                        return;
                    }
                    this.nextStep.PreviousStep = null;
                }
                this.nextStep = value;
                if (value != null)
                {
                    value.PreviousStep = this;
                }
            }
        }

        public StepControl PreviousStep
        {
            get
            {
                return this.previousStep;
            }
            set
            {
                if (this.previousStep != null)
                {
                    if (this.previousStep.Equals(value))
                    {
                        return;
                    }
                    this.previousStep.NextStep = null;
                }
                this.previousStep = value;
                if (value != null)
                {
                    value.NextStep = this;
                }
            }
        }

        public virtual bool SkipStep
        {
            get
            {
                return false;
            }
        }
    }
}

