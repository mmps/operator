﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false, Inherited=false)]
    internal class BindTypeAttribute : Attribute
    {
        public readonly Type BindType;

        public BindTypeAttribute(Type type)
        {
            this.BindType = type;
        }
    }
}

