﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;

    internal enum TimeLabel
    {
        Old,
        Current,
        Future,
        New
    }
}

