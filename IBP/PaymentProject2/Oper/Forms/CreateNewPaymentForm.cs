﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Security;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;

    internal class CreateNewPaymentForm : Form
    {
        private Button _buttonCheckPayment;
        private Button _buttonCreatePayment;
        private ComboBox _comboBoxClient;
        private ComboBox _comboBoxPoint;
        private ComboBox _comboBoxService;
        private ListViewEdit _listViewParams;
        private NumericUpDown _numericTotal;
        private NumericUpDown _numericUpDownComission;
        private Guid _paymentId;
        private Payment _paymentWithBalance;
        private TextBox _textBoxAccount;
        private ColumnHeader columnHeaderKey;
        private ColumnHeader columnHeaderValue;
        private IContainer components;
        private Panel panel3;

        public event GetGuide GetServiceGuide;

        public CreateNewPaymentForm()
        {
            this._paymentId = Guid.NewGuid();
            this.InitializeComponent();
            this.InitComboBoxies();
        }

        public CreateNewPaymentForm(Payment payment)
        {
            this._paymentId = Guid.NewGuid();
            this.InitializeComponent();
            this._paymentWithBalance = payment;
            this._numericTotal.Enabled = false;
            this._numericTotal.Value = this._numericUpDownComission.Maximum = this._paymentWithBalance.Balance;
            this.InitComboBoxies();
        }

        private void CheckPayment(object sender, EventArgs e)
        {
            Payment payment = this.GetPayment();
            if (payment != null)
            {
                try
                {
                    IBP.PaymentProject2.Oper.ServerFasad.Server.CheckAccount(ref payment);
                    PaymentState workState = payment.WorkState;
                    if (workState != PaymentState.AccountExists)
                    {
                        if (workState == PaymentState.AccountNotExists)
                        {
                            goto Label_003C;
                        }
                        goto Label_005E;
                    }
                    this.ShowSuccessfulCheckPaymentResults(payment);
                    return;
                Label_003C:
                    MessageBox.Show(string.Format(UserStrings.UnsuccessfulCheckPayment, payment.Comment), string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                Label_005E:
                    if (string.IsNullOrEmpty(payment.Comment))
                    {
                        throw new SrvException(payment.StringState);
                    }
                    throw new SrvException(payment.Comment);
                }
                catch (SrvException exception)
                {
                    MessageBox.Show(string.Format(UserStrings.ErrorCheckPayment, exception.Message), string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        private void CommissionChanged(object sender, EventArgs e)
        {
            if (this._paymentWithBalance != null)
            {
                this._numericTotal.Value = this._paymentWithBalance.Balance - this._numericUpDownComission.Value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillPaymentParameters(Service service)
        {
            Hashtable hashtable = new Hashtable();
            foreach (string str in service.InAttributes)
            {
                hashtable[str] = string.Empty;
            }
            foreach (DictionaryEntry entry in this.EnteredParameters)
            {
                hashtable[entry.Key] = entry.Value;
            }
            this.EnteredParameters = hashtable;
        }

        private Payment GetPayment()
        {
            PayPoint selectedItem = this._comboBoxPoint.SelectedItem as PayPoint;
            if (selectedItem == null)
            {
                MessageBox.Show(UserStrings.PoSNotChoosen);
                this._comboBoxPoint.Focus();
                return null;
            }
            Service selectedService = this.SelectedService;
            if (selectedService == null)
            {
                MessageBox.Show(UserStrings.ServiceNotChoosen);
                this._comboBoxService.Focus();
                return null;
            }
            if (this._textBoxAccount.Text == string.Empty)
            {
                MessageBox.Show(UserStrings.NoNumber);
                this._textBoxAccount.Focus();
                return null;
            }
            if ((this._numericTotal.Value < selectedService.MinValue) || (this._numericTotal.Value > selectedService.MaxValue))
            {
                MessageBox.Show(string.Format(UserStrings.SumShouldBe, selectedService.MinValue.ToString(), selectedService.MaxValue.ToString()));
                this._numericTotal.Focus();
                return null;
            }
            Payment payment = new Payment(selectedService, selectedItem, this._paymentId, this._numericTotal.Value + this._numericUpDownComission.Value, this._numericTotal.Value, 0M, 0M, 0M, this._textBoxAccount.Text, DateTime.Now, DateTime.Now, PaymentState.New);
            if (this._paymentWithBalance != null)
            {
                payment["Номера чеков"] = this._paymentWithBalance.Number;
            }
            if (!this.VerifyAndFillPaymentParameters(ref payment))
            {
                return null;
            }
            return payment;
        }

        private void InitComboBoxies()
        {
            foreach (Client client in ClientsContainer.Instance.Collection)
            {
                if ((client.OperPermission & Client.Permission.CreatePayment) == Client.Permission.CreatePayment)
                {
                    this._comboBoxClient.Items.Add(client);
                }
            }
            if (this._comboBoxClient.Items.Count == 0)
            {
                MessageBox.Show(UserStrings.ThereIsNoClientsForPaymentCreation, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                base.Close();
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CreateNewPaymentForm));
            this._comboBoxClient = new ComboBox();
            this._comboBoxPoint = new ComboBox();
            this._numericUpDownComission = new NumericUpDown();
            this._comboBoxService = new ComboBox();
            this._numericTotal = new NumericUpDown();
            this._listViewParams = new ListViewEdit();
            this.columnHeaderKey = new ColumnHeader();
            this.columnHeaderValue = new ColumnHeader();
            this.panel3 = new Panel();
            this._textBoxAccount = new TextBox();
            this._buttonCheckPayment = new Button();
            this._buttonCreatePayment = new Button();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            Label label5 = new Label();
            Label label6 = new Label();
            Panel panel = new Panel();
            GroupBox box = new GroupBox();
            Panel panel2 = new Panel();
            Button button = new Button();
            panel.SuspendLayout();
            this._numericUpDownComission.BeginInit();
            this._numericTotal.BeginInit();
            box.SuspendLayout();
            this.panel3.SuspendLayout();
            panel2.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(label, "label6");
            label.Name = "label6";
            manager.ApplyResources(label2, "label8");
            label2.Name = "label8";
            manager.ApplyResources(label3, "label7");
            label3.Name = "label7";
            manager.ApplyResources(label4, "label3");
            label4.Name = "label3";
            manager.ApplyResources(label5, "labelClient");
            label5.Name = "labelClient";
            manager.ApplyResources(label6, "label1");
            label6.Name = "label1";
            panel.Controls.Add(label5);
            panel.Controls.Add(this._comboBoxClient);
            panel.Controls.Add(this._comboBoxPoint);
            panel.Controls.Add(label4);
            panel.Controls.Add(this._numericUpDownComission);
            panel.Controls.Add(this._comboBoxService);
            panel.Controls.Add(this._numericTotal);
            panel.Controls.Add(label6);
            panel.Controls.Add(label2);
            panel.Controls.Add(label3);
            manager.ApplyResources(panel, "panel1");
            panel.Name = "panel1";
            manager.ApplyResources(this._comboBoxClient, "_comboBoxClient");
            this._comboBoxClient.DropDownStyle = ComboBoxStyle.DropDownList;
            this._comboBoxClient.DropDownWidth = 250;
            this._comboBoxClient.FormattingEnabled = true;
            this._comboBoxClient.Name = "_comboBoxClient";
            this._comboBoxClient.Sorted = true;
            this._comboBoxClient.SelectedIndexChanged += new EventHandler(this.SelectedClientChanged);
            manager.ApplyResources(this._comboBoxPoint, "_comboBoxPoint");
            this._comboBoxPoint.DropDownStyle = ComboBoxStyle.DropDownList;
            this._comboBoxPoint.DropDownWidth = 250;
            this._comboBoxPoint.FormattingEnabled = true;
            this._comboBoxPoint.Name = "_comboBoxPoint";
            this._comboBoxPoint.Sorted = true;
            manager.ApplyResources(this._numericUpDownComission, "_numericUpDownComission");
            this._numericUpDownComission.DecimalPlaces = 2;
            int[] bits = new int[4];
            bits[0] = -727379969;
            bits[1] = 0xe8;
            this._numericUpDownComission.Maximum = new decimal(bits);
            this._numericUpDownComission.Name = "_numericUpDownComission";
            this._numericUpDownComission.ValueChanged += new EventHandler(this.CommissionChanged);
            manager.ApplyResources(this._comboBoxService, "_comboBoxService");
            this._comboBoxService.DropDownStyle = ComboBoxStyle.DropDownList;
            this._comboBoxService.DropDownWidth = 250;
            this._comboBoxService.FormattingEnabled = true;
            this._comboBoxService.Name = "_comboBoxService";
            this._comboBoxService.Sorted = true;
            this._comboBoxService.SelectedIndexChanged += new EventHandler(this.SelectedServiceChanged);
            manager.ApplyResources(this._numericTotal, "_numericTotal");
            this._numericTotal.DecimalPlaces = 2;
            int[] numArray2 = new int[4];
            numArray2[0] = -727379969;
            numArray2[1] = 0xe8;
            this._numericTotal.Maximum = new decimal(numArray2);
            this._numericTotal.Name = "_numericTotal";
            box.Controls.Add(this._listViewParams);
            box.Controls.Add(this.panel3);
            manager.ApplyResources(box, "groupBoxParams");
            box.Name = "groupBoxParams";
            box.TabStop = false;
            this._listViewParams.AllowSort = true;
            this._listViewParams.BindType = null;
            this._listViewParams.Columns.AddRange(new ColumnHeader[] { this.columnHeaderKey, this.columnHeaderValue });
            this._listViewParams.ColumnsCollection = null;
            manager.ApplyResources(this._listViewParams, "_listViewParams");
            this._listViewParams.FormatProvider = new CultureInfo("");
            this._listViewParams.FullRowSelect = true;
            this._listViewParams.GridLines = true;
            this._listViewParams.Name = "_listViewParams";
            this._listViewParams.ReadOnly = false;
            this._listViewParams.UseCompatibleStateImageBehavior = false;
            this._listViewParams.View = View.Details;
            manager.ApplyResources(this.columnHeaderKey, "columnHeaderKey");
            manager.ApplyResources(this.columnHeaderValue, "columnHeaderValue");
            this.panel3.Controls.Add(this._textBoxAccount);
            this.panel3.Controls.Add(label);
            manager.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            manager.ApplyResources(this._textBoxAccount, "_textBoxAccount");
            this._textBoxAccount.Name = "_textBoxAccount";
            panel2.Controls.Add(this._buttonCheckPayment);
            panel2.Controls.Add(button);
            panel2.Controls.Add(this._buttonCreatePayment);
            manager.ApplyResources(panel2, "panel2");
            panel2.Name = "panel2";
            manager.ApplyResources(this._buttonCheckPayment, "_buttonCheckPayment");
            this._buttonCheckPayment.Name = "_buttonCheckPayment";
            this._buttonCheckPayment.UseVisualStyleBackColor = true;
            this._buttonCheckPayment.Click += new EventHandler(this.CheckPayment);
            manager.ApplyResources(button, "_buttonCancel");
            button.DialogResult = DialogResult.Cancel;
            button.Name = "_buttonCancel";
            button.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._buttonCreatePayment, "_buttonCreatePayment");
            this._buttonCreatePayment.Name = "_buttonCreatePayment";
            this._buttonCreatePayment.UseVisualStyleBackColor = true;
            this._buttonCreatePayment.Click += new EventHandler(this.ProcessPayment);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = button;
            base.ControlBox = false;
            base.Controls.Add(box);
            base.Controls.Add(panel);
            base.Controls.Add(panel2);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Name = "CreateNewPaymentForm";
            base.ShowInTaskbar = false;
            panel.ResumeLayout(false);
            panel.PerformLayout();
            this._numericUpDownComission.EndInit();
            this._numericTotal.EndInit();
            box.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            panel2.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void ProcessPayment(object sender, EventArgs e)
        {
            Payment payment = this.GetPayment();
            if (payment != null)
            {
                try
                {
                    Account account;
                    IBP.PaymentProject2.Oper.ServerFasad.Server.Process(ref payment, out account);
                    if (payment.WorkState != PaymentState.Rejected)
                    {
                        MessageBox.Show(UserStrings.PayCreated);
                    }
                    else if (payment.FullState == PaymentState.RejectedPosLimitEnded)
                    {
                        MessageBox.Show(UserStrings.PayCreatedAndRejectedPosLimit);
                    }
                    else
                    {
                        PaymentState fullState = payment.FullState;
                        MessageBox.Show(string.Format(UserStrings.PayNotCreated, Environment.NewLine, new EnumTypeInfo<PaymentState>(fullState).Name));
                    }
                    base.DialogResult = DialogResult.OK;
                }
                catch (SrvException exception)
                {
                    if (exception.InnerException is SecurityException)
                    {
                        MessageBox.Show(UserStrings.UHaveNoRights);
                        base.DialogResult = DialogResult.Cancel;
                    }
                    else
                    {
                        MessageBox.Show(exception.Message);
                    }
                }
            }
        }

        private void SelectedClientChanged(object sender, EventArgs e)
        {
            Client selectedItem = this._comboBoxClient.SelectedItem as Client;
            if (selectedItem != null)
            {
                this._comboBoxPoint.Items.Clear();
                foreach (PayPoint point in PointsContainer.Instance.Collection)
                {
                    if (point.ClientId == selectedItem.Id)
                    {
                        if (this._paymentWithBalance == null)
                        {
                            this._comboBoxPoint.Items.Add(point);
                        }
                        else if (point.CanUseCheck)
                        {
                            this._comboBoxPoint.Items.Add(point);
                        }
                    }
                }
                AvailableServiceGuide guide = this.GetServiceGuide(selectedItem.Id);
                this._comboBoxService.Items.Clear();
                foreach (Guid guid in (IEnumerable<Guid>) guide)
                {
                    this._comboBoxService.Items.Add(ServicesContainer.Instance[guid]);
                }
            }
            else
            {
                this._comboBoxPoint.Items.Clear();
                this._comboBoxService.Items.Clear();
            }
        }

        private void SelectedServiceChanged(object sender, EventArgs e)
        {
            Service selectedService = this.SelectedService;
            if (selectedService != null)
            {
                this.FillPaymentParameters(selectedService);
                this._buttonCheckPayment.Enabled = selectedService.CheckNecessity != CheckNecessity.Unnecessary;
            }
        }

        private void ShowSuccessfulCheckPaymentResults(Payment payment)
        {
            string text = null;
            if (payment.Service.HasOutAttributes)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine(UserStrings.SuccessfulCheckPaymentWithResults);
                foreach (string str2 in payment.Service.OutAttributes)
                {
                    string str3 = string.Empty;
                    if (payment.Contains(str2))
                    {
                        str3 = payment[str2];
                    }
                    builder.AppendFormat("{0} = {1}", str2, str3);
                    builder.AppendLine();
                }
                text = builder.ToString();
            }
            else
            {
                text = UserStrings.SuccessfulCheckPayment;
            }
            MessageBox.Show(text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private bool VerifyAndFillPaymentParameters(ref Payment payment)
        {
            Hashtable enteredParameters = this.EnteredParameters;
            foreach (string str in this.SelectedService.BindingAttributes)
            {
                if (!enteredParameters.Contains(str))
                {
                    MessageBox.Show(string.Format(UserStrings.ThereIsNoPaymentParameter, str), UserStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    this._listViewParams.Focus();
                    return false;
                }
            }
            payment.CopyAttributes(enteredParameters);
            return true;
        }

        private Hashtable EnteredParameters
        {
            get
            {
                Hashtable hashtable = new Hashtable();
                foreach (ListViewItem item in this._listViewParams.Items)
                {
                    string text = item.SubItems[1].Text;
                    string str2 = item.Text;
                    if (!string.IsNullOrEmpty(text))
                    {
                        hashtable[str2] = text;
                    }
                }
                return hashtable;
            }
            set
            {
                this._listViewParams.Items.Clear();
                foreach (DictionaryEntry entry in value)
                {
                    string str = entry.Key.ToString();
                    string text = entry.Value.ToString();
                    ListViewItem item = new ListViewItem();
                    item.Text = str;
                    item.SubItems.Add(text);
                    this._listViewParams.Items.Add(item);
                }
            }
        }

        private Service SelectedService
        {
            get
            {
                return (this._comboBoxService.SelectedItem as Service);
            }
        }

        public delegate AvailableServiceGuide GetGuide(Guid clientId);
    }
}

