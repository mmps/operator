﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Contracts;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [BindType(typeof(Client)), FilterForm(TypeSearch.Clients)]
    internal class ClientsForm : ResultForm
    {
        private IContainer components;

        public ClientsForm()
        {
            this.InitializeComponent();
            this.InitMenu();
            base.mainMenuItem.Text = UserStrings.Clients;
            ClientsContainer.Instance.AfterItemEvent += new EventHandler<ItemEventArgs<Client, Guid>>(this.ClientUpdated);
        }

        private void ClientUpdated(object sender, ItemEventArgs<Client, Guid> e)
        {
            if (base.InvokeRequired)
            {
                EventHandler<ItemEventArgs<Client, Guid>> method = new EventHandler<ItemEventArgs<Client, Guid>>(this.ClientUpdated);
                base.BeginInvoke(method, new object[] { sender, e });
            }
            else
            {
                if (e.Action == ItemAction.Update)
                {
                    ClientComparer comparer = new ClientComparer();
                    base.listViewItems.RefreshItem(e.Item, comparer);
                    this.OnSelectedItemChanged(null, null);
                }
                if (e.Action == ItemAction.Add)
                {
                    ClientFilter filter = (ClientFilter) this.Filter.Filter;
                    if (this.IsMatch(e.Item, filter))
                    {
                        base.listViewItems.Add(e.Item);
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientsForm));
            base.SuspendLayout();
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "ClientsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void InitMenu()
        {
            base.CreateMenuCommand(new ClientCommand(true), UserStrings.Add);
            base.CreateMainMenuCommand(new ClientCommand(false), UserStrings.Change);
            base.CreateMenuCommand(new AddSubDealer(), UserStrings.AddSubDealer);
            base.CreateMenuCommand(new AccountCommand(false), UserStrings.ChangeAcc);
            base.CreateMenuCommand(new ShowAccountTransactions(), UserStrings.CheckTrans);
            base.CreateMenuCommand(new ShowAccountBalance(), UserStrings.CheckSaldo);
            base.CreateMenuCommand(new RecountBalanceCommand(), UserStrings.RecountSaldo);
            base.CreateMenuCommand(new AssignPlanToClientCommand(), UserStrings.TariffPlanTie);
            base.CreateMenuCommand(new ShowClientTariffPlanHistory(), UserStrings.Review);
            base.CreateMenuCommand(new CreateClientReviseAct(), UserStrings.CreateReviseAct);
            base.CreateMenuCommand(new SetClientReviseActParams(), UserStrings.ReviseActParams);
            base.CreateMenuCommand(new ShowContractsCommand(), UserStrings.ShowContracts);
        }

        private bool IsMatch(Client client, ClientFilter filter)
        {
            if ((filter.RootDealers && !(client.ParentId == Guid.Empty)) || ((!filter.IsInnPart || !client.Requisites.TaxpayerIdNumber.Contains(filter.Part)) && (filter.IsInnPart || (!string.IsNullOrEmpty(filter.Part) && !client.Name.ToLowerInvariant().Contains(filter.Part)))))
            {
                return false;
            }
            return true;
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            try
            {
                base.listViewItems.SuspendLayout();
                base.listViewItems.Items.Clear();
                base.OnFilterChanged(filter);
                Dictionary<Guid, Client> dictionary = new Dictionary<Guid, Client>();
                ClientFilter filter2 = (ClientFilter) filter.Filter;
                filter2.Part.ToLowerInvariant();
                bool isInnPart = filter2.IsInnPart;
                bool rootDealers = filter2.RootDealers;
                foreach (Client client in ClientsContainer.Instance.Collection)
                {
                    if (this.IsMatch(client, filter2))
                    {
                        dictionary.Add(client.Id, client);
                    }
                }
                if (filter2.WithSubDealers && (dictionary.Count > 0))
                {
                    Guid id;
                    Client client3;
                    List<Client> list = new List<Client>();
                    foreach (Client client2 in ClientsContainer.Instance.Collection)
                    {
                        if (client2.ParentId != Guid.Empty)
                        {
                            list.Add(client2);
                        }
                    }
                    List<Client> list2 = new List<Client>();
                    int count = list.Count;
                    foreach (Client client4 in dictionary.Values)
                    {
                        id = client4.Id;
                        for (int i = 0; i < count; i++)
                        {
                            client3 = list[i];
                            if ((client3.ParentId == id) && !dictionary.ContainsKey(client3.Id))
                            {
                                list2.Add(client3);
                            }
                        }
                    }
                    int num3 = list2.Count;
                    if (num3 > 0)
                    {
                        int num4 = 0;
                        while (num4 < num3)
                        {
                            Client client5 = list2[num4];
                            id = client5.Id;
                            for (int j = 0; j < count; j++)
                            {
                                client3 = list[j];
                                if ((client3.ParentId == id) && !dictionary.ContainsKey(client3.Id))
                                {
                                    num3++;
                                    list2.Add(client3);
                                }
                            }
                            dictionary.Add(id, client5);
                            list2.Remove(client5);
                            num3--;
                        }
                    }
                }
                foreach (Client client6 in dictionary.Values)
                {
                    if (client6.HasAccount)
                    {
                        try
                        {
                            AccountsContainer.Instance[client6.AccountId] = IBP.PaymentProject2.Oper.ServerFasad.Server.GetAccount(client6.AccountId);
                        }
                        catch (SrvException exception)
                        {
                            MessageBox.Show(string.Format(UserStrings.CantGetData, Environment.NewLine, client6.Name, exception.Message));
                        }
                    }
                    base.listViewItems.Add(client6);
                }
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return ClientsSettings.Default.Helper;
            }
        }

        private class ClientComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                Client client = (Client) x;
                Client client2 = (Client) y;
                return client.Id.CompareTo(client2.Id);
            }
        }
    }
}

