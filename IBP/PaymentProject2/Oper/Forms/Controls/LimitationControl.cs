﻿namespace IBP.PaymentProject2.Oper.Forms.Controls
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.CommissionLimitation;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class LimitationControl : UserControl
    {
        private CheckBox _checkBoxMaxAmount;
        private CheckBox _checkBoxMaxRate;
        private CheckBox _checkBoxMinAmount;
        private CheckBox _checkBoxMinRate;
        private DateTimePicker _dateTimePicker;
        private ErrorProvider _errorProvider;
        private GroupBox _groupBoxCommissionType;
        private GroupBox _groupBoxSettings;
        private Label _labelLimitation;
        private NumericUpDown _numericUpDownFixedAmount;
        private NumericUpDown _numericUpDownFixRate;
        private NumericUpDown _numericUpDownMaxAmount;
        private NumericUpDown _numericUpDownMaxRate;
        private NumericUpDown _numericUpDownMinAmount;
        private NumericUpDown _numericUpDownMinRate;
        private RadioButton _radioButtonCombine;
        private RadioButton _radioButtonExternal;
        private RadioButton _radioButtonFixedAmount;
        private RadioButton _radioButtonFixedRate;
        private RadioButton _radioButtonForbidden;
        private RadioButton _radioButtonInternal;
        private RadioButton _radioButtonNotLimited;
        private IContainer components;

        public LimitationControl()
        {
            this.InitializeComponent();
            this._groupBoxSettings.Enabled = false;
        }

        private void CheckBoxesCheckedChanged(object sender, EventArgs e)
        {
            this._numericUpDownMaxAmount.Enabled = this._checkBoxMaxAmount.Checked && this._checkBoxMaxAmount.Enabled;
            this._numericUpDownMaxRate.Enabled = this._checkBoxMaxRate.Checked && this._checkBoxMaxRate.Enabled;
            this._numericUpDownMinAmount.Enabled = this._checkBoxMinAmount.Checked && this._checkBoxMinAmount.Enabled;
            this._numericUpDownMinRate.Enabled = this._checkBoxMinRate.Checked && this._checkBoxMinRate.Enabled;
            this._errorProvider.Clear();
        }

        private bool CheckCorrectValue()
        {
            if (this._numericUpDownFixedAmount.Enabled && (0M == this._numericUpDownFixedAmount.Value))
            {
                this._errorProvider.SetError(this._radioButtonFixedAmount, UserStrings.NumericUpDownErrorString);
                return false;
            }
            if (this._numericUpDownFixRate.Enabled && (0M == this._numericUpDownFixRate.Value))
            {
                this._errorProvider.SetError(this._radioButtonFixedRate, UserStrings.NumericUpDownErrorString);
                return false;
            }
            if (this._numericUpDownMaxAmount.Enabled && (0M == this._numericUpDownMaxAmount.Value))
            {
                this._errorProvider.SetError(this._checkBoxMaxAmount, UserStrings.NumericUpDownErrorString);
                return false;
            }
            if (this._numericUpDownMaxRate.Enabled && (0M == this._numericUpDownMaxRate.Value))
            {
                this._errorProvider.SetError(this._checkBoxMaxRate, UserStrings.NumericUpDownErrorString);
                return false;
            }
            if (this._numericUpDownMinAmount.Enabled && (0M == this._numericUpDownMinAmount.Value))
            {
                this._errorProvider.SetError(this._checkBoxMinAmount, UserStrings.NumericUpDownErrorString);
                return false;
            }
            if (this._numericUpDownMinRate.Enabled && (0M == this._numericUpDownMinRate.Value))
            {
                this._errorProvider.SetError(this._checkBoxMinRate, UserStrings.NumericUpDownErrorString);
                return false;
            }
            if (((this._radioButtonCombine.Checked && !this._checkBoxMaxAmount.Checked) && (!this._checkBoxMaxRate.Checked && !this._checkBoxMinAmount.Checked)) && !this._checkBoxMinRate.Checked)
            {
                this._errorProvider.SetError(this._radioButtonCombine, UserStrings.ChooseOneCondition);
                return false;
            }
            return true;
        }

        private void ClearSetting()
        {
            this._radioButtonNotLimited.Checked = false;
            this._radioButtonForbidden.Checked = false;
            this._radioButtonFixedRate.Checked = false;
            this._radioButtonFixedAmount.Checked = false;
            this._radioButtonCombine.Checked = false;
            this._checkBoxMaxAmount.Checked = false;
            this._checkBoxMaxRate.Checked = false;
            this._checkBoxMinAmount.Checked = false;
            this._checkBoxMinRate.Checked = false;
            this._numericUpDownFixedAmount.Value = 0M;
            this._numericUpDownFixRate.Value = 0M;
            this._numericUpDownMaxAmount.Value = 0M;
            this._numericUpDownMaxRate.Value = 0M;
            this._numericUpDownMinAmount.Value = 0M;
            this._numericUpDownMinRate.Value = 0M;
            this._radioButtonInternal.Checked = false;
            this._radioButtonExternal.Checked = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void GroupBoxSettingsEnabledChanged(object sender, EventArgs e)
        {
            this._groupBoxCommissionType.Enabled = this._groupBoxSettings.Enabled;
            this._dateTimePicker.Enabled = this._groupBoxSettings.Enabled;
            this._labelLimitation.Enabled = this._groupBoxSettings.Enabled;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(LimitationControl));
            this._labelLimitation = new Label();
            this._groupBoxCommissionType = new GroupBox();
            this._radioButtonInternal = new RadioButton();
            this._radioButtonExternal = new RadioButton();
            this._numericUpDownMaxAmount = new NumericUpDown();
            this._checkBoxMaxAmount = new CheckBox();
            this._dateTimePicker = new DateTimePicker();
            this._groupBoxSettings = new GroupBox();
            this._checkBoxMinAmount = new CheckBox();
            this._numericUpDownMinAmount = new NumericUpDown();
            this._checkBoxMaxRate = new CheckBox();
            this._numericUpDownMaxRate = new NumericUpDown();
            this._checkBoxMinRate = new CheckBox();
            this._numericUpDownMinRate = new NumericUpDown();
            this._radioButtonCombine = new RadioButton();
            this._numericUpDownFixedAmount = new NumericUpDown();
            this._radioButtonFixedAmount = new RadioButton();
            this._numericUpDownFixRate = new NumericUpDown();
            this._radioButtonFixedRate = new RadioButton();
            this._radioButtonForbidden = new RadioButton();
            this._radioButtonNotLimited = new RadioButton();
            this._errorProvider = new ErrorProvider(this.components);
            this._groupBoxCommissionType.SuspendLayout();
            this._numericUpDownMaxAmount.BeginInit();
            this._groupBoxSettings.SuspendLayout();
            this._numericUpDownMinAmount.BeginInit();
            this._numericUpDownMaxRate.BeginInit();
            this._numericUpDownMinRate.BeginInit();
            this._numericUpDownFixedAmount.BeginInit();
            this._numericUpDownFixRate.BeginInit();
            ((ISupportInitialize) this._errorProvider).BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(this._labelLimitation, "_labelLimitation");
            this._labelLimitation.Name = "_labelLimitation";
            this._groupBoxCommissionType.Controls.Add(this._radioButtonInternal);
            this._groupBoxCommissionType.Controls.Add(this._radioButtonExternal);
            manager.ApplyResources(this._groupBoxCommissionType, "_groupBoxCommissionType");
            this._groupBoxCommissionType.Name = "_groupBoxCommissionType";
            this._groupBoxCommissionType.TabStop = false;
            manager.ApplyResources(this._radioButtonInternal, "_radioButtonInternal");
            this._radioButtonInternal.Name = "_radioButtonInternal";
            this._radioButtonInternal.UseVisualStyleBackColor = true;
            this._radioButtonInternal.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            manager.ApplyResources(this._radioButtonExternal, "_radioButtonExternal");
            this._radioButtonExternal.Checked = true;
            this._radioButtonExternal.Name = "_radioButtonExternal";
            this._radioButtonExternal.TabStop = true;
            this._radioButtonExternal.UseVisualStyleBackColor = true;
            this._radioButtonExternal.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            this._numericUpDownMaxAmount.DecimalPlaces = 2;
            this._errorProvider.SetIconAlignment(this._numericUpDownMaxAmount, (ErrorIconAlignment) manager.GetObject("_numericUpDownMaxAmount.IconAlignment"));
            manager.ApplyResources(this._numericUpDownMaxAmount, "_numericUpDownMaxAmount");
            int[] bits = new int[4];
            bits[0] = 0x7fffffff;
            this._numericUpDownMaxAmount.Maximum = new decimal(bits);
            this._numericUpDownMaxAmount.Name = "_numericUpDownMaxAmount";
            this._numericUpDownMaxAmount.ValueChanged += new EventHandler(this.NumericUpDownValueChanged);
            manager.ApplyResources(this._checkBoxMaxAmount, "_checkBoxMaxAmount");
            this._checkBoxMaxAmount.Name = "_checkBoxMaxAmount";
            this._checkBoxMaxAmount.UseVisualStyleBackColor = true;
            this._checkBoxMaxAmount.CheckedChanged += new EventHandler(this.CheckBoxesCheckedChanged);
            manager.ApplyResources(this._dateTimePicker, "_dateTimePicker");
            this._dateTimePicker.Name = "_dateTimePicker";
            this._dateTimePicker.Value = new DateTime(0x7d9, 9, 20, 12, 0x17, 0, 0);
            this._groupBoxSettings.Controls.Add(this._numericUpDownMaxAmount);
            this._groupBoxSettings.Controls.Add(this._checkBoxMaxAmount);
            this._groupBoxSettings.Controls.Add(this._checkBoxMinAmount);
            this._groupBoxSettings.Controls.Add(this._numericUpDownMinAmount);
            this._groupBoxSettings.Controls.Add(this._checkBoxMaxRate);
            this._groupBoxSettings.Controls.Add(this._numericUpDownMaxRate);
            this._groupBoxSettings.Controls.Add(this._checkBoxMinRate);
            this._groupBoxSettings.Controls.Add(this._numericUpDownMinRate);
            this._groupBoxSettings.Controls.Add(this._radioButtonCombine);
            this._groupBoxSettings.Controls.Add(this._numericUpDownFixedAmount);
            this._groupBoxSettings.Controls.Add(this._radioButtonFixedAmount);
            this._groupBoxSettings.Controls.Add(this._numericUpDownFixRate);
            this._groupBoxSettings.Controls.Add(this._radioButtonFixedRate);
            this._groupBoxSettings.Controls.Add(this._radioButtonForbidden);
            this._groupBoxSettings.Controls.Add(this._radioButtonNotLimited);
            manager.ApplyResources(this._groupBoxSettings, "_groupBoxSettings");
            this._groupBoxSettings.Name = "_groupBoxSettings";
            this._groupBoxSettings.TabStop = false;
            this._groupBoxSettings.EnabledChanged += new EventHandler(this.GroupBoxSettingsEnabledChanged);
            manager.ApplyResources(this._checkBoxMinAmount, "_checkBoxMinAmount");
            this._checkBoxMinAmount.Name = "_checkBoxMinAmount";
            this._checkBoxMinAmount.UseVisualStyleBackColor = true;
            this._checkBoxMinAmount.CheckedChanged += new EventHandler(this.CheckBoxesCheckedChanged);
            this._numericUpDownMinAmount.DecimalPlaces = 2;
            this._errorProvider.SetIconAlignment(this._numericUpDownMinAmount, (ErrorIconAlignment) manager.GetObject("_numericUpDownMinAmount.IconAlignment"));
            manager.ApplyResources(this._numericUpDownMinAmount, "_numericUpDownMinAmount");
            int[] numArray2 = new int[4];
            numArray2[0] = 0x7fffffff;
            this._numericUpDownMinAmount.Maximum = new decimal(numArray2);
            this._numericUpDownMinAmount.Name = "_numericUpDownMinAmount";
            this._numericUpDownMinAmount.ValueChanged += new EventHandler(this.NumericUpDownValueChanged);
            manager.ApplyResources(this._checkBoxMaxRate, "_checkBoxMaxRate");
            this._checkBoxMaxRate.Name = "_checkBoxMaxRate";
            this._checkBoxMaxRate.UseVisualStyleBackColor = true;
            this._checkBoxMaxRate.CheckedChanged += new EventHandler(this.CheckBoxesCheckedChanged);
            this._numericUpDownMaxRate.DecimalPlaces = 3;
            this._errorProvider.SetIconAlignment(this._numericUpDownMaxRate, (ErrorIconAlignment) manager.GetObject("_numericUpDownMaxRate.IconAlignment"));
            manager.ApplyResources(this._numericUpDownMaxRate, "_numericUpDownMaxRate");
            this._numericUpDownMaxRate.Name = "_numericUpDownMaxRate";
            this._numericUpDownMaxRate.ValueChanged += new EventHandler(this.NumericUpDownValueChanged);
            manager.ApplyResources(this._checkBoxMinRate, "_checkBoxMinRate");
            this._checkBoxMinRate.Name = "_checkBoxMinRate";
            this._checkBoxMinRate.UseVisualStyleBackColor = true;
            this._checkBoxMinRate.CheckedChanged += new EventHandler(this.CheckBoxesCheckedChanged);
            this._numericUpDownMinRate.DecimalPlaces = 3;
            this._errorProvider.SetIconAlignment(this._numericUpDownMinRate, (ErrorIconAlignment) manager.GetObject("_numericUpDownMinRate.IconAlignment"));
            manager.ApplyResources(this._numericUpDownMinRate, "_numericUpDownMinRate");
            this._numericUpDownMinRate.Name = "_numericUpDownMinRate";
            this._numericUpDownMinRate.ValueChanged += new EventHandler(this.NumericUpDownValueChanged);
            manager.ApplyResources(this._radioButtonCombine, "_radioButtonCombine");
            this._radioButtonCombine.Name = "_radioButtonCombine";
            this._radioButtonCombine.TabStop = true;
            this._radioButtonCombine.UseVisualStyleBackColor = true;
            this._radioButtonCombine.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            this._numericUpDownFixedAmount.DecimalPlaces = 2;
            this._errorProvider.SetIconAlignment(this._numericUpDownFixedAmount, (ErrorIconAlignment) manager.GetObject("_numericUpDownFixedAmount.IconAlignment"));
            manager.ApplyResources(this._numericUpDownFixedAmount, "_numericUpDownFixedAmount");
            int[] numArray3 = new int[4];
            numArray3[0] = 0x7fffffff;
            this._numericUpDownFixedAmount.Maximum = new decimal(numArray3);
            this._numericUpDownFixedAmount.Name = "_numericUpDownFixedAmount";
            this._numericUpDownFixedAmount.ValueChanged += new EventHandler(this.NumericUpDownValueChanged);
            manager.ApplyResources(this._radioButtonFixedAmount, "_radioButtonFixedAmount");
            this._radioButtonFixedAmount.Name = "_radioButtonFixedAmount";
            this._radioButtonFixedAmount.TabStop = true;
            this._radioButtonFixedAmount.UseVisualStyleBackColor = true;
            this._radioButtonFixedAmount.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            this._numericUpDownFixRate.DecimalPlaces = 3;
            this._errorProvider.SetIconAlignment(this._numericUpDownFixRate, (ErrorIconAlignment) manager.GetObject("_numericUpDownFixRate.IconAlignment"));
            manager.ApplyResources(this._numericUpDownFixRate, "_numericUpDownFixRate");
            this._numericUpDownFixRate.Name = "_numericUpDownFixRate";
            this._numericUpDownFixRate.ValueChanged += new EventHandler(this.NumericUpDownValueChanged);
            manager.ApplyResources(this._radioButtonFixedRate, "_radioButtonFixedRate");
            this._radioButtonFixedRate.Name = "_radioButtonFixedRate";
            this._radioButtonFixedRate.TabStop = true;
            this._radioButtonFixedRate.UseVisualStyleBackColor = true;
            this._radioButtonFixedRate.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            manager.ApplyResources(this._radioButtonForbidden, "_radioButtonForbidden");
            this._radioButtonForbidden.Name = "_radioButtonForbidden";
            this._radioButtonForbidden.TabStop = true;
            this._radioButtonForbidden.UseVisualStyleBackColor = true;
            this._radioButtonForbidden.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            manager.ApplyResources(this._radioButtonNotLimited, "_radioButtonNotLimited");
            this._radioButtonNotLimited.Checked = true;
            this._radioButtonNotLimited.Name = "_radioButtonNotLimited";
            this._radioButtonNotLimited.TabStop = true;
            this._radioButtonNotLimited.UseVisualStyleBackColor = true;
            this._radioButtonNotLimited.CheckedChanged += new EventHandler(this.RadioButtonsCheckedChanged);
            this._errorProvider.ContainerControl = this;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._groupBoxSettings);
            base.Controls.Add(this._groupBoxCommissionType);
            base.Controls.Add(this._dateTimePicker);
            base.Controls.Add(this._labelLimitation);
            base.Name = "LimitationControl";
            this._groupBoxCommissionType.ResumeLayout(false);
            this._groupBoxCommissionType.PerformLayout();
            this._numericUpDownMaxAmount.EndInit();
            this._groupBoxSettings.ResumeLayout(false);
            this._groupBoxSettings.PerformLayout();
            this._numericUpDownMinAmount.EndInit();
            this._numericUpDownMaxRate.EndInit();
            this._numericUpDownMinRate.EndInit();
            this._numericUpDownFixedAmount.EndInit();
            this._numericUpDownFixRate.EndInit();
            ((ISupportInitialize) this._errorProvider).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void NumericUpDownValueChanged(object sender, EventArgs e)
        {
            this._errorProvider.Clear();
        }

        private void RadioButtonsCheckedChanged(object sender, EventArgs e)
        {
            this.SetCombineControlsEnable(this._radioButtonCombine.Checked);
            this._numericUpDownFixedAmount.Enabled = this._radioButtonFixedAmount.Checked;
            this._numericUpDownFixRate.Enabled = this._radioButtonFixedRate.Checked;
        }

        private void SetCombineControlsEnable(bool check)
        {
            this._checkBoxMaxAmount.Enabled = check;
            this._checkBoxMinAmount.Enabled = check;
            this._checkBoxMaxRate.Enabled = check;
            this._checkBoxMinRate.Enabled = check;
            this.CheckBoxesCheckedChanged(null, null);
        }

        public LimitationSetting Setting
        {
            get
            {
                LimitationSetting setting = new LimitationSetting();
                if (!this.CheckCorrectValue())
                {
                    return null;
                }
                if (this._radioButtonNotLimited.Checked)
                {
                    NoLimitation limitation = new NoLimitation();
                    setting.Limitation = limitation;
                }
                if (this._radioButtonForbidden.Checked)
                {
                    ForbiddenLimitation limitation2 = new ForbiddenLimitation();
                    setting.Limitation = limitation2;
                }
                if (this._radioButtonFixedRate.Checked)
                {
                    RateLimitation limitation3 = new RateLimitation(this._numericUpDownFixRate.Value);
                    setting.Limitation = limitation3;
                }
                if (this._radioButtonFixedAmount.Checked)
                {
                    AmountLimitation limitation4 = new AmountLimitation(this._numericUpDownFixedAmount.Value);
                    setting.Limitation = limitation4;
                }
                if (this._radioButtonCombine.Checked)
                {
                    CombinedLimitation limitation5 = new CombinedLimitation();
                    if (this._checkBoxMaxAmount.Checked)
                    {
                        limitation5.MaxAmount = this._numericUpDownMaxAmount.Value;
                    }
                    if (this._checkBoxMaxRate.Checked)
                    {
                        limitation5.MaxRate = this._numericUpDownMaxRate.Value;
                    }
                    if (this._checkBoxMinAmount.Checked)
                    {
                        limitation5.MinAmount = this._numericUpDownMinAmount.Value;
                    }
                    if (this._checkBoxMinRate.Checked)
                    {
                        limitation5.MinRate = this._numericUpDownMinRate.Value;
                    }
                    setting.Limitation = limitation5;
                }
                if (setting.Limitation == null)
                {
                    return null;
                }
                setting.Limitation.AssignDate = this._dateTimePicker.Value;
                setting.Limitation.CommissionType = this._radioButtonExternal.Checked ? CommissionType.External : CommissionType.Internal;
                return setting;
            }
            set
            {
                LimitationSetting setting = value;
                if (setting == null)
                {
                    this.ClearSetting();
                    this._groupBoxSettings.Enabled = false;
                }
                else
                {
                    if (setting.Limitation == null)
                    {
                        AmountLimitation limitation4 = new AmountLimitation(0M);
                        limitation4.AssignDate = DateTime.Now;
                        setting.Limitation = limitation4;
                    }
                    if ((TimeLabel.Current == setting.Mark) || (setting.Mark == TimeLabel.Old))
                    {
                        this._groupBoxSettings.Enabled = false;
                    }
                    else
                    {
                        this._groupBoxSettings.Enabled = true;
                    }
                    this.ClearSetting();
                    this._dateTimePicker.Value = setting.Limitation.AssignDate;
                    CommissionType commissionType = setting.Limitation.CommissionType;
                    if (CommissionType.External == commissionType)
                    {
                        this._radioButtonExternal.Checked = true;
                    }
                    else
                    {
                        this._radioButtonInternal.Checked = true;
                    }
                    switch (setting.Limitation.Type)
                    {
                        case LimitationType.NoLimitation:
                            this._radioButtonNotLimited.Checked = true;
                            return;

                        case LimitationType.Forbidden:
                            this._radioButtonForbidden.Checked = true;
                            return;

                        case LimitationType.FixRate:
                        {
                            this._radioButtonFixedRate.Checked = true;
                            RateLimitation limitation = (RateLimitation) setting.Limitation;
                            this._numericUpDownFixRate.Value = limitation.Rate;
                            return;
                        }
                        case LimitationType.FixAmount:
                        {
                            this._radioButtonFixedAmount.Checked = true;
                            AmountLimitation limitation2 = (AmountLimitation) setting.Limitation;
                            this._numericUpDownFixedAmount.Value = limitation2.Amount;
                            return;
                        }
                        case LimitationType.Combined:
                        {
                            this._radioButtonCombine.Checked = true;
                            CombinedLimitation limitation3 = (CombinedLimitation) setting.Limitation;
                            if (0M < limitation3.MaxAmount)
                            {
                                this._checkBoxMaxAmount.Checked = true;
                                this._numericUpDownMaxAmount.Value = limitation3.MaxAmount;
                            }
                            if (0M < limitation3.MaxRate)
                            {
                                this._checkBoxMaxRate.Checked = true;
                                this._numericUpDownMaxRate.Value = limitation3.MaxRate;
                            }
                            if (0M < limitation3.MinAmount)
                            {
                                this._checkBoxMinAmount.Checked = true;
                                this._numericUpDownMinAmount.Value = limitation3.MinAmount;
                            }
                            if (0M >= limitation3.MinRate)
                            {
                                break;
                            }
                            this._checkBoxMinRate.Checked = true;
                            this._numericUpDownMinRate.Value = limitation3.MinRate;
                            return;
                        }
                        default:
                            MessageBox.Show("Как же так?");
                            break;
                    }
                }
            }
        }
    }
}

