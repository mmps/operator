﻿namespace IBP.PaymentProject2.Oper.Forms.Controls
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class PaymentTypeCheckBox : CheckBox
    {
        private IContainer components;
        private ContextMenuStrip contextMenu;

        public PaymentTypeCheckBox()
        {
            this.InitializeComponent();
            this.contextMenu = new ContextMenuStrip();
            this.contextMenu.ShowItemToolTips = true;
            this.contextMenu.AutoClose = false;
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.Own);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.Dealer);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.SubDealer);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.NonComission);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.Comission);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.Cash);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.NonCash);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.DigitalMoney);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.CardInvoice);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.OwnCardPayment);
            this.AddContextMenuItem(PaymentType.PaymentTypeFlags.AlienCardPayment);
            this.contextMenu.KeyUp += new KeyEventHandler(this.contextMenu_KeyPress);
            base.MouseLeave += new EventHandler(this.PaymentTypeCheckBox_MouseLeave);
            this.contextMenu.MouseLeave += new EventHandler(this.PaymentTypeCheckBox_MouseLeave);
            base.Leave += new EventHandler(this.PaymentTypeCheckBox_MouseLeave);
            base.MouseUp += new MouseEventHandler(this.PaymentTypeCheckBox_MouseUp);
            base.MouseMove += new MouseEventHandler(this.PaymentTypeCheckBox_MouseUp);
            base.CheckedChanged += new EventHandler(this.PaymentTypeCheckBox_MouseUp);
            base.Enter += new EventHandler(this.PaymentTypeCheckBox_MouseUp);
        }

        private void AddContextMenuItem(PaymentType.PaymentTypeFlags item)
        {
            EnumTypeInfo<PaymentType.PaymentTypeFlags> info = new EnumTypeInfo<PaymentType.PaymentTypeFlags>(item);
            ToolStripMenuItem item2 = new ToolStripMenuItem();
            item2.Text = info.Name;
            item2.ToolTipText = info.Description;
            item2.CheckOnClick = true;
            item2.Tag = item;
            this.contextMenu.Items.Add(item2);
        }

        private void CheckMenu()
        {
            if (base.Checked)
            {
                if (!this.contextMenu.Visible)
                {
                    this.ShowMenu();
                }
            }
            else
            {
                this.HideMenu();
            }
        }

        private void contextMenu_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.contextMenu.Close();
            }
            if (e.KeyCode == Keys.Space)
            {
                foreach (ToolStripMenuItem item in this.contextMenu.Items)
                {
                    if (item.Selected)
                    {
                        item.Checked = !item.Checked;
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void HideMenu()
        {
            this.contextMenu.Close();
        }

        private void InitializeComponent()
        {
            base.SuspendLayout();
            base.ResumeLayout(false);
        }

        private void PaymentTypeCheckBox_MouseLeave(object sender, EventArgs e)
        {
            if (!this.IsMouseOver)
            {
                this.HideMenu();
            }
        }

        private void PaymentTypeCheckBox_MouseUp(object sender, EventArgs e)
        {
            MouseEventArgs args = e as MouseEventArgs;
            bool flag = false;
            if (args != null)
            {
                flag = (((args.X >= 0) && (args.Y >= 0)) && (args.X <= base.Width)) && (args.Y <= base.Height);
            }
            if (flag || this.Focused)
            {
                this.CheckMenu();
            }
        }

        private void ShowMenu()
        {
            Point position = new Point(0, base.Height);
            this.contextMenu.Location = position;
            this.contextMenu.Show(this, position);
            this.contextMenu.Focus();
        }

        private bool IsMouseOver
        {
            get
            {
                Point mousePosition = Control.MousePosition;
                if (!base.Bounds.Contains(mousePosition))
                {
                    return this.contextMenu.Bounds.Contains(mousePosition);
                }
                return true;
            }
        }

        public PaymentType TypeMask
        {
            get
            {
                if (!base.Checked)
                {
                    return 0;
                }
                PaymentType.PaymentTypeFlags none = PaymentType.PaymentTypeFlags.None;
                foreach (ToolStripMenuItem item in this.contextMenu.Items)
                {
                    if (item.Checked)
                    {
                        none |= (PaymentType.PaymentTypeFlags) item.Tag;
                    }
                }
                return none;
            }
            set
            {
                base.CheckState = CheckState.Unchecked;
                foreach (ToolStripMenuItem item in this.contextMenu.Items)
                {
                    PaymentType.PaymentTypeFlags tag = (PaymentType.PaymentTypeFlags) item.Tag;
                    if ((value & ((PaymentType) tag)) == ((PaymentType) tag))
                    {
                        item.Checked = true;
                        base.CheckState = CheckState.Checked;
                    }
                }
            }
        }
    }
}

