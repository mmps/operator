﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class AssignPlanForm : Form
    {
        private Button _enterButton;
        private Button _escapeButton;
        private DateTimePicker _startTime;
        private ComboBox _tariffPlanList;
        private IContainer components;
        private ErrorProvider errorProvider1;

        public AssignPlanForm(ClientTariffPlan[] plans)
        {
            this.InitializeComponent();
            this._startTime.MinDate = DateTime.Today;
            this._tariffPlanList.Items.AddRange(plans);
            this._tariffPlanList.SelectedIndex = 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(AssignPlanForm));
            this._startTime = new DateTimePicker();
            this._tariffPlanList = new ComboBox();
            this._enterButton = new Button();
            this._escapeButton = new Button();
            this.errorProvider1 = new ErrorProvider(this.components);
            Label label = new Label();
            Label label2 = new Label();
            ((ISupportInitialize) this.errorProvider1).BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "label1");
            label.Name = "label1";
            manager.ApplyResources(label2, "label2");
            label2.Name = "label2";
            manager.ApplyResources(this._startTime, "_startTime");
            this._startTime.Format = DateTimePickerFormat.Custom;
            this._startTime.Name = "_startTime";
            this._startTime.Value = new DateTime(0x7d9, 2, 5, 0, 0, 0, 0);
            manager.ApplyResources(this._tariffPlanList, "_tariffPlanList");
            this._tariffPlanList.DropDownStyle = ComboBoxStyle.DropDownList;
            this._tariffPlanList.FormattingEnabled = true;
            this._tariffPlanList.Name = "_tariffPlanList";
            manager.ApplyResources(this._enterButton, "_enterButton");
            this._enterButton.DialogResult = DialogResult.OK;
            this._enterButton.Name = "_enterButton";
            this._enterButton.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._escapeButton, "_escapeButton");
            this._escapeButton.DialogResult = DialogResult.Cancel;
            this._escapeButton.Name = "_escapeButton";
            this._escapeButton.UseVisualStyleBackColor = true;
            this.errorProvider1.ContainerControl = this;
            base.AcceptButton = this._enterButton;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._escapeButton;
            base.Controls.Add(this._escapeButton);
            base.Controls.Add(this._enterButton);
            base.Controls.Add(label2);
            base.Controls.Add(this._tariffPlanList);
            base.Controls.Add(label);
            base.Controls.Add(this._startTime);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "AssignPlanForm";
            base.ShowIcon = false;
            ((ISupportInitialize) this.errorProvider1).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public ClientTariffPlan AssignedTariffPlan
        {
            get
            {
                return (ClientTariffPlan) this._tariffPlanList.SelectedItem;
            }
            set
            {
                if (value != null)
                {
                    this._tariffPlanList.SelectedItem = value;
                }
            }
        }

        public DateTime AssignedTime
        {
            get
            {
                return this._startTime.Value;
            }
        }
    }
}

