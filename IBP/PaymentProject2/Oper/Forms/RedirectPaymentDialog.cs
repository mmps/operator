﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class RedirectPaymentDialog : Form
    {
        private Button _buttonCancel;
        private Button _buttonOk;
        private Label _labelAccount;
        private Label _labelId;
        private Label _labelService;
        private Label _labelStatus;
        private Label _labelTotal;
        private TextBox _newAccount;
        private ComboBox _newService;
        private Payment _payment;
        private TextBox _reason;
        private IContainer components;
        private Label labelHeadStep2;
        private Panel panel1;

        public RedirectPaymentDialog(Payment payment, AvailableServiceGuide serviceGuide)
        {
            this.InitializeComponent();
            this._payment = payment;
            foreach (Guid guid in (IEnumerable<Guid>) serviceGuide)
            {
                this._newService.Items.Add(ServicesContainer.Instance[guid]);
            }
            if (this._newService.Items.Contains(payment.Service))
            {
                this._newService.SelectedItem = payment.Service;
            }
            this._newAccount.Text = this._payment.Account;
            this._labelId.Text = this._payment.Number;
            this._labelService.Text = this._payment.Service.Name;
            this._labelAccount.Text = this._payment.Account;
            this._labelTotal.Text = this._payment.Total.ToString();
            this._labelStatus.Text = new EnumTypeInfo<PaymentState>(this._payment.FullState).ToString();
            this._reason.Text = UserStrings.ClientCall + DateTime.Now.ToString("HH:mm dd/MM/yyyy");
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RedirectPaymentDialog));
            this._labelAccount = new Label();
            this._labelStatus = new Label();
            this._labelTotal = new Label();
            this._labelService = new Label();
            this._labelId = new Label();
            this._newService = new ComboBox();
            this._newAccount = new TextBox();
            this._buttonCancel = new Button();
            this._buttonOk = new Button();
            this.panel1 = new Panel();
            this.labelHeadStep2 = new Label();
            this._reason = new TextBox();
            Label label = new Label();
            Label label2 = new Label();
            GroupBox box = new GroupBox();
            Label label3 = new Label();
            Label label4 = new Label();
            Label label5 = new Label();
            Label label6 = new Label();
            Label label7 = new Label();
            Label label8 = new Label();
            box.SuspendLayout();
            this.panel1.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(label, "labelServiceZag");
            label.Name = "labelServiceZag";
            manager.ApplyResources(label2, "labelAccountEdit");
            label2.Name = "labelAccountEdit";
            box.Controls.Add(this._labelAccount);
            box.Controls.Add(label3);
            box.Controls.Add(this._labelStatus);
            box.Controls.Add(this._labelTotal);
            box.Controls.Add(this._labelService);
            box.Controls.Add(this._labelId);
            box.Controls.Add(label4);
            box.Controls.Add(label5);
            box.Controls.Add(label6);
            box.Controls.Add(label7);
            manager.ApplyResources(box, "groupBox1");
            box.Name = "groupBox1";
            box.TabStop = false;
            manager.ApplyResources(this._labelAccount, "_labelAccount");
            this._labelAccount.BorderStyle = BorderStyle.Fixed3D;
            this._labelAccount.Name = "_labelAccount";
            manager.ApplyResources(label3, "label5");
            label3.Name = "label5";
            manager.ApplyResources(this._labelStatus, "_labelStatus");
            this._labelStatus.BorderStyle = BorderStyle.Fixed3D;
            this._labelStatus.ForeColor = Color.Red;
            this._labelStatus.Name = "_labelStatus";
            manager.ApplyResources(this._labelTotal, "_labelTotal");
            this._labelTotal.BorderStyle = BorderStyle.Fixed3D;
            this._labelTotal.Name = "_labelTotal";
            manager.ApplyResources(this._labelService, "_labelService");
            this._labelService.BorderStyle = BorderStyle.Fixed3D;
            this._labelService.Name = "_labelService";
            manager.ApplyResources(this._labelId, "_labelId");
            this._labelId.BorderStyle = BorderStyle.Fixed3D;
            this._labelId.Name = "_labelId";
            manager.ApplyResources(label4, "label3");
            label4.Name = "label3";
            manager.ApplyResources(label5, "label2");
            label5.Name = "label2";
            manager.ApplyResources(label6, "label1");
            label6.Name = "label1";
            manager.ApplyResources(label7, "labelIdZag");
            label7.Name = "labelIdZag";
            manager.ApplyResources(label8, "label4");
            label8.Name = "label4";
            manager.ApplyResources(this._newService, "_newService");
            this._newService.DropDownStyle = ComboBoxStyle.DropDownList;
            this._newService.FormatString = "{Name}";
            this._newService.FormattingEnabled = true;
            this._newService.Name = "_newService";
            this._newService.Sorted = true;
            manager.ApplyResources(this._newAccount, "_newAccount");
            this._newAccount.Name = "_newAccount";
            manager.ApplyResources(this._buttonCancel, "_buttonCancel");
            this._buttonCancel.DialogResult = DialogResult.Cancel;
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._buttonOk, "_buttonOk");
            this._buttonOk.Name = "_buttonOk";
            this._buttonOk.UseVisualStyleBackColor = true;
            this._buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.panel1.BackColor = Color.White;
            this.panel1.Controls.Add(this.labelHeadStep2);
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this.labelHeadStep2, "labelHeadStep2");
            this.labelHeadStep2.Name = "labelHeadStep2";
            manager.ApplyResources(this._reason, "_reason");
            this._reason.Name = "_reason";
            base.AcceptButton = this._buttonOk;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._buttonCancel;
            base.Controls.Add(box);
            base.Controls.Add(label8);
            base.Controls.Add(this._reason);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this._buttonOk);
            base.Controls.Add(this._buttonCancel);
            base.Controls.Add(label2);
            base.Controls.Add(this._newAccount);
            base.Controls.Add(label);
            base.Controls.Add(this._newService);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "RedirectPaymentDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            box.ResumeLayout(false);
            box.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string RedirectReason
        {
            get
            {
                return this._reason.Text.Replace(Environment.NewLine, " ");
            }
        }

        public string SelectedAccount
        {
            get
            {
                return this._newAccount.Text;
            }
        }

        public Service SelectedService
        {
            get
            {
                Service selectedItem = this._newService.SelectedItem as Service;
                if (selectedItem == null)
                {
                    throw new SrvException(UserStrings.ServiceNotChoosen02);
                }
                return selectedItem;
            }
        }
    }
}

