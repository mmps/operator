﻿using System.Data;

namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Files;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Settings;
    using Microsoft.Reporting.WinForms;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Windows.Forms;

    internal class ReportForm : Form
    {
        private IDataSetConvertible _dataSetObject;
        private Template _template;
        private IContainer components;
        private ReportViewer reportViewer1;

        public ReportForm(IDataSetConvertible dataSetObject, Template template)
        {
            this._dataSetObject = dataSetObject;
            this._template = template;
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ReportForm));
            this.reportViewer1 = new ReportViewer();
            base.SuspendLayout();
            manager.ApplyResources(this.reportViewer1, "reportViewer1");
            this.reportViewer1.Name = "reportViewer1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.reportViewer1);
            base.Name = "ReportForm";
            base.Load += new EventHandler(this.ReportForm_Load);
            base.ResumeLayout(false);
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            try
            {
                using (MemoryStream stream = this._template.GetMemoryStream())
                {
                    this.reportViewer1.LocalReport.LoadReportDefinition(stream);
                    foreach (KeyValuePair<string, DataTable> pair in this._dataSetObject.ToNamedTables())
                    {
                        ReportDataSource item = new ReportDataSource(pair.Key, pair.Value);
                        this.reportViewer1.LocalReport.DataSources.Add(item);
                    }
                    this.reportViewer1.LocalReport.DisplayName = this._dataSetObject.DisplayName;
                    this.reportViewer1.Messages = new ReportMessages();
                    this.reportViewer1.RefreshReport();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(string.Format(UserStrings.ShowDocumentError, Environment.NewLine, exception.ToString()), UserStrings.Error, MessageBoxButtons.OK);
            }
        }
    }
}

