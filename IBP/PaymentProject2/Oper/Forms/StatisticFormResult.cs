﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using IBP.PaymentProject2.Oper.Statistic.Commands;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Windows.Forms;

    [BindType(typeof(StatisticDataItem)), FilterForm(TypeSearch.Statistic)]
    internal class StatisticFormResult : ResultForm
    {
        private StatisticCommand command;
        private IContainer components;
        private PrintPreviewDialog dlgPrintPreview = new PrintPreviewDialog();
        private static StatisticExportCommand exportCmd = new StatisticExportCommand();
        private ToolStripMenuItem outxls;
        private PageSettings pageSett = new PageSettings();
        private int prevcolum = 5;
        private ToolStripMenuItem printItem;
        private StatisticControl statisticControl1;
        private IBP.PaymentProject2.Oper.StatisticType stType;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;

        public StatisticFormResult()
        {
            this.InitializeComponent();
            this.tabPage2.AutoScroll = true;
            this.InitMenu();
            base.Controls.Remove(base.listViewItems);
            this.tabPage1.Controls.Add(base.listViewItems);
        }

        private void _doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            base.SuspendLayout();
            Size size = this.statisticControl1.Size;
            this.statisticControl1.Width = this.pageSett.Landscape ? ((int) this.pageSett.PrintableArea.Height) : ((int) this.pageSett.PrintableArea.Width);
            this.statisticControl1.DrawDiagram(e.Graphics);
            this.statisticControl1.Size = size;
            base.ResumeLayout();
            base.Parent.Refresh();
            Application.DoEvents();
        }

        private void CreateGrafik()
        {
            if ((this.command != null) && (this.command.StaticticGrafix != null))
            {
                try
                {
                    this.command.StaticticGrafix.MaxItem.Total.ToString();
                }
                catch
                {
                    return;
                }
                if (this.command.StaticticGrafix.Items != null)
                {
                    this.statisticControl1.Statictic = this.command.StaticticGrafix;
                    this.statisticControl1.StartPoint = new Point(10, 60);
                    this.statisticControl1.ShowStatistic();
                }
            }
        }

        private void CreateTable()
        {
            exportCmd.SetTableName(this.command.StatisticData.Text);
            base.listViewItems.Items.Clear();
            if (((this.command != null) && (this.command.StatisticData != null)) && (this.command.StatisticData.Items != null))
            {
                base.listViewItems.AddRange(this.command.StatisticData.Items.ToArray());
            }
            this.Refresh();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StatisticFormResult));
            this.tabControl1 = new TabControl();
            this.tabPage1 = new TabPage();
            this.tabPage2 = new TabPage();
            this.statisticControl1 = new StatisticControl();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            manager.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            manager.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage2.Controls.Add(this.statisticControl1);
            manager.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.statisticControl1, "statisticControl1");
            this.statisticControl1.BackColor = Color.White;
            this.statisticControl1.Name = "statisticControl1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = Color.White;
            base.Controls.Add(this.tabControl1);
            base.Name = "StatisticFormResult";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.Controls.SetChildIndex(this.tabControl1, 0);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void InitMenu()
        {
            this.printItem = new ToolStripMenuItem(UserStrings.Print, null, new EventHandler(this.PrintStatistic));
            this.outxls = new ToolStripMenuItem(UserStrings.UploadTable, null, exportCmd.EventDelegate);
            base.mainMenuItem.Text = UserStrings.Statistics;
            base.mainMenuItem.DropDown.Items.Add(this.printItem);
            base.mainMenuItem.DropDown.Items.Add(this.outxls);
        }

        protected override void OnActivated(EventArgs e)
        {
            exportCmd.Tag = base.listViewItems;
            if (this.command != null)
            {
                exportCmd.SetTableName(this.command.StatisticData.Text);
            }
            base.OnActivated(e);
        }

        private void OnColumnClick(object sender, ColumnClickEventArgs e)
        {
            int column = e.Column;
            if ((base.listViewItems.SortedColumn.TextName == UserStrings.Mark) && (this.prevcolum < base.listViewItems.Columns.Count))
            {
                column = this.prevcolum;
            }
            string text = base.listViewItems.Columns[column].Text;
            StaticticGrafix grafix = new StaticticGrafix();
            grafix.StatisticLabel = this.command.StaticticGrafix.StatisticLabel;
            grafix.FilterLabel = this.command.StaticticGrafix.FilterLabel;
            grafix.YLabel = this.command.StaticticGrafix.YLabel;
            grafix.XLabel = text;
            foreach (ListViewItem item in base.listViewItems.Items)
            {
                decimal total = 0M;
                object tag = item.SubItems[column].Tag;
                if (tag is IConvertible)
                {
                    total = Convert.ToDecimal(tag, base.listViewItems.FormatProvider);
                }
                grafix.Items.Add(new StatisticItemGrafix(item.Text, total));
            }
            if (grafix.MaxItem.Total > 0M)
            {
                this.statisticControl1.Statictic = grafix;
                this.statisticControl1.StartPoint = new Point(10, 60);
                this.statisticControl1.ShowStatistic();
                this.prevcolum = column;
            }
            else
            {
                MessageBox.Show(UserStrings.Graphic);
            }
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            this.stType = filter.StatisticType;
            this.command = StatisticCommand.GetCommand(this.stType, (PaymentsFilter) filter.Filter);
            try
            {
                base.SuspendLayout();
                base.UseWaitCursor = true;
                this.CreateTable();
                this.CreateGrafik();
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                base.ResumeLayout();
                base.UseWaitCursor = false;
            }
        }

        private void PrintStatistic(object sender, EventArgs e)
        {
            if (this.statisticControl1.Exist)
            {
                this.pageSett = new PageSettings();
                PageSetupDialog dialog = new PageSetupDialog();
                dialog.PageSettings = this.pageSett;
                Application.DoEvents();
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    MessageBox.Show(UserStrings.PrintFailed);
                }
                else
                {
                    base.Parent.Refresh();
                    Application.DoEvents();
                    PrintDocument document = new PrintDocument();
                    document.DefaultPageSettings = this.pageSett;
                    document.PrintPage += new PrintPageEventHandler(this._doc_PrintPage);
                    this.dlgPrintPreview.Document = document;
                    base.Parent.Refresh();
                    Application.DoEvents();
                    this.dlgPrintPreview.ShowDialog();
                }
            }
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return StatisticSettings.Default.Helper;
            }
        }
    }
}

