﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    internal class ProgressBarDialog : Form
    {
        private Label _label;
        private ProgressBar _progressBar;
        private IContainer components;

        public ProgressBarDialog(string caption, string stepName, int stepCount)
        {
            this.InitializeComponent();
            this.Text = caption;
            this._label.Text = stepName;
            this._progressBar.Maximum = stepCount;
            this._progressBar.Step = 1;
        }

        public void Close()
        {
            if (base.InvokeRequired)
            {
                ParameterlessDelegate method = new ParameterlessDelegate(this.Close);
                base.Invoke(method);
            }
            else
            {
                base.Close();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this._progressBar = new ProgressBar();
            this._label = new Label();
            base.SuspendLayout();
            this._progressBar.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this._progressBar.Location = new Point(12, 0x1b);
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new Size(0x17d, 0x17);
            this._progressBar.TabIndex = 0;
            this._label.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            this._label.AutoSize = true;
            this._label.Location = new Point(12, 11);
            this._label.Name = "_label";
            this._label.Size = new Size(0x23, 13);
            this._label.TabIndex = 1;
            this._label.Text = "label1";
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x195, 0x3e);
            base.ControlBox = false;
            base.Controls.Add(this._label);
            base.Controls.Add(this._progressBar);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ProgressBarDialog";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterParent;
            this.Text = "ProgressBarDialog";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void PerformStep()
        {
            if (base.InvokeRequired)
            {
                ParameterlessDelegate method = new ParameterlessDelegate(this.PerformStep);
                base.Invoke(method);
            }
            else
            {
                this._progressBar.PerformStep();
            }
        }

        public void SetStepName(string stepName)
        {
            if (base.InvokeRequired)
            {
                SetStepNameDelegate method = new SetStepNameDelegate(this.SetStepName);
                base.Invoke(method, new object[] { stepName });
            }
            else
            {
                this._label.Text = stepName;
            }
        }

        private delegate void ParameterlessDelegate();

        private delegate void SetStepNameDelegate(string stepName);
    }
}

