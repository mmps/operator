﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    internal class StepDialog : Form
    {
        private Button buttonBack;
        private Button buttonCancel;
        private Button buttonNext;
        private IContainer components;
        private StepControl currentStep;
        private ToolTip errorToolTip;
        private Panel panel1;
        private ProgressBar progressBar1;
        private int progressBarMaximum;
        private int progressBarMinimum;
        private int progressBarStep;

        public StepDialog()
        {
            this.InitializeComponent();
            base.ControlAdded += new ControlEventHandler(this.StepDialog_ControlAdded);
            base.Activated += new EventHandler(this.StepDialog_Activated);
            this.errorToolTip = new ToolTip();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            try
            {
                this.currentStep = this.currentStep.MoveBack();
                this.SetControls();
            }
            catch (StepException exception)
            {
                this.ShowToolTip(exception);
            }
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            try
            {
                this.currentStep = this.currentStep.MoveNext();
                this.SetControls();
                base.DialogResult = (this.currentStep == null) ? DialogResult.OK : DialogResult.None;
            }
            catch (StepException exception)
            {
                this.ShowToolTip(exception);
            }
        }

        public void DisableControlButtons()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.DisableControlButtons));
            }
            else
            {
                base.ControlBox = this.buttonBack.Enabled = this.buttonCancel.Enabled = this.buttonNext.Enabled = false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void EnableControlButtons()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.EnableControlButtons));
            }
            else
            {
                base.ControlBox = this.buttonNext.Enabled = this.buttonCancel.Enabled = this.buttonBack.Enabled = true;
            }
        }

        public void HideProgressBar()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.HideProgressBar));
            }
            else
            {
                this.progressBar1.Visible = false;
                base.Enabled = true;
                base.Update();
            }
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StepDialog));
            this.buttonBack = new Button();
            this.buttonNext = new Button();
            this.buttonCancel = new Button();
            this.panel1 = new Panel();
            this.progressBar1 = new ProgressBar();
            Panel panel = new Panel();
            panel.SuspendLayout();
            base.SuspendLayout();
            panel.Controls.Add(this.buttonBack);
            panel.Controls.Add(this.buttonNext);
            panel.Controls.Add(this.buttonCancel);
            manager.ApplyResources(panel, "panelButton");
            panel.Name = "panelButton";
            manager.ApplyResources(this.buttonBack, "buttonBack");
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new EventHandler(this.buttonBack_Click);
            manager.ApplyResources(this.buttonNext, "buttonNext");
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new EventHandler(this.buttonNext_Click);
            manager.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.DialogResult = DialogResult.Cancel;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Name = "progressBar1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.progressBar1);
            base.Controls.Add(this.panel1);
            base.Controls.Add(panel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "StepDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            panel.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void InternalShowProgressBar(int[] parameters)
        {
            this.progressBar1.Minimum = parameters[0];
            this.progressBar1.Maximum = parameters[1];
            this.progressBar1.Step = parameters[2];
            this.progressBar1.Value = 0;
            this.progressBar1.Visible = true;
        }

        public void PerformStep()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.PerformStep));
            }
            else
            {
                this.progressBar1.PerformStep();
            }
        }

        private void SetControls()
        {
            if (this.currentStep != null)
            {
                this.buttonBack.Enabled = this.currentStep.CanMoveBack;
                this.buttonNext.Text = this.currentStep.CanMoveNext ? UserStrings.Further : UserStrings.Ready;
            }
        }

        public void ShowProgressBar()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.ShowProgressBar));
            }
            else
            {
                this.progressBar1.Minimum = this.progressBarMinimum;
                this.progressBar1.Maximum = this.progressBarMaximum;
                this.progressBar1.Step = this.progressBarStep;
                this.progressBar1.Value = 0;
                this.progressBar1.Visible = true;
            }
        }

        private void ShowToolTip(StepException e)
        {
            if (!e.SkipMessage)
            {
                this.errorToolTip.ToolTipIcon = ToolTipIcon.Warning;
                this.errorToolTip.Show(e.Message, base.ActiveControl, 0x7d0);
            }
        }

        private void StepDialog_Activated(object sender, EventArgs e)
        {
            if (this.currentStep != null)
            {
                this.currentStep.BringToFront();
                this.currentStep.Show();
            }
            this.SetControls();
        }

        private void StepDialog_ControlAdded(object sender, ControlEventArgs e)
        {
            StepControl control = e.Control as StepControl;
            if (control != null)
            {
                this.panel1.Controls.Add(control);
                control.Dock = DockStyle.Fill;
                control.Hide();
                control.Parent = this.panel1;
                if (this.currentStep != null)
                {
                    StepControl currentStep = this.currentStep;
                    while (currentStep.NextStep != null)
                    {
                        currentStep = currentStep.NextStep;
                    }
                    currentStep.NextStep = control;
                }
                else
                {
                    this.currentStep = control;
                }
                int num = (control.MinimumSize.Width > this.panel1.Size.Width) ? (control.MinimumSize.Width - this.panel1.Size.Width) : 0;
                int num2 = (control.MinimumSize.Height > this.panel1.Size.Height) ? (control.MinimumSize.Height - this.panel1.Size.Height) : 0;
                base.Size = new Size(base.Size.Width + num, base.Size.Height + num2);
            }
        }

        public int ProgressBarMaximum
        {
            get
            {
                return this.progressBarMaximum;
            }
            set
            {
                this.progressBarMaximum = value;
            }
        }

        public int ProgressBarMinimum
        {
            get
            {
                return this.progressBarMinimum;
            }
            set
            {
                this.progressBarMinimum = value;
            }
        }

        public int ProgressBarStep
        {
            get
            {
                return this.progressBarStep;
            }
            set
            {
                this.progressBarStep = value;
            }
        }
    }
}

