﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class SelectMonthForm : Form
    {
        private Button _cancelButton;
        private DateTimePicker _dtPicker;
        private Button _okButton;
        private IContainer components;

        public SelectMonthForm()
        {
            this.InitializeComponent();
            DateTime today = DateTime.Today;
            today = today.AddDays((double) (1 - today.Day));
            this._dtPicker.MaxDate = today;
            this._dtPicker.Value = today;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(SelectMonthForm));
            this._dtPicker = new DateTimePicker();
            this._cancelButton = new Button();
            this._okButton = new Button();
            base.SuspendLayout();
            manager.ApplyResources(this._dtPicker, "_dtPicker");
            this._dtPicker.Format = DateTimePickerFormat.Custom;
            this._dtPicker.Name = "_dtPicker";
            this._dtPicker.ShowUpDown = true;
            this._cancelButton.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this._cancelButton, "_cancelButton");
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.UseVisualStyleBackColor = true;
            this._okButton.DialogResult = DialogResult.OK;
            manager.ApplyResources(this._okButton, "_okButton");
            this._okButton.Name = "_okButton";
            this._okButton.UseVisualStyleBackColor = true;
            base.AcceptButton = this._okButton;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._cancelButton;
            base.Controls.Add(this._okButton);
            base.Controls.Add(this._cancelButton);
            base.Controls.Add(this._dtPicker);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "SelectMonthForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.ResumeLayout(false);
        }

        public DateTime Date
        {
            get
            {
                return this._dtPicker.Value;
            }
        }
    }
}

