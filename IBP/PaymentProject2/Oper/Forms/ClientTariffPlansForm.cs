﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterForm(TypeSearch.ClientTariffPlan), BindType(typeof(ClientTariffPlan))]
    internal class ClientTariffPlansForm : ResultForm
    {
        private ClientTariffPlanFilterOper _filter;
        private IContainer components;

        public ClientTariffPlansForm()
        {
            EventHandler handler = null;
            EventHandler handler2 = null;
            this.InitializeComponent();
            base.mainMenuItem.Text = UserStrings.TariffPlans;
            EditClientTariffPlan cmd = new EditClientTariffPlan();
            cmd.TariffPlanChanged += new EventHandler<TariffPlanEventArgs>(this.OnTariffPlanChanged);
            base.CreateMainMenuCommand(cmd, UserStrings.Review02);
            this.AddCreateCommand(false, UserStrings.Add);
            this.AddCreateCommand(true, UserStrings.AddIfExist);
            ReplaceClientTariffPlanCommand command = new ReplaceClientTariffPlanCommand();
            if (handler == null)
            {
                handler = delegate {
                    this.UpdateForm();
                };
            }
            command.Completed += handler;
            base.CreateMenuCommand(command, UserStrings.ChangeTariffPlan02);
            DeleteClientTariffPlanCommand command2 = new DeleteClientTariffPlanCommand();
            if (handler2 == null)
            {
                handler2 = delegate {
                    this.UpdateForm();
                };
            }
            command2.Completed += handler2;
            base.CreateMenuCommand(command2, UserStrings.DelTariffPlan);
        }

        private void AddCreateCommand(bool createCopy, string commandText)
        {
            CreateNewClientTariffPlan cmd = new CreateNewClientTariffPlan(createCopy);
            cmd.TariffPlanChanged += new EventHandler<TariffPlanEventArgs>(this.OnTariffPlanChanged);
            cmd.TariffPlanCreated += new EventHandler<TariffPlanEventArgs>(this.OnTariffPlanCreated);
            base.CreateMenuCommand(cmd, commandText);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientTariffPlansForm));
            base.SuspendLayout();
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "ClientTariffPlansForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            this._filter = (ClientTariffPlanFilterOper) filter;
            this.UpdateForm();
        }

        private void OnTariffPlanChanged(object sender, TariffPlanEventArgs e)
        {
            if (!base.IsDisposed)
            {
                TariffPlanComparer comparer = new TariffPlanComparer();
                base.listViewItems.RefreshItem(e.TariffPlan, comparer);
            }
        }

        private void OnTariffPlanCreated(object sender, TariffPlanEventArgs e)
        {
            if (!base.IsDisposed)
            {
                base.listViewItems.Add(e.TariffPlan);
            }
        }

        private void UpdateForm()
        {
            try
            {
                base.listViewItems.SuspendLayout();
                List<ClientTariffPlan> clientTariffPlans = ServerFasad.Server.GetClientTariffPlans(this._filter.PlanFilter);
                base.listViewItems.Items.Clear();
                base.listViewItems.AddRange(clientTariffPlans.ToArray());
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }

        private class TariffPlanComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                ClientTariffPlan plan = (ClientTariffPlan) x;
                ClientTariffPlan plan2 = (ClientTariffPlan) y;
                return plan.Id.CompareTo(plan2.Id);
            }
        }
    }
}

