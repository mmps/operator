﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    internal class TableCell : TablePanel
    {
        private Color _currentSelectionColor;
        private bool _selected;
        private Color _selectionBorderColor;
        private Color _selectionColor;
        private double _selectionOpacity;
        private TableMasterCell masterCell;

        public TableCell(TableMasterCell cell, Point position, Size size) : base(cell.Parent, position, size)
        {
            this._currentSelectionColor = Color.SkyBlue;
            this._selectionColor = Color.SkyBlue;
            this._selectionBorderColor = Color.Gold;
            this._selectionOpacity = 1.0;
            this.masterCell = cell;
        }

        protected TableCell(Control owner, Point position, Size size) : base(owner, position, size)
        {
            this._currentSelectionColor = Color.SkyBlue;
            this._selectionColor = Color.SkyBlue;
            this._selectionBorderColor = Color.Gold;
            this._selectionOpacity = 1.0;
        }

        private int interPolate(int ground, int top, double factor)
        {
            return (ground + ((int) Math.Floor((double) (((top - ground) * factor) - 0.5))));
        }

        protected override void PaintSelection(Graphics g)
        {
            if (this._selected)
            {
                int height = base.Height;
                int width = base.Width;
                Rectangle rect = new Rectangle(6, 6, width - 12, height - 12);
                using (Brush brush = new SolidBrush(this._currentSelectionColor))
                {
                    g.FillRectangle(brush, rect);
                }
                using (Pen pen = new Pen(Color.Gold))
                {
                    g.DrawRectangle(pen, rect);
                }
            }
        }

        public TableMasterCell MasterCell
        {
            get
            {
                return this.masterCell;
            }
        }

        public bool Selected
        {
            get
            {
                return this._selected;
            }
            set
            {
                if (this._selected != value)
                {
                    this._selected = value;
                    base._needDraw = true;
                    base.Invalidate();
                }
            }
        }

        public Color SelectionColor
        {
            get
            {
                return this._selectionColor;
            }
            set
            {
                if (this._selectionColor != value)
                {
                    this._selectionColor = value;
                    if (this._selected)
                    {
                        base._needDraw = true;
                        base.Invalidate();
                    }
                }
            }
        }

        public double SelectionOpacity
        {
            get
            {
                return this._selectionOpacity;
            }
            set
            {
                if (this._selectionOpacity != value)
                {
                    this._selectionOpacity = value;
                    this._currentSelectionColor = Color.FromArgb(this.interPolate(this._backGroundColor.R, this._selectionColor.R, this._selectionOpacity), this.interPolate(this._backGroundColor.G, this._selectionColor.G, this._selectionOpacity), this.interPolate(this._backGroundColor.B, this._selectionColor.B, this._selectionOpacity));
                    if (this._selected)
                    {
                        base._needDraw = true;
                        base.Invalidate();
                    }
                }
            }
        }
    }
}

