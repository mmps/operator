﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using System;

    internal class StepInfo
    {
        protected IBP.PaymentProject2.Common.Recipient[] allowedRecipients;
        protected DateTime end;
        private PayPoint point;
        private IBP.PaymentProject2.Common.Recipient recipient;
        protected DateTime start;
        private IBP.PaymentProject2.Common.StatisticInfo statisticInfo;
        private IBP.PaymentProject2.Common.Transactions.Transaction transaction;

        public StepInfo()
        {
            this.start = this.end = DateTime.Now.AddDays(-1.0);
            this.allowedRecipients = RecipientsContainer.Instance.ToArray();
        }

        public StepInfo(IBP.PaymentProject2.Common.Recipient[] allowedRecipients)
        {
            this.start = this.end = DateTime.Now.AddDays(-1.0);
            this.allowedRecipients = allowedRecipients;
        }

        public IBP.PaymentProject2.Common.Recipient[] AllowedRecipients
        {
            get
            {
                return this.allowedRecipients;
            }
        }

        public DateTime End
        {
            get
            {
                return this.end;
            }
            set
            {
                this.end = value;
            }
        }

        public virtual FilterBase Filter
        {
            get
            {
                return PaymentsFilter.Untransfered;
            }
        }

        public PayPoint Point
        {
            get
            {
                return this.point;
            }
            set
            {
                this.point = value;
            }
        }

        public IBP.PaymentProject2.Common.Recipient Recipient
        {
            get
            {
                return this.recipient;
            }
            set
            {
                this.recipient = value;
            }
        }

        public DateTime Start
        {
            get
            {
                return this.start;
            }
            set
            {
                this.start = value;
            }
        }

        public IBP.PaymentProject2.Common.StatisticInfo StatisticInfo
        {
            get
            {
                return this.statisticInfo;
            }
            set
            {
                this.statisticInfo = value;
            }
        }

        public IBP.PaymentProject2.Common.Transactions.Transaction Transaction
        {
            get
            {
                return this.transaction;
            }
            set
            {
                this.transaction = value;
            }
        }

        public virtual decimal Value
        {
            get
            {
                return this.statisticInfo.SumTotal;
            }
        }
    }
}

