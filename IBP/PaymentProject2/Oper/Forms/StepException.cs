﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class StepException : Exception
    {
        public readonly bool SkipMessage;

        public StepException()
        {
        }

        public StepException(string message) : this(message, false)
        {
        }

        protected StepException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public StepException(string message, bool skipMessage) : base(message)
        {
            this.SkipMessage = skipMessage;
        }

        public StepException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}

