﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple=true, Inherited=false)]
    internal sealed class NoFilterAtribute : Attribute
    {
    }
}

