﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class HelpForm : Form
    {
        private WebBrowser _webBrowser;
        private FormWindowState _windowState;
        private IContainer components;

        public HelpForm(string helpFile)
        {
            this.InitializeComponent();
            this._webBrowser.Navigate(new Uri(helpFile));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void HelpForm_Layout(object sender, LayoutEventArgs e)
        {
            if (FormWindowState.Minimized != base.WindowState)
            {
                this._windowState = base.WindowState;
            }
        }

        private void InitializeComponent()
        {
            this._webBrowser = new WebBrowser();
            base.SuspendLayout();
            this._webBrowser.Dock = DockStyle.Fill;
            this._webBrowser.Location = new Point(0, 0);
            this._webBrowser.MinimumSize = new Size(20, 20);
            this._webBrowser.Name = "_webBrowser";
            this._webBrowser.Size = new Size(0x3f6, 0x32e);
            this._webBrowser.TabIndex = 0;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x3f6, 0x32e);
            base.Controls.Add(this._webBrowser);
            base.Name = "HelpForm";
            base.ShowIcon = false;
            this.Text = "Помощь";
            base.Layout += new LayoutEventHandler(this.HelpForm_Layout);
            base.ResumeLayout(false);
        }

        public FormWindowState WinState
        {
            get
            {
                return this._windowState;
            }
        }
    }
}

