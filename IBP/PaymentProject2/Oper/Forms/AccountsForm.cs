﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [BindType(typeof(Account)), FilterForm(TypeSearch.Accounts)]
    internal class AccountsForm : ResultForm
    {
        private AccountFilter _filter;
        private IContainer components;

        public AccountsForm()
        {
            this.InitializeComponent();
            this.InitMenu();
            base.mainMenuItem.Text = UserStrings.Accounts;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillListView(bool updateBalance)
        {
            List<Account> list = new List<Account>();
            base.listViewItems.Items.Clear();
            string str = this._filter.Part.ToLowerInvariant();
            foreach (Account account in AccountsContainer.Instance.Collection)
            {
                if ((account.Name.ToLowerInvariant().Contains(str) && ((this._filter.TownId == 0) || (account.TownId == this._filter.TownId))) && (!this._filter.HideAccountsWithoutBalance || !account.IsExternal))
                {
                    list.Add(account);
                }
            }
            if (updateBalance)
            {
                bool flag = true;
                if (list.Count > 5)
                {
                    DialogResult result = MessageBox.Show(string.Format(UserStrings.Filtered, list.Count), UserStrings.Request, MessageBoxButtons.YesNo);
                    flag = DialogResult.Yes == result;
                }
                if (flag)
                {
                    foreach (Account account2 in list)
                    {
                        if (!account2.IsExternal)
                        {
                            try
                            {
                                account2.Balance = IBP.PaymentProject2.Oper.ServerFasad.Server.GetCurrentBalance(account2.Id);
                            }
                            catch (SrvException exception)
                            {
                                MessageBox.Show(string.Format(UserStrings.CantGetSaldo, Environment.NewLine, account2.Name, exception.Message));
                            }
                        }
                    }
                }
            }
            base.listViewItems.AddRange(list.ToArray());
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(AccountsForm));
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "AccountsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void InitMenu()
        {
            AccountCommand cmd = new AccountCommand(true);
            cmd.Update += new EventHandler(this.Update);
            base.CreateMenuCommand(cmd, UserStrings.Add);
            AccountCommand command2 = new AccountCommand(false);
            command2.Update += new EventHandler(this.Update);
            base.CreateMainMenuCommand(command2, UserStrings.Change);
            base.CreateMenuCommand(new RecountBalanceCommand(), UserStrings.RecountSaldo);
            base.CreateMenuCommand(new ShowAccountTransactions(), UserStrings.ShowTransByAcc);
            base.CreateMenuCommand(new ShowAccountBalance(), UserStrings.ShowHistorySaldo);
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            AccountsContainer.Instance.Clear();
            try
            {
                AccountsContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetAccounts());
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }
            this._filter = (AccountFilter) filter.Filter;
            this.FillListView(true);
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            Account tag = (Account) e.Tag;
            if (tag.IsExternal)
            {
                e.Item.BackColor = AccountsSettings.Default.ExternalColor;
            }
            else if (tag.IsCashAccount)
            {
                e.Item.BackColor = AccountsSettings.Default.CashColor;
            }
        }

        private void Update(object sender, EventArgs e)
        {
            this.FillListView(false);
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return AccountsSettings.Default.Helper;
            }
        }
    }
}

