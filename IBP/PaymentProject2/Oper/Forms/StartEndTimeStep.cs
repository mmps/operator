﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class StartEndTimeStep : StepControl
    {
        private IContainer components;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private StepInfo info;

        public StartEndTimeStep(StepInfo info)
        {
            EventHandler handler = null;
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.info = info;
            this.InitializeComponent();
            this.dateTimeStart.Value = this.info.Start;
            this.dateTimeEnd.Value = this.info.End;
            if (handler == null)
            {
                handler = delegate (object sender, EventArgs e) {
                    this.info.End = this.dateTimeEnd.Value;
                    this.info.Start = this.dateTimeStart.Value;
                };
            }
            base.BeforeLeaveStepForward += handler;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StartEndTimeStep));
            this.dateTimeEnd = new DateTimePicker();
            this.dateTimeStart = new DateTimePicker();
            Label label = new Label();
            Label label2 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "label1");
            label.Name = "label1";
            manager.ApplyResources(label2, "label2");
            label2.Name = "label2";
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Name = "dateTimeEnd";
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Name = "dateTimeStart";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(label);
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(label2);
            base.Controls.Add(this.dateTimeStart);
            base.Name = "StartEndTimeStep";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string DateTimeCustomFormat
        {
            get
            {
                return this.dateTimeEnd.CustomFormat;
            }
            set
            {
                this.dateTimeEnd.CustomFormat = value;
                this.dateTimeStart.CustomFormat = value;
            }
        }

        public DateTimePickerFormat DateTimeFormat
        {
            get
            {
                return this.dateTimeEnd.Format;
            }
            set
            {
                this.dateTimeEnd.Format = value;
                this.dateTimeStart.Format = value;
            }
        }

        public bool EndTimeEnabled
        {
            get
            {
                return this.dateTimeEnd.Enabled;
            }
            set
            {
                this.dateTimeEnd.Enabled = value;
            }
        }
    }
}

