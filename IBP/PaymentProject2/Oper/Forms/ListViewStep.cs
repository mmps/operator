﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using System;
    using System.ComponentModel;
    using System.Threading;
    using System.Windows.Forms;

    public class ListViewStep : StepControl
    {
        private IContainer components;
        private ListViewEdit listView1;

        public event EventHandler<ItemEventArgs> ItemAdded
        {
            add
            {
                this.listView1.ItemAdded += value;
            }
            remove
            {
                this.listView1.ItemAdded -= value;
            }
        }

        public ListViewStep()
        {
            this.InitializeComponent();
            this.listView1.BringToFront();
            this.listView1.FormatProvider = Application.CurrentCulture;
        }

        public void AddItem(object obj)
        {
            this.SetObject(obj);
        }

        public void AddItems(object[] objects)
        {
            this.SetObject(objects);
        }

        public void Clear()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.Clear));
            }
            else
            {
                this.listView1.Items.Clear();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ListViewStep));
            this.listView1 = new ListViewEdit();
            base.SuspendLayout();
            this.listView1.AllowSort = true;
            this.listView1.BindType = null;
            this.listView1.ColumnsCollection = null;
            manager.ApplyResources(this.listView1, "listView1");
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Name = "listView1";
            this.listView1.ReadOnly = true;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = View.Details;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.listView1);
            base.Name = "ListViewStep";
            base.ResumeLayout(false);
        }

        public void ResizeColumns()
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ThreadStart(this.ResizeColumns));
            }
            else
            {
                foreach (ColumnHeader header in this.listView1.Columns)
                {
                    int width = 0;
                    header.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                    width = header.Width;
                    header.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                    width = (header.Width > width) ? header.Width : width;
                    header.Width = width;
                }
            }
        }

        private void SetObject(object param)
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new ParameterizedThreadStart(this.AddItem), new object[] { param });
            }
            else if (param is object[])
            {
                this.listView1.AddRange((object[]) param);
            }
            else
            {
                this.listView1.Add(param);
            }
        }

        public System.Type ItemObjectType
        {
            get
            {
                return this.listView1.BindType;
            }
            set
            {
                this.listView1.BindType = value;
            }
        }

        public object[] SelectedItems
        {
            get
            {
                object[] objArray = new object[this.listView1.CheckedItems.Count];
                for (int i = 0; i < this.listView1.CheckedItems.Count; i++)
                {
                    objArray[i] = this.listView1.CheckedItems[i].Tag;
                }
                return objArray;
            }
        }

        public bool UseCheckBoxes
        {
            get
            {
                return this.listView1.CheckBoxes;
            }
            set
            {
                this.listView1.CheckBoxes = value;
            }
        }
    }
}

