﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.CommissionLimitation;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms.Controls;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class CommissionLimitationForm : Form
    {
        private Button _buttonAdd;
        private Button _buttonClose;
        private Button _buttonDelete;
        private Button _buttonSave;
        private int _currentIndex;
        private Guid _guid;
        private LimitationControl _limitationControl;
        private ListBox _listBoxLimits;
        private List<LimitationSetting> _settingsList = new List<LimitationSetting>();
        private IContainer components;

        public CommissionLimitationForm(string recipientName, Guid guid, LimitationCollection loadCollection)
        {
            this.InitializeComponent();
            this.Text = string.Format(UserStrings.CommissionLimitation, recipientName);
            this._guid = guid;
            foreach (ILimitation limitation in (IEnumerable<ILimitation>) loadCollection)
            {
                LimitationSetting item = new LimitationSetting();
                item.Limitation = (Limitation) limitation;
                this._settingsList.Add(item);
            }
            this.FillListBox();
        }

        private void ButtonAddClick(object sender, EventArgs e)
        {
            this._listBoxLimits.Items.Add(UserStrings.NewLimitation);
            LimitationSetting setting = new LimitationSetting();
            NoLimitation limitation = new NoLimitation();
            limitation.CommissionType = CommissionType.Internal;
            setting.Limitation = limitation;
            setting.Limitation.AssignDate = DateTime.Now.AddDays(1.0);
            setting.Mark = TimeLabel.New;
            this._limitationControl.Setting = setting;
            int count = this._listBoxLimits.Items.Count;
            if (0 < count)
            {
                this._listBoxLimits.SelectedIndex = count - 1;
            }
            this._buttonAdd.Enabled = false;
            this._listBoxLimits.Enabled = false;
            this._buttonSave.Enabled = true;
            this._buttonDelete.Enabled = true;
        }

        private void ButtonDeleteClick(object sender, EventArgs e)
        {
            if (!this._listBoxLimits.Enabled)
            {
                int index = this._listBoxLimits.FindString(UserStrings.NewLimitation);
                if (index != -1)
                {
                    this._listBoxLimits.Items.RemoveAt(index);
                }
                this._buttonAdd.Enabled = true;
                this._listBoxLimits.Enabled = true;
            }
            else if ((this._settingsList.Count > this._currentIndex) && (this._currentIndex >= 0))
            {
                LimitationSetting setting = this._settingsList[this._currentIndex];
                if (this.Delete(setting.Limitation.Id))
                {
                    this._settingsList.RemoveAt(this._currentIndex);
                }
            }
            this.FillListBox();
        }

        private void ButtonSaveClick(object sender, EventArgs e)
        {
            LimitationSetting item = this._limitationControl.Setting;
            if (item != null)
            {
                if (this._listBoxLimits.Enabled)
                {
                    LimitationSetting setting2 = this._settingsList[this._currentIndex];
                    item.Limitation.Serial = setting2.Limitation.Serial;
                }
                int num = this.Save(item.Limitation);
                if (-1 != num)
                {
                    item.Limitation.Serial = num;
                    if (this._listBoxLimits.Enabled)
                    {
                        this._settingsList.RemoveAt(this._currentIndex);
                    }
                    this._settingsList.Add(item);
                    this._listBoxLimits.Enabled = true;
                    this._buttonAdd.Enabled = true;
                    this.FillListBox();
                }
            }
        }

        private bool Delete(int id)
        {
            try
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.DeleteLimitation(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return false;
            }
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillListBox()
        {
            this._listBoxLimits.Items.Clear();
            this.SetTimeLabel();
            foreach (LimitationSetting setting in this._settingsList)
            {
                if (TimeLabel.Current == setting.Mark)
                {
                    this._listBoxLimits.Items.Add(setting.Limitation.AssignDate.Date.ToShortDateString() + UserStrings.CurrentLimitation);
                }
                if ((TimeLabel.Future == setting.Mark) || (setting.Mark == TimeLabel.Old))
                {
                    this._listBoxLimits.Items.Add(setting.Limitation.AssignDate.Date.ToShortDateString());
                }
            }
            int count = this._listBoxLimits.Items.Count;
            if (0 < count)
            {
                this._listBoxLimits.SelectedIndex = count - 1;
            }
            else
            {
                this._limitationControl.Setting = null;
            }
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CommissionLimitationForm));
            LimitationSetting setting = new LimitationSetting();
            this._listBoxLimits = new ListBox();
            this._buttonAdd = new Button();
            this._buttonDelete = new Button();
            this._buttonClose = new Button();
            this._buttonSave = new Button();
            this._limitationControl = new LimitationControl();
            GroupBox box = new GroupBox();
            box.SuspendLayout();
            base.SuspendLayout();
            box.BackColor = SystemColors.Control;
            box.Controls.Add(this._listBoxLimits);
            box.Controls.Add(this._buttonAdd);
            box.Controls.Add(this._buttonDelete);
            manager.ApplyResources(box, "groupBox1");
            box.Name = "groupBox1";
            box.TabStop = false;
            this._listBoxLimits.FormattingEnabled = true;
            manager.ApplyResources(this._listBoxLimits, "_listBoxLimits");
            this._listBoxLimits.Name = "_listBoxLimits";
            this._listBoxLimits.SelectedIndexChanged += new EventHandler(this.ListBoxLimitsSelectedIndexChanged);
            manager.ApplyResources(this._buttonAdd, "_buttonAdd");
            this._buttonAdd.Name = "_buttonAdd";
            this._buttonAdd.UseVisualStyleBackColor = true;
            this._buttonAdd.Click += new EventHandler(this.ButtonAddClick);
            manager.ApplyResources(this._buttonDelete, "_buttonDelete");
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.UseVisualStyleBackColor = true;
            this._buttonDelete.Click += new EventHandler(this.ButtonDeleteClick);
            this._buttonClose.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this._buttonClose, "_buttonClose");
            this._buttonClose.Name = "_buttonClose";
            this._buttonClose.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._buttonSave, "_buttonSave");
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.UseVisualStyleBackColor = true;
            this._buttonSave.Click += new EventHandler(this.ButtonSaveClick);
            manager.ApplyResources(this._limitationControl, "_limitationControl");
            this._limitationControl.Name = "_limitationControl";
            setting.Mark = TimeLabel.Old;
            this._limitationControl.Setting = setting;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._limitationControl);
            base.Controls.Add(this._buttonClose);
            base.Controls.Add(this._buttonSave);
            base.Controls.Add(box);
            this.DoubleBuffered = true;
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "CommissionLimitationForm";
            box.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void ListBoxLimitsSelectedIndexChanged(object sender, EventArgs e)
        {
            this._currentIndex = this._listBoxLimits.SelectedIndex;
            if ((this._settingsList.Count > this._currentIndex) && (this._currentIndex >= 0))
            {
                LimitationSetting setting = this._settingsList[this._currentIndex];
                this._limitationControl.Setting = setting;
                bool flag = TimeLabel.Future == setting.Mark;
                this._buttonDelete.Enabled = flag;
            }
        }

        private int Save(Limitation limitation)
        {
            limitation.RecipientId = this._guid;
            try
            {
                limitation = IBP.PaymentProject2.Oper.ServerFasad.Server.SaveLimitation(limitation);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return -1;
            }
            return limitation.Serial;
        }

        private void SetTimeLabel()
        {
            List<LimitationSetting> list = new List<LimitationSetting>();
            foreach (LimitationSetting setting in this._settingsList)
            {
                if (setting.Limitation.AssignDate <= DateTime.Now)
                {
                    setting.Mark = TimeLabel.Old;
                    list.Add(setting);
                }
                else
                {
                    setting.Mark = TimeLabel.Future;
                }
            }
            list.Sort();
            if (0 < list.Count)
            {
                LimitationSetting item = list[list.Count - 1];
                this._settingsList.Remove(item);
                item.Mark = TimeLabel.Current;
                this._settingsList.Add(item);
            }
            this._settingsList.Sort();
        }
    }
}

