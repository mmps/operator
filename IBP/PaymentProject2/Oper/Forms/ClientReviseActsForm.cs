﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterForm(TypeSearch.ClientReviseAct), BindType(typeof(ClientReviseAct))]
    internal class ClientReviseActsForm : ResultForm
    {
        private ClientReviseActFilterOper _filter;
        private IContainer components;

        public ClientReviseActsForm()
        {
            this.InitializeComponent();
            this.InitMenu();
            base.mainMenuItem.Text = UserStrings.ClientReviseActs;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientReviseActsForm));
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "ClientReviseActsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void InitMenu()
        {
            base.mainMenuItem.Text = UserStrings.Acts;
            base.CreateMainMenuCommand(new ShowClientReviseAct(), UserStrings.ShowAct);
            base.CreateMenuCommand(new DeleteClientReviseAct(), UserStrings.DeleteAct);
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            this._filter = (ClientReviseActFilterOper) filter;
            try
            {
                base.listViewItems.SuspendLayout();
                base.listViewItems.Items.Clear();
                try
                {
                    base.listViewItems.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetClientReviseAct(this._filter.ActFilter).ToArray());
                }
                catch (SrvException exception)
                {
                    MessageBox.Show(exception.Message, UserStrings.Err);
                }
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }
    }
}

