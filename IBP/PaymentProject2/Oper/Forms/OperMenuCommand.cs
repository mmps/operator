﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class OperMenuCommand
    {
        private IBP.PaymentProject2.Oper.Command _cmd;
        private ToolStripMenuItem _contexMenuItem;
        private ToolStripMenuItem _mainMenuItem;

        private OperMenuCommand(IBP.PaymentProject2.Oper.Command cmd, ToolStripMenuItem mainMenu, ContextMenuStrip contextMenu, string caption)
        {
            this._cmd = cmd;
            if (mainMenu == null)
            {
                this._mainMenuItem = null;
            }
            else
            {
                this._mainMenuItem = new ToolStripMenuItem(caption, null, cmd.EventDelegate);
                mainMenu.DropDown.Items.Add(this._mainMenuItem);
                mainMenu.DropDownOpening += new EventHandler(this.OnDropDownOpening);
            }
            if (contextMenu == null)
            {
                this._contexMenuItem = null;
            }
            else
            {
                this._contexMenuItem = new ToolStripMenuItem(caption, null, cmd.EventDelegate);
                contextMenu.Items.Add(this._contexMenuItem);
                contextMenu.Opening += new CancelEventHandler(this.OnDropDownOpening);
            }
        }

        public static OperMenuCommand GetMenuCommand(IBP.PaymentProject2.Oper.Command cmd, ToolStripMenuItem mainMenu, ContextMenuStrip contextMenu, string caption)
        {
            if (caption == null)
            {
                throw new ArgumentNullException(UserStrings.NoCommandSign);
            }
            if (caption.Length == 0)
            {
                throw new ArgumentOutOfRangeException(UserStrings.SignCommand);
            }
            if (cmd == null)
            {
                throw new ArgumentNullException(UserStrings.NoCommand);
            }
            if ((mainMenu == null) && (contextMenu == null))
            {
                throw new ArgumentOutOfRangeException(UserStrings.NoTieCommandMenu);
            }
            OperMenuCommand command = new OperMenuCommand(cmd, mainMenu, contextMenu, caption);
            command.Enabled = cmd.CanExecute;
            return command;
        }

        private void OnDropDownOpening(object sender, EventArgs e)
        {
            this.Enabled = this._cmd.CanExecute;
            this.Visible = this._cmd.MenuItemIsVisible;
        }

        public IBP.PaymentProject2.Oper.Command Command
        {
            get
            {
                return this._cmd;
            }
        }

        public bool CommandTagIsArray
        {
            get
            {
                if (this._cmd == null)
                {
                    throw new ArgumentNullException(UserStrings.CommandWasntSet);
                }
                return this._cmd.TagIsArray;
            }
        }

        private bool Enabled
        {
            get
            {
                if (this._mainMenuItem != null)
                {
                    return this._mainMenuItem.Enabled;
                }
                if (this._contexMenuItem == null)
                {
                    throw new InvalidOperationException(UserStrings.NoMenu);
                }
                return this._contexMenuItem.Enabled;
            }
            set
            {
                if (this._mainMenuItem != null)
                {
                    this._mainMenuItem.Enabled = value;
                }
                if (this._contexMenuItem != null)
                {
                    this._contexMenuItem.Enabled = value;
                }
            }
        }

        public object Tag
        {
            get
            {
                if (this._cmd == null)
                {
                    throw new ArgumentNullException(UserStrings.CommandWasntSet);
                }
                return this._cmd.Tag;
            }
            set
            {
                if (this._cmd == null)
                {
                    throw new ArgumentNullException(UserStrings.CommandWasntSet);
                }
                this._cmd.Tag = value;
            }
        }

        public bool Visible
        {
            get
            {
                if (this._mainMenuItem != null)
                {
                    return this._mainMenuItem.Visible;
                }
                if (this._contexMenuItem == null)
                {
                    throw new InvalidOperationException(UserStrings.NoMenu);
                }
                return this._contexMenuItem.Visible;
            }
            set
            {
                if (this._mainMenuItem != null)
                {
                    this._mainMenuItem.Visible = value;
                }
                if (this._contexMenuItem != null)
                {
                    this._contexMenuItem.Visible = value;
                }
            }
        }
    }
}

