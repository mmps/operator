﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class CommentForm : Form
    {
        private Button btnCancel;
        private Button btnOk;
        private TextBox CommentBox;
        private IContainer components;
        private ErrorProvider errorProvider1;

        public CommentForm(string Caption)
        {
            this.InitializeComponent();
            this.Text = Caption;
        }

        private void CheckComment(object sender, EventArgs e)
        {
            if (this.CommentBox.Text.Replace(" ", "").Length == 0)
            {
                this.errorProvider1.SetIconAlignment(this.btnCancel, ErrorIconAlignment.MiddleLeft);
                this.errorProvider1.SetError(this.btnCancel, UserStrings.WriteComment);
                base.DialogResult = DialogResult.None;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CommentForm));
            this.btnOk = new Button();
            this.CommentBox = new TextBox();
            this.btnCancel = new Button();
            this.errorProvider1 = new ErrorProvider(this.components);
            ((ISupportInitialize) this.errorProvider1).BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.DialogResult = DialogResult.OK;
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.CommentBox, "CommentBox");
            this.CommentBox.Name = "CommentBox";
            manager.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.errorProvider1.ContainerControl = this;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.CommentBox);
            base.Controls.Add(this.btnOk);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "CommentForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            ((ISupportInitialize) this.errorProvider1).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string Comment
        {
            get
            {
                return this.CommentBox.Text;
            }
        }
    }
}

