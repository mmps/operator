﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class RedirectMultiPaymentsDialog : Form
    {
        private Button _buttonCancel;
        private Button _buttonOk;
        private Label _labelId;
        private TextBox _reason;
        private IContainer components;
        private Label labelHeadStep2;
        private Panel panel1;

        public RedirectMultiPaymentsDialog(int count)
        {
            this.InitializeComponent();
            this._labelId.Text = string.Format("{0}", count);
            this._reason.Text = UserStrings.ClientCall + DateTime.Now.ToString("HH:mm dd/MM/yyyy");
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RedirectMultiPaymentsDialog));
            this._buttonCancel = new Button();
            this._buttonOk = new Button();
            this.panel1 = new Panel();
            this.labelHeadStep2 = new Label();
            this._reason = new TextBox();
            this._labelId = new Label();
            Label label = new Label();
            Label label2 = new Label();
            this.panel1.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(label, "label4");
            label.Name = "label4";
            manager.ApplyResources(this._buttonCancel, "_buttonCancel");
            this._buttonCancel.DialogResult = DialogResult.Cancel;
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._buttonOk, "_buttonOk");
            this._buttonOk.Name = "_buttonOk";
            this._buttonOk.UseVisualStyleBackColor = true;
            this._buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.panel1.BackColor = Color.White;
            this.panel1.Controls.Add(this.labelHeadStep2);
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this.labelHeadStep2, "labelHeadStep2");
            this.labelHeadStep2.Name = "labelHeadStep2";
            manager.ApplyResources(this._reason, "_reason");
            this._reason.Name = "_reason";
            manager.ApplyResources(label2, "labelIdZag");
            label2.Name = "labelIdZag";
            manager.ApplyResources(this._labelId, "_labelId");
            this._labelId.BorderStyle = BorderStyle.Fixed3D;
            this._labelId.Name = "_labelId";
            base.AcceptButton = this._buttonOk;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._buttonCancel;
            base.Controls.Add(this._labelId);
            base.Controls.Add(label2);
            base.Controls.Add(label);
            base.Controls.Add(this._reason);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this._buttonOk);
            base.Controls.Add(this._buttonCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "RedirectMultiPaymentsDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string RedirectReason
        {
            get
            {
                return this._reason.Text.Replace(Environment.NewLine, " ");
            }
        }
    }
}

