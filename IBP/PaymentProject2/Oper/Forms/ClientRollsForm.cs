﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [BindType(typeof(ClientRoll)), FilterForm(TypeSearch.ClientsRolls)]
    internal class ClientRollsForm : ResultForm
    {
        private IContainer components;

        public ClientRollsForm()
        {
            this.InitializeComponent();
            base.mainMenuItem.Text = UserStrings.ClientReestr;
            base.CreateMainMenuCommand(new ShowClientRollPayments(), UserStrings.ShowPays02);
            base.CreateMenuCommand(new RebuildClientRoll(), UserStrings.RestructReestr);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientRollsForm));
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "ClientRollsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            try
            {
                base.listViewItems.SuspendLayout();
                ClientRollsFilterOper oper = (ClientRollsFilterOper) filter;
                List<ClientRoll> clientRolls = ServerFasad.Server.GetClientRolls(oper.RollsFilter);
                base.listViewItems.Items.Clear();
                base.listViewItems.AddRange(clientRolls.ToArray());
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            switch (((ClientRoll) e.Tag).State.StateCode)
            {
                case RollStates.NotRevised:
                    e.Item.BackColor = ClientRollsSettings.Default.NotRevisedColor;
                    return;

                case RollStates.Revised:
                    e.Item.BackColor = ClientRollsSettings.Default.RevisedColor;
                    break;
            }
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return ClientRollsSettings.Default.Helper;
            }
        }
    }
}

