﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class InfoStep : StepControl
    {
        private IContainer components;
        private StepInfo info;
        private Label labelResult;

        public InfoStep(StepInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.info = info;
            this.InitializeComponent();
            this.labelResult.BringToFront();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(InfoStep));
            this.labelResult = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.labelResult, "labelResult");
            this.labelResult.Name = "labelResult";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.labelResult);
            this.MinimumSize = new Size(0x197, 0xb1);
            base.Name = "InfoStep";
            base.ResumeLayout(false);
        }

        protected override void OnBeforeEnterStepForward()
        {
            base.OnBeforeEnterStepForward();
            this.labelResult.Text = this.info.ToString();
        }
    }
}

