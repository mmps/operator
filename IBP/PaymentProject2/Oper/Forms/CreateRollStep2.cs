﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    internal class CreateRollStep2 : StepControl
    {
        private IContainer components;
        private StepInfo info;
        private Label label2;
        private Label labelAmount;
        private Label labelAmountHead;
        private Label labelCount;
        private Label labelRecipient;
        private Label labelRecipientHead;
        private List<Payment> payments;
        private Roll roll;

        public CreateRollStep2(StepInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.info = info;
            this.InitializeComponent();
            base.HeaderText = UserStrings.PressButton;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CreateRollStep2));
            this.labelAmount = new Label();
            this.labelAmountHead = new Label();
            this.labelRecipient = new Label();
            this.labelRecipientHead = new Label();
            this.labelCount = new Label();
            this.label2 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.labelAmount, "labelAmount");
            this.labelAmount.ForeColor = Color.Red;
            this.labelAmount.Name = "labelAmount";
            manager.ApplyResources(this.labelAmountHead, "labelAmountHead");
            this.labelAmountHead.Name = "labelAmountHead";
            manager.ApplyResources(this.labelRecipient, "labelRecipient");
            this.labelRecipient.Name = "labelRecipient";
            manager.ApplyResources(this.labelRecipientHead, "labelRecipientHead");
            this.labelRecipientHead.Name = "labelRecipientHead";
            manager.ApplyResources(this.labelCount, "labelCount");
            this.labelCount.Name = "labelCount";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.labelCount);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.labelAmount);
            base.Controls.Add(this.labelAmountHead);
            base.Controls.Add(this.labelRecipient);
            base.Controls.Add(this.labelRecipientHead);
            this.MinimumSize = new Size(390, 0xc9);
            base.Name = "CreateRollStep2";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnBeforeEnterStepForward()
        {
            base.OnBeforeEnterStepForward();
            this.roll = Roll.GetRoll(this.info.Recipient.GatewayType);
            decimal num = 0M;
            PaymentsFilter filter = new PaymentsFilter();
            filter.StartTime = this.info.Start;
            filter.EndTime = this.info.End;
            filter.RecipientID = this.info.Recipient.Id;
            filter.State = PaymentState.Finalized;
            try
            {
                this.payments = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPayments(filter);
            }
            catch (SrvException exception)
            {
                throw new StepException(exception.Message, exception);
            }
            int count = this.payments.Count;
            foreach (Payment payment in this.payments)
            {
                num += payment.Value;
            }
            this.labelRecipient.Text = this.info.Recipient.Name;
            this.labelAmount.Text = num.ToString("C", Application.CurrentCulture);
            this.labelCount.Text = count.ToString(Application.CurrentCulture);
        }

        protected override void OnBeforeLeaveStepForward()
        {
            base.OnBeforeLeaveStepForward();
            try
            {
                using (Stream stream = this.roll.CreateRoll(this.payments.ToArray()))
                {
                    using (SaveFileDialog dialog = new SaveFileDialog())
                    {
                        dialog.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + this.roll.FileName;
                        dialog.CheckFileExists = false;
                        if (dialog.ShowDialog() == DialogResult.OK)
                        {
                            using (Stream stream2 = File.Open(dialog.FileName, FileMode.Create))
                            {
                                int count = 0x200;
                                byte[] buffer = new byte[count];
                                int num2 = 0;
                                while ((num2 = stream.Read(buffer, 0, count)) > 0)
                                {
                                    stream2.Write(buffer, 0, num2);
                                }
                                stream2.Flush();
                                return;
                            }
                        }
                        throw new StepException("", true);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new StepException(exception.Message, exception);
            }
        }
    }
}

