﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterForm(TypeSearch.Payments), BindType(typeof(Payment))]
    internal class PaymentForm : ResultForm
    {
        private IBP.PaymentProject2.Oper.Command _cmdSavePayments = new SavePayments();
        private IContainer components;
        private ToolStripStatusLabel toolStripStatusLabelAmount = new ToolStripStatusLabel();
        private ToolStripStatusLabel toolStripStatusLabelBalance = new ToolStripStatusLabel();
        private ToolStripStatusLabel toolStripStatusLabelCount = new ToolStripStatusLabel();

        public PaymentForm()
        {
            this.InitializeComponent();
            this._cmdSavePayments.Tag = base.listViewItems;
            this.InitData();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitData()
        {
            base.statusStripBottom.Items.Add(this.toolStripStatusLabelAmount);
            base.statusStripBottom.Items.Add(this.toolStripStatusLabelBalance);
            base.statusStripBottom.Items.Add(this.toolStripStatusLabelCount);
            base.CreateCommand(this._cmdSavePayments, UserStrings.UploadToXLS);
            base.CreateMainMenuCommand(new ShowPayment(), UserStrings.PayInfo02);
            base.CreateMenuCommand(new RedirectPayment(), UserStrings.Resend);
            base.CreateMenuCommand(new RecallPayment(), UserStrings.Recall);
            base.CreateMenuCommand(new ShowDstPayment(), UserStrings.ShowPayChild);
            base.CreateMenuCommand(new ShowSrcPayment(), UserStrings.ShowPayParent);
            base.CreateMenuCommand(new UseBalance(), UserStrings.UseChange);
            base.CreateMenuCommand(new ShowStornoChain(), UserStrings.ShowCouplePays);
            base.CreateMenuCommand(new PrintCommand(), UserStrings.PrintCheck02);
            base.CreateMenuCommand(new ShowCardOperation(), UserStrings.ShowPayByCard);
            base.CreateMenuCommand(new ShowRecipientRoll(), UserStrings.ShowReestrOfReceiver);
            base.CreateMenuCommand(new ShowPaymentTransaction(), UserStrings.ShowPaysByTrans);
            base.CreateMenuCommand(new ShowClientRoll(), UserStrings.ShowReestrOfCllient);
            base.mainMenuItem.Text = UserStrings.Pays;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(PaymentForm));
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "PaymentForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnActivated(EventArgs e)
        {
            this._cmdSavePayments.Tag = base.listViewItems;
            base.OnActivated(e);
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            List<Payment> payments;
            base.listViewItems.Items.Clear();
            try
            {
                payments = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPayments((PaymentsFilter) this.Filter.Filter);
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }
            if ((payments.Count <= 0x3e8) || (MessageBox.Show(string.Format(UserStrings.TooMuchPays, payments.Count), UserStrings.Attention, MessageBoxButtons.YesNo) == DialogResult.Yes))
            {
                decimal num = 0M;
                decimal num2 = 0M;
                decimal num3 = 0M;
                decimal num4 = 0M;
                decimal num5 = 0M;
                decimal num6 = 0M;
                foreach (Payment payment in payments)
                {
                    num2 += payment.Total;
                    num += payment.Value;
                    num3 += payment.Amount;
                    num4 += payment.Balance;
                    num5 += payment.Comission;
                    num6 += payment.DispensedCashOut;
                    base.listViewItems.Add(payment);
                }
                this.toolStripStatusLabelAmount.Text = string.Format(UserStrings.SumCommission, num.ToString(), num2.ToString(), num5.ToString());
                this.toolStripStatusLabelCount.Text = string.Format(UserStrings.QtyPays, payments.Count.ToString());
                this.toolStripStatusLabelBalance.Text = string.Format(UserStrings.SumOfPaysTotalChange, num3.ToString(), num4.ToString(), num6.ToString());
            }
            else
            {
                base.MdiParent.Cursor = Cursors.Default;
            }
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            Payment tag = (Payment) e.Item.Tag;
            if (tag.HasParent && tag.HasChild)
            {
                e.Item.BackColor = PaymentsSettings.Default.HasChildAndParentColor;
            }
            else if (tag.HasChild)
            {
                e.Item.BackColor = PaymentsSettings.Default.HasChildColor;
            }
            else if (tag.HasParent)
            {
                e.Item.BackColor = PaymentsSettings.Default.HasParentColor;
            }
            else if (tag.HasSource)
            {
                e.Item.BackColor = PaymentsSettings.Default.UseBalanceColor;
            }
            else
            {
                switch (tag.WorkState)
                {
                    case PaymentState.New:
                        e.Item.BackColor = PaymentsSettings.Default.NewColor;
                        break;

                    case PaymentState.Rejected:
                        e.Item.BackColor = PaymentsSettings.Default.RejectedColor;
                        break;

                    case PaymentState.Accepted:
                        e.Item.BackColor = PaymentsSettings.Default.AcceptedColor;
                        break;

                    case PaymentState.Processing:
                        e.Item.BackColor = PaymentsSettings.Default.ProcessingColor;
                        return;

                    case PaymentState.Delayed:
                        e.Item.BackColor = PaymentsSettings.Default.DelayedColor;
                        return;

                    case PaymentState.Finalized:
                        e.Item.BackColor = PaymentsSettings.Default.FinalizedColor;
                        return;
                }
            }
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return PaymentsSettings.Default.Helper;
            }
        }

        private Payment SelectedPayment
        {
            get
            {
                if (base.listViewItems.SelectedItems.Count == 0)
                {
                    return null;
                }
                return (base.listViewItems.SelectedItems[0].Tag as Payment);
            }
        }
    }
}

