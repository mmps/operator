﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class CashCheckStep : StepControl
    {
        private IContainer components;
        private IBP.PaymentProject2.Oper.Forms.StepInfo info;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label labelAmount;
        private Label labelCount;
        private Label labelTotal;
        private Label labelValue;

        public CashCheckStep(IBP.PaymentProject2.Oper.Forms.StepInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            this.info = info;
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CashCheckStep));
            this.labelTotal = new Label();
            this.labelCount = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.label3 = new Label();
            this.labelValue = new Label();
            this.label5 = new Label();
            this.labelAmount = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.labelTotal, "labelTotal");
            this.labelTotal.ForeColor = Color.Red;
            this.labelTotal.Name = "labelTotal";
            manager.ApplyResources(this.labelCount, "labelCount");
            this.labelCount.ForeColor = Color.Red;
            this.labelCount.Name = "labelCount";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            manager.ApplyResources(this.labelValue, "labelValue");
            this.labelValue.ForeColor = Color.Red;
            this.labelValue.Name = "labelValue";
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            manager.ApplyResources(this.labelAmount, "labelAmount");
            this.labelAmount.ForeColor = Color.Red;
            this.labelAmount.Name = "labelAmount";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.labelTotal);
            base.Controls.Add(this.labelValue);
            base.Controls.Add(this.labelAmount);
            base.Controls.Add(this.labelCount);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.label2);
            this.MinimumSize = new Size(0x151, 0xbb);
            base.Name = "CashCheckStep";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnBeforeEnterStepForward()
        {
            base.OnBeforeEnterStepForward();
            try
            {
                this.info.StatisticInfo = IBP.PaymentProject2.Oper.ServerFasad.Server.GetStatisticInfo((PaymentsFilter) this.info.Filter)[0];
            }
            catch (SrvException exception)
            {
                throw new StepException(exception.Message, exception);
            }
            this.labelCount.Text = this.info.StatisticInfo.Count.ToString(Application.CurrentCulture);
            this.labelTotal.Text = this.info.StatisticInfo.SumTotal.ToString("C", Application.CurrentCulture);
            this.labelValue.Text = this.info.StatisticInfo.SumValue.ToString("C", Application.CurrentCulture);
            this.labelAmount.Text = this.info.StatisticInfo.SumAmount.ToString("C", Application.CurrentCulture);
        }

        public IBP.PaymentProject2.Oper.Forms.StepInfo StepInfo
        {
            get
            {
                return this.info;
            }
        }
    }
}

