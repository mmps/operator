﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    public class ReportErrorForm : Form
    {
        private Button buttonCancel;
        private Button buttonCopy;
        private IContainer components;
        private TextBox textBox1;
        private TextBox textBoxError;

        public ReportErrorForm(Exception exception)
        {
            this.InitializeComponent();
            this.textBoxError.Text = exception.ToString();
            this.textBoxError.Select(0, 0);
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            if (Thread.CurrentThread.GetApartmentState() != ApartmentState.STA)
            {
                FormCollection openForms = Application.OpenForms;
                foreach (Form form in openForms)
                {
                    if (form.IsMdiContainer)
                    {
                        EventHandler method = new EventHandler(this.buttonCopy_Click);
                        openForms[0].BeginInvoke(method, new object[] { sender, e });
                        break;
                    }
                }
            }
            else
            {
                Clipboard.Clear();
                Clipboard.SetText(this.textBoxError.Text);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ReportErrorForm));
            this.buttonCancel = new Button();
            this.buttonCopy = new Button();
            this.textBox1 = new TextBox();
            this.textBoxError = new TextBox();
            base.SuspendLayout();
            this.buttonCancel.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.buttonCopy, "buttonCopy");
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new EventHandler(this.buttonCopy_Click);
            this.textBox1.BackColor = SystemColors.Control;
            this.textBox1.BorderStyle = BorderStyle.None;
            manager.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBoxError.BackColor = SystemColors.Menu;
            manager.ApplyResources(this.textBoxError, "textBoxError");
            this.textBoxError.Name = "textBoxError";
            this.textBoxError.ReadOnly = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.textBoxError);
            base.Controls.Add(this.textBox1);
            base.Controls.Add(this.buttonCopy);
            base.Controls.Add(this.buttonCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ReportErrorForm";
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}

