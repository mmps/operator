﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Files;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [NoFilterAtribute]
    public class ClientReviseActParamsForm : Form
    {
        private Button _cancel;
        private Client _client;
        private DateTime _date;
        private TextBox _footer;
        private TextBox _header;
        private TextBox _nameSide1;
        private TextBox _nameSide2;
        private Button _ok;
        private ClientReviseActParams _param;
        private Button _preview;
        private TextBox _statusSide1;
        private TextBox _statusSide2;
        private Template _template;
        private IContainer components;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;

        public ClientReviseActParamsForm(Client client, ClientReviseActParams param, DateTime date)
        {
            this.InitializeComponent();
            this._param = param;
            this._client = client;
            this._date = date;
            this.Text = string.Format(UserStrings.ClientReviseActParams, client.Name);
            this.ActParams = param;
        }

        private void _preview_Click(object sender, EventArgs e)
        {
            ClientReviseAct clientReviseAct = new ClientReviseAct();
            clientReviseAct.ClientId = this._client.Id;
            clientReviseAct.ClientName = this._client.Name;
            clientReviseAct.CreationTime = DateTime.Now;
            clientReviseAct.DealDate = this._date.AddMonths(-1);
            clientReviseAct.DealNumber = "45/127";
            clientReviseAct.Deleted = false;
            clientReviseAct.StartTime = this._date;
            clientReviseAct.EndTime = this._date.AddMonths(1).AddDays(-1.0);
            clientReviseAct.Params = new ClientReviseActParams {
                DealerSign = this._nameSide1.Text,
                SubDealerSign = this._nameSide2.Text,
                TitleDealer = this._statusSide1.Text,
                TitleSubDealer = this._statusSide2.Text,
                Header = this._header.Text,
                Footer = this._footer.Text
            };
            Template template = this.GetTemplate();
            if (template != null) {
                new ReportForm(clientReviseAct, template) {
                    Text = UserStrings.Preview
                }.ShowDialog();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Template GetTemplate()
        {
            if (this._template == null)
            {
                TemplateFilter filter = new TemplateFilter();
                filter.Type = TemplateType.ClientReviseAct;
                filter.StartTime = this._date;
                filter.EndTime = this._date.AddMonths(1).AddDays(-1.0);
                filter.ByTime = true;
                filter.AbsolutlyFind = true;
                try
                {
                    this._template = IBP.PaymentProject2.Oper.ServerFasad.Server.GetFullTemplate(filter);
                }
                catch (SrvException exception)
                {
                    if (MessageBox.Show(UserStrings.ErrorReadingTemplate, string.Format(UserStrings.TryAgainQuestionString, exception.Message), MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        this.GetTemplate();
                    }
                    else
                    {
                        this._template = null;
                    }
                }
            }
            return this._template;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientReviseActParamsForm));
            this._header = new TextBox();
            this.label1 = new Label();
            this._footer = new TextBox();
            this.label2 = new Label();
            this._statusSide1 = new TextBox();
            this.groupBox1 = new GroupBox();
            this.label6 = new Label();
            this.label4 = new Label();
            this._nameSide2 = new TextBox();
            this._statusSide2 = new TextBox();
            this.groupBox2 = new GroupBox();
            this.label5 = new Label();
            this.label3 = new Label();
            this._nameSide1 = new TextBox();
            this._cancel = new Button();
            this._ok = new Button();
            this._preview = new Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this._header, "_header");
            this._header.Name = "_header";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this._footer, "_footer");
            this._footer.Name = "_footer";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this._statusSide1, "_statusSide1");
            this._statusSide1.Name = "_statusSide1";
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._nameSide2);
            this.groupBox1.Controls.Add(this._statusSide2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            manager.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            manager.ApplyResources(this._nameSide2, "_nameSide2");
            this._nameSide2.Name = "_nameSide2";
            manager.ApplyResources(this._statusSide2, "_statusSide2");
            this._statusSide2.Name = "_statusSide2";
            manager.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this._nameSide1);
            this.groupBox2.Controls.Add(this._statusSide1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            manager.ApplyResources(this._nameSide1, "_nameSide1");
            this._nameSide1.Name = "_nameSide1";
            manager.ApplyResources(this._cancel, "_cancel");
            this._cancel.DialogResult = DialogResult.Cancel;
            this._cancel.Name = "_cancel";
            this._cancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._ok, "_ok");
            this._ok.DialogResult = DialogResult.OK;
            this._ok.Name = "_ok";
            this._ok.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._preview, "_preview");
            this._preview.Name = "_preview";
            this._preview.UseVisualStyleBackColor = true;
            this._preview.Click += new EventHandler(this._preview_Click);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._cancel;
            base.Controls.Add(this._preview);
            base.Controls.Add(this._ok);
            base.Controls.Add(this._cancel);
            base.Controls.Add(this.groupBox2);
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this._footer);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this._header);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ClientReviseActParamsForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public ClientReviseActParams ActParams
        {
            get {
                return new ClientReviseActParams {
                    DealerSign = this._nameSide1.Text,
                    SubDealerSign = this._nameSide2.Text,
                    TitleDealer = this._statusSide1.Text,
                    TitleSubDealer = this._statusSide2.Text,
                    Footer = this._footer.Text,
                    Header = this._header.Text
                };
            }
            set {
                try {
                    base.SuspendLayout();
                    this._header.Text = value.Header;
                    this._footer.Text = value.Footer;
                    this._nameSide1.Text = value.DealerSign;
                    this._nameSide2.Text = value.SubDealerSign;
                    this._statusSide1.Text = value.TitleDealer;
                    this._statusSide2.Text = value.TitleSubDealer;
                }
                finally {
                    base.ResumeLayout();
                }
            }
        }

        public bool IsChange
        {
            get
            {
                return (this.ActParams != this._param);
            }
        }
    }
}

