﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    [FilterForm(TypeSearch.RecipientRolls), BindType(typeof(RecipientRoll))]
    internal class RecipientRollsForm : ResultForm
    {
        private RecipientRollsFilterOper _filter;
        private IContainer components;

        public RecipientRollsForm()
        {
            InterfaceCommand.UpdateFormEventHandler handler = null;
            this.InitializeComponent();
            base.mainMenuItem.Text = UserStrings.ReceiverReestr;
            base.CreateMainMenuCommand(new ShowRecipientRollPayments(), UserStrings.ShowPays03);
            base.CreateMenuCommand(new RebuildRecipientRoll(), UserStrings.RestructReestr02);
            base.CreateMenuCommand(new ShowRecipientRollTransaction(), UserStrings.ShowTrans);
            CreateRecipientRollTransaction cmd = new CreateRecipientRollTransaction();
            cmd.Completed += new EventHandler(this.OnCreateTransactionCommandCompleted);
            base.CreateMenuCommand(cmd, UserStrings.CreateTrans);
            ResendRecipientRoll roll = new ResendRecipientRoll();
            if (handler == null)
            {
                handler = delegate (InterfaceCommand sender) {
                    ListViewItem item = base.listViewItems.SelectedItems[0];
                    item.Tag = sender.Tag;
                    item.BackColor = this.GetItemColour(((RecipientRoll) item.Tag).State.StateCode);
                };
            }
            roll.UpdateForm += handler;
            base.CreateMenuCommand(roll, UserStrings.ResendRoll);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private Color GetItemColour(RollStates rollState)
        {
            switch (rollState)
            {
                case RollStates.Revised:
                    return RecipientRollsSettings.Default.RevisedColor;

                case RollStates.NotRevised:
                    return RecipientRollsSettings.Default.NotRevisedColor;
            }
            return SystemColors.Window;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecipientRollsForm));
            base.SuspendLayout();
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "RecipientRollsForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void OnCreateTransactionCommandCompleted(object sender, EventArgs e)
        {
            this.Filter = this._filter;
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            try
            {
                base.listViewItems.SuspendLayout();
                this._filter = (RecipientRollsFilterOper) filter;
                List<RecipientRoll> recipientRolls = ServerFasad.Server.GetRecipientRolls(this._filter.RollsFilter);
                base.listViewItems.Items.Clear();
                base.listViewItems.AddRange(recipientRolls.ToArray());
            }
            finally
            {
                base.listViewItems.ResumeLayout();
            }
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            RollStates stateCode = ((RecipientRoll) e.Tag).State.StateCode;
            e.Item.BackColor = this.GetItemColour(stateCode);
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return RecipientRollsSettings.Default.Helper;
            }
        }
    }
}

