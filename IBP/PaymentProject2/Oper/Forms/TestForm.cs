﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.ComponentModel;
    using System.Text;
    using System.Windows.Forms;

    internal class TestForm : System.Windows.Forms.Form
    {
        private IContainer components;
        private static TestForm form;
        private Label label1;

        private TestForm()
        {
            this.InitializeComponent();
        }

        public void AddLog(string log)
        {
            StringBuilder builder = new StringBuilder(this.label1.Text);
            builder.Insert(0, Environment.NewLine);
            builder.Insert(0, DateTime.Now.TimeOfDay.ToString());
            builder.Insert(0, Environment.NewLine);
            builder.Insert(0, log);
            this.label1.Text = builder.ToString();
            this.label1.Refresh();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TestForm));
            this.label1 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.label1);
            base.FormBorderStyle = FormBorderStyle.None;
            base.Name = "TestForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.TopMost = true;
            base.ResumeLayout(false);
        }

        public static TestForm Form
        {
            get
            {
                if (form == null)
                {
                    form = new TestForm();
                    form.Show();
                }
                return form;
            }
        }
    }
}

