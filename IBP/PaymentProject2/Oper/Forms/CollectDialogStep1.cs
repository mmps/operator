﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class CollectDialogStep1 : StepControl
    {
        private ComboBox comboBoxPoints;
        private IContainer components;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private StepInfo info;
        private Label label1;
        private Label label2;
        private Label labelRecipient;

        public CollectDialogStep1(StepInfo info)
        {
            if (info == null)
            {
                throw new ArgumentOutOfRangeException("info");
            }
            this.InitializeComponent();
            this.info = info;
            this.comboBoxPoints.Items.AddRange(PointsContainer.Instance.ToArray());
            this.dateTimeStart.Value = this.info.Start;
            this.dateTimeEnd.Value = this.info.End;
            base.HeaderText = UserStrings.ChoosePoS;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CollectDialogStep1));
            this.labelRecipient = new Label();
            this.comboBoxPoints = new ComboBox();
            this.dateTimeEnd = new DateTimePicker();
            this.label2 = new Label();
            this.label1 = new Label();
            this.dateTimeStart = new DateTimePicker();
            base.SuspendLayout();
            manager.ApplyResources(this.labelRecipient, "labelRecipient");
            this.labelRecipient.Name = "labelRecipient";
            manager.ApplyResources(this.comboBoxPoints, "comboBoxPoints");
            this.comboBoxPoints.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxPoints.DropDownWidth = 300;
            this.comboBoxPoints.FormatString = "{Name}";
            this.comboBoxPoints.FormattingEnabled = true;
            this.comboBoxPoints.Name = "comboBoxPoints";
            this.comboBoxPoints.Sorted = true;
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Format = DateTimePickerFormat.Custom;
            this.dateTimeEnd.Name = "dateTimeEnd";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Format = DateTimePickerFormat.Custom;
            this.dateTimeStart.Name = "dateTimeStart";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.dateTimeStart);
            base.Controls.Add(this.labelRecipient);
            base.Controls.Add(this.comboBoxPoints);
            this.MinimumSize = new Size(0x1a2, 0x10d);
            base.Name = "CollectDialogStep1";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnBeforeLeaveStepForward()
        {
            base.OnBeforeLeaveStepForward();
            this.info.End = this.dateTimeEnd.Value;
            this.info.Start = this.dateTimeStart.Value;
            this.info.Point = this.SelectedPoint;
            if (this.info.Point == null)
            {
                this.comboBoxPoints.Focus();
                throw new StepException(UserStrings.PoSNotChoosen);
            }
        }

        public PayPoint SelectedPoint
        {
            get
            {
                return (this.comboBoxPoints.SelectedItem as PayPoint);
            }
        }
    }
}

