﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    internal class TableMasterCell : TableCell
    {
        private List<TableCell> list;

        public TableMasterCell(Control owner, Point position, Size size) : base(owner, position, size)
        {
            this.list = new List<TableCell>();
        }

        public TableCell AddSlaveCell()
        {
            return this.AddSlaveCell(new Point(0, 0), new Size(100, 100));
        }

        public TableCell AddSlaveCell(Point position, Size size)
        {
            TableCell item = new TableCell(this, position, size);
            this.list.Add(item);
            return item;
        }

        public void RemoveAllCells()
        {
            this.list.Clear();
        }

        public bool RemoveCell(TableCell cell)
        {
            return ((cell != null) && this.list.Remove(cell));
        }

        public TableCell[] SlaveCells
        {
            get
            {
                return this.list.ToArray();
            }
        }
    }
}

