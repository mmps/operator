﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;
    using System.Windows.Forms;

    [BindType(typeof(Transaction)), FilterForm(TypeSearch.Transactions)]
    internal class TransactionForm : ResultForm
    {
        private IContainer components;
        private IBP.PaymentProject2.Oper.Command showPayments = new ShowTranPayments();
        private ToolStripStatusLabel toolStripStatusLabelInBalance;

        public TransactionForm()
        {
            this.InitializeComponent();
            base.CreateMainMenuCommand(new ShowTranPayments(), UserStrings.ShowPays04);
            base.statusStripBottom.Items.Add(this.toolStripStatusLabelInBalance);
            base.mainMenuItem.Text = UserStrings.Transactions;
        }

        private decimal AppendTransactions(IEnumerable<Transaction> trans)
        {
            decimal num = 0M;
            foreach (Transaction transaction in trans)
            {
                num += transaction.Amount1;
                base.listViewItems.Add(transaction);
            }
            return num;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private string GetString(decimal value)
        {
            return value.ToString("C", MainSettings.Default.CultureInfo);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TransactionForm));
            this.toolStripStatusLabelInBalance = new ToolStripStatusLabel();
            base.SuspendLayout();
            manager.ApplyResources(base.listViewItems, "listViewItems");
            this.toolStripStatusLabelInBalance.Name = "toolStripStatusLabelInBalance";
            manager.ApplyResources(this.toolStripStatusLabelInBalance, "toolStripStatusLabelInBalance");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "TransactionForm";
            base.Controls.SetChildIndex(base.listViewItems, 0);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        protected override void OnFilterChanged(FilterOper filter)
        {
            this.toolStripStatusLabelInBalance.Text = "";
            base.listViewItems.Items.Clear();
            decimal inBalance = 0M;
            StringBuilder builder = new StringBuilder(200);
            TransactionsFilter filter2 = (TransactionsFilter) filter.Filter;
            try
            {
                if ((filter2.SourceAccountId != 0) && !AccountsContainer.Instance[filter2.SourceAccountId].IsExternal)
                {
                    inBalance = IBP.PaymentProject2.Oper.ServerFasad.Server.GetInBalance(filter2);
                }
                else
                {
                    inBalance = -1M;
                }
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
            builder.AppendFormat(UserStrings.InputSaldo, (inBalance == -1M) ? "---" : this.GetString(inBalance));
            builder.Append(' ', 5);
            try
            {
                filter2 = (TransactionsFilter) filter2.Clone();
                if (filter2.SourceAccountId == 0)
                {
                    List<Transaction> transactions = IBP.PaymentProject2.Oper.ServerFasad.Server.GetTransactions(filter2);
                    decimal num2 = this.AppendTransactions(transactions);
                    builder.AppendFormat(UserStrings.QtyOfTransactions, transactions.Count);
                    builder.AppendFormat(UserStrings.TransSumm, this.GetString(num2));
                }
                else
                {
                    int sourceAccountId = filter2.SourceAccountId;
                    filter2.DestAccountId = sourceAccountId;
                    filter2.SourceAccountId = 0;
                    decimal num4 = this.AppendTransactions(IBP.PaymentProject2.Oper.ServerFasad.Server.GetTransactions(filter2));
                    filter2.SourceAccountId = sourceAccountId;
                    filter2.DestAccountId = 0;
                    decimal num5 = this.AppendTransactions(IBP.PaymentProject2.Oper.ServerFasad.Server.GetTransactions(filter2));
                    builder.AppendFormat(UserStrings.CreditDebet, this.GetString(num4), this.GetString(num5), this.GetString((inBalance + num4) - num5));
                }
            }
            catch (SrvException exception2)
            {
                MessageBox.Show(exception2.Message);
            }
            this.toolStripStatusLabelInBalance.Text = builder.ToString();
        }

        protected override void OnItemAdded(object sender, ItemEventArgs e)
        {
            Transaction tag = (Transaction) e.Tag;
            int sourceAccountId = ((TransactionsFilter) this.Filter.Filter).SourceAccountId;
            if (sourceAccountId != 0)
            {
                e.Item.BackColor = (sourceAccountId == tag.SourceAccountId) ? TransactionsSettings.Default.CreditColor : TransactionsSettings.Default.DebetColor;
            }
        }

        protected override SettingsHelper SelectedFieldsHelper
        {
            get
            {
                return TransactionsSettings.Default.Helper;
            }
        }
    }
}

