﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class RedirectPaymentsForServiceDialog : Form
    {
        private Button _buttonCancel;
        private Button _buttonOk;
        private Label _labelId;
        private Label _labelService;
        private ComboBox _newService;
        private TextBox _reason;
        private IContainer components;
        private Label labelHeadStep2;
        private Panel panel1;

        public RedirectPaymentsForServiceDialog(int count, Service service)
        {
            this.InitializeComponent();
            this._newService.Items.AddRange(ServicesContainer.Instance.ToArray());
            if (this._newService.Items.Contains(service))
            {
                this._newService.SelectedItem = service;
            }
            this._labelId.Text = string.Format("{0}", count);
            this._labelService.Text = service.Name;
            this._reason.Text = UserStrings.ClientCall + DateTime.Now.ToString("HH:mm dd/MM/yyyy");
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RedirectPaymentsForServiceDialog));
            this._labelService = new Label();
            this._labelId = new Label();
            this._newService = new ComboBox();
            this._buttonCancel = new Button();
            this._buttonOk = new Button();
            this.panel1 = new Panel();
            this.labelHeadStep2 = new Label();
            this._reason = new TextBox();
            Label label = new Label();
            GroupBox box = new GroupBox();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            box.SuspendLayout();
            this.panel1.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(label, "labelServiceZag");
            label.Name = "labelServiceZag";
            box.Controls.Add(this._labelService);
            box.Controls.Add(this._labelId);
            box.Controls.Add(label2);
            box.Controls.Add(label3);
            manager.ApplyResources(box, "groupBox1");
            box.Name = "groupBox1";
            box.TabStop = false;
            manager.ApplyResources(this._labelService, "_labelService");
            this._labelService.BorderStyle = BorderStyle.Fixed3D;
            this._labelService.Name = "_labelService";
            manager.ApplyResources(this._labelId, "_labelId");
            this._labelId.BorderStyle = BorderStyle.Fixed3D;
            this._labelId.Name = "_labelId";
            manager.ApplyResources(label2, "label1");
            label2.Name = "label1";
            manager.ApplyResources(label3, "labelIdZag");
            label3.Name = "labelIdZag";
            manager.ApplyResources(label4, "label4");
            label4.Name = "label4";
            manager.ApplyResources(this._newService, "_newService");
            this._newService.DropDownStyle = ComboBoxStyle.DropDownList;
            this._newService.FormatString = "{Name}";
            this._newService.FormattingEnabled = true;
            this._newService.Name = "_newService";
            this._newService.Sorted = true;
            manager.ApplyResources(this._buttonCancel, "_buttonCancel");
            this._buttonCancel.DialogResult = DialogResult.Cancel;
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._buttonOk, "_buttonOk");
            this._buttonOk.Name = "_buttonOk";
            this._buttonOk.UseVisualStyleBackColor = true;
            this._buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.panel1.BackColor = Color.White;
            this.panel1.Controls.Add(this.labelHeadStep2);
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this.labelHeadStep2, "labelHeadStep2");
            this.labelHeadStep2.Name = "labelHeadStep2";
            manager.ApplyResources(this._reason, "_reason");
            this._reason.Name = "_reason";
            base.AcceptButton = this._buttonOk;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._buttonCancel;
            base.Controls.Add(box);
            base.Controls.Add(label4);
            base.Controls.Add(this._reason);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this._buttonOk);
            base.Controls.Add(this._buttonCancel);
            base.Controls.Add(label);
            base.Controls.Add(this._newService);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "RedirectPaymentsForServiceDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            box.ResumeLayout(false);
            box.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public string RedirectReason
        {
            get
            {
                return this._reason.Text.Replace(Environment.NewLine, " ");
            }
        }

        public Service SelectedService
        {
            get
            {
                Service selectedItem = this._newService.SelectedItem as Service;
                if (selectedItem == null)
                {
                    throw new SrvException(UserStrings.ServiceNotChoosen02);
                }
                return selectedItem;
            }
        }
    }
}

