﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false, Inherited=false)]
    internal sealed class FilterFormAttribute : Attribute
    {
        public readonly IBP.PaymentProject2.Oper.TypeSearch TypeSearch;

        public FilterFormAttribute(IBP.PaymentProject2.Oper.TypeSearch typeSearch)
        {
            this.TypeSearch = typeSearch;
        }
    }
}

