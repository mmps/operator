﻿namespace IBP.PaymentProject2.Oper.Forms
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public class TablePanel : Control
    {
        protected Color _backGroundColor;
        private Bitmap _bmp;
        protected bool _needDraw;
        private Color _textColor;
        internal const char Infinity = '∞';

        public TablePanel(Control owner, Point position, Size size)
        {
            EventHandler handler = null;
            this._backGroundColor = Color.White;
            this._textColor = Color.Black;
            this._needDraw = true;
            if (owner == null)
            {
                throw new ArgumentException(UserStrings.NoParentForm);
            }
            base.Location = position;
            this.DoubleBuffered = true;
            base.Size = size;
            owner.Controls.Add(this);
            if (handler == null)
            {
                handler = delegate {
                    base.Invalidate();
                };
            }
            base.Resize += handler;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            int height = base.Height;
            int width = base.Width;
            bool flag = (this._bmp == null) || ((this._bmp.Height != height) || (this._bmp.Width != width));
            if (this._needDraw || flag)
            {
                if (flag)
                {
                    this._bmp = new Bitmap(width, height);
                }
                Graphics g = Graphics.FromImage(this._bmp);
                using (Brush brush = new SolidBrush(this._backGroundColor))
                {
                    g.FillRectangle(brush, 0, 0, width, height);
                }
                using (Pen pen = new Pen(Color.Black, 1f))
                {
                    g.DrawRectangle(pen, 1, 1, width - 2, height - 2);
                }
                this.PaintSelection(g);
                using (SolidBrush brush2 = new SolidBrush(this._textColor))
                {
                    g.DrawString(this.Text, this.Font, brush2, new RectangleF(new PointF(8f, 8f), new SizeF((float) (width - 0x10), (float) (height - 0x10))));
                }
                this._needDraw = false;
            }
            e.Graphics.DrawImageUnscaled(this._bmp, 0, 0);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        protected virtual void PaintSelection(Graphics g)
        {
        }

        public SizeF renderSizeText(int width)
        {
            SizeF ef;
            using (Bitmap bitmap = new Bitmap(1, 1))
            {
                Graphics graphics = Graphics.FromImage(bitmap);
                if (this.Text.Length == 0)
                {
                    ef = new SizeF(0f, 0f);
                }
                else
                {
                    ef = graphics.MeasureString(this.Text, this.Font, (int) (width - 0x10));
                }
            }
            return new SizeF(ef.Width + 16f, ef.Height + 16f);
        }

        public Color BackGroundColor
        {
            get
            {
                return this._backGroundColor;
            }
            set
            {
                if (this._backGroundColor != value)
                {
                    this._backGroundColor = value;
                    this._needDraw = true;
                    base.Invalidate();
                }
            }
        }

        public override System.Drawing.Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                if (base.Font != value)
                {
                    base.Font = value;
                    if (this.Text.Length > 0)
                    {
                        this._needDraw = true;
                        base.Invalidate();
                    }
                }
            }
        }

        public int RequredWordLength
        {
            get
            {
                SizeF ef;
                using (Bitmap bitmap = new Bitmap(1, 1))
                {
                    Graphics graphics = Graphics.FromImage(bitmap);
                    string[] strArray = this.Text.Split(new char[] { ' ' });
                    int length = strArray.Length;
                    if (length == 0)
                    {
                        return 0;
                    }
                    int num3 = 0;
                    int index = 0;
                    for (int i = 0; i < length; i++)
                    {
                        int num2 = strArray[i].Length;
                        if (num3 < num2)
                        {
                            num3 = num2;
                            index = i;
                        }
                    }
                    ef = graphics.MeasureString(strArray[index], this.Font);
                }
                return (((int) ef.Width) + 5);
            }
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (base.Text != value)
                {
                    base.Text = value;
                    this._needDraw = true;
                    base.Invalidate();
                }
            }
        }

        public Color TextColor
        {
            get
            {
                return this._textColor;
            }
            set
            {
                if (this._textColor != value)
                {
                    this._needDraw = true;
                    this._textColor = value;
                    base.Invalidate();
                }
            }
        }
    }
}

