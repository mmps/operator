﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Globalization;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;

    internal class ContractsListForm : Form
    {
        private Button _activate;
        private Button _add;
        private bool _canActivate;
        private bool _canDelete;
        private bool _canOpen;
        private Guid _clientId;
        private string _clientName;
        private Button _close;
        private ContractComparer _comparer = new ContractComparer();
        private List<DisplayContract> _contracts;
        private IBP.PaymentProject2.Oper.IsolationCookie _isolationCookie;
        protected ListViewEdit _list;
        private Button _open;
        private Button _remove;
        private Button _restore;
        private CheckBox _showDeleted;
        private const string ACTIVATE_CONTRACT = "ActivateSelectedContract";
        private const string ADD_CONTRACT = "AddNewContract";
        private IContainer components;
        private const string LOCK_CONTRACT = "LockAndGetContractLastVersion";
        private const string RELEASE_CONTRACT = "ReleaseContract";
        private const string REMOVE_CONTRACT = "RemoveSelectedContract";
        private const string RESTORE_CONTRACT = "RestoreSelectedContract";
        private const string SAVE_CONTRACT = "SaveContract";

        public event EventHandler<ContractEventArgs> ActivateSelectedContract
        {
            add
            {
                base.Events.AddHandler("ActivateSelectedContract", value);
            }
            remove
            {
                base.Events.RemoveHandler("ActivateSelectedContract", value);
            }
        }

        public event EventHandler<ContractEventArgs> AddNewContract
        {
            add
            {
                base.Events.AddHandler("AddNewContract", value);
            }
            remove
            {
                base.Events.RemoveHandler("AddNewContract", value);
            }
        }

        public event IBP.PaymentProject2.Oper.Contracts.LoadOperator LoadOperator;

        public event IBP.PaymentProject2.Oper.Contracts.LoadRecipient LoadRecipient;

        public event EventHandler<ContractEventArgs> LockAndGetContractLastVersion
        {
            add
            {
                base.Events.AddHandler("LockAndGetContractLastVersion", value);
            }
            remove
            {
                base.Events.RemoveHandler("LockAndGetContractLastVersion", value);
            }
        }

        public event EventHandler<ContractEventArgs> ReleaseContract
        {
            add
            {
                base.Events.AddHandler("ReleaseContract", value);
            }
            remove
            {
                base.Events.RemoveHandler("ReleaseContract", value);
            }
        }

        public event EventHandler<ContractEventArgs> RemoveSelectedContract
        {
            add
            {
                base.Events.AddHandler("RemoveSelectedContract", value);
            }
            remove
            {
                base.Events.RemoveHandler("RemoveSelectedContract", value);
            }
        }

        public event EventHandler<ContractEventArgs> RestoreSelectedContract
        {
            add
            {
                base.Events.AddHandler("RestoreSelectedContract", value);
            }
            remove
            {
                base.Events.RemoveHandler("RestoreSelectedContract", value);
            }
        }

        public event EventHandler<ContractEventArgs> SaveContract
        {
            add
            {
                base.Events.AddHandler("SaveContract", value);
            }
            remove
            {
                base.Events.RemoveHandler("SaveContract", value);
            }
        }

        public ContractsListForm(DisplayContract[] contracts, string clientName, Guid clientId)
        {
            this.InitializeComponent();
            this.Text = string.Format(ContractStrings.WindowCaptionFormat, clientName);
            this._contracts = new List<DisplayContract>(contracts);
            this._clientId = clientId;
            this._clientName = clientName;
            foreach (DisplayContract contract in this._contracts)
            {
                contract.GetOperatorsData += new GetDataDelegate(this.GetOperatorsData);
            }
            this._list.ItemAdded += new EventHandler<ItemEventArgs>(this.OnItemAdded);
            this._list.ItemChanged += new EventHandler<ItemEventArgs>(this.OnItemAdded);
            this._canActivate = Thread.CurrentPrincipal.IsInRole(UserRole.contractActivator.ToString());
            this._canDelete = Thread.CurrentPrincipal.IsInRole(UserRole.contractController.ToString()) || this._canActivate;
            this._canOpen = (Thread.CurrentPrincipal.IsInRole(UserRole.contractEditor.ToString()) || this._canDelete) || Thread.CurrentPrincipal.IsInRole(UserRole.contractViewer.ToString());
            this._add.Enabled = this._canDelete;
            this._activate.Enabled = this._open.Enabled = this._remove.Enabled = this._restore.Enabled = false;
        }

        private void ActivateContract(object sender, EventArgs e)
        {
            DisplayContract selectedItem = this.SelectedItem;
            if ((selectedItem != null) && (MessageBox.Show(ContractStrings.AreYouWantActivate, ContractStrings.SelectedContracts, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                selectedItem.Contract.Activated = true;
                this._list.RefreshItem(selectedItem, new ContractComparer());
                this.RaiseActivateSelectedContract();
            }
        }

        private void AddContract(object sender, EventArgs e)
        {
            DisplayContract viewItem = new DisplayContract(new Contract());
            viewItem.Contract.ClientId = this._clientId;
            viewItem.ClientName = this._clientName;
            viewItem.Contract.CreateDate = DateTime.Today;
            viewItem.GetOperatorsData += new GetDataDelegate(this.GetOperatorsData);
            using (ContractViewForm form = new ContractViewForm(viewItem, false))
            {
                form.LoadRecipient += new IBP.PaymentProject2.Oper.Contracts.LoadRecipient(this.FormLoadRecipient);
                form.LoadOperator += new IBP.PaymentProject2.Oper.Contracts.LoadOperator(this.FormLoadOperator);
                form.SetEditMode(true);
                form.LockAndLoadContract += delegate (object s, ContractEventArgs args) {
                    throw new NotImplementedException("SetLock");
                };
                if (form.ShowDialog() == DialogResult.OK)
                {
                    this.RaiseAddNewContract(form.Contract);
                    viewItem = form.Contract;
                    this._contracts.Add(viewItem);
                    this._list.Add(viewItem);
                }
            }
        }

        private void CloseClick(object sender, EventArgs e)
        {
            base.Close();
        }

        private void ContractsListForm_Load(object sender, EventArgs e)
        {
            this.FillList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillList()
        {
            try
            {
                this._list.SuspendLayout();
                this._list.Items.Clear();
                if (this._showDeleted.Checked)
                {
                    this._list.AddRange(this._contracts.ToArray());
                }
                else
                {
                    foreach (DisplayContract contract in this._contracts)
                    {
                        if (!contract.Contract.Removed)
                        {
                            this._list.Add(contract);
                        }
                    }
                }
            }
            finally
            {
                this._list.ResumeLayout();
            }
        }

        public static bool FindAndShowContractListWindow(Guid contractId)
        {
            foreach (Form form in Application.OpenForms)
            {
                ContractsListForm form2 = form as ContractsListForm;
                if ((form2 != null) && (form2.ClientId == contractId))
                {
                    form2.Select();
                    form2.Show();
                    return true;
                }
            }
            return false;
        }

        private Operator[] FormLoadOperator(int id)
        {
            if (this.LoadOperator != null)
            {
                return this.LoadOperator(id);
            }
            return new Operator[0];
        }

        private LightRecipient[] FormLoadRecipient(Guid id)
        {
            if (this.LoadRecipient != null)
            {
                return this.LoadRecipient(id);
            }
            return new LightRecipient[0];
        }

        private string GetOperatorsData(int[] operators)
        {
            if (this.LoadOperator == null)
            {
                return string.Empty;
            }
            int length = operators.Length;
            if (length <= 0)
            {
                return string.Empty;
            }
            StringBuilder builder = new StringBuilder(length * 10);
            for (int i = 0; i < length; i++)
            {
                Operator[] operatorArray = this.LoadOperator(operators[i]);
                if (operatorArray.Length > 0)
                {
                    builder.Append(operatorArray[0].Name);
                    builder.Append(';');
                }
                else
                {
                    builder.Append("<Not_Found>");
                    builder.Append(';');
                }
            }
            int num3 = builder.Length;
            return builder.ToString(0, num3 - 1);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ContractsListForm));
            this._showDeleted = new CheckBox();
            this._add = new Button();
            this._open = new Button();
            this._remove = new Button();
            this._restore = new Button();
            this._activate = new Button();
            this._close = new Button();
            this._list = new ListViewEdit();
            base.SuspendLayout();
            manager.ApplyResources(this._showDeleted, "_showDeleted");
            this._showDeleted.Name = "_showDeleted";
            this._showDeleted.UseVisualStyleBackColor = true;
            this._showDeleted.CheckedChanged += new EventHandler(this.ShowDeletedChecked);
            manager.ApplyResources(this._add, "_add");
            this._add.Name = "_add";
            this._add.UseVisualStyleBackColor = true;
            this._add.Click += new EventHandler(this.AddContract);
            manager.ApplyResources(this._open, "_open");
            this._open.Name = "_open";
            this._open.UseVisualStyleBackColor = true;
            this._open.Click += new EventHandler(this.OpenContract);
            manager.ApplyResources(this._remove, "_remove");
            this._remove.Name = "_remove";
            this._remove.UseVisualStyleBackColor = true;
            this._remove.Click += new EventHandler(this.RemoveContract);
            manager.ApplyResources(this._restore, "_restore");
            this._restore.Name = "_restore";
            this._restore.UseVisualStyleBackColor = true;
            this._restore.Click += new EventHandler(this.RestoreContract);
            manager.ApplyResources(this._activate, "_activate");
            this._activate.Name = "_activate";
            this._activate.UseVisualStyleBackColor = true;
            this._activate.Click += new EventHandler(this.ActivateContract);
            manager.ApplyResources(this._close, "_close");
            this._close.Name = "_close";
            this._close.UseVisualStyleBackColor = true;
            this._close.Click += new EventHandler(this.CloseClick);
            this._list.AllowSort = true;
            this._list.BindType = typeof(DisplayContract);
            manager.ApplyResources(this._list, "_list");
            this._list.ColumnsCollection = (ListViewEditColumnsCollection) manager.GetObject("_list.ColumnsCollection");
            this._list.FormatProvider = new CultureInfo("ru-RU");
            this._list.FullRowSelect = true;
            this._list.GridLines = true;
            this._list.HideSelection = false;
            this._list.MultiSelect = false;
            this._list.Name = "_list";
            this._list.ReadOnly = true;
            this._list.UseCompatibleStateImageBehavior = false;
            this._list.View = View.Details;
            this._list.SelectedIndexChanged += new EventHandler(this.ListSelectionChanged);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._close);
            base.Controls.Add(this._activate);
            base.Controls.Add(this._restore);
            base.Controls.Add(this._remove);
            base.Controls.Add(this._open);
            base.Controls.Add(this._add);
            base.Controls.Add(this._showDeleted);
            base.Controls.Add(this._list);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ContractsListForm";
            base.ShowIcon = false;
            base.Load += new EventHandler(this.ContractsListForm_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void ListSelectionChanged(object sender, EventArgs e)
        {
            if (this._list.SelectedItems.Count > 0)
            {
                DisplayContract tag = (DisplayContract) this._list.SelectedItems[0].Tag;
                if (!tag.Contract.Activated)
                {
                    this._activate.Enabled = this._canActivate;
                }
                if (tag.Contract.Removed)
                {
                    this._restore.Enabled = this._canDelete;
                    this._remove.Enabled = false;
                }
                else
                {
                    this._remove.Enabled = this._canDelete;
                    this._restore.Enabled = false;
                }
                this._open.Enabled = this._canOpen;
            }
            else
            {
                this._open.Enabled = this._remove.Enabled = this._restore.Enabled = this._activate.Enabled = false;
            }
        }

        private void OnItemAdded(object sender, ItemEventArgs e)
        {
            Color removed;
            DisplayContract tag = (DisplayContract) e.Item.Tag;
            if (tag.Contract.Removed)
            {
                removed = ContractSettings.Default.Removed;
            }
            else if (tag.Contract.Activated)
            {
                removed = ContractSettings.Default.Activated;
            }
            else
            {
                removed = ContractSettings.Default.Deactivated;
            }
            e.Item.BackColor = removed;
        }

        private void OpenContract(object sender, EventArgs e)
        {
            DisplayContract selectedItem = this.SelectedItem;
            if (selectedItem != null)
            {
                selectedItem = new DisplayContract((Contract) selectedItem.Contract.Clone());
                selectedItem.GetOperatorsData += new GetDataDelegate(this.GetOperatorsData);
                if (string.IsNullOrEmpty(selectedItem.ClientName))
                {
                    selectedItem.ClientName = this._clientName;
                }
                using (ContractViewForm form = new ContractViewForm(selectedItem, true))
                {
                    form.LoadRecipient += new IBP.PaymentProject2.Oper.Contracts.LoadRecipient(this.FormLoadRecipient);
                    form.LoadOperator += new IBP.PaymentProject2.Oper.Contracts.LoadOperator(this.FormLoadOperator);
                    bool edited = false;
                    form.LockAndLoadContract += delegate (object s, ContractEventArgs args) {
                        this.RaiseLockAndGetContractLastVersion(args);
                        edited = args.Successfull;
                    };
                    DialogResult result = form.ShowDialog();
                    if (edited)
                    {
                        if (result == DialogResult.OK)
                        {
                            DisplayContract contract = form.Contract;
                            this._list.RefreshItem(contract, this._comparer);
                            this.RaiseSaveContract();
                        }
                        this.RaiseReleaseContract();
                    }
                }
            }
        }

        private bool RaiseActivateSelectedContract()
        {
            Delegate delegate2 = base.Events["ActivateSelectedContract"];
            if (delegate2 != null)
            {
                ContractEventArgs e = new ContractEventArgs(this.SelectedItem);
                ((EventHandler<ContractEventArgs>) delegate2)(this, e);
                return e.Successfull;
            }
            return false;
        }

        private bool RaiseAddNewContract(DisplayContract contract)
        {
            Delegate delegate2 = base.Events["AddNewContract"];
            if (delegate2 != null)
            {
                ContractEventArgs e = new ContractEventArgs(contract);
                ((EventHandler<ContractEventArgs>) delegate2)(this, e);
                return e.Successfull;
            }
            return false;
        }

        private void RaiseLockAndGetContractLastVersion(ContractEventArgs args)
        {
            Delegate delegate2 = base.Events["LockAndGetContractLastVersion"];
            if (delegate2 != null)
            {
                ((EventHandler<ContractEventArgs>) delegate2)(this, args);
            }
        }

        private bool RaiseReleaseContract()
        {
            Delegate delegate2 = base.Events["ReleaseContract"];
            if (delegate2 != null)
            {
                ContractEventArgs e = new ContractEventArgs(this.SelectedItem);
                ((EventHandler<ContractEventArgs>) delegate2)(this, e);
                return e.Successfull;
            }
            return false;
        }

        private bool RaiseRemoveSelectedContract()
        {
            Delegate delegate2 = base.Events["RemoveSelectedContract"];
            if (delegate2 != null)
            {
                ContractEventArgs e = new ContractEventArgs(this.SelectedItem);
                ((EventHandler<ContractEventArgs>) delegate2)(this, e);
                return e.Successfull;
            }
            return false;
        }

        private bool RaiseRestoreContract()
        {
            Delegate delegate2 = base.Events["RestoreSelectedContract"];
            if (delegate2 != null)
            {
                ContractEventArgs e = new ContractEventArgs(this.SelectedItem);
                ((EventHandler<ContractEventArgs>) delegate2)(this, e);
                return e.Successfull;
            }
            return false;
        }

        private bool RaiseSaveContract()
        {
            Delegate delegate2 = base.Events["SaveContract"];
            if (delegate2 != null)
            {
                ContractEventArgs e = new ContractEventArgs(this.SelectedItem);
                ((EventHandler<ContractEventArgs>) delegate2)(this, e);
                return e.Successfull;
            }
            return false;
        }

        private void RemoveContract(object sender, EventArgs e)
        {
            DisplayContract selectedItem = this.SelectedItem;
            if ((selectedItem != null) && (MessageBox.Show(ContractStrings.AreYouWantRemove, ContractStrings.SelectedContracts, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                selectedItem.Contract.Removed = true;
                this.RaiseRemoveSelectedContract();
                this.FillList();
            }
        }

        private void RestoreContract(object sender, EventArgs e)
        {
            DisplayContract selectedItem = this.SelectedItem;
            if ((selectedItem != null) && (MessageBox.Show(ContractStrings.AreYouWantRestore, ContractStrings.SelectedContracts, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                selectedItem.Contract.Removed = false;
                this.RaiseRestoreContract();
                this.FillList();
            }
        }

        private void ShowDeletedChecked(object sender, EventArgs e)
        {
            this.FillList();
        }

        public Guid ClientId
        {
            get
            {
                return this._clientId;
            }
        }

        public IBP.PaymentProject2.Oper.IsolationCookie IsolationCookie
        {
            get
            {
                return this._isolationCookie;
            }
            set
            {
                this._isolationCookie = value;
            }
        }

        private DisplayContract SelectedItem
        {
            get
            {
                if (this._list.SelectedItems.Count > 0)
                {
                    return (DisplayContract) this._list.SelectedItems[0].Tag;
                }
                return null;
            }
        }

        private class ContractComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                DisplayContract contract = (DisplayContract) x;
                DisplayContract contract2 = (DisplayContract) y;
                return contract.Contract.Id.CompareTo(contract2.Contract.Id);
            }
        }
    }
}

