﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class ChooseOperatorForm : Form
    {
        private Operator[] _allOperators;
        private Button _cancel;
        private ListBox _list;
        private Button _ok;
        private TextBox _partName;
        private IContainer components;

        public ChooseOperatorForm(Operator[] allOperators)
        {
            this.InitializeComponent();
            this._allOperators = allOperators;
            this.FillList();
        }

        private void _partName_KeyUp(object sender, KeyEventArgs e)
        {
            this.FillList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillList()
        {
            string text = this._partName.Text;
            try
            {
                this._list.SuspendLayout();
                this._list.Items.Clear();
                if (text.Length == 0)
                {
                    this._list.Items.AddRange(this._allOperators);
                }
                else
                {
                    text = text.ToLower();
                    foreach (Operator @operator in this._allOperators)
                    {
                        if (@operator.Name.ToLower().Contains(text))
                        {
                            this._list.Items.Add(@operator);
                        }
                    }
                }
            }
            finally
            {
                this._list.ResumeLayout();
            }
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ChooseOperatorForm));
            this._ok = new Button();
            this._cancel = new Button();
            this._partName = new TextBox();
            this._list = new ListBox();
            Label label = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "_labelName");
            label.Name = "_labelName";
            manager.ApplyResources(this._ok, "_ok");
            this._ok.DialogResult = DialogResult.OK;
            this._ok.Name = "_ok";
            this._ok.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._cancel, "_cancel");
            this._cancel.DialogResult = DialogResult.Cancel;
            this._cancel.Name = "_cancel";
            this._cancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._partName, "_partName");
            this._partName.Name = "_partName";
            this._partName.KeyUp += new KeyEventHandler(this._partName_KeyUp);
            this._list.FormattingEnabled = true;
            manager.ApplyResources(this._list, "_list");
            this._list.Name = "_list";
            this._list.SelectionMode = SelectionMode.MultiSimple;
            base.AcceptButton = this._ok;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._cancel;
            base.Controls.Add(this._list);
            base.Controls.Add(this._partName);
            base.Controls.Add(label);
            base.Controls.Add(this._cancel);
            base.Controls.Add(this._ok);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ChooseOperatorForm";
            base.ShowIcon = false;
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public Operator[] Selected
        {
            get
            {
                List<Operator> list = new List<Operator>(this._list.SelectedItems.Count);
                foreach (Operator @operator in this._list.SelectedItems)
                {
                    list.Add(@operator);
                }
                return list.ToArray();
            }
        }
    }
}

