﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common.Component;
    using System;

    public enum AllowType
    {
        [DisplayName(typeof(ContractStrings), "Allowed")]
        Allowed = 0,
        [DisplayName(typeof(ContractStrings), "Blocked")]
        Blocked = 1,
        [DisplayName(typeof(ContractStrings), "Exclusive")]
        Exclusive = 2
    }
}

