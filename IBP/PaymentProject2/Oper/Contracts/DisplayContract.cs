﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Component;
    using System;
    using System.ComponentModel;
    using System.Threading;

    public class DisplayContract
    {
        private string _clientName;
        private IBP.PaymentProject2.Common.Contract _contract;

        public event GetDataDelegate GetOperatorsData;

        public DisplayContract(IBP.PaymentProject2.Common.Contract contract)
        {
            this._contract = contract;
        }

        public string ClientName
        {
            get
            {
                return this._clientName;
            }
            set
            {
                this._clientName = value;
            }
        }

        public IBP.PaymentProject2.Common.Contract Contract
        {
            get
            {
                return this._contract;
            }
            set
            {
                this._contract = value;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(ContractStrings), "DisplayNumber"), Browsable(true)]
        public string DisplayNumber
        {
            get
            {
                return this._contract.Number;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(ContractStrings), "CreationDate")]
        public DateTime DisplayTime
        {
            get
            {
                return this._contract.CreateDate;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(ContractStrings), "ExternalNumber"), Browsable(true)]
        public string ExternalNumber
        {
            get
            {
                return this._contract.ExternalId;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(ContractStrings), "Operators")]
        public string OperatorStrings
        {
            get
            {
                if (this.GetOperatorsData == null)
                {
                    return string.Empty;
                }
                int[] operatorsId = this._contract.OperatorsId;
                return this.GetOperatorsData(operatorsId);
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(ContractStrings), "Status")]
        public string StringStatus
        {
            get
            {
                if (this._contract.Removed)
                {
                    return ContractStrings.Removed;
                }
                if (this._contract.Activated)
                {
                    return ContractStrings.Activated;
                }
                return ContractStrings.Deactivated;
            }
        }
    }
}

