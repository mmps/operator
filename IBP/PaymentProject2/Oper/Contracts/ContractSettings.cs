﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class ContractSettings : ApplicationSettingsBase
    {
        private static ContractSettings defaultInstance = ((ContractSettings) SettingsBase.Synchronized(new ContractSettings()));

        [DefaultSettingValue("192, 255, 192"), UserScopedSetting, DebuggerNonUserCode]
        public Color Activated
        {
            get
            {
                return (Color) this["Activated"];
            }
            set
            {
                this["Activated"] = value;
            }
        }

        [DebuggerNonUserCode, DefaultSettingValue("224, 224, 224"), UserScopedSetting]
        public Color Deactivated
        {
            get
            {
                return (Color) this["Deactivated"];
            }
            set
            {
                this["Deactivated"] = value;
            }
        }

        public static ContractSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DebuggerNonUserCode, DefaultSettingValue("255, 192, 192"), UserScopedSetting]
        public Color Removed
        {
            get
            {
                return (Color) this["Removed"];
            }
            set
            {
                this["Removed"] = value;
            }
        }
    }
}

