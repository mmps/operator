﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common.Contracts;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class ChooseRecipientForm : Form
    {
        private LightRecipient[] _allRecipients;
        private Button _cancel;
        private ListBox _list;
        private Button _ok;
        private TextBox _partName;
        private RadioButton _rbBlocked;
        private RadioButton _rbExclusive;
        private IContainer components;
        private Button selectAll;

        public ChooseRecipientForm(LightRecipient[] allRecipients)
        {
            this.InitializeComponent();
            this._allRecipients = allRecipients;
            this.selectAll.Enabled = this._allRecipients.Length != 0;
            this.FillList();
        }

        private void _partName_KeyUp(object sender, KeyEventArgs e)
        {
            this.FillList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillList()
        {
            string text = this._partName.Text;
            try
            {
                this._list.SuspendLayout();
                this._list.Items.Clear();
                SortedList<string, LightRecipient> list = new SortedList<string, LightRecipient>(this._allRecipients.Length);
                if (text.Length == 0)
                {
                    foreach (LightRecipient recipient in this._allRecipients)
                    {
                        string key = string.Format("{0}_{1}", recipient.Name, recipient.Id);
                        list.Add(key, recipient);
                    }
                }
                else
                {
                    text = text.ToLower();
                    foreach (LightRecipient recipient2 in this._allRecipients)
                    {
                        if (recipient2.Name.ToLower().Contains(text))
                        {
                            string.Format("{0}_{1}", recipient2.Name, recipient2.Id);
                            list.Add(recipient2.Name, recipient2);
                        }
                    }
                }
                foreach (LightRecipient recipient3 in list.Values)
                {
                    this._list.Items.Add(recipient3);
                }
            }
            finally
            {
                this._list.ResumeLayout();
            }
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ChooseRecipientForm));
            this._rbBlocked = new RadioButton();
            this._rbExclusive = new RadioButton();
            this._ok = new Button();
            this._cancel = new Button();
            this._partName = new TextBox();
            this._list = new ListBox();
            this.selectAll = new Button();
            Label label = new Label();
            GroupBox box = new GroupBox();
            box.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(label, "_labelName");
            label.Name = "_labelName";
            manager.ApplyResources(box, "_gbType");
            box.Controls.Add(this._rbBlocked);
            box.Controls.Add(this._rbExclusive);
            box.Name = "_gbType";
            box.TabStop = false;
            manager.ApplyResources(this._rbBlocked, "_rbBlocked");
            this._rbBlocked.Name = "_rbBlocked";
            this._rbBlocked.UseVisualStyleBackColor = true;
            this._rbBlocked.CheckedChanged += new EventHandler(this.RadioButtonChecked);
            manager.ApplyResources(this._rbExclusive, "_rbExclusive");
            this._rbExclusive.Name = "_rbExclusive";
            this._rbExclusive.UseVisualStyleBackColor = true;
            this._rbExclusive.CheckedChanged += new EventHandler(this.RadioButtonChecked);
            manager.ApplyResources(this._ok, "_ok");
            this._ok.Name = "_ok";
            this._ok.UseVisualStyleBackColor = true;
            this._ok.Click += new EventHandler(this.OkClick);
            manager.ApplyResources(this._cancel, "_cancel");
            this._cancel.DialogResult = DialogResult.Cancel;
            this._cancel.Name = "_cancel";
            this._cancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._partName, "_partName");
            this._partName.Name = "_partName";
            this._partName.KeyUp += new KeyEventHandler(this._partName_KeyUp);
            manager.ApplyResources(this._list, "_list");
            this._list.FormattingEnabled = true;
            this._list.Name = "_list";
            this._list.SelectionMode = SelectionMode.MultiSimple;
            manager.ApplyResources(this.selectAll, "selectAll");
            this.selectAll.Name = "selectAll";
            this.selectAll.UseVisualStyleBackColor = true;
            this.selectAll.Click += new EventHandler(this.SelectAllClick);
            base.AcceptButton = this._ok;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._cancel;
            base.Controls.Add(this.selectAll);
            base.Controls.Add(this._list);
            base.Controls.Add(this._cancel);
            base.Controls.Add(this._ok);
            base.Controls.Add(box);
            base.Controls.Add(this._partName);
            base.Controls.Add(label);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ChooseRecipientForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            box.ResumeLayout(false);
            box.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void OkClick(object sender, EventArgs e)
        {
            if (this.RecipientsType == RecipientType.Exclusive)
            {
                if (MessageBox.Show(ContractStrings.AreYouSureMakeExclusive, ContractStrings.AddExclusiveCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    base.DialogResult = DialogResult.OK;
                }
            }
            else
            {
                base.DialogResult = DialogResult.OK;
            }
        }

        private void RadioButtonChecked(object sender, EventArgs e)
        {
            this._ok.Enabled = true;
        }

        private void SelectAllClick(object sender, EventArgs e)
        {
            int count = this._list.SelectedItems.Count;
            int num2 = this._list.Items.Count;
            bool flag = count != num2;
            try
            {
                base.SuspendLayout();
                for (int i = 0; i < num2; i++)
                {
                    this._list.SetSelected(i, flag);
                }
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        public RecipientType RecipientsType
        {
            get
            {
                if (this._rbBlocked.Checked)
                {
                    return RecipientType.Blocked;
                }
                if (!this._rbExclusive.Checked)
                {
                    throw new InvalidProgramException();
                }
                return RecipientType.Exclusive;
            }
        }

        public LightRecipient[] Selected
        {
            get
            {
                List<LightRecipient> list = new List<LightRecipient>(this._list.SelectedItems.Count);
                foreach (LightRecipient recipient in this._list.SelectedItems)
                {
                    list.Add(recipient);
                }
                return list.ToArray();
            }
        }
    }
}

