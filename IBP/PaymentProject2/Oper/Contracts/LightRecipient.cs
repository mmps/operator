﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common;
    using System;

    public class LightRecipient
    {
        private Guid _id;
        private string _name;
        private int _operatorId;
        private int _serial;
        private AllowType _type;

        public LightRecipient()
        {
        }

        public LightRecipient(Recipient recipient, AllowType type)
        {
            this._serial = recipient.Serial;
            this._id = recipient.Id;
            this._name = recipient.Name;
            this._type = type;
            this._operatorId = recipient.OperatorId;
        }

        public override string ToString()
        {
            return this._name;
        }

        public Guid Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public int OperatorId
        {
            get
            {
                return this._operatorId;
            }
        }

        public int Serial
        {
            get
            {
                return this._serial;
            }
            set
            {
                this._serial = value;
            }
        }

        public AllowType Type
        {
            get
            {
                return this._type;
            }
            set
            {
                this._type = value;
            }
        }
    }
}

