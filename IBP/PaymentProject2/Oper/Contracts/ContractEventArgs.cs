﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using System;

    internal class ContractEventArgs : EventArgs
    {
        public IBP.PaymentProject2.Oper.Contracts.DisplayContract DisplayContract;
        public bool Successfull = true;

        public ContractEventArgs(IBP.PaymentProject2.Oper.Contracts.DisplayContract displayContract)
        {
            this.DisplayContract = displayContract;
        }
    }
}

