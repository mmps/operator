﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Isolation;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections.Generic;
    using System.Security.Principal;
    using System.Threading;

    internal class ShowContractsCommand : Command
    {
        public override void Execute()
        {
            Client currentClient = this.CurrentClient;
            if (!ContractsListForm.FindAndShowContractListWindow(currentClient.Id))
            {
                try
                {
                    string[] currentUserRolles = IBP.PaymentProject2.Oper.ServerFasad.Server.GetCurrentUserRolles();
                    IPrincipal principal = new GenericPrincipal(Thread.CurrentPrincipal.Identity, currentUserRolles);
                    Thread.CurrentPrincipal = principal;
                    if (((principal.IsInRole(UserRole.contractViewer.ToString()) || principal.IsInRole(UserRole.contractEditor.ToString())) || principal.IsInRole(UserRole.contractController.ToString())) || principal.IsInRole(UserRole.contractActivator.ToString()))
                    {
                        try
                        {
                            Contract[] contractArray = IBP.PaymentProject2.Oper.ServerFasad.Server.FindContracts(currentClient.Id);
                            List<DisplayContract> list = new List<DisplayContract>(contractArray.Length);
                            foreach (Contract contract in contractArray)
                            {
                                DisplayContract item = new DisplayContract(contract);
                                list.Add(item);
                            }
                            ContractsListForm form = new ContractsListForm(list.ToArray(), currentClient.Name, currentClient.Id);
                            form.AddNewContract += new EventHandler<ContractEventArgs>(this.form_AddContract);
                            form.LockAndGetContractLastVersion += new EventHandler<ContractEventArgs>(this.LockAndGetContractLastVersion);
                            form.ReleaseContract += new EventHandler<ContractEventArgs>(this.form_ReleaseContract);
                            form.SaveContract += new EventHandler<ContractEventArgs>(this.form_SaveContract);
                            form.ActivateSelectedContract += new EventHandler<ContractEventArgs>(this.form_ActivateSelectedContract);
                            form.RemoveSelectedContract += new EventHandler<ContractEventArgs>(this.form_RemoveSelectedContract);
                            form.RestoreSelectedContract += new EventHandler<ContractEventArgs>(this.form_RestoreSelectedContract);
                            form.LoadOperator += new IBP.PaymentProject2.Oper.Contracts.LoadOperator(this.LoadOperator);
                            form.LoadRecipient += new IBP.PaymentProject2.Oper.Contracts.LoadRecipient(this.LoadRecipient);
                            form.Show();
                        }
                        catch (SrvException exception)
                        {
                            base.ShowError(ContractStrings.GetContractsErrorFormat, new object[] { exception.Message });
                        }
                    }
                    else
                    {
                        base.ShowError(ContractStrings.YouHaveNoRulesToViewContracts, new object[] { 0 });
                    }
                }
                catch (SrvException exception2)
                {
                    base.ShowError(UserStrings.GetUserRolesErrorFormat, new object[] { exception2.Message });
                }
            }
        }

        private void form_ActivateSelectedContract(object sender, ContractEventArgs e)
        {
            try
            {
                Contract isolableObject = e.DisplayContract.Contract;
                IsolationCookie cookie = IsolationCookie.Create(IBP.PaymentProject2.Oper.ServerFasad.Server, isolableObject);
                int isolationId = ((IIsolableObject) isolableObject).IsolationId;
                IBP.PaymentProject2.Oper.ServerFasad.Server.ActivateContract(isolableObject.Id, isolationId);
                cookie.Release();
            }
            catch (IsolationSrvException exception)
            {
                base.ShowError(ContractStrings.LoseIsolation, new object[] { exception.Message });
                e.Successfull = false;
            }
            catch (SrvException exception2)
            {
                base.ShowError(ContractStrings.ActivateContractErrorFormat, new object[] { exception2.Message });
                e.Successfull = false;
            }
        }

        private void form_AddContract(object sender, ContractEventArgs e)
        {
            try
            {
                int num = IBP.PaymentProject2.Oper.ServerFasad.Server.AddContract(e.DisplayContract.Contract);
                e.DisplayContract.Contract.Serial = num;
            }
            catch (SrvException exception)
            {
                base.ShowError(ContractStrings.AddContractErrorFormat, new object[] { exception.Message });
                e.Successfull = false;
            }
        }

        private void form_ReleaseContract(object sender, ContractEventArgs e)
        {
            ContractsListForm form = (ContractsListForm) sender;
            try
            {
                form.IsolationCookie.Release();
            }
            catch (IsolationSrvException exception)
            {
                base.ShowError(ContractStrings.LoseIsolation, new object[] { exception.Message });
                e.Successfull = false;
            }
        }

        private void form_RemoveSelectedContract(object sender, ContractEventArgs e)
        {
            try
            {
                Contract isolableObject = e.DisplayContract.Contract;
                IsolationCookie cookie = IsolationCookie.Create(IBP.PaymentProject2.Oper.ServerFasad.Server, isolableObject);
                int isolationId = ((IIsolableObject) isolableObject).IsolationId;
                IBP.PaymentProject2.Oper.ServerFasad.Server.RemoveContract(isolableObject.Id, isolationId);
                cookie.Release();
            }
            catch (IsolationSrvException exception)
            {
                base.ShowError(ContractStrings.LoseIsolation, new object[] { exception.Message });
                e.Successfull = false;
            }
            catch (SrvException exception2)
            {
                base.ShowError(ContractStrings.RemoveContractErrorFormat, new object[] { exception2.Message });
                e.Successfull = false;
            }
        }

        private void form_RestoreSelectedContract(object sender, ContractEventArgs e)
        {
            try
            {
                Contract isolableObject = e.DisplayContract.Contract;
                IsolationCookie cookie = IsolationCookie.Create(IBP.PaymentProject2.Oper.ServerFasad.Server, isolableObject);
                int isolationId = ((IIsolableObject) isolableObject).IsolationId;
                IBP.PaymentProject2.Oper.ServerFasad.Server.RestoreContract(isolableObject.Id, isolationId);
                cookie.Release();
            }
            catch (IsolationSrvException exception)
            {
                base.ShowError(ContractStrings.LoseIsolation, new object[] { exception.Message });
                e.Successfull = false;
            }
            catch (SrvException exception2)
            {
                base.ShowError(ContractStrings.RestoreContractErrorFormat, new object[] { exception2.Message });
                e.Successfull = false;
            }
        }

        private void form_SaveContract(object sender, ContractEventArgs e)
        {
            try
            {
                Contract contract = e.DisplayContract.Contract;
                IBP.PaymentProject2.Oper.ServerFasad.Server.SaveContract(contract);
            }
            catch (SrvException exception)
            {
                base.ShowError(ContractStrings.SaveContractErrorFormat, new object[] { exception.Message });
                e.Successfull = false;
            }
        }

        private Operator[] LoadOperator(int id)
        {
            if (id == -1)
            {
                return OperatorsContainer.Instance.ToArray();
            }
            if (OperatorsContainer.Instance.Contains(id))
            {
                return new Operator[] { OperatorsContainer.Instance[id] };
            }
            return new Operator[0];
        }

        private LightRecipient[] LoadRecipient(Guid id)
        {
            LightRecipient[] recipientArray;
            if (id == Guid.Empty)
            {
                Recipient[] recipientArray2 = RecipientsContainer.Instance.ToArray();
                int length = recipientArray2.Length;
                recipientArray = new LightRecipient[length];
                for (int i = 0; i < length; i++)
                {
                    recipientArray[i] = new LightRecipient(recipientArray2[i], AllowType.Allowed);
                }
                return recipientArray;
            }
            if (RecipientsContainer.Instance.Contains(id))
            {
                recipientArray = new LightRecipient[1];
                Recipient recipient = RecipientsContainer.Instance[id];
                recipientArray[0] = new LightRecipient(recipient, AllowType.Allowed);
                return recipientArray;
            }
            return new LightRecipient[0];
        }

        private void LockAndGetContractLastVersion(object sender, ContractEventArgs e)
        {
            Contract isolableObject = e.DisplayContract.Contract;
            ContractsListForm form = (ContractsListForm) sender;
            try
            {
                form.IsolationCookie = IsolationCookie.Create(IBP.PaymentProject2.Oper.ServerFasad.Server, isolableObject);
                int isolationId = ((IIsolableObject) isolableObject).IsolationId;
                isolableObject = IBP.PaymentProject2.Oper.ServerFasad.Server.GetContract(isolableObject.Id);
                ((IIsolableObject) isolableObject).IsolationId = isolationId;
                e.DisplayContract.Contract = isolableObject;
            }
            catch (IsolationSrvException exception)
            {
                base.ShowError(ContractStrings.LoseIsolation, new object[] { exception.Message });
                e.Successfull = false;
            }
            catch (SrvException exception2)
            {
                base.ShowError(ContractStrings.GetSingleContractErrorFormat, new object[] { exception2.Message });
                e.Successfull = false;
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.CurrentClient != null);
            }
        }

        private Client CurrentClient
        {
            get
            {
                return (base._tag as Client);
            }
        }
    }
}

