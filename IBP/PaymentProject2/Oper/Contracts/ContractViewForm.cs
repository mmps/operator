﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Forms;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading;
    using System.Windows.Forms;

    internal class ContractViewForm : Form
    {
        private Button _addOperator;
        private Button _attributes;
        private Button _cancel;
        private Label _clientName;
        private DisplayContract _contract;
        private DateTimePicker _createDate;
        private Button _editContract;
        private TextBox _externalId;
        private TextBox _filter;
        private TextBox _number;
        private Button _ok;
        private ListBox _operators;
        private DataGridView _recipientsList;
        private Button _removeOperator;
        private IContainer components;
        private DataGridViewTextBoxColumn Recipient;
        private DataGridViewComboBoxColumn Type;

        public event IBP.PaymentProject2.Oper.Contracts.LoadOperator LoadOperator;

        public event IBP.PaymentProject2.Oper.Contracts.LoadRecipient LoadRecipient;

        public event EventHandler<ContractEventArgs> LockAndLoadContract;

        public ContractViewForm(DisplayContract viewItem, bool edit)
        {
            this.Initialize(edit ? ContractStrings.Contract : ContractStrings.NewContract);
            this._contract = viewItem;
            this._clientName.Text = this._contract.ClientName;
            this._editContract.Enabled = (Thread.CurrentPrincipal.IsInRole(UserRole.contractActivator.ToString()) || Thread.CurrentPrincipal.IsInRole(UserRole.contractController.ToString())) || Thread.CurrentPrincipal.IsInRole(UserRole.contractEditor.ToString());
        }

        private void _filter_TextChanged(object sender, EventArgs e)
        {
            this.FillRecipients();
        }

        private void AddOperator(object sender, EventArgs e)
        {
            Operator[] operatorArray = this.LoadOperator(-1);
            List<int> list = new List<int>(this._contract.Contract.OperatorsId);
            List<Operator> list2 = new List<Operator>(operatorArray.Length);
            foreach (Operator @operator in operatorArray)
            {
                if (!list.Contains(@operator.Id))
                {
                    list2.Add(@operator);
                }
            }
            using (ChooseOperatorForm form = new ChooseOperatorForm(list2.ToArray()))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    Operator[] selected = form.Selected;
                    if (selected.Length > 0)
                    {
                        foreach (Operator operator2 in selected)
                        {
                            this._contract.Contract.AddOperator(operator2.Id);
                        }
                        this.FillOperators();
                        this.FillRecipients();
                    }
                }
            }
        }

        private bool CheckForBlocked(Guid id)
        {
            foreach (Guid guid in this._contract.Contract.BlockedRecipientsId)
            {
                if (guid == id)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckForExclusive(Guid id)
        {
            foreach (Guid guid in this._contract.Contract.ExclusiveRecipientId)
            {
                if (guid == id)
                {
                    return true;
                }
            }
            return false;
        }

        private bool CheckForOperatorId(int id)
        {
            foreach (int num in this._contract.Contract.OperatorsId)
            {
                if (id == num)
                {
                    return true;
                }
            }
            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void EditAttributes(object sender, EventArgs e)
        {
            EventHandler handler = null;
            IBP.PaymentProject2.Common.Contract clone = (IBP.PaymentProject2.Common.Contract) this._contract.Contract.Clone();
            using (ItemForm form = new ItemForm())
            {
                form.Item = clone;
                if (handler == null)
                {
                    handler = delegate {
                        this._contract.Contract.Clear();
                        this._contract.Contract.CopyAttributes(clone);
                    };
                }
                form.Save += handler;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    this._contract.Contract.Clear();
                    this._contract.Contract.CopyAttributes(clone);
                }
            }
        }

        private void EditContract(object sender, EventArgs e)
        {
            ContractEventArgs args = new ContractEventArgs(this._contract);
            this.LockAndLoadContract(this, args);
            this._contract = args.DisplayContract;
            this.ShowContract();
            this.SetEditMode(args.Successfull);
        }

        private void FillOperators()
        {
            try
            {
                this._operators.SuspendLayout();
                this._operators.Items.Clear();
                foreach (int num in this._contract.Contract.OperatorsId)
                {
                    foreach (Operator @operator in this.LoadOperator(num))
                    {
                        this._operators.Items.Add(@operator);
                    }
                }
            }
            finally
            {
                this._operators.ResumeLayout();
            }
        }

        private void FillRecipients()
        {
            try
            {
                this._recipientsList.SuspendLayout();
                this._recipientsList.Rows.Clear();
                LightRecipient[] recipientArray = this.LoadRecipient(Guid.Empty);
                string[] allowedTypes = this.GetAllowedTypes();
                string str = this._filter.Text.ToLower();
                foreach (LightRecipient recipient in recipientArray)
                {
                    if (this.CheckForOperatorId(recipient.OperatorId) && ((str == string.Empty) || (recipient.Name.ToLower().IndexOf(str) != -1)))
                    {
                        DataGridViewRow dataGridViewRow = new DataGridViewRow();
                        DataGridViewCell dataGridViewCell = new DataGridViewTextBoxCell();
                        dataGridViewCell.Value = recipient.Name;
                        dataGridViewRow.Cells.Add(dataGridViewCell);
                        DataGridViewComboBoxCell cell2 = new DataGridViewComboBoxCell();
                        cell2.Items.AddRange(allowedTypes);
                        if (this.CheckForBlocked(recipient.Id))
                        {
                            recipient.Type = AllowType.Blocked;
                        }
                        else if (this.CheckForExclusive(recipient.Id))
                        {
                            recipient.Type = AllowType.Exclusive;
                        }
                        else
                        {
                            recipient.Type = AllowType.Allowed;
                        }
                        cell2.Value = new EnumTypeInfo<AllowType>(recipient.Type).ToString();
                        dataGridViewRow.Cells.Add(cell2);
                        dataGridViewRow.Tag = recipient.Id;
                        this._recipientsList.Rows.Add(dataGridViewRow);
                    }
                }
                this._recipientsList.Sort(this._recipientsList.Columns[0], ListSortDirection.Ascending);
                this._recipientsList.CellEndEdit += new DataGridViewCellEventHandler(this.RecipientsListCellEndEdit);
            }
            finally
            {
                this._recipientsList.ResumeLayout();
            }
        }

        private string[] GetAllowedTypes()
        {
            List<AllowType> list = new List<AllowType>(3);
            foreach (EnumTypeInfo<AllowType> info in EnumTypeInfo<AllowType>.GetInfo())
            {
                list.Add(info.EnumValue);
            }
            int count = list.Count;
            string[] strArray = new string[count];
            for (int i = 0; i < count; i++)
            {
                strArray[i] = new EnumTypeInfo<AllowType>(list[i]).ToString();
            }
            return strArray;
        }

        private void Initialize(string caption)
        {
            this.InitializeComponent();
            this.Text = caption;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ContractViewForm));
            this._createDate = new DateTimePicker();
            this._attributes = new Button();
            this._externalId = new TextBox();
            this._number = new TextBox();
            this._operators = new ListBox();
            this._removeOperator = new Button();
            this._addOperator = new Button();
            this._filter = new TextBox();
            this._recipientsList = new DataGridView();
            this.Recipient = new DataGridViewTextBoxColumn();
            this.Type = new DataGridViewComboBoxColumn();
            this._clientName = new Label();
            this._cancel = new Button();
            this._ok = new Button();
            this._editContract = new Button();
            Label label = new Label();
            GroupBox box = new GroupBox();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            GroupBox box2 = new GroupBox();
            GroupBox box3 = new GroupBox();
            Label label5 = new Label();
            Label label6 = new Label();
            box.SuspendLayout();
            box2.SuspendLayout();
            box3.SuspendLayout();
            ((ISupportInitialize) this._recipientsList).BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "_clientLabel");
            label.Name = "_clientLabel";
            manager.ApplyResources(box, "_gbContract");
            box.Controls.Add(this._createDate);
            box.Controls.Add(this._attributes);
            box.Controls.Add(this._externalId);
            box.Controls.Add(this._number);
            box.Controls.Add(label2);
            box.Controls.Add(label3);
            box.Controls.Add(label4);
            box.Name = "_gbContract";
            box.TabStop = false;
            manager.ApplyResources(this._createDate, "_createDate");
            this._createDate.Name = "_createDate";
            manager.ApplyResources(this._attributes, "_attributes");
            this._attributes.Name = "_attributes";
            this._attributes.UseVisualStyleBackColor = true;
            this._attributes.Click += new EventHandler(this.EditAttributes);
            manager.ApplyResources(this._externalId, "_externalId");
            this._externalId.Name = "_externalId";
            manager.ApplyResources(this._number, "_number");
            this._number.Name = "_number";
            manager.ApplyResources(label2, "label3");
            label2.Name = "label3";
            manager.ApplyResources(label3, "_labelExternalId");
            label3.Name = "_labelExternalId";
            manager.ApplyResources(label4, "_labelNumber");
            label4.Name = "_labelNumber";
            manager.ApplyResources(box2, "_gbOperators");
            box2.Controls.Add(this._operators);
            box2.Controls.Add(this._removeOperator);
            box2.Controls.Add(this._addOperator);
            box2.Name = "_gbOperators";
            box2.TabStop = false;
            manager.ApplyResources(this._operators, "_operators");
            this._operators.FormattingEnabled = true;
            this._operators.Name = "_operators";
            this._operators.SelectionMode = SelectionMode.MultiSimple;
            manager.ApplyResources(this._removeOperator, "_removeOperator");
            this._removeOperator.Name = "_removeOperator";
            this._removeOperator.UseVisualStyleBackColor = true;
            this._removeOperator.Click += new EventHandler(this.RemoveOperator);
            manager.ApplyResources(this._addOperator, "_addOperator");
            this._addOperator.Name = "_addOperator";
            this._addOperator.UseVisualStyleBackColor = true;
            this._addOperator.Click += new EventHandler(this.AddOperator);
            manager.ApplyResources(box3, "_gbRecipient");
            box3.Controls.Add(this._filter);
            box3.Controls.Add(label5);
            box3.Controls.Add(label6);
            box3.Controls.Add(this._recipientsList);
            box3.Name = "_gbRecipient";
            box3.TabStop = false;
            manager.ApplyResources(this._filter, "_filter");
            this._filter.Name = "_filter";
            this._filter.TextChanged += new EventHandler(this._filter_TextChanged);
            manager.ApplyResources(label5, "_filterLabel");
            label5.Name = "_filterLabel";
            manager.ApplyResources(label6, "_desc");
            label6.Name = "_desc";
            this._recipientsList.AllowUserToAddRows = false;
            this._recipientsList.AllowUserToDeleteRows = false;
            manager.ApplyResources(this._recipientsList, "_recipientsList");
            this._recipientsList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._recipientsList.Columns.AddRange(new DataGridViewColumn[] { this.Recipient, this.Type });
            this._recipientsList.Name = "_recipientsList";
            this._recipientsList.ReadOnly = true;
            this._recipientsList.RowHeadersVisible = false;
            manager.ApplyResources(this.Recipient, "Recipient");
            this.Recipient.Name = "Recipient";
            this.Recipient.ReadOnly = true;
            manager.ApplyResources(this.Type, "Type");
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Resizable = DataGridViewTriState.True;
            this.Type.SortMode = DataGridViewColumnSortMode.Automatic;
            manager.ApplyResources(this._clientName, "_clientName");
            this._clientName.Name = "_clientName";
            manager.ApplyResources(this._cancel, "_cancel");
            this._cancel.DialogResult = DialogResult.Cancel;
            this._cancel.Name = "_cancel";
            this._cancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._ok, "_ok");
            this._ok.DialogResult = DialogResult.OK;
            this._ok.Name = "_ok";
            this._ok.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._editContract, "_editContract");
            this._editContract.Name = "_editContract";
            this._editContract.UseVisualStyleBackColor = true;
            this._editContract.Click += new EventHandler(this.EditContract);
            base.AcceptButton = this._ok;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.CancelButton = this._cancel;
            base.Controls.Add(box3);
            base.Controls.Add(this._cancel);
            base.Controls.Add(this._editContract);
            base.Controls.Add(this._ok);
            base.Controls.Add(this._clientName);
            base.Controls.Add(box2);
            base.Controls.Add(label);
            base.Controls.Add(box);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ContractViewForm";
            base.ShowIcon = false;
            base.Load += new EventHandler(this.OnFormLoad);
            box.ResumeLayout(false);
            box.PerformLayout();
            box2.ResumeLayout(false);
            box3.ResumeLayout(false);
            box3.PerformLayout();
            ((ISupportInitialize) this._recipientsList).EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            this.ShowContract();
        }

        private void RecipientsListCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                string str = new EnumTypeInfo<AllowType>(AllowType.Exclusive).ToString();
                string str2 = new EnumTypeInfo<AllowType>(AllowType.Blocked).ToString();
                string str3 = (string) this._recipientsList.Rows[e.RowIndex].Cells[1].Value;
                Guid tag = (Guid) this._recipientsList.Rows[e.RowIndex].Tag;
                if (str3 == str)
                {
                    this._contract.Contract.RemoveBlockedRecipient(tag);
                    this._contract.Contract.AddExclusiveRecipient(tag);
                }
                else if (str3 == str2)
                {
                    this._contract.Contract.RemoveExclusiveRecipient(tag);
                    this._contract.Contract.AddBlockedRecipient(tag);
                }
                else
                {
                    this._contract.Contract.RemoveExclusiveRecipient(tag);
                    this._contract.Contract.RemoveBlockedRecipient(tag);
                }
            }
        }

        private void RemoveOperator(object sender, EventArgs e)
        {
            if ((MessageBox.Show(ContractStrings.AreYouWantRemove, ContractStrings.SelectedOperators, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) && (this._operators.SelectedItems.Count > 0))
            {
                foreach (Operator @operator in this._operators.SelectedItems)
                {
                    this._contract.Contract.RemoveOperator(@operator.Id);
                }
                this.FillOperators();
                this.FillRecipients();
            }
        }

        public void SetEditMode(bool value)
        {
            try
            {
                base.SuspendLayout();
                this._number.Enabled = this._createDate.Enabled = this._addOperator.Enabled = this._removeOperator.Enabled = this._externalId.Enabled = this._attributes.Enabled = value;
                if (value)
                {
                    this._attributes.Enabled = Thread.CurrentPrincipal.IsInRole(UserRole.contractActivator.ToString());
                }
                this._editContract.Enabled = !value;
                this._recipientsList.ReadOnly = !value;
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        protected void ShowContract()
        {
            try
            {
                base.SuspendLayout();
                this._clientName.Text = this._contract.ClientName;
                this._number.Text = this._contract.Contract.Number;
                this._createDate.Value = this._contract.Contract.CreateDate;
                this._externalId.Text = this._contract.Contract.ExternalId;
                this.FillOperators();
                this.FillRecipients();
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        public DisplayContract Contract
        {
            get
            {
                this._contract.Contract.Number = this._number.Text;
                this._contract.Contract.CreateDate = this._createDate.Value;
                this._contract.Contract.ExternalId = this._externalId.Text;
                return this._contract;
            }
        }
    }
}

