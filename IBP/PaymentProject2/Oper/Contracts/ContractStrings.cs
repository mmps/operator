﻿namespace IBP.PaymentProject2.Oper.Contracts
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;

    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), CompilerGenerated, DebuggerNonUserCode]
    internal class ContractStrings
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal ContractStrings()
        {
        }

        internal static string ActivateContractErrorFormat
        {
            get
            {
                return ResourceManager.GetString("ActivateContractErrorFormat", resourceCulture);
            }
        }

        internal static string Activated
        {
            get
            {
                return ResourceManager.GetString("Activated", resourceCulture);
            }
        }

        internal static string AddContractErrorFormat
        {
            get
            {
                return ResourceManager.GetString("AddContractErrorFormat", resourceCulture);
            }
        }

        internal static string AddExclusiveCaption
        {
            get
            {
                return ResourceManager.GetString("AddExclusiveCaption", resourceCulture);
            }
        }

        internal static string Allowed
        {
            get
            {
                return ResourceManager.GetString("Allowed", resourceCulture);
            }
        }

        internal static string AreYouSureMakeExclusive
        {
            get
            {
                return ResourceManager.GetString("AreYouSureMakeExclusive", resourceCulture);
            }
        }

        internal static string AreYouWantActivate
        {
            get
            {
                return ResourceManager.GetString("AreYouWantActivate", resourceCulture);
            }
        }

        internal static string AreYouWantRemove
        {
            get
            {
                return ResourceManager.GetString("AreYouWantRemove", resourceCulture);
            }
        }

        internal static string AreYouWantRestore
        {
            get
            {
                return ResourceManager.GetString("AreYouWantRestore", resourceCulture);
            }
        }

        internal static string Blocked
        {
            get
            {
                return ResourceManager.GetString("Blocked", resourceCulture);
            }
        }

        internal static string Contract
        {
            get
            {
                return ResourceManager.GetString("Contract", resourceCulture);
            }
        }

        internal static string CreationDate
        {
            get
            {
                return ResourceManager.GetString("CreationDate", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string Deactivated
        {
            get
            {
                return ResourceManager.GetString("Deactivated", resourceCulture);
            }
        }

        internal static string DisplayNumber
        {
            get
            {
                return ResourceManager.GetString("DisplayNumber", resourceCulture);
            }
        }

        internal static string EditContract
        {
            get
            {
                return ResourceManager.GetString("EditContract", resourceCulture);
            }
        }

        internal static string Exclusive
        {
            get
            {
                return ResourceManager.GetString("Exclusive", resourceCulture);
            }
        }

        internal static string ExternalNumber
        {
            get
            {
                return ResourceManager.GetString("ExternalNumber", resourceCulture);
            }
        }

        internal static string GetContractsErrorFormat
        {
            get
            {
                return ResourceManager.GetString("GetContractsErrorFormat", resourceCulture);
            }
        }

        internal static string GetSingleContractErrorFormat
        {
            get
            {
                return ResourceManager.GetString("GetSingleContractErrorFormat", resourceCulture);
            }
        }

        internal static string LoseIsolation
        {
            get
            {
                return ResourceManager.GetString("LoseIsolation", resourceCulture);
            }
        }

        internal static string NewContract
        {
            get
            {
                return ResourceManager.GetString("NewContract", resourceCulture);
            }
        }

        internal static string Operators
        {
            get
            {
                return ResourceManager.GetString("Operators", resourceCulture);
            }
        }

        internal static string RecipientName
        {
            get
            {
                return ResourceManager.GetString("RecipientName", resourceCulture);
            }
        }

        internal static string RecipientType
        {
            get
            {
                return ResourceManager.GetString("RecipientType", resourceCulture);
            }
        }

        internal static string RemoveContractErrorFormat
        {
            get
            {
                return ResourceManager.GetString("RemoveContractErrorFormat", resourceCulture);
            }
        }

        internal static string Removed
        {
            get
            {
                return ResourceManager.GetString("Removed", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("IBP.PaymentProject2.Oper.Contracts.ContractStrings", typeof(ContractStrings).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static string RestoreContractErrorFormat
        {
            get
            {
                return ResourceManager.GetString("RestoreContractErrorFormat", resourceCulture);
            }
        }

        internal static string SaveContractErrorFormat
        {
            get
            {
                return ResourceManager.GetString("SaveContractErrorFormat", resourceCulture);
            }
        }

        internal static string SelectedContracts
        {
            get
            {
                return ResourceManager.GetString("SelectedContracts", resourceCulture);
            }
        }

        internal static string SelectedOperators
        {
            get
            {
                return ResourceManager.GetString("SelectedOperators", resourceCulture);
            }
        }

        internal static string SelectedRecipients
        {
            get
            {
                return ResourceManager.GetString("SelectedRecipients", resourceCulture);
            }
        }

        internal static string Status
        {
            get
            {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }

        internal static string WindowCaptionFormat
        {
            get
            {
                return ResourceManager.GetString("WindowCaptionFormat", resourceCulture);
            }
        }

        internal static string YouHaveNoRulesToViewContracts
        {
            get
            {
                return ResourceManager.GetString("YouHaveNoRulesToViewContracts", resourceCulture);
            }
        }
    }
}

