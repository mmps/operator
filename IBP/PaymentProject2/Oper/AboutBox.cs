﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using System.Windows.Forms;

    internal class AboutBox : Form
    {
        private IContainer components;
        private Label labelCompanyName;
        private Label labelCopyright;
        private Label labelProductName;
        private Label labelServerVer;
        private Label labelVersion;
        private PictureBox logoPictureBox;
        private Button okButton;
        private TableLayoutPanel tableLayoutPanel;
        private TextBox textBoxDescription;

        public AboutBox()
        {
            this.InitializeComponent();
            this.Text = string.Format(UserStrings.About, base.ProductName);
            this.labelProductName.Text = this.AssemblyProduct;
            this.labelVersion.Text = string.Format(UserStrings.Version, this.AssemblyVersion);
            this.labelCopyright.Text = this.AssemblyCopyright;
            this.labelCompanyName.Text = this.AssemblyCompany;
            this.textBoxDescription.Text = this.AssemblyDescription + Environment.NewLine;
            try
            {
                this.labelServerVer.Text = UserStrings.ServerVer + IBP.PaymentProject2.Oper.ServerFasad.Server.GetVersion().ToString();
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(AboutBox));
            this.tableLayoutPanel = new TableLayoutPanel();
            this.logoPictureBox = new PictureBox();
            this.labelProductName = new Label();
            this.labelVersion = new Label();
            this.labelCopyright = new Label();
            this.labelCompanyName = new Label();
            this.okButton = new Button();
            this.textBoxDescription = new TextBox();
            this.labelServerVer = new Label();
            this.tableLayoutPanel.SuspendLayout();
            ((ISupportInitialize) this.logoPictureBox).BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(this.tableLayoutPanel, "tableLayoutPanel");
            this.tableLayoutPanel.Controls.Add(this.logoPictureBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelProductName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelVersion, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.labelCopyright, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.labelCompanyName, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.okButton, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.textBoxDescription, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.labelServerVer, 1, 4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            manager.ApplyResources(this.logoPictureBox, "logoPictureBox");
            this.logoPictureBox.Name = "logoPictureBox";
            this.tableLayoutPanel.SetRowSpan(this.logoPictureBox, 6);
            this.logoPictureBox.TabStop = false;
            this.logoPictureBox.DoubleClick += new EventHandler(this.logoPictureBox_DoubleClick);
            manager.ApplyResources(this.labelProductName, "labelProductName");
            this.labelProductName.MaximumSize = new Size(0, 0x11);
            this.labelProductName.Name = "labelProductName";
            manager.ApplyResources(this.labelVersion, "labelVersion");
            this.labelVersion.MaximumSize = new Size(0, 0x11);
            this.labelVersion.Name = "labelVersion";
            manager.ApplyResources(this.labelCopyright, "labelCopyright");
            this.labelCopyright.MaximumSize = new Size(0, 0x11);
            this.labelCopyright.Name = "labelCopyright";
            manager.ApplyResources(this.labelCompanyName, "labelCompanyName");
            this.labelCompanyName.MaximumSize = new Size(0, 0x11);
            this.labelCompanyName.Name = "labelCompanyName";
            manager.ApplyResources(this.okButton, "okButton");
            this.okButton.DialogResult = DialogResult.Cancel;
            this.okButton.Name = "okButton";
            manager.ApplyResources(this.textBoxDescription, "textBoxDescription");
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.TabStop = false;
            manager.ApplyResources(this.labelServerVer, "labelServerVer");
            this.labelServerVer.Name = "labelServerVer";
            base.AcceptButton = this.okButton;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.tableLayoutPanel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "AboutBox";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((ISupportInitialize) this.logoPictureBox).EndInit();
            base.ResumeLayout(false);
        }

        private void logoPictureBox_DoubleClick(object sender, EventArgs e)
        {
            LocalDataStoreSlot namedDataSlot = Thread.GetNamedDataSlot("statistic");
            if ((Thread.GetData(namedDataSlot) == null) || !((bool) Thread.GetData(namedDataSlot)))
            {
                Thread.SetData(namedDataSlot, true);
            }
            else
            {
                Thread.SetData(namedDataSlot, false);
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (customAttributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute) customAttributes[0]).Company;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (customAttributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute) customAttributes[0]).Copyright;
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (customAttributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute) customAttributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (customAttributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute) customAttributes[0]).Product;
            }
        }

        public string AssemblyTitle
        {
            get
            {
                object[] customAttributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (customAttributes.Length > 0)
                {
                    AssemblyTitleAttribute attribute = (AssemblyTitleAttribute) customAttributes[0];
                    if (attribute.Title != "")
                    {
                        return attribute.Title;
                    }
                }
                return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
    }
}

