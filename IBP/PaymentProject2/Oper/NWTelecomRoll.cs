﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.IO;
    using System.Text;

    [Roll(GatewayType.NWTelecom)]
    public class NWTelecomRoll : Roll
    {
        private Encoding coder = Encoding.ASCII;
        private string diffcode;
        private static string FF = "nwt_ff";
        private StringBuilder fstr = new StringBuilder();
        private static string K = "nwt_k";
        private static string NN = "nwt_nn";
        private string nrf;
        private string pccode;
        private MemoryStream stream;
        private static string TOWN_CODE = "nwt_town_code";
        private string towncode;

        public override Stream CreateRoll(Payment[] payments)
        {
            if (payments.Length == 0)
            {
                throw new RollException(UserStrings.NoChoosenPays);
            }
            Service service = ServicesContainer.Instance[payments[0].ServiceId];
            if ((!service.Contains(NN) || !service.Contains(FF)) || (!service.Contains(K) || !service.Contains(TOWN_CODE)))
            {
                throw new RollException(string.Format(UserStrings.ServiceNotTuned, ServicesContainer.Instance[payments[0].ServiceId].Name));
            }
            this.pccode = service[NN].ToString();
            this.nrf = service[FF].ToString();
            this.diffcode = service[K].ToString();
            this.towncode = service[TOWN_CODE].ToString();
            if (((this.pccode.Length != 2) || (this.nrf.Length != 2)) || ((this.diffcode.Length != 1) || (this.towncode.Length != 3)))
            {
                throw new RollException(string.Format(UserStrings.ServiceNotRight, ServicesContainer.Instance[payments[0].ServiceId].Name));
            }
            DateTime inputDate = payments[0].InputDate;
            base.filename = string.Format("{0}{1}{2}{3}", new object[] { this.pccode, this.nrf, this.diffcode, inputDate.DayOfYear.ToString() });
            base.ext = string.Format("{0}1", inputDate.Year.ToString().Substring(2, 2));
            string str3 = "0000000 ";
            string str4 = "01 ";
            string str5 = "32 ";
            decimal num = 0M;
            int num2 = 0;
            foreach (Payment payment in payments)
            {
                num2++;
                num += payment.Value;
                this.fstr.Append(this.towncode + " ");
                this.fstr.Append(str3);
                this.fstr.Append(str4);
                this.fstr.Append(payment.Value.ToString().PadLeft(0x10, '0') + " ");
                this.fstr.Append(payment.Account + " ");
                this.fstr.Append(str5);
                this.fstr.Append(payment.InputDate.ToString("dd.MM.yyyy HH:mm:ss"));
                this.fstr.Append(Environment.NewLine);
            }
            this.fstr.Append("END");
            this.fstr.Append(Environment.NewLine);
            string str6 = string.Format("{0}:{1}", num2.ToString().PadLeft(5, '0'), num.ToString().PadLeft(0x10, '0'));
            this.fstr.Append(str6);
            try
            {
                this.stream = new MemoryStream(this.coder.GetBytes(this.fstr.ToString()));
            }
            catch (ArgumentException exception)
            {
                throw new RollException(exception.Message);
            }
            return this.stream;
        }
    }
}

