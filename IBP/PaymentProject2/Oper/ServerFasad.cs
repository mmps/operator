﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.CommissionLimitation;
    using IBP.PaymentProject2.Common.Files;
    using IBP.PaymentProject2.Common.Isolation;
    using IBP.PaymentProject2.Common.License;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Common.Reports;
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Common.Transactions;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class ServerFasad : ServerFasadBase, IOper, IClientTariffPlanStorage, IClientActsStorage, ITemplateStorage, IFileStorage, IReportsStorage, ILimitationStorage, ILicenseStorage, IIsolationController, IGateway
    {
        private static IBP.PaymentProject2.Oper.ServerFasad server;
        private static object syncRoot = new object();

        private ServerFasad()
        {
        }

        public void ActivateContract(int contractId, int isolationId)
        {
            base.CheckConnection();
            try
            {
                this.Oper.ActivateContract(contractId, isolationId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.ActivateContract(contractId, isolationId);
            }
        }

        public Account AddAccount(Account account)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.AddAccount(account);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.AddAccount(account);
            }
        }

        public Client AddClient(Client client)
        {
            try
            {
                return this.Oper.AddClient(client);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.AddClient(client);
            }
        }

        public ClientTariffPlan AddClientTariffPlan(ClientTariffPlan plan)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.AddClientTariffPlan(plan);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.AddClientTariffPlan(plan);
            }
        }

        public int AddContract(Contract contract)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.AddContract(contract);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.AddContract(contract);
            }
        }

        public Transaction AddTransaction(Transaction transaction)
        {
            try
            {
                return this.Oper.AddTransaction(transaction);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.AddTransaction(transaction);
            }
        }

        public void AssignClientTariffPlan(Guid[] clientIds, int planId, DateTime startDate)
        {
            base.CheckConnection();
            try
            {
                this.Oper.AssignClientTariffPlan(clientIds, planId, startDate);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.AssignClientTariffPlan(clientIds, planId, startDate);
            }
        }

        public void CheckAccount(ref Payment payment)
        {
            base.CheckConnection();
            try
            {
                this.Gateway.CheckAccount(ref payment);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.CheckAccount(ref payment);
            }
        }

        public bool CheckFeature(FeatureType type)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.CheckFeature(type);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.CheckFeature(type);
            }
        }

        protected override bool CheckVersions()
        {
            if (!base.CheckVersions())
            {
                return false;
            }
            Version version = typeof(IOper).Assembly.GetName().Version;
            Version version2 = ((IOper) this).GetVersion();
            return ((version.Minor == version2.Minor) && (version.Major == version2.Major));
        }

        public int CreateClientReviseAct(Guid clientId, DateTime startTime)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.CreateClientReviseAct(clientId, startTime);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.CreateClientReviseAct(clientId, startTime);
            }
        }

        public List<Transaction> CreateRecipientRollTransaction(int rollID)
        {
            try
            {
                return this.Oper.CreateRecipientRollTransaction(rollID);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.CreateRecipientRollTransaction(rollID);
            }
        }

        public void DeleteClientReviseAct(int actId)
        {
            base.CheckConnection();
            try
            {
                this.Oper.DeleteClientReviseAct(actId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.DeleteClientReviseAct(actId);
            }
        }

        public void DeleteFile(int id)
        {
            base.CheckConnection();
            try
            {
                this.Oper.DeleteFile(id);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.DeleteFile(id);
            }
        }

        public void DeleteLimitation(int id)
        {
            base.CheckConnection();
            try
            {
                this.Oper.DeleteLimitation(id);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.DeleteLimitation(id);
            }
        }

        public void DeleteReport(int id)
        {
            base.CheckConnection();
            try
            {
                this.Oper.DeleteReport(id);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.DeleteReport(id);
            }
        }

        public void DeleteTariffPlan(int planId)
        {
            base.CheckConnection();
            try
            {
                this.Oper.DeleteTariffPlan(planId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.DeleteTariffPlan(planId);
            }
        }

        public void ExtendedRequest(ExtendedEventArgs args)
        {
            base.CheckConnection();
            try
            {
                ((IGateway) this.GetProxy(typeof(IGateway))).ExtendedRequest(args);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.ExtendedRequest(args);
            }
        }

        public Contract[] FindContracts(Guid contractId)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.FindContracts(contractId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.FindContracts(contractId);
            }
        }

        public Feature[] GetAllFeatures()
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetAllFeatures();
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetAllFeatures();
            }
        }

        public List<Guid> GetAssignedClientIds(int planId)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetAssignedClientIds(planId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetAssignedClientIds(planId);
            }
        }

        public List<BalanceInfo> GetBalanceHistory(TransactionsFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetBalanceHistory(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetBalanceHistory(filter);
            }
        }

        public List<CardOperation> GetCardOperations(CardOperationsFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetCardOperations(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetCardOperations(filter);
            }
        }

        public List<ClientReviseAct> GetClientReviseAct(ClientReviseActFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetClientReviseAct(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetClientReviseAct(filter);
            }
        }

        public ClientReviseActParams GetClientReviseActParams(Guid clientId, DateTime date)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetClientReviseActParams(clientId, date);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetClientReviseActParams(clientId, date);
            }
        }

        public List<ClientRoll> GetClientRolls(ClientRollFilter filter)
        {
            try
            {
                return this.Oper.GetClientRolls(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetClientRolls(filter);
            }
        }

        public List<ClientTariffPlanHistoryItem> GetClientTariffPlanHistory(Guid clientId)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetClientTariffPlanHistory(clientId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetClientTariffPlanHistory(clientId);
            }
        }

        public List<ClientTariffPlan> GetClientTariffPlans(ClientTariffPlanFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetClientTariffPlans(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetClientTariffPlans(filter);
            }
        }

        public Contract GetContract(int contractId)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetContract(contractId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetContract(contractId);
            }
        }

        public decimal GetCurrentBalance(int accountId)
        {
            try
            {
                return this.Oper.GetCurrentBalance(accountId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.Oper.GetCurrentBalance(accountId);
            }
        }

        public List<ExternalTransaction> GetExternalTransactions()
        {
            try
            {
                return this.Oper.GetExternalTransactions();
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetExternalTransactions();
            }
        }

        public FunctionalFeature[] GetFeatures()
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetFeatures();
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetFeatures();
            }
        }

        public ClientReviseAct GetFullClientReviseAct(int actId)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetFullClientReviseAct(actId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetFullClientReviseAct(actId);
            }
        }

        public ClientTariffPlan GetFullClientTariffPlan(int id)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetFullClientTariffPlan(id);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetFullClientTariffPlan(id);
            }
        }

        public Template GetFullTemplate(TemplateFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetFullTemplate(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetFullTemplate(filter);
            }
        }

        public GatewayFeature[] GetGateways()
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetGateways();
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetGateways();
            }
        }

        public decimal GetInBalance(TransactionsFilter filter)
        {
            try
            {
                return this.Oper.GetInBalance(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetInBalance(filter);
            }
        }

        public IsolationInfo GetIsolation(string globalObjectId, bool ignoreOthers)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetIsolation(globalObjectId, ignoreOthers);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetIsolation(globalObjectId, ignoreOthers);
            }
        }

        public PointFeature[] GetLicenses()
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetLicenses();
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetLicenses();
            }
        }

        public PaymentHistory GetPaymentHistory(int paymentId, int generation)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetPaymentHistory(paymentId, generation);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetPaymentHistory(paymentId, generation);
            }
        }

        public List<Payment> GetPayments(PaymentsFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetPayments(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetPayments(filter);
            }
        }

        public List<RecipientRoll> GetRecipientRolls(RecipientRollFilter filter)
        {
            try
            {
                return this.Oper.GetRecipientRolls(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetRecipientRolls(filter);
            }
        }

        public void GetSource(string number, bool block, out SourceInfo info)
        {
            base.CheckConnection();
            try
            {
                this.Gateway.GetSource(number, block, out info);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.GetSource(number, block, out info);
            }
        }

        public StatisticInfo[] GetStatisticInfo(PaymentsFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetStatisticInfo(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetStatisticInfo(filter);
            }
        }

        public List<Template> GetTemplates(TemplateFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetTemplates(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetTemplates(filter);
            }
        }

        public List<Transaction> GetTransactions(TransactionsFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetTransactions(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.GetTransactions(filter);
            }
        }

        EventArgs[] IGateway.GetEventInfo()
        {
            throw new SrvException(UserStrings.NotAvailable);
        }

        byte[] IGateway.GetPublicKey(Guid pointId)
        {
            throw new SrvException(UserStrings.NotAvailable);
        }

        byte[] IGateway.GetSecretKey()
        {
            throw new SrvException(UserStrings.NotAvailable);
        }

        void IGateway.RegisterGW()
        {
            throw new SrvException(UserStrings.NotAvailable);
        }

        void IGateway.Result(ResultEventArgs args)
        {
            throw new SrvException(UserStrings.NotAvailable);
        }

        Version IOper.GetVersion()
        {
            base.CheckConnection();
            try
            {
                return this.Oper.GetVersion();
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return ((IOper) this).GetVersion();
            }
        }

        public AbsFile LoadFile(AbsFile file)
        {
            base.CheckConnection();
            try
            {
                file.Clear();
                file = this.Oper.LoadFile(file);
                return file;
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.LoadFile(file);
            }
        }

        public LimitationCollection LoadLimitation(LimitationFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.LoadLimitation(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.LoadLimitation(filter);
            }
        }

        public List<Report> LoadReport(ReportFilter repFilter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.LoadReport(repFilter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.LoadReport(repFilter);
            }
        }

        public void Process(ref Payment payment, out Account account)
        {
            base.CheckConnection();
            try
            {
                this.Gateway.Process(ref payment, out account);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.Process(ref payment, out account);
            }
        }

        public ClientRoll RebuildClientRoll(int rollID, string comment)
        {
            try
            {
                return this.Oper.RebuildClientRoll(rollID, comment);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.RebuildClientRoll(rollID, comment);
            }
        }

        public RecipientRoll RebuildRecipientRoll(int rollID, string comment)
        {
            try
            {
                return this.Oper.RebuildRecipientRoll(rollID, comment);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.RebuildRecipientRoll(rollID, comment);
            }
        }

        public bool RecallPayment(Payment payment)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.RecallPayment(payment);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.RecallPayment(payment);
            }
        }

        public void RecallPayment(Guid paymentId, Guid pointId, out PaymentState state)
        {
            base.CheckConnection();
            try
            {
                ((IGateway) this.GetProxy(typeof(IGateway))).RecallPayment(paymentId, pointId, out state);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.RecallPayment(paymentId, pointId, out state);
            }
        }

        public Account RecountInBalance(TransactionsFilter filter)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.RecountInBalance(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.RecountInBalance(filter);
            }
        }

        public void RedirectPayment(int paymentId, Guid serviceId, string account, string description)
        {
            base.CheckConnection();
            try
            {
                this.Oper.RedirectPayment(paymentId, serviceId, account, description);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.RedirectPayment(paymentId, serviceId, account, description);
            }
        }

        public void ReleaseIsolation(int isolationId)
        {
            base.CheckConnection();
            try
            {
                this.Oper.ReleaseIsolation(isolationId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.ReleaseIsolation(isolationId);
            }
        }

        public void RemoveContract(int contractId, int isolationId)
        {
            base.CheckConnection();
            try
            {
                this.Oper.RemoveContract(contractId, isolationId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.RemoveContract(contractId, isolationId);
            }
        }

        public void ReplaceTariffPlan(int oldPlanId, int newPlanId, DateTime startDate)
        {
            base.CheckConnection();
            try
            {
                this.Oper.ReplaceTariffPlan(oldPlanId, newPlanId, startDate);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.ReplaceTariffPlan(oldPlanId, newPlanId, startDate);
            }
        }

        public RecipientRoll ResendRecipientRoll(int rollID)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.ResendRecipientRoll(rollID);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.ResendRecipientRoll(rollID);
            }
        }

        public void RestoreContract(int contractId, int isolationId)
        {
            base.CheckConnection();
            try
            {
                this.Oper.RestoreContract(contractId, isolationId);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.RestoreContract(contractId, isolationId);
            }
        }

        public void SaveContract(Contract contract)
        {
            base.CheckConnection();
            try
            {
                this.Oper.SaveContract(contract);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.SaveContract(contract);
            }
        }

        public AbsFile SaveFile(AbsFile file)
        {
            base.CheckConnection();
            try
            {
                AbsFile file2 = this.Oper.SaveFile(file);
                file.CopyProperty(file2);
                return file;
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.SaveFile(file);
            }
        }

        public Limitation SaveLimitation(Limitation limitation)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.SaveLimitation(limitation);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.SaveLimitation(limitation);
            }
        }

        public Report SaveReport(Report report, AbsFile file)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.SaveReport(report, file);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.SaveReport(report, file);
            }
        }

        public void SendCustomRoll(PaymentsFilter filter)
        {
            base.CheckConnection();
            try
            {
                this.Oper.SendCustomRoll(filter);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.SendCustomRoll(filter);
            }
        }

        public void SetClientReviseActParams(ClientReviseActParams actParams, Guid clientId, DateTime date)
        {
            base.CheckConnection();
            try
            {
                this.Oper.SetClientReviseActParams(actParams, clientId, date);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.SetClientReviseActParams(actParams, clientId, date);
            }
        }

        public Account UpdateAccount(Account account)
        {
            base.CheckConnection();
            try
            {
                return this.Oper.UpdateAccount(account);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.UpdateAccount(account);
            }
        }

        public Client UpdateClient(Client client)
        {
            try
            {
                return this.Oper.UpdateClient(client);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                return this.UpdateClient(client);
            }
        }

        public void UpdateClientTariffPlan(ClientTariffPlan plan)
        {
            base.CheckConnection();
            try
            {
                this.Oper.UpdateClientTariffPlan(plan);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.UpdateClientTariffPlan(plan);
            }
        }

        public void UpdatePayment(Payment payment)
        {
            try
            {
                this.Oper.UpdatePayment(payment);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.UpdatePayment(payment);
            }
        }

        public void UpdateTemplate(Template template)
        {
            base.CheckConnection();
            try
            {
                this.Oper.UpdateTemplate(template.EmptyCopy);
            }
            catch (Exception exception)
            {
                if (this.CheckException(exception))
                {
                    throw;
                }
                this.UpdateTemplate(template);
            }
        }

        private IGateway Gateway
        {
            get
            {
                return (IGateway) this.GetProxy(typeof(IGateway));
            }
        }

        private IOper Oper
        {
            get
            {
                return (IOper) this.GetProxy(typeof(IOper));
            }
        }

        public static IBP.PaymentProject2.Oper.ServerFasad Server
        {
            get
            {
                if (server == null)
                {
                    lock (syncRoot)
                    {
                        if (server == null)
                        {
                            server = new IBP.PaymentProject2.Oper.ServerFasad();
                        }
                    }
                }
                return server;
            }
        }
    }
}

