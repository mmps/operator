﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Reflection;
    using System.Windows.Forms;

    public class ShowPaymentForm : Form
    {
        private static Dictionary<System.Type, string> _formatsTable = new Dictionary<System.Type, string>();
        private Button buttonOk;
        private ColumnHeader columnHeaderComment;
        private ColumnHeader columnHeaderDate;
        private ColumnHeader columnHeaderKey;
        private ColumnHeader columnHeaderParam;
        private ColumnHeader columnHeaderParamValue;
        private ColumnHeader columnHeaderStatus;
        private ColumnHeader columnHeaderValue;
        private ComboBox comboBoxStatus;
        private IContainer components;
        private ToolStripMenuItem copyToClipboardItem;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBoxParams;
        private Label label3;
        private ListView listViewHistory;
        private ListViewEdit listViewParams;
        private ListView listViewPaymentInfo;
        private ContextMenuStrip menuStripCopy = new ContextMenuStrip();
        private Payment payment;
        private int x;
        private int y;

        static ShowPaymentForm()
        {
            _formatsTable.Add(typeof(PaymentType), "S");
        }

        public ShowPaymentForm(Payment payment)
        {
            this.InitializeComponent();
            this.payment = payment;
        }

        private void AddParam(string param, string value)
        {
            ListViewItem item = new ListViewItem();
            item.Text = param;
            item.SubItems.Add(value);
            this.listViewPaymentInfo.Items.Add(item);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Payment payment = (Payment) this.payment.Clone();
            if ((this.SelectedPaymentState != this.payment.FullState) || this.listViewParams.Edited)
            {
                switch (MessageBox.Show(UserStrings.WantToSaveChanges, UserStrings.Attention, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        payment.FullState = this.SelectedPaymentState;
                        payment.Clear();
                        foreach (ListViewItem item in this.listViewParams.Items)
                        {
                            payment[item.Text] = item.SubItems[1].Text;
                        }
                        try
                        {
                            IBP.PaymentProject2.Oper.ServerFasad.Server.UpdatePayment(payment);
                            base.DialogResult = DialogResult.OK;
                        }
                        catch (SrvException exception)
                        {
                            MessageBox.Show(exception.Message);
                        }
                        break;

                    case DialogResult.No:
                        base.DialogResult = DialogResult.OK;
                        break;

                    case DialogResult.Cancel:
                        return;
                }
            }
            base.DialogResult = DialogResult.OK;
        }

        private void CopyToClipboard(object sender, EventArgs e)
        {
            if (this.listViewPaymentInfo.SelectedItems.Count != 0)
            {
                try
                {
                    Clipboard.Clear();
                    Clipboard.SetDataObject(this.listViewPaymentInfo.SelectedItems[0].GetSubItemAt(this.x, this.y).Text, true);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(UserStrings.ErrCopingBuffer + exception.Message);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitDate()
        {
            this.copyToClipboardItem = new ToolStripMenuItem(UserStrings.CopyToBuffer, null, new EventHandler(this.CopyToClipboard));
            this.menuStripCopy.Items.Add(this.copyToClipboardItem);
            this.listViewPaymentInfo.ContextMenuStrip = this.menuStripCopy;
            foreach (PropertyInfo info in this.payment.GetType().GetProperties())
            {
                if (info.Name != "Item")
                {
                    object[] customAttributes = info.GetCustomAttributes(typeof(DisplayNameAttribute), true);
                    if (customAttributes.Length > 0)
                    {
                        string displayName = ((DisplayNameAttribute) customAttributes[0]).DisplayName;
                        object obj2 = info.GetValue(this.payment, null);
                        if (obj2 != null)
                        {
                            IConvertible convertible = obj2 as IConvertible;
                            if (convertible != null)
                            {
                                this.AddParam(displayName, convertible.ToString(MainSettings.Default.CultureInfo));
                            }
                            else
                            {
                                string str2;
                                System.Type key = obj2.GetType();
                                if ((obj2 is IFormattable) && _formatsTable.ContainsKey(key))
                                {
                                    string format = _formatsTable[key];
                                    str2 = ((IFormattable) obj2).ToString(format, MainSettings.Default.CultureInfo);
                                }
                                else
                                {
                                    str2 = obj2.ToString();
                                }
                                this.AddParam(displayName, str2);
                            }
                        }
                    }
                }
            }
            this.SelectedPaymentState = this.payment.FullState;
            foreach (DictionaryEntry entry in this.payment)
            {
                ListViewItem item = new ListViewItem();
                item.Text = entry.Key.ToString();
                item.SubItems.Add(entry.Value.ToString());
                this.listViewParams.Items.Add(item);
            }
            PaymentHistory paymentHistory = null;
            try
            {
                paymentHistory = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPaymentHistory(this.payment.Serial, this.payment.Generation);
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }
            foreach (PaymentHistory.Item item2 in (IEnumerable<PaymentHistory.Item>) paymentHistory)
            {
                ListViewItem item3 = new ListViewItem();
                item3.Text = item2.Time.ToString();
                item3.SubItems.Add(item2.StringState);
                item3.SubItems.Add(item2.Comment);
                this.listViewHistory.Items.Add(item3);
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ShowPaymentForm));
            this.label3 = new Label();
            this.groupBox1 = new GroupBox();
            this.listViewPaymentInfo = new ListView();
            this.columnHeaderParam = new ColumnHeader();
            this.columnHeaderParamValue = new ColumnHeader();
            this.comboBoxStatus = new ComboBox();
            this.buttonOk = new Button();
            this.groupBoxParams = new GroupBox();
            this.listViewParams = new ListViewEdit();
            this.columnHeaderKey = new ColumnHeader();
            this.columnHeaderValue = new ColumnHeader();
            this.groupBox2 = new GroupBox();
            this.listViewHistory = new ListView();
            this.columnHeaderDate = new ColumnHeader();
            this.columnHeaderStatus = new ColumnHeader();
            this.columnHeaderComment = new ColumnHeader();
            this.groupBox1.SuspendLayout();
            this.groupBoxParams.SuspendLayout();
            this.groupBox2.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.groupBox1.Controls.Add(this.listViewPaymentInfo);
            this.groupBox1.Controls.Add(this.comboBoxStatus);
            this.groupBox1.Controls.Add(this.label3);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            this.listViewPaymentInfo.Columns.AddRange(new ColumnHeader[] { this.columnHeaderParam, this.columnHeaderParamValue });
            manager.ApplyResources(this.listViewPaymentInfo, "listViewPaymentInfo");
            this.listViewPaymentInfo.FullRowSelect = true;
            this.listViewPaymentInfo.GridLines = true;
            this.listViewPaymentInfo.Name = "listViewPaymentInfo";
            this.listViewPaymentInfo.UseCompatibleStateImageBehavior = false;
            this.listViewPaymentInfo.View = View.Details;
            this.listViewPaymentInfo.MouseClick += new MouseEventHandler(this.listViewPaymentInfo_MouseClick);
            manager.ApplyResources(this.columnHeaderParam, "columnHeaderParam");
            manager.ApplyResources(this.columnHeaderParamValue, "columnHeaderParamValue");
            this.comboBoxStatus.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxStatus, "comboBoxStatus");
            this.comboBoxStatus.Name = "comboBoxStatus";
            manager.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.groupBoxParams.Controls.Add(this.listViewParams);
            manager.ApplyResources(this.groupBoxParams, "groupBoxParams");
            this.groupBoxParams.Name = "groupBoxParams";
            this.groupBoxParams.TabStop = false;
            this.listViewParams.AllowSort = true;
            this.listViewParams.BindType = null;
            this.listViewParams.Columns.AddRange(new ColumnHeader[] { this.columnHeaderKey, this.columnHeaderValue });
            this.listViewParams.ColumnsCollection = null;
            manager.ApplyResources(this.listViewParams, "listViewParams");
            this.listViewParams.FullRowSelect = true;
            this.listViewParams.GridLines = true;
            this.listViewParams.Name = "listViewParams";
            this.listViewParams.ReadOnly = false;
            this.listViewParams.UseCompatibleStateImageBehavior = false;
            this.listViewParams.View = View.Details;
            manager.ApplyResources(this.columnHeaderKey, "columnHeaderKey");
            manager.ApplyResources(this.columnHeaderValue, "columnHeaderValue");
            this.groupBox2.Controls.Add(this.listViewHistory);
            manager.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            this.listViewHistory.Columns.AddRange(new ColumnHeader[] { this.columnHeaderDate, this.columnHeaderStatus, this.columnHeaderComment });
            manager.ApplyResources(this.listViewHistory, "listViewHistory");
            this.listViewHistory.GridLines = true;
            this.listViewHistory.Name = "listViewHistory";
            this.listViewHistory.UseCompatibleStateImageBehavior = false;
            this.listViewHistory.View = View.Details;
            manager.ApplyResources(this.columnHeaderDate, "columnHeaderDate");
            manager.ApplyResources(this.columnHeaderStatus, "columnHeaderStatus");
            manager.ApplyResources(this.columnHeaderComment, "columnHeaderComment");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.groupBox2);
            base.Controls.Add(this.groupBoxParams);
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.buttonOk);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ShowPaymentForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxParams.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void listViewPaymentInfo_MouseClick(object sender, MouseEventArgs e)
        {
            this.x = e.X;
            this.y = e.Y;
            this.copyToClipboardItem.Text = UserStrings.CopyToBuffer03 + this.listViewPaymentInfo.SelectedItems[0].GetSubItemAt(this.x, this.y).Text + "]";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.InitDate();
        }

        private PaymentState SelectedPaymentState
        {
            get
            {
                EnumTypeInfo<PaymentState> selectedItem = this.comboBoxStatus.SelectedItem as EnumTypeInfo<PaymentState>;
                if (selectedItem != null)
                {
                    return selectedItem.EnumValue;
                }
                return this.payment.FullState;
            }
            set
            {
                if (this.comboBoxStatus.Items.Count == 0)
                {
                    foreach (EnumTypeInfo<PaymentState> info in EnumTypeInfo<PaymentState>.GetInfo())
                    {
                        if (info.HasAttribute<ProtocolVisibleAttribute>())
                        {
                            this.comboBoxStatus.Items.Add(info);
                        }
                    }
                }
                this.comboBoxStatus.SelectedItem = new EnumTypeInfo<PaymentState>(this.payment.FullState);
            }
        }
    }
}

