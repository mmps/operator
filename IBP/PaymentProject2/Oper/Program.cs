﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Threading;
    using System.Windows.Forms;

    internal static class Program
    {
        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowErrorWindow(e.Exception);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowErrorWindow((Exception) e.ExceptionObject);
        }

        private static string GetInvalidConfigPath(Exception exception)
        {
            Exception innerException = exception;
            while (innerException.InnerException != null)
            {
                innerException = innerException.InnerException;
                if (innerException is ConfigurationErrorsException)
                {
                    ConfigurationErrorsException exception3 = (ConfigurationErrorsException) innerException;
                    if (!string.IsNullOrEmpty(exception3.Filename))
                    {
                        return exception3.Filename;
                    }
                }
            }
            return string.Empty;
        }

        [STAThread]
        private static void Main()
        {
            try
            {
                ConnectionSettings settings1 = ConnectionSettings.Default;
            }
            catch (Exception exception)
            {
                MessageBox.Show(string.Format(UserStrings.PersonalSettingsFileError, exception.Message), UserStrings.PersonalSettingsFileFailed, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                string invalidConfigPath = GetInvalidConfigPath(exception);
                if ((invalidConfigPath.Length > 0) && File.Exists(invalidConfigPath))
                {
                    File.Delete(invalidConfigPath);
                }
                Application.Restart();
                return;
            }
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(Program.CurrentDomain_UnhandledException);
            Application.ThreadException += new ThreadExceptionEventHandler(Program.Application_ThreadException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void ShowErrorWindow(Exception exc)
        {
            using (ApplicationErrorForm form = new ApplicationErrorForm(exc))
            {
                form.ShowDialog();
            }
            Application.Restart();
        }
    }
}

