﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class RecallPaymentDialog : Form
    {
        private Button buttonCancel;
        private Button buttonOk;
        private IContainer components;
        private GroupBox groupBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label labelAccount;
        private Label labelHeadStep2;
        private Label labelId;
        private Label labelIdZag;
        private Label labelService;
        private Label labelStatus;
        private Label labelTotal;
        private Panel panel1;
        private Payment payment;

        public RecallPaymentDialog(Payment payment)
        {
            this.InitializeComponent();
            this.payment = payment;
            this.labelId.Text = this.payment.Number;
            this.labelService.Text = ServicesContainer.Instance[payment.ServiceId].Name;
            this.labelAccount.Text = this.payment.Account;
            this.labelTotal.Text = this.payment.Total.ToString();
            this.labelStatus.Text = new EnumTypeInfo<PaymentState>(this.payment.FullState).ToString();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (IBP.PaymentProject2.Oper.ServerFasad.Server.RecallPayment(this.payment))
                {
                    MessageBox.Show(UserStrings.AskOnReturn);
                }
                else
                {
                    MessageBox.Show(UserStrings.AskOnReturnNotTaken);
                }
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
            base.DialogResult = DialogResult.OK;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecallPaymentDialog));
            this.groupBox1 = new GroupBox();
            this.labelAccount = new Label();
            this.label5 = new Label();
            this.labelStatus = new Label();
            this.labelTotal = new Label();
            this.labelService = new Label();
            this.labelId = new Label();
            this.label3 = new Label();
            this.label2 = new Label();
            this.label1 = new Label();
            this.labelIdZag = new Label();
            this.panel1 = new Panel();
            this.labelHeadStep2 = new Label();
            this.buttonOk = new Button();
            this.buttonCancel = new Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            base.SuspendLayout();
            this.groupBox1.Controls.Add(this.labelAccount);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.labelStatus);
            this.groupBox1.Controls.Add(this.labelTotal);
            this.groupBox1.Controls.Add(this.labelService);
            this.groupBox1.Controls.Add(this.labelId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelIdZag);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            manager.ApplyResources(this.labelAccount, "labelAccount");
            this.labelAccount.BorderStyle = BorderStyle.Fixed3D;
            this.labelAccount.Name = "labelAccount";
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            manager.ApplyResources(this.labelStatus, "labelStatus");
            this.labelStatus.BorderStyle = BorderStyle.Fixed3D;
            this.labelStatus.ForeColor = Color.Red;
            this.labelStatus.Name = "labelStatus";
            manager.ApplyResources(this.labelTotal, "labelTotal");
            this.labelTotal.BorderStyle = BorderStyle.Fixed3D;
            this.labelTotal.Name = "labelTotal";
            manager.ApplyResources(this.labelService, "labelService");
            this.labelService.BorderStyle = BorderStyle.Fixed3D;
            this.labelService.Name = "labelService";
            manager.ApplyResources(this.labelId, "labelId");
            this.labelId.BorderStyle = BorderStyle.Fixed3D;
            this.labelId.Name = "labelId";
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.labelIdZag, "labelIdZag");
            this.labelIdZag.Name = "labelIdZag";
            this.panel1.BackColor = Color.White;
            this.panel1.Controls.Add(this.labelHeadStep2);
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this.labelHeadStep2, "labelHeadStep2");
            this.labelHeadStep2.Name = "labelHeadStep2";
            manager.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new EventHandler(this.buttonOk_Click);
            this.buttonCancel.DialogResult = DialogResult.Cancel;
            manager.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this.buttonOk);
            base.Controls.Add(this.buttonCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "RecallPaymentDialog";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            base.ResumeLayout(false);
        }
    }
}

