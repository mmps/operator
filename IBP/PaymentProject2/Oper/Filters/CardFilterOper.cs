﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using System;

    internal class CardFilterOper : FilterOper
    {
        private CardOperationsFilter cardFilter = new CardOperationsFilter();

        public CardOperationsFilter CardFilter
        {
            get
            {
                return this.cardFilter;
            }
            set
            {
                this.cardFilter = value;
            }
        }
    }
}

