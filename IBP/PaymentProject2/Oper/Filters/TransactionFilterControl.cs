﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.Transactions)]
    internal class TransactionFilterControl : UserControl, IFilterControl
    {
        private ComboBox comboBoxAccount;
        private ComboBox comboBoxTown;
        private IContainer components;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;

        public TransactionFilterControl()
        {
            EventHandler handler = null;
            this.InitializeComponent();
            this.ReadContainers();
            this.dateTimeEnd.Format = this.dateTimeStart.Format = DateTimePickerFormat.Custom;
            this.dateTimeEnd.CustomFormat = this.dateTimeStart.CustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
            MainSettings.Default.CultureInfoChanged += new EventHandler(this.Default_CultureInfoChanged);
            if (handler == null)
            {
                handler = delegate (object sender, EventArgs e) {
                    this.FillAccounts();
                };
            }
            this.comboBoxTown.SelectedIndexChanged += handler;
        }

        private void Default_CultureInfoChanged(object sender, EventArgs e)
        {
            this.dateTimeEnd.CustomFormat = this.dateTimeStart.CustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FillAccounts()
        {
            Account selectedAccount = this.SelectedAccount;
            this.comboBoxAccount.Items.Clear();
            this.comboBoxAccount.Items.Add("");
            int selectedTownId = this.SelectedTownId;
            foreach (Account account2 in AccountsContainer.Instance.Collection)
            {
                if ((selectedTownId == account2.TownId) || (selectedTownId == 0))
                {
                    this.comboBoxAccount.Items.Add(account2);
                }
            }
            this.SelectedAccount = selectedAccount;
        }

        private void FillTowns()
        {
            this.comboBoxTown.Items.Clear();
            this.comboBoxTown.Items.Add("");
            this.comboBoxTown.Items.AddRange(TownsContainer.Instance.ToArray());
        }

        private int GetSelectIndexAccount(int id)
        {
            for (int i = 1; i < this.comboBoxAccount.Items.Count; i++)
            {
                if ((this.comboBoxAccount.Items[i] as Account).Serial == id)
                {
                    return i;
                }
            }
            return 0;
        }

        private int GetSelectIndexTown(int id)
        {
            foreach (object obj2 in this.comboBoxTown.Items)
            {
                Town town = obj2 as Town;
                if ((town != null) && (town.Serial == id))
                {
                    return this.comboBoxTown.Items.IndexOf(town);
                }
            }
            return 0;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(TransactionFilterControl));
            this.dateTimeEnd = new DateTimePicker();
            this.dateTimeStart = new DateTimePicker();
            this.comboBoxAccount = new ComboBox();
            this.comboBoxTown = new ComboBox();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "label2");
            label.Name = "label2";
            manager.ApplyResources(label2, "label1");
            label2.Name = "label1";
            manager.ApplyResources(label3, "label3");
            label3.Name = "label3";
            manager.ApplyResources(label4, "label4");
            label4.Name = "label4";
            this.dateTimeEnd.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeStart.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Name = "dateTimeStart";
            this.comboBoxAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxAccount.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxAccount.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxAccount.DropDownWidth = 250;
            this.comboBoxAccount.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxAccount, "comboBoxAccount");
            this.comboBoxAccount.Items.AddRange(new object[] { manager.GetString("comboBoxAccount.Items") });
            this.comboBoxAccount.Name = "comboBoxAccount";
            this.comboBoxAccount.Sorted = true;
            this.comboBoxTown.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxTown.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxTown.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxTown.DropDownWidth = 250;
            this.comboBoxTown.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxTown, "comboBoxTown");
            this.comboBoxTown.Items.AddRange(new object[] { manager.GetString("comboBoxTown.Items") });
            this.comboBoxTown.Name = "comboBoxTown";
            this.comboBoxTown.Sorted = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.comboBoxTown);
            base.Controls.Add(this.comboBoxAccount);
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(label4);
            base.Controls.Add(label3);
            base.Controls.Add(label);
            base.Controls.Add(label2);
            base.Controls.Add(this.dateTimeStart);
            base.Name = "TransactionFilterControl";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            this.FillTowns();
            this.FillAccounts();
        }

        public void ResetFilter()
        {
            this.dateTimeEnd.Value = this.dateTimeStart.Value = DateTime.Now;
            this.comboBoxAccount.SelectedIndex = 0;
        }

        public FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                TransactionsFilter filter = new TransactionsFilter();
                oper.Filter = filter;
                filter.StartTime = this.dateTimeStart.Value;
                filter.EndTime = this.dateTimeEnd.Value;
                filter.SourceAccountId = this.SelectedAccountId;
                filter.TownID = this.SelectedTownId;
                return oper;
            }
            set
            {
                FilterOper oper = value;
                TransactionsFilter filter = (TransactionsFilter) oper.Filter;
                this.dateTimeStart.Value = filter.StartTime;
                this.dateTimeEnd.Value = filter.EndTime;
                this.SelectedAccountId = filter.SourceAccountId;
                this.SelectedTownId = filter.TownID;
            }
        }

        private Account SelectedAccount
        {
            get
            {
                return (this.comboBoxAccount.SelectedItem as Account);
            }
            set
            {
                if (value == null)
                {
                    this.comboBoxAccount.SelectedIndex = 0;
                }
                else
                {
                    int selectIndexAccount = this.GetSelectIndexAccount(value.Serial);
                    this.comboBoxAccount.SelectedIndex = selectIndexAccount;
                    this.comboBoxAccount.Refresh();
                }
            }
        }

        private int SelectedAccountId
        {
            get
            {
                if (this.SelectedAccount == null)
                {
                    return 0;
                }
                return this.SelectedAccount.Id;
            }
            set
            {
                if (value == 0)
                {
                    this.SelectedAccount = null;
                }
                else
                {
                    this.SelectedAccount = AccountsContainer.Instance[value];
                }
            }
        }

        private Town SelectedTown
        {
            get
            {
                return (this.comboBoxTown.SelectedItem as Town);
            }
            set
            {
                if (value == null)
                {
                    this.comboBoxTown.SelectedIndex = 0;
                }
                else
                {
                    this.comboBoxTown.SelectedIndex = this.GetSelectIndexTown(value.Serial);
                }
            }
        }

        private int SelectedTownId
        {
            get
            {
                if (this.SelectedTown != null)
                {
                    return this.SelectedTown.Id;
                }
                return 0;
            }
            set
            {
                if (value == 0)
                {
                    this.SelectedTown = null;
                }
                else
                {
                    this.SelectedTown = TownsContainer.Instance[value];
                }
            }
        }
    }
}

