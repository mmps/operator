﻿using IBP.PaymentProject2.Common.Component;

namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms.Controls;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Globalization;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.Payments)]
    internal class PaymentFilterControl : UserControl, IFilterControl
    {
        private static string ACCOUNT = UserStrings.PhoneNumber;
        private CheckBox checkBoxAttr;
        private CheckBox checkBoxByStateTime;
        private PaymentTypeCheckBox checkBoxPaymentType;
        private ComboBox comboBoxClient;
        private ComboBox comboBoxOption;
        private ComboBox comboBoxPoint;
        private ComboBox comboBoxRecipient;
        private ComboBox comboBoxService;
        private ComboBox comboBoxStatus;
        private IContainer components;
        private ContextMenuStrip contextMenuStripTime;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private static string ID = UserStrings.CheckId;
        private Label label1;
        private Label label10;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label7;
        private Label label8;
        private Label label9;
        private static string NUMBER = UserStrings.CheckNumber;
        private NumericUpDown numericEndTotal;
        private NumericUpDown numericStartTotal;
        private TextBox textBoxAccount;
        private ToolStripMenuItem toolStripMenuItemDay;
        private ToolStripMenuItem toolStripMenuItemHour;
        private CheckBox withSubDealers;

        public PaymentFilterControl()
        {
            this.InitializeComponent();
            this.ReadContainers();
            foreach (EnumTypeInfo<PaymentState> info in EnumTypeInfo<PaymentState>.GetInfo())
            {
                if (info.HasAttribute<UserVisible>())
                {
                    this.comboBoxStatus.Items.Add(info);
                }
            }
            if (this.comboBoxStatus.Items.Count > 0)
            {
                this.comboBoxStatus.SelectedIndex = 0;
            }
            this.dateTimeStart.Format = this.dateTimeEnd.Format = DateTimePickerFormat.Custom;
            this.SubFormat = null;
            MainSettings.Default.CultureInfoChanged += new EventHandler(this.OnCultureInfoChanged);
            this.comboBoxOption.Items.Add(ACCOUNT);
            this.comboBoxOption.Items.Add(NUMBER);
            this.comboBoxOption.Items.Add(ID);
            this.comboBoxOption.SelectedItem = ACCOUNT;
        }

        private void comboBoxClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.comboBoxPoint.Items.Clear();
            this.comboBoxPoint.Items.Add(string.Empty);
            if (this.comboBoxClient.SelectedItem is Client)
            {
                Client selectedItem = this.comboBoxClient.SelectedItem as Client;
                foreach (PayPoint point in PointsContainer.Instance.Collection)
                {
                    if (point.ClientId.Equals(selectedItem.Id))
                    {
                        this.comboBoxPoint.Items.Add(point);
                    }
                }
            }
            else
            {
                this.comboBoxPoint.Items.AddRange(PointsContainer.Instance.ToArray());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(PaymentFilterControl));
            this.textBoxAccount = new TextBox();
            this.label5 = new Label();
            this.comboBoxStatus = new ComboBox();
            this.numericEndTotal = new NumericUpDown();
            this.numericStartTotal = new NumericUpDown();
            this.label9 = new Label();
            this.label8 = new Label();
            this.label7 = new Label();
            this.comboBoxClient = new ComboBox();
            this.comboBoxPoint = new ComboBox();
            this.comboBoxService = new ComboBox();
            this.label4 = new Label();
            this.label3 = new Label();
            this.dateTimeEnd = new DateTimePicker();
            this.contextMenuStripTime = new ContextMenuStrip(this.components);
            this.toolStripMenuItemDay = new ToolStripMenuItem();
            this.toolStripMenuItemHour = new ToolStripMenuItem();
            this.label2 = new Label();
            this.label1 = new Label();
            this.dateTimeStart = new DateTimePicker();
            this.label10 = new Label();
            this.comboBoxRecipient = new ComboBox();
            this.comboBoxOption = new ComboBox();
            this.checkBoxAttr = new CheckBox();
            this.withSubDealers = new CheckBox();
            this.checkBoxPaymentType = new PaymentTypeCheckBox();
            this.checkBoxByStateTime = new CheckBox();
            this.numericEndTotal.BeginInit();
            this.numericStartTotal.BeginInit();
            this.contextMenuStripTime.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.textBoxAccount, "textBoxAccount");
            this.textBoxAccount.Name = "textBoxAccount";
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            this.comboBoxStatus.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxStatus.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxStatus.DropDownWidth = 250;
            this.comboBoxStatus.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxStatus, "comboBoxStatus");
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Sorted = true;
            this.numericEndTotal.DecimalPlaces = 2;
            manager.ApplyResources(this.numericEndTotal, "numericEndTotal");
            int[] bits = new int[4];
            bits[0] = -727379969;
            bits[1] = 0xe8;
            this.numericEndTotal.Maximum = new decimal(bits);
            this.numericEndTotal.Name = "numericEndTotal";
            this.numericStartTotal.DecimalPlaces = 2;
            manager.ApplyResources(this.numericStartTotal, "numericStartTotal");
            int[] numArray2 = new int[4];
            numArray2[0] = -727379969;
            numArray2[1] = 0xe8;
            this.numericStartTotal.Maximum = new decimal(numArray2);
            this.numericStartTotal.Name = "numericStartTotal";
            manager.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            manager.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            manager.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            this.comboBoxClient.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxClient.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxClient.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxClient.DropDownWidth = 250;
            this.comboBoxClient.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxClient, "comboBoxClient");
            this.comboBoxClient.Name = "comboBoxClient";
            this.comboBoxClient.Sorted = true;
            this.comboBoxClient.SelectedIndexChanged += new EventHandler(this.comboBoxClient_SelectedIndexChanged);
            this.comboBoxPoint.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxPoint.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxPoint.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxPoint.DropDownWidth = 500;
            this.comboBoxPoint.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxPoint, "comboBoxPoint");
            this.comboBoxPoint.Name = "comboBoxPoint";
            this.comboBoxPoint.Sorted = true;
            this.comboBoxService.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxService.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxService.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxService.DropDownWidth = 250;
            this.comboBoxService.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxService, "comboBoxService");
            this.comboBoxService.Name = "comboBoxService";
            this.comboBoxService.Sorted = true;
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.dateTimeEnd.ContextMenuStrip = this.contextMenuStripTime;
            this.dateTimeEnd.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.contextMenuStripTime.Items.AddRange(new ToolStripItem[] { this.toolStripMenuItemDay, this.toolStripMenuItemHour });
            this.contextMenuStripTime.Name = "contextMenuStripTime";
            manager.ApplyResources(this.contextMenuStripTime, "contextMenuStripTime");
            this.contextMenuStripTime.ItemClicked += new ToolStripItemClickedEventHandler(this.OnTimeItemClicked);
            this.toolStripMenuItemDay.Checked = true;
            this.toolStripMenuItemDay.CheckState = CheckState.Checked;
            this.toolStripMenuItemDay.Name = "toolStripMenuItemDay";
            manager.ApplyResources(this.toolStripMenuItemDay, "toolStripMenuItemDay");
            this.toolStripMenuItemHour.Name = "toolStripMenuItemHour";
            manager.ApplyResources(this.toolStripMenuItemHour, "toolStripMenuItemHour");
            this.toolStripMenuItemHour.Tag = " HHч";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.dateTimeStart.ContextMenuStrip = this.contextMenuStripTime;
            this.dateTimeStart.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Name = "dateTimeStart";
            manager.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            this.comboBoxRecipient.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxRecipient.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxRecipient.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxRecipient.DropDownWidth = 250;
            this.comboBoxRecipient.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxRecipient, "comboBoxRecipient");
            this.comboBoxRecipient.Name = "comboBoxRecipient";
            this.comboBoxRecipient.Sorted = true;
            this.comboBoxOption.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxOption.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxOption.BackColor = SystemColors.Control;
            this.comboBoxOption.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxOption.DropDownWidth = 250;
            manager.ApplyResources(this.comboBoxOption, "comboBoxOption");
            this.comboBoxOption.FormattingEnabled = true;
            this.comboBoxOption.Name = "comboBoxOption";
            this.comboBoxOption.Sorted = true;
            manager.ApplyResources(this.checkBoxAttr, "checkBoxAttr");
            this.checkBoxAttr.Checked = true;
            this.checkBoxAttr.CheckState = CheckState.Checked;
            this.checkBoxAttr.Name = "checkBoxAttr";
            this.checkBoxAttr.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.withSubDealers, "withSubDealers");
            this.withSubDealers.Name = "withSubDealers";
            this.withSubDealers.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.checkBoxPaymentType, "checkBoxPaymentType");
            this.checkBoxPaymentType.Name = "checkBoxPaymentType";
            this.checkBoxPaymentType.TypeMask = (PaymentType) manager.GetObject("checkBoxPaymentType.TypeMask");
            this.checkBoxPaymentType.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.checkBoxByStateTime, "checkBoxByStateTime");
            this.checkBoxByStateTime.Checked = true;
            this.checkBoxByStateTime.CheckState = CheckState.Checked;
            this.checkBoxByStateTime.Name = "checkBoxByStateTime";
            this.checkBoxByStateTime.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.checkBoxPaymentType);
            base.Controls.Add(this.checkBoxAttr);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.comboBoxClient);
            base.Controls.Add(this.textBoxAccount);
            base.Controls.Add(this.withSubDealers);
            base.Controls.Add(this.comboBoxStatus);
            base.Controls.Add(this.label10);
            base.Controls.Add(this.comboBoxRecipient);
            base.Controls.Add(this.numericEndTotal);
            base.Controls.Add(this.numericStartTotal);
            base.Controls.Add(this.label7);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.label9);
            base.Controls.Add(this.comboBoxService);
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.label8);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.comboBoxOption);
            base.Controls.Add(this.comboBoxPoint);
            base.Controls.Add(this.checkBoxByStateTime);
            base.Controls.Add(this.dateTimeStart);
            base.Controls.Add(this.label1);
            base.Name = "PaymentFilterControl";
            this.numericEndTotal.EndInit();
            this.numericStartTotal.EndInit();
            this.contextMenuStripTime.ResumeLayout(false);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void OnCultureInfoChanged(object sender, EventArgs e)
        {
            this.dateTimeStart.CustomFormat = this.dateTimeEnd.CustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
        }

        private void OnTimeItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.SubFormat = e.ClickedItem.Tag as string;
        }

        public void ReadContainers()
        {
            this.comboBoxClient.Items.Clear();
            this.comboBoxPoint.Items.Clear();
            this.comboBoxService.Items.Clear();
            this.comboBoxRecipient.Items.Clear();
            this.comboBoxClient.Items.Add("");
            this.comboBoxPoint.Items.Add("");
            this.comboBoxService.Items.Add("");
            this.comboBoxRecipient.Items.Add("");
            this.comboBoxClient.Items.AddRange(ClientsContainer.Instance.ToArray());
            this.comboBoxPoint.Items.AddRange(PointsContainer.Instance.ToArray());
            this.comboBoxService.Items.AddRange(ServicesContainer.Instance.ToArray());
            this.comboBoxRecipient.Items.AddRange(RecipientsContainer.Instance.ToArray());
        }

        public void ResetFilter()
        {
            this.dateTimeEnd.Value = this.dateTimeStart.Value = DateTime.Now;
            this.SubFormat = null;
            this.numericEndTotal.Value = this.numericStartTotal.Value = 0M;
            this.comboBoxService.SelectedIndex = this.comboBoxClient.SelectedIndex = this.comboBoxPoint.SelectedIndex = this.comboBoxStatus.SelectedIndex = 0;
            this.textBoxAccount.Text = string.Empty;
        }

        private DateTime TruncateDateTime(DateTime source, string format)
        {
            return DateTime.ParseExact(source.ToString(format, this.CurrentCulture), format, this.CurrentCulture);
        }

        private CultureInfo CurrentCulture
        {
            get
            {
                return MainSettings.Default.CultureInfo;
            }
        }

        private string DateTimeFormat
        {
            get
            {
                return this.dateTimeStart.CustomFormat;
            }
            set
            {
                this.dateTimeStart.CustomFormat = this.dateTimeEnd.CustomFormat = value;
            }
        }

        public FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                string subFormat = this.SubFormat;
                PaymentsFilter filter = string.IsNullOrEmpty(subFormat) ? new PaymentsFilter() : PaymentsFilter.WithStrictTime;
                oper.Tag = subFormat;
                filter.StartTotal = this.numericStartTotal.Value;
                filter.EndTotal = this.numericEndTotal.Value;
                filter.StartTime = this.dateTimeStart.Value;
                filter.EndTime = this.dateTimeEnd.Value;
                filter.NoAttributes = this.checkBoxAttr.Checked;
                if (this.comboBoxClient.SelectedItem is Client)
                {
                    filter.ClientID = (this.comboBoxClient.SelectedItem as Client).Id;
                }
                else
                {
                    filter.ClientID = Guid.Empty;
                }
                if (this.comboBoxPoint.SelectedItem is PayPoint)
                {
                    filter.PointID = (this.comboBoxPoint.SelectedItem as PayPoint).Id;
                }
                else
                {
                    filter.PointID = Guid.Empty;
                }
                if (this.comboBoxService.SelectedItem is Service)
                {
                    filter.ServiceID = (this.comboBoxService.SelectedItem as Service).Id;
                }
                else
                {
                    filter.ServiceID = Guid.Empty;
                }
                if (this.comboBoxOption.SelectedItem.Equals(ACCOUNT))
                {
                    filter.Account = this.textBoxAccount.Text;
                }
                if (this.comboBoxOption.SelectedItem.Equals(NUMBER))
                {
                    filter.PaymentNumber = this.textBoxAccount.Text;
                }
                if (this.comboBoxOption.SelectedItem.Equals(ID))
                {
                    filter.PaymentID = new Guid(this.textBoxAccount.Text);
                }
                if (this.comboBoxStatus.SelectedItem is EnumTypeInfo<PaymentState>)
                {
                    filter.State = (this.comboBoxStatus.SelectedItem as EnumTypeInfo<PaymentState>).EnumValue;
                }
                else
                {
                    filter.State = PaymentState.New;
                }
                if (this.comboBoxRecipient.SelectedItem is Recipient)
                {
                    filter.RecipientID = (this.comboBoxRecipient.SelectedItem as Recipient).Id;
                }
                else
                {
                    filter.RecipientID = Guid.Empty;
                }
                filter.PaymentTypeMask = this.checkBoxPaymentType.TypeMask;
                filter.DealerOnly = !this.withSubDealers.Checked;
                filter.ByStateTime = this.checkBoxByStateTime.Checked;
                oper.Filter = filter;
                return oper;
            }
            set
            {
                FilterOper oper = value;
                PaymentsFilter filter = (PaymentsFilter) oper.Filter;
                this.dateTimeStart.Value = filter.StartTime;
                this.dateTimeEnd.Value = filter.EndTime;
                this.SubFormat = oper.Tag as string;
                this.numericStartTotal.Value = filter.StartTotal;
                this.numericEndTotal.Value = filter.EndTotal;
                if (!string.IsNullOrEmpty(filter.Account))
                {
                    this.comboBoxOption.SelectedItem = ACCOUNT;
                    this.textBoxAccount.Text = filter.Account;
                }
                else if (!string.IsNullOrEmpty(filter.PaymentNumber))
                {
                    this.comboBoxOption.SelectedItem = NUMBER;
                    this.textBoxAccount.Text = filter.PaymentNumber;
                }
                else
                {
                    this.textBoxAccount.Text = "";
                }
                this.checkBoxAttr.Checked = filter.NoAttributes;
                this.checkBoxByStateTime.Checked = filter.ByStateTime;
                if (filter.ServiceID != Guid.Empty)
                {
                    this.comboBoxService.SelectedItem = ServicesContainer.Instance[filter.ServiceID];
                }
                else
                {
                    this.comboBoxService.SelectedIndex = 0;
                }
                if (filter.ClientID != Guid.Empty)
                {
                    this.comboBoxClient.SelectedItem = ClientsContainer.Instance[filter.ClientID];
                }
                else
                {
                    this.comboBoxClient.SelectedIndex = 0;
                }
                if (filter.PointID != Guid.Empty)
                {
                    this.comboBoxPoint.SelectedItem = PointsContainer.Instance[filter.PointID];
                }
                else
                {
                    this.comboBoxPoint.SelectedIndex = 0;
                }
                if (filter.RecipientID != Guid.Empty)
                {
                    this.comboBoxRecipient.SelectedItem = RecipientsContainer.Instance[filter.RecipientID];
                }
                else
                {
                    this.comboBoxRecipient.SelectedIndex = 0;
                }
                this.checkBoxPaymentType.TypeMask = filter.PaymentTypeMask;
                foreach (object obj2 in this.comboBoxStatus.Items)
                {
                    EnumTypeInfo<PaymentState> info = obj2 as EnumTypeInfo<PaymentState>;
                    if (((PaymentState) info.EnumValue) == filter.State)
                    {
                        this.comboBoxStatus.SelectedItem = obj2;
                        break;
                    }
                }
                this.withSubDealers.Checked = !filter.DealerOnly;
            }
        }

        private string SubFormat
        {
            get
            {
                foreach (ToolStripMenuItem item in this.contextMenuStripTime.Items)
                {
                    if (item.Checked)
                    {
                        string tag = item.Tag as string;
                        return (string.IsNullOrEmpty(tag) ? string.Empty : tag);
                    }
                }
                return "";
            }
            set
            {
                foreach (ToolStripMenuItem item in this.contextMenuStripTime.Items)
                {
                    if (((value == null) && (item.Tag == null)) || ((item.Tag != null) && item.Tag.Equals(value)))
                    {
                        string str = string.IsNullOrEmpty(value) ? "" : value;
                        item.Checked = true;
                        string format = this.CurrentCulture.DateTimeFormat.ShortDatePattern + str;
                        this.DateTimeFormat = format;
                        this.dateTimeEnd.Value = this.TruncateDateTime(this.dateTimeEnd.Value, format);
                        this.dateTimeStart.Value = this.TruncateDateTime(this.dateTimeStart.Value, format);
                    }
                    else
                    {
                        item.Checked = false;
                    }
                }
            }
        }
    }
}

