﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Oper;
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited=false)]
    internal class FilterControlAttribute : Attribute
    {
        private TypeSearch type;

        public FilterControlAttribute(TypeSearch type)
        {
            this.type = type;
        }

        public TypeSearch Type
        {
            get
            {
                return this.type;
            }
        }
    }
}

