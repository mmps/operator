﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.Clients)]
    internal class ClientFilterControl : UserControl, IFilterControl
    {
        private TextBox _part;
        private CheckBox _rootDealers;
        private CheckBox _subDealers;
        private IContainer components;

        public ClientFilterControl()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        void IFilterControl.ReadContainers()
        {
        }

        void IFilterControl.ResetFilter()
        {
            this._part.Text = "";
            this._subDealers.Checked = false;
            this._rootDealers.Checked = false;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientFilterControl));
            this._part = new TextBox();
            this._subDealers = new CheckBox();
            this._rootDealers = new CheckBox();
            Label label = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "label6");
            label.Name = "label6";
            manager.ApplyResources(this._part, "_part");
            this._part.Name = "_part";
            manager.ApplyResources(this._subDealers, "_subDealers");
            this._subDealers.Name = "_subDealers";
            this._subDealers.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._rootDealers, "_rootDealers");
            this._rootDealers.Name = "_rootDealers";
            this._rootDealers.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._rootDealers);
            base.Controls.Add(this._subDealers);
            base.Controls.Add(label);
            base.Controls.Add(this._part);
            base.Name = "ClientFilterControl";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        FilterOper IFilterControl.Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                ClientFilter filter = new ClientFilter();
                filter.Part = this._part.Text;
                filter.WithSubDealers = this._subDealers.Checked;
                filter.RootDealers = this._rootDealers.Checked;
                oper.Filter = filter;
                return oper;
            }
            set
            {
                ClientFilter filter = (ClientFilter) value.Filter;
                this._part.Text = filter.Part;
                this._subDealers.Checked = filter.WithSubDealers;
                this._rootDealers.Checked = filter.RootDealers;
            }
        }
    }
}

