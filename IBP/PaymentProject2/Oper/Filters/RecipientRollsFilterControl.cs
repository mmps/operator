﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.RecipientRolls)]
    internal class RecipientRollsFilterControl : UserControl, IFilterControl
    {
        private CheckBox _activeOnly;
        private DateTimePicker _beginDate;
        private DateTimePicker _endDate;
        private NumericUpDown _rollId;
        private ComboBox cmbRecipientId;
        private IContainer components;

        public RecipientRollsFilterControl()
        {
            this.InitializeComponent();
            this.ReadContainers();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecipientRollsFilterControl));
            this._beginDate = new DateTimePicker();
            this._endDate = new DateTimePicker();
            this._activeOnly = new CheckBox();
            this._rollId = new NumericUpDown();
            this.cmbRecipientId = new ComboBox();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            this._rollId.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "label1");
            label.Name = "label1";
            manager.ApplyResources(label2, "label2");
            label2.Name = "label2";
            manager.ApplyResources(label3, "label4");
            label3.Name = "label4";
            manager.ApplyResources(label4, "label5");
            label4.Name = "label5";
            manager.ApplyResources(this._beginDate, "_beginDate");
            this._beginDate.Format = DateTimePickerFormat.Short;
            this._beginDate.Name = "_beginDate";
            this._endDate.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this._endDate, "_endDate");
            this._endDate.Name = "_endDate";
            manager.ApplyResources(this._activeOnly, "_activeOnly");
            this._activeOnly.Name = "_activeOnly";
            this._activeOnly.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._rollId, "_rollId");
            int[] bits = new int[4];
            bits[0] = 0x7fffffff;
            this._rollId.Maximum = new decimal(bits);
            this._rollId.Name = "_rollId";
            this.cmbRecipientId.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cmbRecipientId.FormattingEnabled = true;
            manager.ApplyResources(this.cmbRecipientId, "cmbRecipientId");
            this.cmbRecipientId.Name = "cmbRecipientId";
            this.cmbRecipientId.Sorted = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.cmbRecipientId);
            base.Controls.Add(this._rollId);
            base.Controls.Add(this._activeOnly);
            base.Controls.Add(label);
            base.Controls.Add(this._beginDate);
            base.Controls.Add(label2);
            base.Controls.Add(this._endDate);
            base.Controls.Add(label4);
            base.Controls.Add(label3);
            base.Name = "RecipientRollsFilterControl";
            this._rollId.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            try
            {
                base.SuspendLayout();
                this._rollId.Value = 0M;
                this.cmbRecipientId.Items.Clear();
                this.cmbRecipientId.Items.Add("");
                Recipient[] items = RecipientsContainer.Instance.ToArray();
                this.cmbRecipientId.Items.AddRange(items);
                this.cmbRecipientId.SelectedIndex = 0;
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        public void ResetFilter()
        {
            this._activeOnly.Checked = true;
            this._rollId.Value = 0M;
            this.cmbRecipientId.SelectedItem = "";
            this._beginDate.Value = DateTime.Today;
            this._endDate.Value = DateTime.Today.AddDays(-1.0);
        }

        public FilterOper Filter
        {
            get
            {
                RecipientRollsFilterOper oper = new RecipientRollsFilterOper();
                oper.RollsFilter.EndTime = this._endDate.Value;
                oper.RollsFilter.ActiveOnly = this._activeOnly.Checked;
                oper.RollsFilter.RecipientId = this.SelectedRecipientId;
                oper.RollsFilter.RollID = (int) this._rollId.Value;
                oper.RollsFilter.StartTime = this._beginDate.Value;
                return oper;
            }
            set
            {
                RecipientRollsFilterOper oper = (RecipientRollsFilterOper) value;
                this._beginDate.Value = oper.RollsFilter.StartTime;
                this._endDate.Value = oper.RollsFilter.EndTime;
                this._rollId.Value = oper.RollsFilter.RollID;
                this.SelectedRecipientId = oper.RollsFilter.RecipientId;
                this._activeOnly.Checked = oper.RollsFilter.ActiveOnly;
            }
        }

        public Guid SelectedRecipientId
        {
            get
            {
                Recipient selectedItem = this.cmbRecipientId.SelectedItem as Recipient;
                if (selectedItem != null)
                {
                    return selectedItem.Id;
                }
                return Guid.Empty;
            }
            set
            {
                Recipient recipient = (value == Guid.Empty) ? null : RecipientsContainer.Instance[value];
                if (recipient == null)
                {
                    this.cmbRecipientId.SelectedItem = "";
                }
                else
                {
                    this.cmbRecipientId.SelectedItem = recipient;
                }
            }
        }
    }
}

