﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;

    public class FilterOper
    {
        private FilterBase filter;
        private IBP.PaymentProject2.Oper.StatisticType statisticType;
        private object tag;

        public FilterBase Filter
        {
            get
            {
                return this.filter;
            }
            set
            {
                this.filter = value;
            }
        }

        public IBP.PaymentProject2.Oper.StatisticType StatisticType
        {
            get
            {
                return this.statisticType;
            }
            set
            {
                this.statisticType = value;
            }
        }

        public object Tag
        {
            get
            {
                return this.tag;
            }
            set
            {
                this.tag = value;
            }
        }
    }
}

