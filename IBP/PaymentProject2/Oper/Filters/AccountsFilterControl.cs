﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.Accounts)]
    internal class AccountsFilterControl : UserControl, IFilterControl
    {
        private CheckBox checkBoxHideExternal;
        private ComboBox comboBoxTown;
        private IContainer components;
        private TextBox textBoxAccount;

        public AccountsFilterControl()
        {
            this.InitializeComponent();
            this.ReadContainers();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(AccountsFilterControl));
            this.comboBoxTown = new ComboBox();
            this.textBoxAccount = new TextBox();
            this.checkBoxHideExternal = new CheckBox();
            Label label = new Label();
            Label label2 = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "label4");
            label.Name = "label4";
            manager.ApplyResources(label2, "label6");
            label2.Name = "label6";
            this.comboBoxTown.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxTown.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxTown.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxTown.DropDownWidth = 250;
            this.comboBoxTown.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxTown, "comboBoxTown");
            this.comboBoxTown.Items.AddRange(new object[] { manager.GetString("comboBoxTown.Items") });
            this.comboBoxTown.Name = "comboBoxTown";
            this.comboBoxTown.Sorted = true;
            manager.ApplyResources(this.textBoxAccount, "textBoxAccount");
            this.textBoxAccount.Name = "textBoxAccount";
            manager.ApplyResources(this.checkBoxHideExternal, "checkBoxHideExternal");
            this.checkBoxHideExternal.Checked = true;
            this.checkBoxHideExternal.CheckState = CheckState.Checked;
            this.checkBoxHideExternal.Name = "checkBoxHideExternal";
            this.checkBoxHideExternal.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.checkBoxHideExternal);
            base.Controls.Add(label2);
            base.Controls.Add(this.textBoxAccount);
            base.Controls.Add(this.comboBoxTown);
            base.Controls.Add(label);
            base.Name = "AccountsFilterControl";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            this.comboBoxTown.Items.Clear();
            this.comboBoxTown.Items.Add("");
            this.comboBoxTown.Items.AddRange(TownsContainer.Instance.ToArray());
            this.comboBoxTown.SelectedIndex = 0;
        }

        public void ResetFilter()
        {
            this.comboBoxTown.SelectedIndex = 0;
            this.textBoxAccount.Text = "";
        }

        public FilterOper Filter
        {
            get
            {
                AccountFilter filter = new AccountFilter();
                FilterOper oper = new FilterOper();
                filter.Part = this.textBoxAccount.Text;
                filter.TownId = this.SelectedTownId;
                filter.HideAccountsWithoutBalance = this.checkBoxHideExternal.Checked;
                oper.Filter = filter;
                return oper;
            }
            set
            {
                AccountFilter filter = (AccountFilter) value.Filter;
                this.SelectedTownId = filter.TownId;
                this.textBoxAccount.Text = filter.Part;
                this.checkBoxHideExternal.Checked = filter.HideAccountsWithoutBalance;
            }
        }

        private Town SelectedTown
        {
            get
            {
                return (this.comboBoxTown.SelectedItem as Town);
            }
            set
            {
                if (value == null)
                {
                    this.comboBoxTown.SelectedIndex = 0;
                }
                else
                {
                    this.comboBoxTown.SelectedItem = value;
                }
            }
        }

        private int SelectedTownId
        {
            get
            {
                if (this.SelectedTown != null)
                {
                    return this.SelectedTown.Id;
                }
                return 0;
            }
            set
            {
                if (value == 0)
                {
                    this.SelectedTown = null;
                }
                else
                {
                    this.SelectedTown = TownsContainer.Instance[value];
                }
            }
        }
    }
}

