﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms.Controls;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.Statistic)]
    internal class StatisticFilter : UserControl, IFilterControl
    {
        private CheckedListBox cbfilteroption;
        private PaymentTypeCheckBox checkBoxPaymentType;
        private ComboBox comboBoxClient;
        private ComboBox comboBoxPoint;
        private ComboBox comboBoxRecipient;
        private ComboBox comboBoxService;
        private ComboBox comboBoxStatType;
        private ComboBox comboBoxStatus;
        private IContainer components;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private Label label1;
        private Label label10;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private NumericUpDown numericEndTotal;
        private NumericUpDown numericStartTotal;
        private StatisticOptionsController options;
        private CheckBox withSubDealers;

        public StatisticFilter()
        {
            this.InitializeComponent();
            this.ReadContainers();
            foreach (EnumTypeInfo<PaymentState> info in EnumTypeInfo<PaymentState>.GetInfo())
            {
                this.comboBoxStatus.Items.Add(info);
            }
            LocalDataStoreSlot namedDataSlot = Thread.GetNamedDataSlot("statistic");
            bool flag = (Thread.GetData(namedDataSlot) != null) && ((bool) Thread.GetData(namedDataSlot));
            foreach (EnumTypeInfo<IBP.PaymentProject2.Oper.StatisticType> info2 in EnumTypeInfo<IBP.PaymentProject2.Oper.StatisticType>.GetInfo())
            {
                if (flag || (((IBP.PaymentProject2.Oper.StatisticType) info2.EnumValue) != IBP.PaymentProject2.Oper.StatisticType.byDaysofWorkState))
                {
                    this.comboBoxStatType.Items.Add(info2);
                }
            }
            if (this.comboBoxStatus.Items.Count > 0)
            {
                this.comboBoxStatus.SelectedIndex = 0;
            }
            this.dateTimeEnd.Format = this.dateTimeStart.Format = DateTimePickerFormat.Custom;
            this.dateTimeEnd.CustomFormat = this.dateTimeStart.CustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
            this.options = new StatisticOptionsController(this.cbfilteroption);
            MainSettings.Default.CultureInfoChanged += new EventHandler(this.Default_CultureInfoChanged);
        }

        private void comboBoxClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.comboBoxPoint.Items.Clear();
            this.comboBoxPoint.Items.Add("");
            if (this.comboBoxClient.SelectedItem is Client)
            {
                Client selectedItem = this.comboBoxClient.SelectedItem as Client;
                foreach (PayPoint point in PointsContainer.Instance.Collection)
                {
                    if (point.ClientId.Equals(selectedItem.Id))
                    {
                        this.comboBoxPoint.Items.Add(point);
                    }
                }
            }
            else
            {
                this.comboBoxPoint.Items.AddRange(PointsContainer.Instance.ToArray());
            }
        }

        private void Default_CultureInfoChanged(object sender, EventArgs e)
        {
            this.dateTimeEnd.CustomFormat = this.dateTimeStart.CustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(StatisticFilter));
            this.label6 = new Label();
            this.label5 = new Label();
            this.comboBoxStatus = new ComboBox();
            this.numericEndTotal = new NumericUpDown();
            this.numericStartTotal = new NumericUpDown();
            this.label9 = new Label();
            this.label8 = new Label();
            this.label7 = new Label();
            this.comboBoxClient = new ComboBox();
            this.comboBoxPoint = new ComboBox();
            this.comboBoxService = new ComboBox();
            this.label4 = new Label();
            this.label3 = new Label();
            this.dateTimeEnd = new DateTimePicker();
            this.label2 = new Label();
            this.label1 = new Label();
            this.dateTimeStart = new DateTimePicker();
            this.comboBoxStatType = new ComboBox();
            this.label10 = new Label();
            this.comboBoxRecipient = new ComboBox();
            this.cbfilteroption = new CheckedListBox();
            this.checkBoxPaymentType = new PaymentTypeCheckBox();
            this.withSubDealers = new CheckBox();
            this.numericEndTotal.BeginInit();
            this.numericStartTotal.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            this.comboBoxStatus.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxStatus.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxStatus.DropDownWidth = 250;
            this.comboBoxStatus.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxStatus, "comboBoxStatus");
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Sorted = true;
            this.numericEndTotal.DecimalPlaces = 2;
            manager.ApplyResources(this.numericEndTotal, "numericEndTotal");
            int[] bits = new int[4];
            bits[0] = -727379969;
            bits[1] = 0xe8;
            this.numericEndTotal.Maximum = new decimal(bits);
            this.numericEndTotal.Name = "numericEndTotal";
            this.numericStartTotal.DecimalPlaces = 2;
            manager.ApplyResources(this.numericStartTotal, "numericStartTotal");
            int[] numArray2 = new int[4];
            numArray2[0] = -727379969;
            numArray2[1] = 0xe8;
            this.numericStartTotal.Maximum = new decimal(numArray2);
            this.numericStartTotal.Name = "numericStartTotal";
            manager.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            manager.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            manager.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            this.comboBoxClient.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxClient.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxClient.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxClient.DropDownWidth = 250;
            this.comboBoxClient.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxClient, "comboBoxClient");
            this.comboBoxClient.Name = "comboBoxClient";
            this.comboBoxClient.Sorted = true;
            this.comboBoxClient.SelectedIndexChanged += new EventHandler(this.comboBoxClient_SelectedIndexChanged);
            this.comboBoxPoint.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxPoint.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxPoint.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxPoint.DropDownWidth = 250;
            this.comboBoxPoint.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxPoint, "comboBoxPoint");
            this.comboBoxPoint.Name = "comboBoxPoint";
            this.comboBoxPoint.Sorted = true;
            this.comboBoxService.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxService.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxService.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxService.DropDownWidth = 250;
            this.comboBoxService.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxService, "comboBoxService");
            this.comboBoxService.Name = "comboBoxService";
            this.comboBoxService.Sorted = true;
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.dateTimeEnd.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Name = "dateTimeEnd";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.dateTimeStart.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Name = "dateTimeStart";
            this.comboBoxStatType.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxStatType.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxStatType.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxStatType.DropDownWidth = 250;
            this.comboBoxStatType.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxStatType, "comboBoxStatType");
            this.comboBoxStatType.Name = "comboBoxStatType";
            this.comboBoxStatType.Sorted = true;
            manager.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            this.comboBoxRecipient.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxRecipient.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxRecipient.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxRecipient.DropDownWidth = 250;
            this.comboBoxRecipient.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxRecipient, "comboBoxRecipient");
            this.comboBoxRecipient.Name = "comboBoxRecipient";
            this.comboBoxRecipient.Sorted = true;
            this.cbfilteroption.BackColor = SystemColors.Control;
            this.cbfilteroption.BorderStyle = BorderStyle.None;
            this.cbfilteroption.FormattingEnabled = true;
            this.cbfilteroption.Items.AddRange(new object[] { manager.GetString("cbfilteroption.Items"), manager.GetString("cbfilteroption.Items1") });
            manager.ApplyResources(this.cbfilteroption, "cbfilteroption");
            this.cbfilteroption.Name = "cbfilteroption";
            this.cbfilteroption.ThreeDCheckBoxes = true;
            manager.ApplyResources(this.checkBoxPaymentType, "checkBoxPaymentType");
            this.checkBoxPaymentType.Name = "checkBoxPaymentType";
            this.checkBoxPaymentType.TypeMask = (PaymentType) manager.GetObject("checkBoxPaymentType.TypeMask");
            this.checkBoxPaymentType.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.withSubDealers, "withSubDealers");
            this.withSubDealers.Name = "withSubDealers";
            this.withSubDealers.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.comboBoxClient);
            base.Controls.Add(this.checkBoxPaymentType);
            base.Controls.Add(this.withSubDealers);
            base.Controls.Add(this.cbfilteroption);
            base.Controls.Add(this.label10);
            base.Controls.Add(this.comboBoxRecipient);
            base.Controls.Add(this.comboBoxStatType);
            base.Controls.Add(this.label6);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.comboBoxStatus);
            base.Controls.Add(this.numericEndTotal);
            base.Controls.Add(this.numericStartTotal);
            base.Controls.Add(this.label9);
            base.Controls.Add(this.label8);
            base.Controls.Add(this.label7);
            base.Controls.Add(this.comboBoxPoint);
            base.Controls.Add(this.comboBoxService);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.dateTimeStart);
            base.Name = "StatisticFilter";
            this.numericEndTotal.EndInit();
            this.numericStartTotal.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            this.comboBoxClient.Items.Clear();
            this.comboBoxPoint.Items.Clear();
            this.comboBoxService.Items.Clear();
            this.comboBoxRecipient.Items.Clear();
            this.comboBoxClient.Items.Add("");
            this.comboBoxPoint.Items.Add("");
            this.comboBoxService.Items.Add("");
            this.comboBoxRecipient.Items.Add("");
            this.comboBoxClient.Items.AddRange(ClientsContainer.Instance.ToArray());
            this.comboBoxPoint.Items.AddRange(PointsContainer.Instance.ToArray());
            this.comboBoxService.Items.AddRange(ServicesContainer.Instance.ToArray());
            this.comboBoxRecipient.Items.AddRange(RecipientsContainer.Instance.ToArray());
        }

        public void ResetFilter()
        {
            this.dateTimeEnd.Value = this.dateTimeStart.Value = DateTime.Now;
            this.numericEndTotal.Value = this.numericStartTotal.Value = 0M;
            this.comboBoxService.SelectedIndex = this.comboBoxClient.SelectedIndex = this.comboBoxPoint.SelectedIndex = this.comboBoxStatus.SelectedIndex = 0;
            this.options = new StatisticOptionsController(this.cbfilteroption);
        }

        public FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                PaymentsFilter filter = new PaymentsFilter();
                oper.Filter = filter;
                filter.StartTime = this.dateTimeStart.Value;
                filter.EndTime = this.dateTimeEnd.Value;
                filter.StartTotal = this.numericStartTotal.Value;
                filter.EndTotal = this.numericEndTotal.Value;
                if (this.comboBoxClient.SelectedItem is Client)
                {
                    filter.ClientID = (this.comboBoxClient.SelectedItem as Client).Id;
                }
                else
                {
                    filter.ClientID = Guid.Empty;
                }
                if (this.comboBoxPoint.SelectedItem is PayPoint)
                {
                    filter.PointID = (this.comboBoxPoint.SelectedItem as PayPoint).Id;
                }
                else
                {
                    filter.PointID = Guid.Empty;
                }
                if (this.comboBoxService.SelectedItem is Service)
                {
                    filter.ServiceID = (this.comboBoxService.SelectedItem as Service).Id;
                }
                else
                {
                    filter.ServiceID = Guid.Empty;
                }
                if (this.comboBoxStatus.SelectedItem is EnumTypeInfo<PaymentState>)
                {
                    filter.State = (this.comboBoxStatus.SelectedItem as EnumTypeInfo<PaymentState>).EnumValue;
                }
                else
                {
                    filter.State = PaymentState.New;
                }
                if (this.comboBoxRecipient.SelectedItem is Recipient)
                {
                    filter.RecipientID = (this.comboBoxRecipient.SelectedItem as Recipient).Id;
                }
                else
                {
                    filter.RecipientID = Guid.Empty;
                }
                if (this.comboBoxStatType.SelectedItem is EnumTypeInfo<IBP.PaymentProject2.Oper.StatisticType>)
                {
                    oper.StatisticType = (this.comboBoxStatType.SelectedItem as EnumTypeInfo<IBP.PaymentProject2.Oper.StatisticType>).EnumValue;
                }
                filter.PaymentTypeMask = this.checkBoxPaymentType.TypeMask;
                filter.DealerOnly = !this.withSubDealers.Checked;
                this.options.SetOptionttoFilter(filter);
                return oper;
            }
            set
            {
                FilterOper oper = value;
                PaymentsFilter filter = (PaymentsFilter) oper.Filter;
                this.dateTimeStart.Value = filter.StartTime;
                this.dateTimeEnd.Value = filter.EndTime;
                this.numericStartTotal.Value = filter.StartTotal;
                this.numericEndTotal.Value = filter.EndTotal;
                if (filter.ServiceID != Guid.Empty)
                {
                    this.comboBoxService.SelectedItem = ServicesContainer.Instance[filter.ServiceID];
                }
                else
                {
                    this.comboBoxService.SelectedIndex = 0;
                }
                if (filter.ClientID != Guid.Empty)
                {
                    this.comboBoxClient.SelectedItem = ClientsContainer.Instance[filter.ClientID];
                }
                else
                {
                    this.comboBoxClient.SelectedIndex = 0;
                }
                if (filter.PointID != Guid.Empty)
                {
                    this.comboBoxPoint.SelectedItem = PointsContainer.Instance[filter.PointID];
                }
                else
                {
                    this.comboBoxPoint.SelectedIndex = 0;
                }
                if (filter.RecipientID != Guid.Empty)
                {
                    this.comboBoxRecipient.SelectedItem = RecipientsContainer.Instance[filter.RecipientID];
                }
                else
                {
                    this.comboBoxRecipient.SelectedIndex = 0;
                }
                foreach (object obj2 in this.comboBoxStatus.Items)
                {
                    EnumTypeInfo<PaymentState> info = obj2 as EnumTypeInfo<PaymentState>;
                    if (((PaymentState) info.EnumValue) == filter.State)
                    {
                        this.comboBoxStatus.SelectedItem = obj2;
                        break;
                    }
                }
                foreach (object obj3 in this.comboBoxStatType.Items)
                {
                    EnumTypeInfo<IBP.PaymentProject2.Oper.StatisticType> info2 = obj3 as EnumTypeInfo<IBP.PaymentProject2.Oper.StatisticType>;
                    if (((IBP.PaymentProject2.Oper.StatisticType) info2.EnumValue) == oper.StatisticType)
                    {
                        this.comboBoxStatType.SelectedItem = obj3;
                        break;
                    }
                }
                this.checkBoxPaymentType.TypeMask = filter.PaymentTypeMask;
                this.options.SetOptiontofromFilter(filter);
                this.withSubDealers.Checked = !filter.DealerOnly;
            }
        }
    }
}

