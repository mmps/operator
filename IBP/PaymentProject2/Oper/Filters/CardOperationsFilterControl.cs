﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.CardOperations)]
    internal class CardOperationsFilterControl : UserControl, IFilterControl
    {
        private TextBox _textBoxApprovalCode;
        private ComboBox comboBoxPoints;
        private ComboBox comboBoxSuccess;
        private IContainer components;
        private ContextMenuStrip contextMenuStripTime;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private NumericUpDown numericUpDownPaymentId;
        private TextBox textBoxCardNumber;
        private TextBox textBoxInvoiceId;
        private TextBox textBoxPosId;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;

        public CardOperationsFilterControl()
        {
            this.InitializeComponent();
            this.dateTimeEnd.Format = this.dateTimeStart.Format = DateTimePickerFormat.Custom;
            this.TimeFormat = null;
            this.DateTimeFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
            this.numericUpDownPaymentId.Maximum = 2147483647M;
            MainSettings.Default.CultureInfoChanged += new EventHandler(this.CultureInfoChanged);
            this.ReadContainers();
        }

        private void contextMenuStripTime_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.TimeFormat = e.ClickedItem.Tag as string;
        }

        private void CultureInfoChanged(object sender, EventArgs e)
        {
            this.DateTimeFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        void IFilterControl.ResetFilter()
        {
            this.Filter = new CardFilterOper();
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CardOperationsFilterControl));
            this.comboBoxPoints = new ComboBox();
            this.dateTimeEnd = new DateTimePicker();
            this.dateTimeStart = new DateTimePicker();
            this.contextMenuStripTime = new ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new ToolStripMenuItem();
            this.toolStripMenuItem2 = new ToolStripMenuItem();
            this.toolStripMenuItem3 = new ToolStripMenuItem();
            this.textBoxPosId = new TextBox();
            this.textBoxInvoiceId = new TextBox();
            this.numericUpDownPaymentId = new NumericUpDown();
            this.comboBoxSuccess = new ComboBox();
            this.textBoxCardNumber = new TextBox();
            this._textBoxApprovalCode = new TextBox();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            Label label5 = new Label();
            Label label6 = new Label();
            Label label7 = new Label();
            Label label8 = new Label();
            Label label9 = new Label();
            this.contextMenuStripTime.SuspendLayout();
            this.numericUpDownPaymentId.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "label2");
            label.Name = "label2";
            manager.ApplyResources(label2, "label1");
            label2.Name = "label1";
            manager.ApplyResources(label3, "label3");
            label3.Name = "label3";
            manager.ApplyResources(label4, "label4");
            label4.Name = "label4";
            manager.ApplyResources(label5, "label5");
            label5.Name = "label5";
            manager.ApplyResources(label6, "label6");
            label6.Name = "label6";
            manager.ApplyResources(label7, "label7");
            label7.Name = "label7";
            manager.ApplyResources(label8, "label8");
            label8.Name = "label8";
            this.comboBoxPoints.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxPoints.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxPoints.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxPoints.DropDownWidth = 250;
            this.comboBoxPoints.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxPoints, "comboBoxPoints");
            this.comboBoxPoints.Name = "comboBoxPoints";
            this.comboBoxPoints.Sorted = true;
            this.dateTimeEnd.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeStart.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Name = "dateTimeStart";
            this.contextMenuStripTime.Items.AddRange(new ToolStripItem[] { this.toolStripMenuItem1, this.toolStripMenuItem2, this.toolStripMenuItem3 });
            this.contextMenuStripTime.Name = "contextMenuStripTime";
            manager.ApplyResources(this.contextMenuStripTime, "contextMenuStripTime");
            this.contextMenuStripTime.ItemClicked += new ToolStripItemClickedEventHandler(this.contextMenuStripTime_ItemClicked);
            this.toolStripMenuItem1.Checked = true;
            this.toolStripMenuItem1.CheckState = CheckState.Checked;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            manager.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            manager.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            this.toolStripMenuItem2.Tag = "HH";
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            manager.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            this.toolStripMenuItem3.Tag = "HH:mm";
            manager.ApplyResources(this.textBoxPosId, "textBoxPosId");
            this.textBoxPosId.Name = "textBoxPosId";
            manager.ApplyResources(this.textBoxInvoiceId, "textBoxInvoiceId");
            this.textBoxInvoiceId.Name = "textBoxInvoiceId";
            int[] bits = new int[4];
            bits[0] = 100;
            this.numericUpDownPaymentId.Increment = new decimal(bits);
            manager.ApplyResources(this.numericUpDownPaymentId, "numericUpDownPaymentId");
            int[] numArray2 = new int[4];
            numArray2[0] = 0xf4240;
            this.numericUpDownPaymentId.Maximum = new decimal(numArray2);
            this.numericUpDownPaymentId.Name = "numericUpDownPaymentId";
            this.comboBoxSuccess.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.comboBoxSuccess.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.comboBoxSuccess.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxSuccess.DropDownWidth = 250;
            this.comboBoxSuccess.FormattingEnabled = true;
            manager.ApplyResources(this.comboBoxSuccess, "comboBoxSuccess");
            this.comboBoxSuccess.Name = "comboBoxSuccess";
            this.comboBoxSuccess.Sorted = true;
            manager.ApplyResources(this.textBoxCardNumber, "textBoxCardNumber");
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            manager.ApplyResources(this._textBoxApprovalCode, "_textBoxApprovalCode");
            this._textBoxApprovalCode.Name = "_textBoxApprovalCode";
            manager.ApplyResources(label9, "label9");
            label9.Name = "label9";
            base.Controls.Add(label8);
            base.Controls.Add(this.textBoxCardNumber);
            base.Controls.Add(label7);
            base.Controls.Add(this.comboBoxSuccess);
            base.Controls.Add(label9);
            base.Controls.Add(label6);
            base.Controls.Add(this._textBoxApprovalCode);
            base.Controls.Add(this.numericUpDownPaymentId);
            base.Controls.Add(label5);
            base.Controls.Add(label4);
            base.Controls.Add(this.textBoxPosId);
            base.Controls.Add(this.textBoxInvoiceId);
            base.Controls.Add(label3);
            base.Controls.Add(this.dateTimeEnd);
            base.Controls.Add(label);
            base.Controls.Add(label2);
            base.Controls.Add(this.dateTimeStart);
            base.Controls.Add(this.comboBoxPoints);
            base.Name = "CardOperationsFilterControl";
            manager.ApplyResources(this, "$this");
            this.contextMenuStripTime.ResumeLayout(false);
            this.numericUpDownPaymentId.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            try
            {
                base.SuspendLayout();
                this.comboBoxSuccess.Items.Clear();
                this.comboBoxSuccess.Items.AddRange(EnumTypeInfo<CardOperationsFilter.SuccessFilter>.GetInfo());
                this.comboBoxSuccess.SelectedIndex = 0;
                this.comboBoxPoints.Items.Clear();
                this.comboBoxPoints.Items.Add("");
                this.comboBoxPoints.Items.AddRange(PointsContainer.Instance.ToArray());
                this.comboBoxPoints.SelectedIndex = 0;
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        private DateTime TruncateDateTime(DateTime source, string format)
        {
            return DateTime.ParseExact(source.ToString(format), format, MainSettings.Default.CultureInfo);
        }

        private string DateTimeFormat
        {
            get
            {
                return this.dateTimeStart.CustomFormat;
            }
            set
            {
                this.dateTimeStart.CustomFormat = this.dateTimeEnd.CustomFormat = value;
            }
        }

        private DateTime EndTime
        {
            get
            {
                DateTime time = this.dateTimeEnd.Value;
                if (!(this.TimeFormat == ""))
                {
                    return time;
                }
                return time.AddDays(1.0);
            }
            set
            {
                DateTime time = (this.TimeFormat == "") ? value.AddDays(-1.0) : value;
                this.dateTimeEnd.Value = (time < this.dateTimeStart.Value) ? this.dateTimeStart.Value : time;
            }
        }

        public FilterOper Filter
        {
            get
            {
                CardFilterOper oper = new CardFilterOper();
                oper.Tag = this.TimeFormat;
                oper.CardFilter.InvoiceId = this.textBoxInvoiceId.Text;
                oper.CardFilter.CardNumber = this.textBoxCardNumber.Text;
                oper.CardFilter.EndTime = this.dateTimeEnd.Value;
                oper.CardFilter.PaymentId = (int) this.numericUpDownPaymentId.Value;
                oper.CardFilter.PointId = this.SelectedPointId;
                oper.CardFilter.PosId = this.textBoxPosId.Text;
                oper.CardFilter.StartTime = this.dateTimeStart.Value;
                oper.CardFilter.Success = this.SelectedSuccess;
                oper.CardFilter.ApprovalCode = this._textBoxApprovalCode.Text;
                return oper;
            }
            set
            {
                CardFilterOper oper = (CardFilterOper) value;
                this.TimeFormat = oper.Tag as string;
                this.textBoxCardNumber.Text = oper.CardFilter.CardNumber;
                this.textBoxInvoiceId.Text = oper.CardFilter.InvoiceId;
                this.textBoxPosId.Text = oper.CardFilter.PosId;
                this.StartTime = oper.CardFilter.StartTime;
                this.EndTime = oper.CardFilter.EndTime;
                this.numericUpDownPaymentId.Value = oper.CardFilter.PaymentId;
                this.SelectedPointId = oper.CardFilter.PointId;
                this.SelectedSuccess = oper.CardFilter.Success;
                this._textBoxApprovalCode.Text = oper.CardFilter.ApprovalCode;
            }
        }

        private int SelectedPointId
        {
            get
            {
                PayPoint selectedItem = this.comboBoxPoints.SelectedItem as PayPoint;
                if (selectedItem != null)
                {
                    return selectedItem.Serial;
                }
                return 0;
            }
            set
            {
                PayPoint pointBySerial = PointsContainer.Instance.GetPointBySerial(value);
                if (pointBySerial == null)
                {
                    this.comboBoxPoints.SelectedItem = "";
                }
                else
                {
                    this.comboBoxPoints.SelectedItem = pointBySerial;
                }
            }
        }

        private CardOperationsFilter.SuccessFilter SelectedSuccess
        {
            get
            {
                EnumTypeInfo<CardOperationsFilter.SuccessFilter> selectedItem = this.comboBoxSuccess.SelectedItem as EnumTypeInfo<CardOperationsFilter.SuccessFilter>;
                return selectedItem.EnumValue;
            }
            set
            {
                this.comboBoxSuccess.SelectedItem = new EnumTypeInfo<CardOperationsFilter.SuccessFilter>(value);
            }
        }

        private DateTime StartTime
        {
            get
            {
                return this.dateTimeStart.Value;
            }
            set
            {
                this.dateTimeStart.Value = value;
            }
        }

        private string TimeFormat
        {
            get
            {
                foreach (ToolStripMenuItem item in this.contextMenuStripTime.Items)
                {
                    if (item.Checked)
                    {
                        if (item.Tag != null)
                        {
                            return (item.Tag as string);
                        }
                        return "";
                    }
                }
                return "";
            }
            set
            {
                foreach (ToolStripMenuItem item in this.contextMenuStripTime.Items)
                {
                    if (((item.Tag == null) && (value == null)) || ((item.Tag != null) && item.Tag.Equals(value)))
                    {
                        item.Checked = true;
                        string str = (value == null) ? "" : value;
                        string format = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern + " " + str;
                        this.DateTimeFormat = format;
                        this.dateTimeEnd.Value = this.TruncateDateTime(this.dateTimeEnd.Value, format);
                        this.dateTimeStart.Value = this.TruncateDateTime(this.dateTimeStart.Value, format);
                    }
                    else
                    {
                        item.Checked = false;
                    }
                }
            }
        }
    }
}

