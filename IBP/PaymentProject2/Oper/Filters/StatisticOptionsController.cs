﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class StatisticOptionsController
    {
        private CheckedListBox list;
        private Dictionary<string, Option> options = Option.GetOptions();

        public StatisticOptionsController(CheckedListBox list)
        {
            this.list = list;
            this.Rebild();
        }

        private void Rebild()
        {
            this.list.Items.Clear();
            foreach (Option option in this.options.Values)
            {
                this.list.Items.Add(option, option.Checked);
            }
        }

        public void SetOptiontofromFilter(PaymentsFilter _filter)
        {
            if (_filter != null)
            {
                this.NeedEmpty = _filter.ShowEmpty;
                this.ByStateTime = _filter.ByStateTime;
            }
            this.Rebild();
        }

        public void SetOptionttoFilter(PaymentsFilter filter)
        {
            foreach (Option option in this.options.Values)
            {
                option.Checked = false;
            }
            foreach (object obj2 in this.list.CheckedItems)
            {
                Option option2 = (Option) obj2;
                option2.Checked = true;
                this.options[option2.Name] = option2;
            }
            filter.ShowEmpty = this.NeedEmpty;
            filter.ByStateTime = this.ByStateTime;
        }

        private bool ByStateTime
        {
            get
            {
                return this.options[ByStateTimeOption.FullName].Checked;
            }
            set
            {
                this.options[ByStateTimeOption.FullName].Checked = value;
            }
        }

        private bool NeedEmpty
        {
            get
            {
                return this.options[NeedEmptyOption.FullName].Checked;
            }
            set
            {
                this.options[NeedEmptyOption.FullName].Checked = value;
            }
        }

        private class ByStateTimeOption : StatisticOptionsController.Option
        {
            public static string FullName = UserStrings.OnStatusTime;

            public ByStateTimeOption()
            {
                base.Name = FullName;
            }
        }

        private class NeedEmptyOption : StatisticOptionsController.Option
        {
            public static string FullName = UserStrings.LeftEmpty;

            public NeedEmptyOption()
            {
                base.Name = FullName;
            }
        }

        private class Option
        {
            public bool Checked;
            public string Name;

            public static Dictionary<string, StatisticOptionsController.Option> GetOptions()
            {
                Dictionary<string, StatisticOptionsController.Option> dictionary = new Dictionary<string, StatisticOptionsController.Option>();
                StatisticOptionsController.Option option = new StatisticOptionsController.ByStateTimeOption();
                dictionary.Add(option.Name, option);
                option = new StatisticOptionsController.NeedEmptyOption();
                dictionary.Add(option.Name, option);
                return dictionary;
            }

            public override string ToString()
            {
                return this.Name;
            }
        }
    }
}

