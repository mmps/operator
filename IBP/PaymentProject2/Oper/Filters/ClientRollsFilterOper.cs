﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common.Rolls;
    using System;

    internal class ClientRollsFilterOper : FilterOper
    {
        public ClientRollsFilterOper()
        {
            base.Filter = new ClientRollFilter();
        }

        public ClientRollFilter RollsFilter
        {
            get
            {
                return (ClientRollFilter) base.Filter;
            }
            set
            {
                base.Filter = value;
            }
        }
    }
}

