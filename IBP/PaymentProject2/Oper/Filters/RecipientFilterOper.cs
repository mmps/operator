﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using System;

    internal class RecipientFilterOper : FilterOper
    {
        public RecipientFilterOper()
        {
            base.Filter = new RecipientFilter();
        }

        public RecipientFilter RecipientsFilter
        {
            get
            {
                return (RecipientFilter) base.Filter;
            }
            set
            {
                base.Filter = value;
            }
        }
    }
}

