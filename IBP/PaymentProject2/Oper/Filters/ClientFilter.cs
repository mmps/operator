﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Text.RegularExpressions;

    internal class ClientFilter : FilterBase
    {
        private string _part;
        private bool _rootDealers;
        private bool _withSubDealers;

        public bool IsInnPart
        {
            get
            {
                return Regex.IsMatch(this._part, "^[0-9]*$");
            }
        }

        public string Part
        {
            get
            {
                return this._part;
            }
            set
            {
                this._part = value;
            }
        }

        public bool RootDealers
        {
            get
            {
                return this._rootDealers;
            }
            set
            {
                this._rootDealers = value;
            }
        }

        public bool WithSubDealers
        {
            get
            {
                return this._withSubDealers;
            }
            set
            {
                this._withSubDealers = value;
            }
        }
    }
}

