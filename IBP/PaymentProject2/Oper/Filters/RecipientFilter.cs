﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Text.RegularExpressions;

    internal class RecipientFilter : FilterBase
    {
        private string _part;
        private int _recipientID;

        public bool IsInnPart
        {
            get
            {
                return Regex.IsMatch(this._part, "^[0-9]*$");
            }
        }

        public string Part
        {
            get
            {
                return this._part;
            }
            set
            {
                this._part = value;
            }
        }

        public int RecipientID
        {
            get
            {
                return this._recipientID;
            }
            set
            {
                this._recipientID = value;
            }
        }
    }
}

