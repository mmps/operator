﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using System;
    using System.ComponentModel;
    using System.Text;
    using System.Windows.Forms;

    [Category("Pluss+"), Description("Ширина столбиков диаграмы"), Browsable(true)]
    internal class Comboboxplus : ComboBox
    {
        private StringBuilder findstring = new StringBuilder();
        private Timer timer = new Timer();

        public Comboboxplus()
        {
            this.timer.Interval = 0xbb8;
            this.timer.Tick += new EventHandler(this.timer_Tick);
        }

        private int FindIndex(string str)
        {
            for (int i = 0; i < base.Items.Count; i++)
            {
                if (base.Items[i].ToString().Contains(str))
                {
                    return i;
                }
            }
            return 0;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            this.timer.Start();
            this.findstring.Append(e.KeyChar);
            this.SelectedIndex = this.FindIndex(this.findstring.ToString());
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.findstring.Remove(0, this.findstring.Length);
            this.timer.Stop();
        }

        [Description("Интервалл ввода"), Category("Pluss+"), Browsable(true)]
        public int Interval
        {
            get
            {
                return this.timer.Interval;
            }
            set
            {
                this.timer.Interval = value;
            }
        }
    }
}

