﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.ExternalTransactions)]
    internal class ExternalTransactionFilter : UserControl, IFilterControl
    {
        private IContainer components;

        public ExternalTransactionFilter()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ExternalTransactionFilter));
            base.SuspendLayout();
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Name = "ExternalTransactionFilter";
            base.ResumeLayout(false);
        }

        public void ReadContainers()
        {
        }

        public void ResetFilter()
        {
        }

        public FilterOper Filter
        {
            get
            {
                return new FilterOper();
            }
            set
            {
            }
        }
    }
}

