﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.Recipients)]
    public class RecipientFilterControl : UserControl, IFilterControl
    {
        private TextBox _part;
        private NumericUpDown _recipientId;
        private IContainer components;
        private Label label5;

        public RecipientFilterControl()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecipientFilterControl));
            this._part = new TextBox();
            this._recipientId = new NumericUpDown();
            this.label5 = new Label();
            Label label = new Label();
            this._recipientId.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(label, "label6");
            label.Name = "label6";
            manager.ApplyResources(this._part, "_part");
            this._part.Name = "_part";
            manager.ApplyResources(this._recipientId, "_recipientId");
            int[] bits = new int[4];
            bits[0] = 0x7fffffff;
            this._recipientId.Maximum = new decimal(bits);
            this._recipientId.Name = "_recipientId";
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._recipientId);
            base.Controls.Add(this.label5);
            base.Controls.Add(label);
            base.Controls.Add(this._part);
            base.Name = "RecipientFilterControl";
            this._recipientId.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
        }

        public void ResetFilter()
        {
            this._part.Text = string.Empty;
            this._recipientId.Value = 0M;
        }

        public FilterOper Filter
        {
            get
            {
                RecipientFilterOper oper = new RecipientFilterOper();
                oper.RecipientsFilter.Part = this._part.Text;
                oper.RecipientsFilter.RecipientID = (int) this._recipientId.Value;
                return oper;
            }
            set
            {
                RecipientFilterOper oper = (RecipientFilterOper) value;
                this._part.Text = oper.RecipientsFilter.Part;
                this._recipientId.Value = oper.RecipientsFilter.RecipientID;
            }
        }
    }
}

