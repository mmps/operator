﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Windows.Forms;

    internal static class FilterControl
    {
        private static Hashtable filtersTable = new Hashtable();

        private static UserControl CreateFilterControl(TypeSearch type)
        {
            return (FindType(type).InvokeMember(null, BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly, null, null, null) as UserControl);
        }

        private static System.Type FindType(TypeSearch type)
        {
            foreach (System.Type type2 in typeof(FilterControl).Assembly.GetTypes())
            {
                IEnumerator enumerator = type2.GetCustomAttributes(typeof(FilterControlAttribute), false).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    FilterControlAttribute current = enumerator.Current as FilterControlAttribute;
                    if (current.Type == type)
                    {
                        return type2;
                    }
                }
            }
            throw new ArgumentOutOfRangeException("type", "Can't find suitable gateway.");
        }

        private static UserControl GetF(TypeSearch type)
        {
            if (filtersTable.ContainsKey(type))
            {
                return (filtersTable[type] as UserControl);
            }
            UserControl control = null;
            try
            {
                control = CreateFilterControl(type);
                filtersTable.Add(type, control);
            }
            catch (ArgumentOutOfRangeException)
            {
                return GetF(TypeSearch.Payments);
            }
            return control;
        }

        public static UserControl GetFilter(TypeSearch type)
        {
            lock (filtersTable)
            {
                return GetF(type);
            }
        }
    }
}

