﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.ClientsRolls)]
    internal class ClientRollsFilterControl : UserControl, IFilterControl
    {
        private CheckBox chbActiveOnly;
        private ComboBox cmbClientId;
        private IContainer components;
        private DateTimePicker dtpEnd;
        private DateTimePicker dtpStart;
        private Label label1;
        private Label label2;
        private Label label4;
        private Label label5;
        private NumericUpDown nudRollId;

        public ClientRollsFilterControl()
        {
            this.InitializeComponent();
            this.ReadContainers();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientRollsFilterControl));
            this.cmbClientId = new ComboBox();
            this.nudRollId = new NumericUpDown();
            this.chbActiveOnly = new CheckBox();
            this.label1 = new Label();
            this.dtpStart = new DateTimePicker();
            this.label2 = new Label();
            this.dtpEnd = new DateTimePicker();
            this.label5 = new Label();
            this.label4 = new Label();
            this.nudRollId.BeginInit();
            base.SuspendLayout();
            this.cmbClientId.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cmbClientId.FormattingEnabled = true;
            manager.ApplyResources(this.cmbClientId, "cmbClientId");
            this.cmbClientId.Name = "cmbClientId";
            this.cmbClientId.Sorted = true;
            manager.ApplyResources(this.nudRollId, "nudRollId");
            int[] bits = new int[4];
            bits[0] = 0x7fffffff;
            this.nudRollId.Maximum = new decimal(bits);
            this.nudRollId.Name = "nudRollId";
            manager.ApplyResources(this.chbActiveOnly, "chbActiveOnly");
            this.chbActiveOnly.Name = "chbActiveOnly";
            this.chbActiveOnly.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.dtpStart, "dtpStart");
            this.dtpStart.Format = DateTimePickerFormat.Short;
            this.dtpStart.Name = "dtpStart";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.dtpEnd.Format = DateTimePickerFormat.Short;
            manager.ApplyResources(this.dtpEnd, "dtpEnd");
            this.dtpEnd.Name = "dtpEnd";
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.cmbClientId);
            base.Controls.Add(this.nudRollId);
            base.Controls.Add(this.chbActiveOnly);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.dtpStart);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.dtpEnd);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.label4);
            base.Name = "ClientRollsFilterControl";
            this.nudRollId.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            try
            {
                base.SuspendLayout();
                this.nudRollId.Value = 0M;
                this.cmbClientId.Items.Clear();
                this.cmbClientId.Items.Add("");
                this.cmbClientId.Items.AddRange(ClientsContainer.Instance.ToArray());
                this.cmbClientId.SelectedIndex = 0;
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        public void ResetFilter()
        {
            this.chbActiveOnly.Checked = true;
            this.nudRollId.Value = 0M;
            this.cmbClientId.SelectedItem = "";
            this.dtpStart.Value = DateTime.Today.AddDays(-1.0);
            this.dtpEnd.Value = DateTime.Today;
        }

        public FilterOper Filter
        {
            get
            {
                ClientRollsFilterOper oper = new ClientRollsFilterOper();
                oper.RollsFilter.EndTime = this.dtpEnd.Value;
                oper.RollsFilter.ActiveOnly = this.chbActiveOnly.Checked;
                oper.RollsFilter.ClientId = this.SelectedClientID;
                oper.RollsFilter.RollID = (int) this.nudRollId.Value;
                oper.RollsFilter.StartTime = this.dtpStart.Value;
                return oper;
            }
            set
            {
                ClientRollsFilterOper oper = (ClientRollsFilterOper) value;
                this.dtpStart.Value = oper.RollsFilter.StartTime;
                this.dtpEnd.Value = oper.RollsFilter.EndTime;
                this.nudRollId.Value = oper.RollsFilter.RollID;
                this.SelectedClientID = oper.RollsFilter.ClientId;
                this.chbActiveOnly.Checked = oper.RollsFilter.ActiveOnly;
            }
        }

        public Guid SelectedClientID
        {
            get
            {
                Client selectedItem = this.cmbClientId.SelectedItem as Client;
                if (selectedItem != null)
                {
                    return selectedItem.Id;
                }
                return Guid.Empty;
            }
            set
            {
                Client client = (value == Guid.Empty) ? null : ClientsContainer.Instance[value];
                if (client == null)
                {
                    this.cmbClientId.SelectedItem = "";
                }
                else
                {
                    this.cmbClientId.SelectedItem = client;
                }
            }
        }
    }
}

