﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using System;

    internal class AccountFilter : ClientFilter
    {
        public bool HideAccountsWithoutBalance;
        public int TownId;
    }
}

