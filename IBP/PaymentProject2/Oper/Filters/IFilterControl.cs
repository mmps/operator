﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using System;

    internal interface IFilterControl
    {
        void ReadContainers();
        void ResetFilter();

        FilterOper Filter { get; set; }
    }
}

