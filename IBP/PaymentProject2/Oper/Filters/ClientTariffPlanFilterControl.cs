﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.ClientTariffPlan)]
    internal class ClientTariffPlanFilterControl : UserControl, IFilterControl
    {
        private CheckBox activeOnly;
        private CheckBox byTime;
        private IContainer components;
        private DateTimePicker dateTimeEnd;
        private DateTimePicker dateTimeStart;
        private GroupBox groupBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox namePart;

        public ClientTariffPlanFilterControl()
        {
            EventHandler handler = null;
            this.InitializeComponent();
            if (handler == null)
            {
                handler = delegate {
                    this.dateTimeEnd.Enabled = this.dateTimeStart.Enabled = this.byTime.Checked;
                };
            }
            this.byTime.CheckedChanged += handler;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientTariffPlanFilterControl));
            this.activeOnly = new CheckBox();
            this.namePart = new TextBox();
            this.label1 = new Label();
            this.dateTimeEnd = new DateTimePicker();
            this.label2 = new Label();
            this.label3 = new Label();
            this.dateTimeStart = new DateTimePicker();
            this.groupBox1 = new GroupBox();
            this.byTime = new CheckBox();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.activeOnly, "activeOnly");
            this.activeOnly.Checked = true;
            this.activeOnly.CheckState = CheckState.Checked;
            this.activeOnly.Name = "activeOnly";
            this.activeOnly.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.namePart, "namePart");
            this.namePart.Name = "namePart";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.dateTimeEnd, "dateTimeEnd");
            this.dateTimeEnd.Format = DateTimePickerFormat.Short;
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Value = new DateTime(0x7d9, 3, 10, 0, 0, 0, 0);
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            manager.ApplyResources(this.dateTimeStart, "dateTimeStart");
            this.dateTimeStart.Format = DateTimePickerFormat.Short;
            this.dateTimeStart.Name = "dateTimeStart";
            this.dateTimeStart.Value = new DateTime(0x7d9, 3, 10, 0, 0, 0, 0);
            this.groupBox1.Controls.Add(this.byTime);
            this.groupBox1.Controls.Add(this.dateTimeStart);
            this.groupBox1.Controls.Add(this.dateTimeEnd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            manager.ApplyResources(this.byTime, "byTime");
            this.byTime.Name = "byTime";
            this.byTime.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.groupBox1);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.namePart);
            base.Controls.Add(this.activeOnly);
            base.Name = "ClientTariffPlanFilterControl";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
        }

        public void ResetFilter()
        {
            this.activeOnly.Checked = true;
            this.namePart.Text = string.Empty;
            this.dateTimeStart.Value = DateTime.Today;
            this.dateTimeEnd.Value = DateTime.Today;
            this.byTime.Checked = false;
        }

        public FilterOper Filter
        {
            get
            {
                ClientTariffPlanFilterOper oper = new ClientTariffPlanFilterOper();
                ClientTariffPlanFilter planFilter = oper.PlanFilter;
                planFilter.ActiveOnly = this.activeOnly.Checked;
                planFilter.ByTime = this.byTime.Checked;
                planFilter.StartTime = this.dateTimeStart.Value;
                planFilter.EndTime = this.dateTimeEnd.Value;
                planFilter.NamePart = this.namePart.Text;
                return oper;
            }
            set
            {
                ClientTariffPlanFilterOper oper = (ClientTariffPlanFilterOper) value;
                ClientTariffPlanFilter planFilter = oper.PlanFilter;
                this.activeOnly.Checked = planFilter.ActiveOnly;
                this.byTime.Checked = planFilter.ByTime;
                this.dateTimeStart.Value = planFilter.StartTime;
                this.dateTimeEnd.Value = planFilter.EndTime;
                this.namePart.Text = planFilter.NamePart;
            }
        }
    }
}

