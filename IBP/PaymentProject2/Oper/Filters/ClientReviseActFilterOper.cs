﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;

    internal class ClientReviseActFilterOper : FilterOper
    {
        public ClientReviseActFilterOper()
        {
            base.Filter = new ClientReviseActFilter();
        }

        public ClientReviseActFilter ActFilter
        {
            get
            {
                return (ClientReviseActFilter) base.Filter;
            }
            set
            {
                base.Filter = value;
            }
        }
    }
}

