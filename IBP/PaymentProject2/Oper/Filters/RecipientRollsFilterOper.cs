﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common.Rolls;
    using System;

    internal class RecipientRollsFilterOper : FilterOper
    {
        public RecipientRollsFilterOper()
        {
            base.Filter = new RecipientRollFilter();
        }

        public RecipientRollFilter RollsFilter
        {
            get
            {
                return (RecipientRollFilter) base.Filter;
            }
            set
            {
                base.Filter = value;
            }
        }
    }
}

