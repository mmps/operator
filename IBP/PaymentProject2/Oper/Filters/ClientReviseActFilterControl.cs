﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    [FilterControl(TypeSearch.ClientReviseAct)]
    internal class ClientReviseActFilterControl : UserControl, IFilterControl
    {
        private NumericUpDown actID;
        private CheckBox byTime;
        private ComboBox client;
        private IContainer components;
        private CheckBox deleted;
        private DateTimePicker endTime;
        private GroupBox groupBox1;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private DateTimePicker startTime;

        public ClientReviseActFilterControl()
        {
            EventHandler handler = null;
            this.InitializeComponent();
            this.ReadContainers();
            DateTime today = DateTime.Today;
            today = today.AddDays((double) (-today.Day + 1)).AddMonths(-1);
            this.startTime.Value = this.endTime.Value = today;
            if (handler == null)
            {
                handler = delegate {
                    this.startTime.Enabled = this.endTime.Enabled = this.byTime.Checked;
                };
            }
            this.byTime.CheckedChanged += handler;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientReviseActFilterControl));
            this.groupBox1 = new GroupBox();
            this.endTime = new DateTimePicker();
            this.startTime = new DateTimePicker();
            this.label2 = new Label();
            this.label1 = new Label();
            this.byTime = new CheckBox();
            this.label3 = new Label();
            this.actID = new NumericUpDown();
            this.label4 = new Label();
            this.client = new ComboBox();
            this.deleted = new CheckBox();
            this.groupBox1.SuspendLayout();
            this.actID.BeginInit();
            base.SuspendLayout();
            this.groupBox1.Controls.Add(this.endTime);
            this.groupBox1.Controls.Add(this.startTime);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.byTime);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            manager.ApplyResources(this.endTime, "endTime");
            this.endTime.Format = DateTimePickerFormat.Custom;
            this.endTime.Name = "endTime";
            this.endTime.ShowUpDown = true;
            this.endTime.Value = new DateTime(0x7d9, 3, 30, 0, 0, 0, 0);
            manager.ApplyResources(this.startTime, "startTime");
            this.startTime.Format = DateTimePickerFormat.Custom;
            this.startTime.Name = "startTime";
            this.startTime.ShowUpDown = true;
            this.startTime.Value = new DateTime(0x7d9, 3, 30, 0, 0, 0, 0);
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.byTime, "byTime");
            this.byTime.Name = "byTime";
            this.byTime.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            manager.ApplyResources(this.actID, "actID");
            this.actID.Name = "actID";
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            this.client.FormattingEnabled = true;
            manager.ApplyResources(this.client, "client");
            this.client.Name = "client";
            this.client.Sorted = true;
            manager.ApplyResources(this.deleted, "deleted");
            this.deleted.Name = "deleted";
            this.deleted.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.deleted);
            base.Controls.Add(this.client);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.actID);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.groupBox1);
            base.Name = "ClientReviseActFilterControl";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.actID.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public void ReadContainers()
        {
            try
            {
                base.SuspendLayout();
                this.actID.Value = 0M;
                this.client.Items.Clear();
                this.client.Items.Add("");
                this.client.Items.AddRange(ClientsContainer.Instance.ToArray());
                this.client.SelectedIndex = 0;
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        public void ResetFilter()
        {
            this.deleted.Checked = false;
            this.actID.Value = 0M;
            this.client.SelectedItem = "";
            DateTime today = DateTime.Today;
            today = today.AddDays((double) (-today.Day + 1)).AddMonths(-1);
            this.startTime.Value = this.endTime.Value = today;
            this.byTime.Checked = false;
        }

        public FilterOper Filter
        {
            get
            {
                ClientReviseActFilterOper oper = new ClientReviseActFilterOper();
                oper.ActFilter.StartTime = this.startTime.Value;
                DateTime time = this.endTime.Value;
                oper.ActFilter.EndTime = time.AddDays((double) (-time.Day + 1)).AddMonths(1).AddDays(-1.0);
                oper.ActFilter.ClientId = this.SelectedClientID;
                oper.ActFilter.ActId = (int) this.actID.Value;
                oper.ActFilter.ByTime = this.byTime.Checked;
                oper.ActFilter.Deleted = this.deleted.Checked;
                return oper;
            }
            set
            {
                ClientReviseActFilterOper oper = (ClientReviseActFilterOper) value;
                this.startTime.Value = oper.ActFilter.StartTime;
                this.endTime.Value = oper.ActFilter.EndTime;
                this.byTime.Checked = oper.ActFilter.ByTime;
                this.deleted.Checked = oper.ActFilter.Deleted;
                this.SelectedClientID = oper.ActFilter.ClientId;
                this.actID.Value = oper.ActFilter.ActId;
            }
        }

        private Guid SelectedClientID
        {
            get
            {
                Client selectedItem = this.client.SelectedItem as Client;
                if (selectedItem != null)
                {
                    return selectedItem.Id;
                }
                return Guid.Empty;
            }
            set
            {
                Client client = (value == Guid.Empty) ? null : ClientsContainer.Instance[value];
                if (client == null)
                {
                    this.client.SelectedItem = "";
                }
                else
                {
                    this.client.SelectedItem = client;
                }
            }
        }
    }
}

