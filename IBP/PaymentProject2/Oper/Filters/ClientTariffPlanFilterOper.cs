﻿namespace IBP.PaymentProject2.Oper.Filters
{
    using IBP.PaymentProject2.Common.Reconing;
    using System;

    internal class ClientTariffPlanFilterOper : FilterOper
    {
        public ClientTariffPlanFilterOper()
        {
            base.Filter = new ClientTariffPlanFilter();
        }

        public ClientTariffPlanFilter PlanFilter
        {
            get
            {
                return (ClientTariffPlanFilter) base.Filter;
            }
            set
            {
                base.Filter = value;
            }
        }
    }
}

