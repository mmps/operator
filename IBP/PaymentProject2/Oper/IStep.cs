﻿namespace IBP.PaymentProject2.Oper
{
    using System;

    public interface IStep
    {
        event EventHandler<StepEventArgs> SendObject;

        bool Activate();
        void Back(IStep beforeStep);
        bool Next(IStep nextStep);
        void ShowBack();

        IStep NextStep { get; set; }

        IStep PreviousStep { get; set; }
    }
}

