﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class HistoryPaymentDialog : Form
    {
        private Button buttonClose;
        private ColumnHeader columnHeaderDate;
        private ColumnHeader columnHeaderStatus;
        private IContainer components;
        private int generation;
        private ListView listViewHistory;
        private int paymentId;

        public HistoryPaymentDialog(int paymentId, int generation)
        {
            this.paymentId = paymentId;
            this.generation = generation;
            this.InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(HistoryPaymentDialog));
            this.listViewHistory = new ListView();
            this.columnHeaderDate = new ColumnHeader();
            this.columnHeaderStatus = new ColumnHeader();
            this.buttonClose = new Button();
            base.SuspendLayout();
            this.listViewHistory.Columns.AddRange(new ColumnHeader[] { this.columnHeaderDate, this.columnHeaderStatus });
            manager.ApplyResources(this.listViewHistory, "listViewHistory");
            this.listViewHistory.FullRowSelect = true;
            this.listViewHistory.GridLines = true;
            this.listViewHistory.Name = "listViewHistory";
            this.listViewHistory.ShowItemToolTips = true;
            this.listViewHistory.UseCompatibleStateImageBehavior = false;
            this.listViewHistory.View = View.Details;
            manager.ApplyResources(this.columnHeaderDate, "columnHeaderDate");
            manager.ApplyResources(this.columnHeaderStatus, "columnHeaderStatus");
            manager.ApplyResources(this.buttonClose, "buttonClose");
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new EventHandler(this.buttonClose_Click);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ControlBox = false;
            base.Controls.Add(this.listViewHistory);
            base.Controls.Add(this.buttonClose);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "HistoryPaymentDialog";
            base.ResumeLayout(false);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            PaymentHistory paymentHistory = null;
            try
            {
                paymentHistory = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPaymentHistory(this.paymentId, this.generation);
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
                base.DialogResult = DialogResult.Cancel;
            }
            foreach (PaymentHistory.Item item in (IEnumerable<PaymentHistory.Item>) paymentHistory)
            {
                ListViewItem item2 = new ListViewItem();
                item2.Text = item.Time.ToString();
                item2.SubItems.Add(item.StringState);
                item2.ToolTipText = item.Comment;
                this.listViewHistory.Items.Add(item2);
            }
        }
    }
}

