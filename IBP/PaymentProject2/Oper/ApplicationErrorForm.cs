﻿namespace IBP.PaymentProject2.Oper
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    public class ApplicationErrorForm : Form
    {
        private Button _buttonCancel;
        private Button _buttonCopy;
        private TextBox _textBoxError;
        private IContainer components;

        public ApplicationErrorForm(Exception exception)
        {
            this.InitializeComponent();
            this._textBoxError.MaxLength = 0x7fffffff;
            this._textBoxError.Text = exception.ToString();
            this._textBoxError.Select(0, 0);
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            if (Thread.CurrentThread.GetApartmentState() != ApartmentState.STA)
            {
                FormCollection openForms = Application.OpenForms;
                foreach (Form form in openForms)
                {
                    if (form.IsMdiContainer)
                    {
                        EventHandler method = new EventHandler(this.buttonCopy_Click);
                        openForms[0].BeginInvoke(method, new object[] { sender, e });
                        break;
                    }
                }
            }
            else
            {
                Clipboard.Clear();
                Clipboard.SetText(this._textBoxError.Text);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ApplicationErrorForm));
            this._buttonCancel = new Button();
            this._buttonCopy = new Button();
            this._textBoxError = new TextBox();
            TextBox box = new TextBox();
            base.SuspendLayout();
            manager.ApplyResources(this._buttonCancel, "_buttonCancel");
            this._buttonCancel.DialogResult = DialogResult.Cancel;
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this._buttonCopy, "_buttonCopy");
            this._buttonCopy.Name = "_buttonCopy";
            this._buttonCopy.UseVisualStyleBackColor = true;
            this._buttonCopy.Click += new EventHandler(this.buttonCopy_Click);
            box.BackColor = SystemColors.Control;
            box.BorderStyle = BorderStyle.None;
            manager.ApplyResources(box, "_textBoxDescription");
            box.Name = "_textBoxDescription";
            box.ReadOnly = true;
            this._textBoxError.BackColor = SystemColors.Menu;
            manager.ApplyResources(this._textBoxError, "_textBoxError");
            this._textBoxError.Name = "_textBoxError";
            this._textBoxError.ReadOnly = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this._textBoxError);
            base.Controls.Add(box);
            base.Controls.Add(this._buttonCopy);
            base.Controls.Add(this._buttonCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ApplicationErrorForm";
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}

