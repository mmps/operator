﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal interface IFormResultSearch
    {
        FilterOper Filter { get; set; }
    }
}

