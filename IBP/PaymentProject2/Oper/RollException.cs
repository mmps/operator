﻿namespace IBP.PaymentProject2.Oper
{
    using System;

    public class RollException : Exception
    {
        public RollException()
        {
        }

        public RollException(string message) : base(message)
        {
        }

        public RollException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

