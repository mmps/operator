﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Isolation;
    using System;
    using System.Windows.Forms;

    internal class IsolationCookie
    {
        private IIsolableObject _isolableObject;
        private IIsolationController _isolation;
        private IsolationInfo _isolationInfo;

        private IsolationCookie(IIsolationController isolation, IIsolableObject isolableObject, IsolationInfo isolationInfo)
        {
            this._isolation = isolation;
            this._isolableObject = isolableObject;
            this._isolationInfo = isolationInfo;
        }

        public static IsolationCookie Create(IIsolationController isolation, IIsolableObject isolableObject)
        {
            IsolationInfo isolationInfo = null;
            try
            {
                isolationInfo = isolation.GetIsolation(isolableObject.GlobalId, false);
            }
            catch (IsolationSrvException exception)
            {
                if (DoIgnoreOthers(exception))
                {
                    isolationInfo = isolation.GetIsolation(isolableObject.GlobalId, true);
                }
            }
            if (isolationInfo != null)
            {
                IsolationCookie cookie = new IsolationCookie(isolation, isolableObject, isolationInfo);
                isolableObject.IsolationId = isolationInfo.Id;
                return cookie;
            }
            return null;
        }

        private static bool DoIgnoreOthers(IsolationSrvException isolationException)
        {
            return (MessageBox.Show(string.Format(UserStrings.DoIgnoreOthersMessage, isolationException.CurrentIsolation.FirstName, isolationException.CurrentIsolation.SecondName), UserStrings.Error, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
        }

        public void Release()
        {
            try
            {
                this._isolation.ReleaseIsolation(this._isolationInfo.Id);
            }
            catch (SrvException)
            {
            }
        }
    }
}

