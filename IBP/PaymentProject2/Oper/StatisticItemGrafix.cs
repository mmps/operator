﻿namespace IBP.PaymentProject2.Oper
{
    using System;

    public class StatisticItemGrafix : IStatisticItemGrafix
    {
        private string label;
        private decimal total;

        public StatisticItemGrafix()
        {
        }

        public StatisticItemGrafix(string label, decimal total)
        {
            this.label = label;
            this.total = total;
        }

        public string Label
        {
            get
            {
                return this.label;
            }
            set
            {
                this.label = value;
            }
        }

        public decimal Total
        {
            get
            {
                return this.total;
            }
            set
            {
                this.total = value;
            }
        }
    }
}

