﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    [Roll(GatewayType.SkyLink_spb)]
    internal class SkyLinkSpbDelta : Roll
    {
        private StringBuilder fstr;

        public SkyLinkSpbDelta()
        {
            base.filename = "597";
            base.ext = "csv";
        }

        private void AddRow(DateTime dat, int nomre, decimal amount, int count)
        {
            this.fstr.Append(dat.ToString("d", Application.CurrentCulture) + ";" + nomre.ToString() + ";" + amount.ToString(Application.CurrentCulture) + ";" + count.ToString() + ";;");
            this.fstr.Append(Environment.NewLine);
        }

        public override Stream CreateRoll(Payment[] payments)
        {
            this.fstr = new StringBuilder();
            this.fstr.Append(UserStrings.Firm);
            this.fstr.Append(Environment.NewLine);
            this.fstr.Append(UserStrings.Kassa);
            this.fstr.Append(Environment.NewLine);
            this.fstr.Append(UserStrings.Report02);
            this.fstr.Append(Environment.NewLine);
            this.fstr.Append(UserStrings.ReestrDate);
            this.fstr.Append(Environment.NewLine);
            Dictionary<DateTime, List<Payment>> dictionary = new Dictionary<DateTime, List<Payment>>();
            foreach (Payment payment in payments)
            {
                if (payment.Contains("dlt_reg"))
                {
                    DateTime date = payment.StateTime.Date;
                    if (dictionary.ContainsKey(date))
                    {
                        dictionary[date].Add(payment);
                    }
                    else
                    {
                        List<Payment> list = new List<Payment>();
                        list.Add(payment);
                        dictionary.Add(date, list);
                    }
                }
            }
            Dictionary<int, List<Payment>> dictionary2 = new Dictionary<int, List<Payment>>();
            foreach (KeyValuePair<DateTime, List<Payment>> pair in dictionary)
            {
                foreach (Payment payment2 in pair.Value)
                {
                    int key = int.Parse(payment2["dlt_reg"].Substring(0, 4));
                    if (dictionary2.ContainsKey(key))
                    {
                        dictionary2[key].Add(payment2);
                    }
                    else
                    {
                        List<Payment> list2 = new List<Payment>();
                        list2.Add(payment2);
                        dictionary2.Add(key, list2);
                    }
                }
            }
            List<Reestr> list3 = new List<Reestr>();
            foreach (KeyValuePair<int, List<Payment>> pair2 in dictionary2)
            {
                Reestr item = new Reestr(pair2.Value[0].StateTime.Date, pair2.Key);
                item.Payments = pair2.Value.ToArray();
                list3.Add(item);
            }
            list3.Sort();
            base.filename = "597-" + ((list3[0].Date.Month < 10) ? ("0" + list3[0].Date.Month.ToString()) : list3[0].Date.Month.ToString()) + "-" + list3[0].Date.Year.ToString().Remove(0, 2);
            decimal num2 = 0M;
            int num3 = 0;
            foreach (Reestr reestr2 in list3)
            {
                this.AddRow(reestr2.Date, reestr2.Number, reestr2.Amount, reestr2.Count);
                num2 += reestr2.Amount;
                num3 += reestr2.Count;
            }
            this.fstr.Append(";;;;;" + Environment.NewLine);
            this.fstr.Append("итого;;" + num2.ToString(Application.CurrentCulture) + ";" + num3.ToString() + ";;" + Environment.NewLine);
            this.fstr.Append("Сумма комиссионного вознаграждения составляет;;;;;\r\nв том числе НДС;;;;;\r\n;;;;;\r\nНалог с продаж к уплате в бюджет;;;;;\r\n;;;;;\r\nУслуги оказаны полностью;;;;;\r\n;;;;;\r\n;;;;;\r\n;;;;;\r\nРуководитель;;;\"Представитель ЗАО \"Дельта Телеком\"\";;\r\nГлавный бухгалтер;;;;;\r\n;;;;;\r\n;;;;;");
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(Encoding.Default.GetBytes(this.fstr.ToString()));
                this.fstr = null;
            }
            catch (ArgumentException exception)
            {
                throw new RollException(exception.Message);
            }
            return stream;
        }

        private class Reestr : IComparable
        {
            private decimal amount;
            private int count;
            private DateTime date;
            private int number;
            private Payment[] payments;

            public Reestr(DateTime date, int number)
            {
                this.date = date;
                this.number = number;
            }

            public int CompareTo(object obj)
            {
                return this.date.CompareTo(((SkyLinkSpbDelta.Reestr) obj).date);
            }

            public decimal Amount
            {
                get
                {
                    return this.amount;
                }
            }

            public int Count
            {
                get
                {
                    return this.count;
                }
            }

            public DateTime Date
            {
                get
                {
                    return this.date;
                }
            }

            public int Number
            {
                get
                {
                    return this.number;
                }
            }

            public Payment[] Payments
            {
                get
                {
                    return this.payments;
                }
                set
                {
                    this.payments = value;
                    foreach (Payment payment in this.payments)
                    {
                        this.amount += payment.Value;
                        this.count++;
                    }
                }
            }
        }
    }
}

