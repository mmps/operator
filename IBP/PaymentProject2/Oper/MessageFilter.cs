﻿namespace IBP.PaymentProject2.Oper
{
    using System;
    using System.Windows.Forms;

    public class MessageFilter : IMessageFilter
    {
        private bool _ctrl;
        private Form _mainForm;
        private bool _shift;

        public MessageFilter(Form mainForm)
        {
            this._mainForm = mainForm;
            this._ctrl = this._shift = false;
        }

        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == 0x100)
            {
                switch (((Keys) ((int) m.WParam)))
                {
                    case Keys.Return:
                        if (!this._shift)
                        {
                        }
                        break;

                    case Keys.ShiftKey:
                        this._shift = true;
                        break;

                    case Keys.ControlKey:
                        this._ctrl = true;
                        break;

                    case Keys.F1:
                    case Keys.F2:
                    case Keys.F3:
                    case Keys.F4:
                    case Keys.F5:
                        if (!this._shift)
                        {
                        }
                        break;
                }
            }
            else if (m.Msg == 0x101)
            {
                switch (((Keys) ((int) m.WParam)))
                {
                    case Keys.ShiftKey:
                        this._shift = false;
                        break;

                    case Keys.ControlKey:
                        this._ctrl = false;
                        break;
                }
            }
            return false;
        }
    }
}

