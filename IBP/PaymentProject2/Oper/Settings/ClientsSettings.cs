﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class ClientsSettings : ApplicationSettingsBase
    {
        private static ClientsSettings defaultInstance = ((ClientsSettings) SettingsBase.Synchronized(new ClientsSettings()));
        public readonly SettingsHelper Helper;

        public ClientsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
            }
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        public static ClientsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [UserScopedSetting, DefaultSettingValue("True"), DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DefaultSettingValue(""), DebuggerNonUserCode, UserScopedSetting]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }
    }
}

