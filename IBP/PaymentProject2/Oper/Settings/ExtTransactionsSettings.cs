﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class ExtTransactionsSettings : ApplicationSettingsBase
    {
        private static ExtTransactionsSettings defaultInstance = ((ExtTransactionsSettings) SettingsBase.Synchronized(new ExtTransactionsSettings()));
        public readonly SettingsHelper Helper;

        public ExtTransactionsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
            }
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("128, 255, 128"), SettingsDescription("Цвет подтверждающих проводок")]
        public Color CallbackColor
        {
            get
            {
                return (Color) this["CallbackColor"];
            }
            set
            {
                this["CallbackColor"] = value;
            }
        }

        public static ExtTransactionsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DefaultSettingValue("128, 255, 255"), DebuggerNonUserCode, UserScopedSetting, SettingsDescription("Цвет проводок, импортируемых в eKassir")]
        public Color ExternalColor
        {
            get
            {
                return (Color) this["ExternalColor"];
            }
            set
            {
                this["ExternalColor"] = value;
            }
        }

        [DefaultSettingValue("True"), UserScopedSetting, DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }
    }
}

