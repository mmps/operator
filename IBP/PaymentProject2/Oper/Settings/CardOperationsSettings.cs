﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class CardOperationsSettings : ApplicationSettingsBase
    {
        private static CardOperationsSettings defaultInstance = ((CardOperationsSettings) SettingsBase.Synchronized(new CardOperationsSettings()));
        public readonly SettingsHelper Helper;

        public CardOperationsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
                this.Helper.SelectedFieldsString = this.SelectedFieldsString;
            }
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        public static CardOperationsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("255, 192, 192")]
        public Color FailedColor
        {
            get
            {
                return (Color) this["FailedColor"];
            }
            set
            {
                this["FailedColor"] = value;
            }
        }

        [DebuggerNonUserCode, DefaultSettingValue("True"), UserScopedSetting]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }
    }
}

