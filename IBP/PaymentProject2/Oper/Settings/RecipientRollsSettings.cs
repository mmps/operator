﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class RecipientRollsSettings : ApplicationSettingsBase
    {
        private static RecipientRollsSettings defaultInstance = ((RecipientRollsSettings) SettingsBase.Synchronized(new RecipientRollsSettings()));
        public readonly SettingsHelper Helper;

        public RecipientRollsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
            }
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        public static RecipientRollsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DefaultSettingValue("True"), UserScopedSetting, DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DefaultSettingValue("Red"), UserScopedSetting, DebuggerNonUserCode]
        public Color NotRevisedColor
        {
            get
            {
                return (Color) this["NotRevisedColor"];
            }
            set
            {
                this["NotRevisedColor"] = value;
            }
        }

        [DefaultSettingValue("Yellow"), UserScopedSetting, DebuggerNonUserCode]
        public Color RevisedColor
        {
            get
            {
                return (Color) this["RevisedColor"];
            }
            set
            {
                this["RevisedColor"] = value;
            }
        }

        [UserScopedSetting, DefaultSettingValue(""), DebuggerNonUserCode]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }
    }
}

