﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class PringCheckSettingControl : UserControl
    {
        private IContainer components;
        private Label label1;
        private Label label2;
        private NumericUpDown nudcopycount;
        private CheckBox rbPreeView;
        private TextBox tbsetting;

        public PringCheckSettingControl()
        {
            this.InitializeComponent();
            this.rbPreeView.Checked = PrintCheckSettings.Default.PreView;
            string paymentFormat = PrintCheckSettings.Default.PaymentFormat;
            this.tbsetting.Text = string.IsNullOrEmpty(paymentFormat) ? UserStrings.PrintFormat : paymentFormat;
            this.nudcopycount.Value = PrintCheckSettings.Default.CopyCount;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(PringCheckSettingControl));
            this.tbsetting = new TextBox();
            this.rbPreeView = new CheckBox();
            this.label1 = new Label();
            this.nudcopycount = new NumericUpDown();
            this.label2 = new Label();
            this.nudcopycount.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(this.tbsetting, "tbsetting");
            this.tbsetting.Name = "tbsetting";
            manager.ApplyResources(this.rbPreeView, "rbPreeView");
            this.rbPreeView.Name = "rbPreeView";
            this.rbPreeView.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.nudcopycount, "nudcopycount");
            int[] bits = new int[4];
            bits[0] = 2;
            this.nudcopycount.Maximum = new decimal(bits);
            int[] numArray2 = new int[4];
            numArray2[0] = 1;
            this.nudcopycount.Minimum = new decimal(numArray2);
            this.nudcopycount.Name = "nudcopycount";
            int[] numArray3 = new int[4];
            numArray3[0] = 1;
            this.nudcopycount.Value = new decimal(numArray3);
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.label2);
            base.Controls.Add(this.nudcopycount);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.rbPreeView);
            base.Controls.Add(this.tbsetting);
            base.Name = "PringCheckSettingControl";
            this.nudcopycount.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public int CopyCount
        {
            get
            {
                return (int) this.nudcopycount.Value;
            }
        }

        public string Format
        {
            get
            {
                return this.tbsetting.Text;
            }
        }

        public bool PreeView
        {
            get
            {
                return this.rbPreeView.Checked;
            }
        }
    }
}

