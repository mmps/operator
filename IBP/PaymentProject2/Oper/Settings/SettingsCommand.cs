﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Oper;
    using System;

    internal class SettingsCommand : Command
    {
        private SettingsForm form;

        public override void Execute()
        {
            if (this.form == null)
            {
                this.form = new SettingsForm();
                this.form.Show();
                this.form.Disposed += new EventHandler(this.OnFormDisposed);
            }
            this.form.Activate();
        }

        private void OnFormDisposed(object sender, EventArgs e)
        {
            this.form = null;
        }
    }
}

