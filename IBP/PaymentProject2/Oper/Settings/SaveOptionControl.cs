﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class SaveOptionControl : UserControl
    {
        private CheckBox checkBoxExistingFile;
        private CheckBox checkBoxOpenFile;
        private IContainer components;
        private Label label1;
        private RadioButton radioButtonSelectFile;
        private RadioButton radioButtonTempFile;
        private ToolTip toolTipOpenFile;

        public SaveOptionControl()
        {
            this.InitializeComponent();
            this.toolTipOpenFile.SetToolTip(this.checkBoxOpenFile, UserStrings.SomeDescription02);
            this.toolTipOpenFile.SetToolTip(this.checkBoxExistingFile, UserStrings.SomeDescription03);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(SaveOptionControl));
            this.radioButtonSelectFile = new RadioButton();
            this.radioButtonTempFile = new RadioButton();
            this.label1 = new Label();
            this.checkBoxOpenFile = new CheckBox();
            this.checkBoxExistingFile = new CheckBox();
            this.toolTipOpenFile = new ToolTip(this.components);
            base.SuspendLayout();
            manager.ApplyResources(this.radioButtonSelectFile, "radioButtonSelectFile");
            this.radioButtonSelectFile.Name = "radioButtonSelectFile";
            this.radioButtonSelectFile.TabStop = true;
            this.radioButtonSelectFile.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.radioButtonTempFile, "radioButtonTempFile");
            this.radioButtonTempFile.Name = "radioButtonTempFile";
            this.radioButtonTempFile.TabStop = true;
            this.radioButtonTempFile.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.checkBoxOpenFile, "checkBoxOpenFile");
            this.checkBoxOpenFile.Name = "checkBoxOpenFile";
            this.checkBoxOpenFile.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.checkBoxExistingFile, "checkBoxExistingFile");
            this.checkBoxExistingFile.Name = "checkBoxExistingFile";
            this.checkBoxExistingFile.UseVisualStyleBackColor = true;
            this.toolTipOpenFile.ToolTipTitle = "Программа после экспорта автоматически откроет файл с сохраненными данными";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.checkBoxExistingFile);
            base.Controls.Add(this.checkBoxOpenFile);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.radioButtonSelectFile);
            base.Controls.Add(this.radioButtonTempFile);
            base.Name = "SaveOptionControl";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public bool OpenFile
        {
            get
            {
                return this.checkBoxOpenFile.Checked;
            }
            set
            {
                this.checkBoxOpenFile.Checked = value;
            }
        }

        public bool SaveToExistingFile
        {
            get
            {
                return this.checkBoxExistingFile.Checked;
            }
            set
            {
                this.checkBoxExistingFile.Checked = value;
            }
        }

        public bool SelectFile
        {
            get
            {
                return this.radioButtonSelectFile.Checked;
            }
            set
            {
                this.radioButtonSelectFile.Checked = value;
                this.radioButtonTempFile.Checked = !value;
            }
        }
    }
}

