﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"), CompilerGenerated]
    internal sealed class ConnectionSettings : ApplicationSettingsBase
    {
        private static ConnectionSettings defaultInstance = ((ConnectionSettings) SettingsBase.Synchronized(new ConnectionSettings()));

        public ConnectionSettings()
        {
            if (this.NeedUpgrade)
            {
                string defaultServer = this.DefaultServer;
                this.Upgrade();
                this.NeedUpgrade = false;
                if (!string.IsNullOrEmpty(defaultServer))
                {
                    this.DefaultServer = defaultServer;
                }
            }
        }

        public static ConnectionSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DefaultSettingValue("localhost:443 (Default)"), DebuggerNonUserCode, UserScopedSetting]
        public string DefaultServer
        {
            get
            {
                return (string) this["DefaultServer"];
            }
            set
            {
                this["DefaultServer"] = value;
            }
        }

        [DefaultSettingValue("localhost:443 (Default)"), UserScopedSetting, DebuggerNonUserCode]
        public string LastServer
        {
            get
            {
                return (string) this["LastServer"];
            }
            set
            {
                this["LastServer"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("")]
        public string LastUser
        {
            get
            {
                return (string) this["LastUser"];
            }
            set
            {
                this["LastUser"] = value;
            }
        }

        [DefaultSettingValue(""), DebuggerNonUserCode, UserScopedSetting]
        public string Localization
        {
            get
            {
                return (string) this["Localization"];
            }
            set
            {
                this["Localization"] = value;
            }
        }

        [DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
        public string MemoString
        {
            get
            {
                return (string) this["MemoString"];
            }
            set
            {
                this["MemoString"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("True")]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }
    }
}

