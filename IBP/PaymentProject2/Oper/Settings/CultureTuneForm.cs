﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    internal class CultureTuneForm : Form
    {
        private Button buttonApply;
        private Button buttonCancel;
        private Button buttonOK;
        private ComboBox comboBoxAMDesignator;
        private ComboBox comboBoxCurrency;
        private ComboBox comboBoxCurrencyDecimalDigits;
        private ComboBox comboBoxCurrencyDecimalSeparator;
        private ComboBox comboBoxCurrencyGroupSeparator;
        private ComboBox comboBoxCurrencyNegativePattern;
        private ComboBox comboBoxCurrencyPositivePattern;
        private ComboBox comboBoxCurrencySymbol;
        private ComboBox comboBoxDateSeparator;
        private ComboBox comboBoxLongDatePattern;
        private ComboBox comboBoxNegativeSign;
        private ComboBox comboBoxNumberDecimalDigits;
        private ComboBox comboBoxNumberDecimalSeparator;
        private ComboBox comboBoxNumberGroupSeparator;
        private ComboBox comboBoxNumberGroupSizes;
        private ComboBox comboBoxNumberNegativePattern;
        private ComboBox comboBoxPMDesignator;
        private ComboBox comboBoxShortDatePattern;
        private ComboBox comboBoxShortTimePattern;
        private ComboBox comboBoxTimeSeparator;
        private IContainer components;
        private CultureInfo culture;
        [DecimalConstant(0, 0, (uint) 0, (uint) 0, (uint) 0x75bcd15)]
        private static readonly decimal NEGATIVE_SAMPLE = 123456789M;
        private NumericUpDown numericUpDownTwoDigitYearMax;
        [DecimalConstant(0, 0x80, (uint) 0, (uint) 0, (uint) 0x75bcd15)]
        private static readonly decimal POSITIVE_SAMPLE = -123456789M;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private TextBox textBoxFullDateSample;
        private TextBox textBoxNegative;
        private TextBox textBoxNegativeCurrencySample;
        private TextBox textBoxPositive;
        private TextBox textBoxPositiveCurrencySample;
        private TextBox textBoxShortDateSample;
        private TextBox textBoxTimeSample;
        private TextBox textBoxTwoDigitYearMin;

        public CultureTuneForm(CultureInfo culture)
        {
            this.culture = culture;
            Application.CurrentCulture = culture;
            this.InitializeComponent();
            this.SetControls();
        }

        private void Apply()
        {
            this.culture.NumberFormat.NegativeSign = this.comboBoxNegativeSign.Text;
            this.culture.NumberFormat.NumberDecimalSeparator = this.comboBoxNumberDecimalSeparator.Text;
            this.culture.NumberFormat.NumberDecimalDigits = Convert.ToInt32(this.comboBoxNumberDecimalDigits.Text);
            this.culture.NumberFormat.NumberGroupSizes = ((GroupSize) this.comboBoxNumberGroupSizes.SelectedItem).Size;
            this.culture.NumberFormat.NumberGroupSeparator = this.comboBoxNumberGroupSeparator.Text;
            this.culture.NumberFormat.NumberNegativePattern = ((Pattern) this.comboBoxNumberNegativePattern.SelectedItem).PatternNumber;
            this.culture.NumberFormat.CurrencySymbol = this.comboBoxCurrencySymbol.Text;
            this.culture.NumberFormat.CurrencyPositivePattern = ((Pattern) this.comboBoxCurrencyPositivePattern.SelectedItem).PatternNumber;
            this.culture.NumberFormat.CurrencyNegativePattern = ((Pattern) this.comboBoxCurrencyNegativePattern.SelectedItem).PatternNumber;
            this.culture.NumberFormat.CurrencyDecimalSeparator = this.comboBoxCurrencyDecimalSeparator.Text;
            this.culture.NumberFormat.CurrencyDecimalDigits = (int) this.comboBoxCurrencyDecimalDigits.SelectedItem;
            this.culture.NumberFormat.CurrencyGroupSeparator = this.comboBoxCurrencyGroupSeparator.Text;
            this.culture.NumberFormat.CurrencyGroupSizes = ((GroupSize) this.comboBoxCurrency.SelectedItem).Size;
            this.culture.DateTimeFormat.ShortTimePattern = this.comboBoxShortTimePattern.Text;
            this.culture.DateTimeFormat.TimeSeparator = this.comboBoxTimeSeparator.Text;
            this.culture.DateTimeFormat.AMDesignator = this.comboBoxAMDesignator.Text;
            this.culture.DateTimeFormat.PMDesignator = this.comboBoxPMDesignator.Text;
            this.culture.Calendar.TwoDigitYearMax = (int) this.numericUpDownTwoDigitYearMax.Value;
            this.culture.DateTimeFormat.DateSeparator = this.comboBoxDateSeparator.Text;
            this.culture.DateTimeFormat.ShortDatePattern = this.comboBoxShortDatePattern.Text;
            this.culture.DateTimeFormat.LongDatePattern = this.comboBoxLongDatePattern.Text;
            this.SetControls();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            this.Apply();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Apply();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CultureTuneForm));
            this.textBoxPositive = new TextBox();
            this.textBoxNegative = new TextBox();
            this.textBoxPositiveCurrencySample = new TextBox();
            this.textBoxNegativeCurrencySample = new TextBox();
            this.textBoxTimeSample = new TextBox();
            this.numericUpDownTwoDigitYearMax = new NumericUpDown();
            this.textBoxTwoDigitYearMin = new TextBox();
            this.comboBoxDateSeparator = new ComboBox();
            this.comboBoxShortDatePattern = new ComboBox();
            this.textBoxShortDateSample = new TextBox();
            this.textBoxFullDateSample = new TextBox();
            this.comboBoxLongDatePattern = new ComboBox();
            this.comboBoxNumberGroupSeparator = new ComboBox();
            this.buttonCancel = new Button();
            this.buttonOK = new Button();
            this.tabControl1 = new TabControl();
            this.tabPage1 = new TabPage();
            this.comboBoxNumberNegativePattern = new ComboBox();
            this.comboBoxNegativeSign = new ComboBox();
            this.comboBoxNumberGroupSizes = new ComboBox();
            this.comboBoxNumberDecimalDigits = new ComboBox();
            this.comboBoxNumberDecimalSeparator = new ComboBox();
            this.tabPage2 = new TabPage();
            this.comboBoxCurrency = new ComboBox();
            this.comboBoxCurrencyGroupSeparator = new ComboBox();
            this.comboBoxCurrencyDecimalDigits = new ComboBox();
            this.comboBoxCurrencyDecimalSeparator = new ComboBox();
            this.comboBoxCurrencyNegativePattern = new ComboBox();
            this.comboBoxCurrencyPositivePattern = new ComboBox();
            this.comboBoxCurrencySymbol = new ComboBox();
            this.tabPage3 = new TabPage();
            this.comboBoxPMDesignator = new ComboBox();
            this.comboBoxAMDesignator = new ComboBox();
            this.comboBoxTimeSeparator = new ComboBox();
            this.comboBoxShortTimePattern = new ComboBox();
            this.tabPage4 = new TabPage();
            this.buttonApply = new Button();
            Label label = new Label();
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            Label label5 = new Label();
            Label label6 = new Label();
            GroupBox box = new GroupBox();
            Label label7 = new Label();
            Label label8 = new Label();
            GroupBox box2 = new GroupBox();
            Label label9 = new Label();
            Label label10 = new Label();
            Label label11 = new Label();
            Label label12 = new Label();
            Label label13 = new Label();
            Label label14 = new Label();
            Label label15 = new Label();
            Label label16 = new Label();
            Label label17 = new Label();
            GroupBox box3 = new GroupBox();
            Label label18 = new Label();
            Label label19 = new Label();
            Label label20 = new Label();
            Label label21 = new Label();
            Label label22 = new Label();
            GroupBox box4 = new GroupBox();
            Label label23 = new Label();
            GroupBox box5 = new GroupBox();
            Label label24 = new Label();
            Label label25 = new Label();
            GroupBox box6 = new GroupBox();
            Label label26 = new Label();
            Label label27 = new Label();
            Label label28 = new Label();
            GroupBox box7 = new GroupBox();
            Label label29 = new Label();
            Label label30 = new Label();
            box.SuspendLayout();
            box2.SuspendLayout();
            box3.SuspendLayout();
            box4.SuspendLayout();
            box5.SuspendLayout();
            this.numericUpDownTwoDigitYearMax.BeginInit();
            box6.SuspendLayout();
            box7.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(label, "label8");
            label.Name = "label8";
            manager.ApplyResources(label2, "label7");
            label2.Name = "label7";
            manager.ApplyResources(label3, "label6");
            label3.Name = "label6";
            manager.ApplyResources(label4, "label5");
            label4.Name = "label5";
            manager.ApplyResources(label5, "label4");
            label5.Name = "label4";
            manager.ApplyResources(label6, "label3");
            label6.Name = "label3";
            manager.ApplyResources(box, "groupBox1");
            box.Controls.Add(this.textBoxPositive);
            box.Controls.Add(this.textBoxNegative);
            box.Controls.Add(label7);
            box.Controls.Add(label8);
            box.Name = "groupBox1";
            box.TabStop = false;
            manager.ApplyResources(this.textBoxPositive, "textBoxPositive");
            this.textBoxPositive.Name = "textBoxPositive";
            this.textBoxPositive.ReadOnly = true;
            manager.ApplyResources(this.textBoxNegative, "textBoxNegative");
            this.textBoxNegative.Name = "textBoxNegative";
            this.textBoxNegative.ReadOnly = true;
            manager.ApplyResources(label7, "label2");
            label7.Name = "label2";
            manager.ApplyResources(label8, "label1");
            label8.Name = "label1";
            manager.ApplyResources(box2, "groupBox2");
            box2.Controls.Add(this.textBoxPositiveCurrencySample);
            box2.Controls.Add(this.textBoxNegativeCurrencySample);
            box2.Controls.Add(label9);
            box2.Controls.Add(label10);
            box2.Name = "groupBox2";
            box2.TabStop = false;
            manager.ApplyResources(this.textBoxPositiveCurrencySample, "textBoxPositiveCurrencySample");
            this.textBoxPositiveCurrencySample.Name = "textBoxPositiveCurrencySample";
            this.textBoxPositiveCurrencySample.ReadOnly = true;
            manager.ApplyResources(this.textBoxNegativeCurrencySample, "textBoxNegativeCurrencySample");
            this.textBoxNegativeCurrencySample.Name = "textBoxNegativeCurrencySample";
            this.textBoxNegativeCurrencySample.ReadOnly = true;
            manager.ApplyResources(label9, "label9");
            label9.Name = "label9";
            manager.ApplyResources(label10, "label10");
            label10.Name = "label10";
            manager.ApplyResources(label11, "label11");
            label11.Name = "label11";
            manager.ApplyResources(label12, "label12");
            label12.Name = "label12";
            manager.ApplyResources(label13, "label13");
            label13.Name = "label13";
            manager.ApplyResources(label14, "label14");
            label14.Name = "label14";
            manager.ApplyResources(label15, "label15");
            label15.Name = "label15";
            manager.ApplyResources(label16, "label16");
            label16.Name = "label16";
            manager.ApplyResources(label17, "label17");
            label17.Name = "label17";
            manager.ApplyResources(box3, "groupBox3");
            box3.Controls.Add(this.textBoxTimeSample);
            box3.Controls.Add(label18);
            box3.Name = "groupBox3";
            box3.TabStop = false;
            manager.ApplyResources(this.textBoxTimeSample, "textBoxTimeSample");
            this.textBoxTimeSample.Name = "textBoxTimeSample";
            this.textBoxTimeSample.ReadOnly = true;
            manager.ApplyResources(label18, "label18");
            label18.Name = "label18";
            manager.ApplyResources(label19, "label19");
            label19.Name = "label19";
            manager.ApplyResources(label20, "label20");
            label20.Name = "label20";
            manager.ApplyResources(label21, "label21");
            label21.Name = "label21";
            manager.ApplyResources(label22, "label22");
            label22.Name = "label22";
            manager.ApplyResources(box4, "groupBox4");
            box4.Controls.Add(label23);
            box4.Name = "groupBox4";
            box4.TabStop = false;
            manager.ApplyResources(label23, "label23");
            label23.Name = "label23";
            manager.ApplyResources(box5, "groupBox5");
            box5.Controls.Add(this.numericUpDownTwoDigitYearMax);
            box5.Controls.Add(label24);
            box5.Controls.Add(this.textBoxTwoDigitYearMin);
            box5.Controls.Add(label25);
            box5.Name = "groupBox5";
            box5.TabStop = false;
            manager.ApplyResources(this.numericUpDownTwoDigitYearMax, "numericUpDownTwoDigitYearMax");
            int[] bits = new int[4];
            bits[0] = 0x989680;
            this.numericUpDownTwoDigitYearMax.Maximum = new decimal(bits);
            this.numericUpDownTwoDigitYearMax.Name = "numericUpDownTwoDigitYearMax";
            this.numericUpDownTwoDigitYearMax.ValueChanged += new EventHandler(this.numericUpDownLastDigit_ValueChanged);
            manager.ApplyResources(label24, "label25");
            label24.Name = "label25";
            manager.ApplyResources(this.textBoxTwoDigitYearMin, "textBoxTwoDigitYearMin");
            this.textBoxTwoDigitYearMin.Name = "textBoxTwoDigitYearMin";
            this.textBoxTwoDigitYearMin.ReadOnly = true;
            manager.ApplyResources(label25, "label24");
            label25.Name = "label24";
            manager.ApplyResources(box6, "groupBox6");
            box6.Controls.Add(this.comboBoxDateSeparator);
            box6.Controls.Add(this.comboBoxShortDatePattern);
            box6.Controls.Add(label26);
            box6.Controls.Add(label27);
            box6.Controls.Add(label28);
            box6.Controls.Add(this.textBoxShortDateSample);
            box6.Name = "groupBox6";
            box6.TabStop = false;
            manager.ApplyResources(this.comboBoxDateSeparator, "comboBoxDateSeparator");
            this.comboBoxDateSeparator.FormattingEnabled = true;
            this.comboBoxDateSeparator.Name = "comboBoxDateSeparator";
            manager.ApplyResources(this.comboBoxShortDatePattern, "comboBoxShortDatePattern");
            this.comboBoxShortDatePattern.Name = "comboBoxShortDatePattern";
            manager.ApplyResources(label26, "label28");
            label26.Name = "label28";
            manager.ApplyResources(label27, "label27");
            label27.Name = "label27";
            manager.ApplyResources(label28, "label26");
            label28.Name = "label26";
            manager.ApplyResources(this.textBoxShortDateSample, "textBoxShortDateSample");
            this.textBoxShortDateSample.Name = "textBoxShortDateSample";
            this.textBoxShortDateSample.ReadOnly = true;
            manager.ApplyResources(box7, "groupBox7");
            box7.Controls.Add(this.textBoxFullDateSample);
            box7.Controls.Add(this.comboBoxLongDatePattern);
            box7.Controls.Add(label29);
            box7.Controls.Add(label30);
            box7.Name = "groupBox7";
            box7.TabStop = false;
            manager.ApplyResources(this.textBoxFullDateSample, "textBoxFullDateSample");
            this.textBoxFullDateSample.Name = "textBoxFullDateSample";
            this.textBoxFullDateSample.ReadOnly = true;
            manager.ApplyResources(this.comboBoxLongDatePattern, "comboBoxLongDatePattern");
            this.comboBoxLongDatePattern.Name = "comboBoxLongDatePattern";
            manager.ApplyResources(label29, "label29");
            label29.Name = "label29";
            manager.ApplyResources(label30, "label30");
            label30.Name = "label30";
            manager.ApplyResources(this.comboBoxNumberGroupSeparator, "comboBoxNumberGroupSeparator");
            this.comboBoxNumberGroupSeparator.FormattingEnabled = true;
            this.comboBoxNumberGroupSeparator.Name = "comboBoxNumberGroupSeparator";
            manager.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.DialogResult = DialogResult.Cancel;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.buttonOK, "buttonOK");
            this.buttonOK.DialogResult = DialogResult.OK;
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
            manager.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabPage1.Controls.Add(label);
            this.tabPage1.Controls.Add(label2);
            this.tabPage1.Controls.Add(label3);
            this.tabPage1.Controls.Add(label4);
            this.tabPage1.Controls.Add(label5);
            this.tabPage1.Controls.Add(label6);
            this.tabPage1.Controls.Add(this.comboBoxNumberNegativePattern);
            this.tabPage1.Controls.Add(this.comboBoxNegativeSign);
            this.tabPage1.Controls.Add(this.comboBoxNumberGroupSizes);
            this.tabPage1.Controls.Add(this.comboBoxNumberGroupSeparator);
            this.tabPage1.Controls.Add(this.comboBoxNumberDecimalDigits);
            this.tabPage1.Controls.Add(this.comboBoxNumberDecimalSeparator);
            this.tabPage1.Controls.Add(box);
            manager.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.comboBoxNumberNegativePattern, "comboBoxNumberNegativePattern");
            this.comboBoxNumberNegativePattern.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxNumberNegativePattern.FormatString = "N";
            this.comboBoxNumberNegativePattern.FormattingEnabled = true;
            this.comboBoxNumberNegativePattern.Name = "comboBoxNumberNegativePattern";
            manager.ApplyResources(this.comboBoxNegativeSign, "comboBoxNegativeSign");
            this.comboBoxNegativeSign.FormattingEnabled = true;
            this.comboBoxNegativeSign.Name = "comboBoxNegativeSign";
            manager.ApplyResources(this.comboBoxNumberGroupSizes, "comboBoxNumberGroupSizes");
            this.comboBoxNumberGroupSizes.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxNumberGroupSizes.FormatString = "N0";
            this.comboBoxNumberGroupSizes.FormattingEnabled = true;
            this.comboBoxNumberGroupSizes.Name = "comboBoxNumberGroupSizes";
            manager.ApplyResources(this.comboBoxNumberDecimalDigits, "comboBoxNumberDecimalDigits");
            this.comboBoxNumberDecimalDigits.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxNumberDecimalDigits.FormattingEnabled = true;
            this.comboBoxNumberDecimalDigits.Name = "comboBoxNumberDecimalDigits";
            manager.ApplyResources(this.comboBoxNumberDecimalSeparator, "comboBoxNumberDecimalSeparator");
            this.comboBoxNumberDecimalSeparator.FormattingEnabled = true;
            this.comboBoxNumberDecimalSeparator.Name = "comboBoxNumberDecimalSeparator";
            this.tabPage2.Controls.Add(label17);
            this.tabPage2.Controls.Add(label11);
            this.tabPage2.Controls.Add(label12);
            this.tabPage2.Controls.Add(label13);
            this.tabPage2.Controls.Add(label14);
            this.tabPage2.Controls.Add(label15);
            this.tabPage2.Controls.Add(label16);
            this.tabPage2.Controls.Add(this.comboBoxCurrency);
            this.tabPage2.Controls.Add(this.comboBoxCurrencyGroupSeparator);
            this.tabPage2.Controls.Add(this.comboBoxCurrencyDecimalDigits);
            this.tabPage2.Controls.Add(this.comboBoxCurrencyDecimalSeparator);
            this.tabPage2.Controls.Add(this.comboBoxCurrencyNegativePattern);
            this.tabPage2.Controls.Add(this.comboBoxCurrencyPositivePattern);
            this.tabPage2.Controls.Add(this.comboBoxCurrencySymbol);
            this.tabPage2.Controls.Add(box2);
            manager.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.comboBoxCurrency, "comboBoxCurrency");
            this.comboBoxCurrency.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxCurrency.FormatString = "N0";
            this.comboBoxCurrency.FormattingEnabled = true;
            this.comboBoxCurrency.Name = "comboBoxCurrency";
            manager.ApplyResources(this.comboBoxCurrencyGroupSeparator, "comboBoxCurrencyGroupSeparator");
            this.comboBoxCurrencyGroupSeparator.Name = "comboBoxCurrencyGroupSeparator";
            manager.ApplyResources(this.comboBoxCurrencyDecimalDigits, "comboBoxCurrencyDecimalDigits");
            this.comboBoxCurrencyDecimalDigits.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxCurrencyDecimalDigits.Name = "comboBoxCurrencyDecimalDigits";
            manager.ApplyResources(this.comboBoxCurrencyDecimalSeparator, "comboBoxCurrencyDecimalSeparator");
            this.comboBoxCurrencyDecimalSeparator.Name = "comboBoxCurrencyDecimalSeparator";
            manager.ApplyResources(this.comboBoxCurrencyNegativePattern, "comboBoxCurrencyNegativePattern");
            this.comboBoxCurrencyNegativePattern.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxCurrencyNegativePattern.FormatString = "C";
            this.comboBoxCurrencyNegativePattern.FormattingEnabled = true;
            this.comboBoxCurrencyNegativePattern.Name = "comboBoxCurrencyNegativePattern";
            manager.ApplyResources(this.comboBoxCurrencyPositivePattern, "comboBoxCurrencyPositivePattern");
            this.comboBoxCurrencyPositivePattern.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxCurrencyPositivePattern.FormatString = "C";
            this.comboBoxCurrencyPositivePattern.FormattingEnabled = true;
            this.comboBoxCurrencyPositivePattern.Name = "comboBoxCurrencyPositivePattern";
            manager.ApplyResources(this.comboBoxCurrencySymbol, "comboBoxCurrencySymbol");
            this.comboBoxCurrencySymbol.Name = "comboBoxCurrencySymbol";
            this.tabPage3.Controls.Add(box4);
            this.tabPage3.Controls.Add(label22);
            this.tabPage3.Controls.Add(label21);
            this.tabPage3.Controls.Add(label20);
            this.tabPage3.Controls.Add(label19);
            this.tabPage3.Controls.Add(this.comboBoxPMDesignator);
            this.tabPage3.Controls.Add(this.comboBoxAMDesignator);
            this.tabPage3.Controls.Add(this.comboBoxTimeSeparator);
            this.tabPage3.Controls.Add(this.comboBoxShortTimePattern);
            this.tabPage3.Controls.Add(box3);
            manager.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.comboBoxPMDesignator, "comboBoxPMDesignator");
            this.comboBoxPMDesignator.Name = "comboBoxPMDesignator";
            manager.ApplyResources(this.comboBoxAMDesignator, "comboBoxAMDesignator");
            this.comboBoxAMDesignator.Name = "comboBoxAMDesignator";
            manager.ApplyResources(this.comboBoxTimeSeparator, "comboBoxTimeSeparator");
            this.comboBoxTimeSeparator.Name = "comboBoxTimeSeparator";
            manager.ApplyResources(this.comboBoxShortTimePattern, "comboBoxShortTimePattern");
            this.comboBoxShortTimePattern.Name = "comboBoxShortTimePattern";
            this.tabPage4.Controls.Add(box7);
            this.tabPage4.Controls.Add(box6);
            this.tabPage4.Controls.Add(box5);
            manager.ApplyResources(this.tabPage4, "tabPage4");
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            manager.ApplyResources(this.buttonApply, "buttonApply");
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new EventHandler(this.buttonApply_Click);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.buttonApply);
            base.Controls.Add(this.tabControl1);
            base.Controls.Add(this.buttonOK);
            base.Controls.Add(this.buttonCancel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.HelpButton = true;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "CultureTuneForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            box.ResumeLayout(false);
            box.PerformLayout();
            box2.ResumeLayout(false);
            box2.PerformLayout();
            box3.ResumeLayout(false);
            box3.PerformLayout();
            box4.ResumeLayout(false);
            box5.ResumeLayout(false);
            box5.PerformLayout();
            this.numericUpDownTwoDigitYearMax.EndInit();
            box6.ResumeLayout(false);
            box6.PerformLayout();
            box7.ResumeLayout(false);
            box7.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        private void numericUpDownLastDigit_ValueChanged(object sender, EventArgs e)
        {
            int num = (int) this.numericUpDownTwoDigitYearMax.Value;
            this.textBoxTwoDigitYearMin.Text = (num - 0x3e7).ToString();
        }

        private void SetControls()
        {
            if (this.comboBoxNumberGroupSizes.Items.Count == 0)
            {
                this.comboBoxNumberGroupSizes.Items.AddRange(GroupSize.GetSizes());
                this.comboBoxNumberGroupSizes.Text = new GroupSize(this.culture.NumberFormat.NumberGroupSizes).ToString("N0", this.culture);
            }
            if (this.comboBoxNumberDecimalDigits.Items.Count == 0)
            {
                this.comboBoxNumberDecimalDigits.Items.AddRange(new object[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                this.comboBoxNumberDecimalDigits.SelectedItem = this.culture.NumberFormat.NumberDecimalDigits;
            }
            if (!this.comboBoxNumberDecimalSeparator.Items.Contains(this.culture.NumberFormat.NumberDecimalSeparator))
            {
                this.comboBoxNumberDecimalSeparator.Items.Add(this.culture.NumberFormat.NumberDecimalSeparator);
            }
            this.comboBoxNumberDecimalSeparator.Text = this.culture.NumberFormat.NumberDecimalSeparator;
            if (!this.comboBoxNumberGroupSeparator.Items.Contains(this.culture.NumberFormat.NumberGroupSeparator))
            {
                this.comboBoxNumberGroupSeparator.Items.Add(this.culture.NumberFormat.NumberGroupSeparator);
            }
            this.comboBoxNumberGroupSeparator.Text = this.culture.NumberFormat.NumberGroupSeparator;
            if (!this.comboBoxNegativeSign.Items.Contains(this.culture.NumberFormat.NegativeSign))
            {
                this.comboBoxNegativeSign.Items.Add(this.culture.NumberFormat.NegativeSign);
            }
            this.comboBoxNegativeSign.Text = this.culture.NumberFormat.NegativeSign;
            if (this.comboBoxNumberNegativePattern.Items.Count == 0)
            {
                this.comboBoxNumberNegativePattern.Items.AddRange(Pattern.GetNumberNegativePatterns());
                this.comboBoxNumberNegativePattern.Text = Pattern.GetNegativeSample("N", this.culture.NumberFormat.NumberNegativePattern, this.culture);
            }
            if (!this.comboBoxCurrencySymbol.Items.Contains(this.culture.NumberFormat.CurrencySymbol))
            {
                this.comboBoxCurrencySymbol.Items.Add(this.culture.NumberFormat.CurrencySymbol);
            }
            this.comboBoxCurrencySymbol.Text = this.culture.NumberFormat.CurrencySymbol;
            if (this.comboBoxCurrencyPositivePattern.Items.Count == 0)
            {
                this.comboBoxCurrencyPositivePattern.Items.AddRange(Pattern.GetCurrencyPositivePatterns());
                this.comboBoxCurrencyPositivePattern.Text = Pattern.GetPositiveSample("C", this.culture.NumberFormat.CurrencyPositivePattern, this.culture);
            }
            if (this.comboBoxCurrencyNegativePattern.Items.Count == 0)
            {
                this.comboBoxCurrencyNegativePattern.Items.AddRange(Pattern.GetCurrencyNegativePatterns());
                this.comboBoxCurrencyNegativePattern.Text = Pattern.GetNegativeSample("C", this.culture.NumberFormat.CurrencyNegativePattern, this.culture);
            }
            if (!this.comboBoxCurrencyDecimalSeparator.Items.Contains(this.culture.NumberFormat.CurrencyDecimalSeparator))
            {
                this.comboBoxCurrencyDecimalSeparator.Items.Add(this.culture.NumberFormat.CurrencyDecimalSeparator);
            }
            this.comboBoxCurrencyDecimalSeparator.Text = this.culture.NumberFormat.CurrencyDecimalSeparator;
            if (this.comboBoxCurrencyDecimalDigits.Items.Count == 0)
            {
                this.comboBoxCurrencyDecimalDigits.Items.AddRange(new object[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
                this.comboBoxCurrencyDecimalDigits.SelectedItem = this.culture.NumberFormat.CurrencyDecimalDigits;
            }
            if (!this.comboBoxCurrencyGroupSeparator.Items.Contains(this.culture.NumberFormat.CurrencyGroupSeparator))
            {
                this.comboBoxCurrencyGroupSeparator.Items.Add(this.culture.NumberFormat.CurrencyGroupSeparator);
            }
            this.comboBoxCurrencyGroupSeparator.Text = this.culture.NumberFormat.CurrencyGroupSeparator;
            if (this.comboBoxCurrency.Items.Count == 0)
            {
                this.comboBoxCurrency.Items.AddRange(GroupSize.GetSizes());
                this.comboBoxCurrency.Text = new GroupSize(this.culture.NumberFormat.CurrencyGroupSizes).ToString("N0", this.culture);
            }
            this.comboBoxShortTimePattern.Items.Clear();
            this.comboBoxShortTimePattern.Items.AddRange(this.culture.DateTimeFormat.GetAllDateTimePatterns('t'));
            this.comboBoxShortTimePattern.Text = this.culture.DateTimeFormat.ShortTimePattern;
            if (!this.comboBoxTimeSeparator.Items.Contains(this.culture.DateTimeFormat.TimeSeparator))
            {
                this.comboBoxTimeSeparator.Items.Add(this.culture.DateTimeFormat.TimeSeparator);
            }
            this.comboBoxTimeSeparator.Text = this.culture.DateTimeFormat.TimeSeparator;
            if (!this.comboBoxAMDesignator.Items.Contains(this.culture.DateTimeFormat.AMDesignator))
            {
                this.comboBoxAMDesignator.Items.Add(this.culture.DateTimeFormat.AMDesignator);
            }
            this.comboBoxAMDesignator.Text = this.culture.DateTimeFormat.AMDesignator;
            this.numericUpDownTwoDigitYearMax.Value = new decimal(this.culture.Calendar.TwoDigitYearMax);
            if (!this.comboBoxPMDesignator.Items.Contains(this.culture.DateTimeFormat.PMDesignator))
            {
                this.comboBoxPMDesignator.Items.Add(this.culture.DateTimeFormat.PMDesignator);
            }
            this.comboBoxPMDesignator.Text = this.culture.DateTimeFormat.PMDesignator;
            this.comboBoxShortDatePattern.Items.Clear();
            this.comboBoxShortDatePattern.Items.AddRange(this.culture.DateTimeFormat.GetAllDateTimePatterns('d'));
            this.comboBoxShortDatePattern.Text = this.culture.DateTimeFormat.ShortDatePattern;
            this.comboBoxLongDatePattern.Items.Clear();
            this.comboBoxLongDatePattern.Items.AddRange(this.culture.DateTimeFormat.GetAllDateTimePatterns('D'));
            this.comboBoxLongDatePattern.Text = this.culture.DateTimeFormat.LongDatePattern;
            if (!this.comboBoxDateSeparator.Items.Contains(this.culture.DateTimeFormat.DateSeparator))
            {
                this.comboBoxDateSeparator.Items.Add(this.culture.DateTimeFormat.DateSeparator);
            }
            this.comboBoxDateSeparator.Text = this.culture.DateTimeFormat.DateSeparator;
            this.SetSample();
        }

        private void SetSample()
        {
            this.textBoxNegativeCurrencySample.Text = 123456789M.ToString("C", this.culture);
            this.textBoxPositiveCurrencySample.Text = (-123456789M).ToString("C", this.culture);
            this.textBoxNegative.Text = 123456789M.ToString("N", this.culture);
            this.textBoxPositive.Text = (-123456789M).ToString("N", this.culture);
            this.textBoxTimeSample.Text = DateTime.Now.ToString("T", this.culture);
            this.textBoxFullDateSample.Text = DateTime.Now.ToString("D", this.culture);
            this.textBoxShortDateSample.Text = DateTime.Now.ToString("d", this.culture);
        }

        private abstract class Formattable : IFormattable
        {
            protected Formattable()
            {
            }

            protected NumberFormatInfo GetInfo(IFormatProvider provider)
            {
                CultureInfo info = provider as CultureInfo;
                if (info != null)
                {
                    return (NumberFormatInfo) info.NumberFormat.Clone();
                }
                NumberFormatInfo info2 = provider as NumberFormatInfo;
                if (info2 != null)
                {
                    return (NumberFormatInfo) info2.Clone();
                }
                return (NumberFormatInfo) Application.CurrentCulture.NumberFormat.Clone();
            }

            public abstract string ToString(string format, IFormatProvider formatProvider);
        }

        private class GroupSize : CultureTuneForm.Formattable
        {
            [DecimalConstant(0, 0, (uint) 0, (uint) 0, (uint) 0x75bcd15)]
            private static readonly decimal SAMPLE = 123456789M;
            public readonly int[] Size;

            public GroupSize(int[] groupSize)
            {
                this.Size = groupSize;
            }

            public static CultureTuneForm.GroupSize[] GetSizes()
            {
                List<CultureTuneForm.GroupSize> list = new List<CultureTuneForm.GroupSize>();
                int[] groupSize = new int[1];
                list.Add(new CultureTuneForm.GroupSize(groupSize));
                list.Add(new CultureTuneForm.GroupSize(new int[] { 3, 3, 3 }));
                list.Add(new CultureTuneForm.GroupSize(new int[] { 3, 2, 2, 2 }));
                return list.ToArray();
            }

            public override string ToString(string format, IFormatProvider formatProvider)
            {
                NumberFormatInfo provider = base.GetInfo(formatProvider);
                if (format.ToLower().Contains("c"))
                {
                    provider.CurrencyGroupSizes = this.Size;
                }
                else
                {
                    provider.NumberGroupSizes = this.Size;
                }
                decimal num = 123456789M;
                return num.ToString(format, provider);
            }
        }

        private abstract class Pattern : CultureTuneForm.Formattable
        {
            [DecimalConstant(1, 0x80, (uint) 0, (uint) 0, (uint) 11)]
            private static readonly decimal NEGATIVE_SAMPLE = -1.1M;
            public readonly int PatternNumber;
            [DecimalConstant(1, 0, (uint) 0, (uint) 0, (uint) 11)]
            private static readonly decimal POSITIVE_SAMPLE = 1.1M;

            protected Pattern(int i)
            {
                this.PatternNumber = i;
            }

            public static CultureTuneForm.Pattern[] GetCurrencyNegativePatterns()
            {
                return GetPatternsInternal(0x10, false);
            }

            public static CultureTuneForm.Pattern[] GetCurrencyPositivePatterns()
            {
                return GetPatternsInternal(4, true);
            }

            public static string GetNegativeSample(string format, int patternNumber, CultureInfo culture)
            {
                return new NegativePattern(patternNumber).ToString(format, culture);
            }

            public static CultureTuneForm.Pattern[] GetNumberNegativePatterns()
            {
                return GetPatternsInternal(5, false);
            }

            private static CultureTuneForm.Pattern[] GetPatternsInternal(int count, bool positive)
            {
                List<CultureTuneForm.Pattern> list = new List<CultureTuneForm.Pattern>();
                for (int i = 0; i < count; i++)
                {
                    list.Add(positive ? ((CultureTuneForm.Pattern) new PositivePattern(i)) : ((CultureTuneForm.Pattern) new NegativePattern(i)));
                }
                return list.ToArray();
            }

            public static string GetPositiveSample(string format, int patternNumber, CultureInfo culture)
            {
                return new PositivePattern(patternNumber).ToString(format, culture);
            }

            private class NegativePattern : CultureTuneForm.Pattern
            {
                public NegativePattern(int i) : base(i)
                {
                }

                public override string ToString(string format, IFormatProvider formatProvider)
                {
                    NumberFormatInfo provider = base.GetInfo(formatProvider);
                    if (format.ToLower().Contains("c"))
                    {
                        provider.CurrencyNegativePattern = base.PatternNumber;
                    }
                    else
                    {
                        provider.NumberNegativePattern = base.PatternNumber;
                    }
                    decimal num = -1.1M;
                    return num.ToString(format, provider);
                }
            }

            private class PositivePattern : CultureTuneForm.Pattern
            {
                internal PositivePattern(int i) : base(i)
                {
                }

                public override string ToString(string format, IFormatProvider formatProvider)
                {
                    NumberFormatInfo provider = base.GetInfo(formatProvider);
                    if (!format.ToLower().Contains("n"))
                    {
                        provider.CurrencyPositivePattern = base.PatternNumber;
                    }
                    decimal num = 1.1M;
                    return num.ToString(format, provider);
                }
            }
        }
    }
}

