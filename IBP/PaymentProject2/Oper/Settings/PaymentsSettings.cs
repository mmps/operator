﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class PaymentsSettings : ApplicationSettingsBase
    {
        private static PaymentsSettings defaultInstance = ((PaymentsSettings) SettingsBase.Synchronized(new PaymentsSettings()));
        public readonly SettingsHelper Helper;

        public PaymentsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
                this.Helper.SelectedFieldsString = this.SelectedFieldsString;
            }
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("LightGreen")]
        public Color AcceptedColor
        {
            get
            {
                return (Color) this["AcceptedColor"];
            }
            set
            {
                this["AcceptedColor"] = value;
            }
        }

        public static PaymentsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("LightSkyBlue")]
        public Color DelayedColor
        {
            get
            {
                return (Color) this["DelayedColor"];
            }
            set
            {
                this["DelayedColor"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("White")]
        public Color FinalizedColor
        {
            get
            {
                return (Color) this["FinalizedColor"];
            }
            set
            {
                this["FinalizedColor"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("Azure")]
        public Color HasChildAndParentColor
        {
            get
            {
                return (Color) this["HasChildAndParentColor"];
            }
            set
            {
                this["HasChildAndParentColor"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("LightSteelBlue")]
        public Color HasChildColor
        {
            get
            {
                return (Color) this["HasChildColor"];
            }
            set
            {
                this["HasChildColor"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("Lavender")]
        public Color HasParentColor
        {
            get
            {
                return (Color) this["HasParentColor"];
            }
            set
            {
                this["HasParentColor"] = value;
            }
        }

        [UserScopedSetting, DefaultSettingValue("True"), DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("Plum")]
        public Color NewColor
        {
            get
            {
                return (Color) this["NewColor"];
            }
            set
            {
                this["NewColor"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("True")]
        public bool OpenFile
        {
            get
            {
                return (bool) this["OpenFile"];
            }
            set
            {
                this["OpenFile"] = value;
            }
        }

        [DefaultSettingValue("Yellow"), UserScopedSetting, DebuggerNonUserCode]
        public Color ProcessingColor
        {
            get
            {
                return (Color) this["ProcessingColor"];
            }
            set
            {
                this["ProcessingColor"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("LightPink")]
        public Color RejectedColor
        {
            get
            {
                return (Color) this["RejectedColor"];
            }
            set
            {
                this["RejectedColor"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("False")]
        public bool SaveToExistingFile
        {
            get
            {
                return (bool) this["SaveToExistingFile"];
            }
            set
            {
                this["SaveToExistingFile"] = value;
            }
        }

        [DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("False")]
        public bool SelectFile
        {
            get
            {
                return (bool) this["SelectFile"];
            }
            set
            {
                this["SelectFile"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("Aquamarine")]
        public Color UseBalanceColor
        {
            get
            {
                return (Color) this["UseBalanceColor"];
            }
            set
            {
                this["UseBalanceColor"] = value;
            }
        }
    }
}

