﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Oper;
    using Microsoft.Reporting.WinForms;
    using System;

    internal class ReportMessages : IReportViewerMessages
    {
        public string BackButtonToolTip
        {
            get
            {
                return UserStrings.Back;
            }
        }

        public string BackMenuItemText
        {
            get
            {
                return UserStrings.Back;
            }
        }

        public string ChangeCredentialsText
        {
            get
            {
                return UserStrings.ChangeRights;
            }
        }

        public string CurrentPageTextBoxToolTip
        {
            get
            {
                return UserStrings.CurrentPage;
            }
        }

        public string DocumentMapButtonToolTip
        {
            get
            {
                return UserStrings.DocStructure;
            }
        }

        public string DocumentMapMenuItemText
        {
            get
            {
                return UserStrings.Structure;
            }
        }

        public string ExportButtonToolTip
        {
            get
            {
                return UserStrings.ReportExport;
            }
        }

        public string ExportMenuItemText
        {
            get
            {
                return UserStrings.Export;
            }
        }

        public string FalseValueText
        {
            get
            {
                return UserStrings.Lie;
            }
        }

        public string FindButtonText
        {
            get
            {
                return UserStrings.Find;
            }
        }

        public string FindButtonToolTip
        {
            get
            {
                return UserStrings.FindCoincidence;
            }
        }

        public string FindNextButtonText
        {
            get
            {
                return UserStrings.Next;
            }
        }

        public string FindNextButtonToolTip
        {
            get
            {
                return UserStrings.GoNextPage;
            }
        }

        public string FirstPageButtonToolTip
        {
            get
            {
                return UserStrings.GoFirstPage;
            }
        }

        public string LastPageButtonToolTip
        {
            get
            {
                return UserStrings.LastPage;
            }
        }

        public string NextPageButtonToolTip
        {
            get
            {
                return UserStrings.NexpPageJust;
            }
        }

        public string NoMoreMatches
        {
            get
            {
                return UserStrings.NoCoincidence;
            }
        }

        public string NullCheckBoxText
        {
            get
            {
                return "";
            }
        }

        public string NullCheckBoxToolTip
        {
            get
            {
                return "";
            }
        }

        public string NullValueText
        {
            get
            {
                return UserStrings.Empty;
            }
        }

        public string PageOf
        {
            get
            {
                return UserStrings.Page;
            }
        }

        public string PageSetupButtonToolTip
        {
            get
            {
                return UserStrings.ChoosePrintSettings;
            }
        }

        public string PageSetupMenuItemText
        {
            get
            {
                return UserStrings.PrintSettings;
            }
        }

        public string ParameterAreaButtonToolTip
        {
            get
            {
                return UserStrings.ReportSettings;
            }
        }

        public string PasswordPrompt
        {
            get
            {
                return UserStrings.Password;
            }
        }

        public string PreviousPageButtonToolTip
        {
            get
            {
                return UserStrings.PrevPage;
            }
        }

        public string PrintButtonToolTip
        {
            get
            {
                return UserStrings.PrintReport;
            }
        }

        public string PrintLayoutButtonToolTip
        {
            get
            {
                return UserStrings.Preview;
            }
        }

        public string PrintLayoutMenuItemText
        {
            get
            {
                return UserStrings.Preview;
            }
        }

        public string PrintMenuItemText
        {
            get
            {
                return UserStrings.Print02;
            }
        }

        public string ProgressText
        {
            get
            {
                return UserStrings.Execution;
            }
        }

        public string RefreshButtonToolTip
        {
            get
            {
                return UserStrings.RenewReport;
            }
        }

        public string RefreshMenuItemText
        {
            get
            {
                return UserStrings.Renew;
            }
        }

        public string SearchTextBoxToolTip
        {
            get
            {
                return UserStrings.CoincidenceSearch;
            }
        }

        public string SelectAll
        {
            get
            {
                return UserStrings.ChooseAll;
            }
        }

        public string SelectAValue
        {
            get
            {
                return UserStrings.ChooseValue;
            }
        }

        public string StopButtonToolTip
        {
            get
            {
                return UserStrings.StopReportGeneration;
            }
        }

        public string StopMenuItemText
        {
            get
            {
                return UserStrings.Stop;
            }
        }

        public string TextNotFound
        {
            get
            {
                return UserStrings.TextNotFound;
            }
        }

        public string TotalPagesToolTip
        {
            get
            {
                return UserStrings.AllQtyOfPages;
            }
        }

        public string TrueValueText
        {
            get
            {
                return UserStrings.Ok;
            }
        }

        public string UserNamePrompt
        {
            get
            {
                return UserStrings.UserName;
            }
        }

        public string ViewReportButtonText
        {
            get
            {
                return UserStrings.UserName;
            }
        }

        public string ViewReportButtonToolTip
        {
            get
            {
                return UserStrings.ShowReport;
            }
        }

        public string ZoomControlToolTip
        {
            get
            {
                return UserStrings.ShowReport;
            }
        }

        public string ZoomMenuItemText
        {
            get
            {
                return UserStrings.ShowReport;
            }
        }

        public string ZoomToPageWidth
        {
            get
            {
                return UserStrings.ByPageWidth;
            }
        }

        public string ZoomToWholePage
        {
            get
            {
                return UserStrings.WholePage;
            }
        }
    }
}

