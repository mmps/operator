﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"), CompilerGenerated]
    internal sealed class AccountsSettings : ApplicationSettingsBase
    {
        private static AccountsSettings defaultInstance = ((AccountsSettings) SettingsBase.Synchronized(new AccountsSettings()));
        public readonly SettingsHelper Helper;

        public AccountsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
            }
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        [SettingsDescription("Цвет для кассового счета"), DefaultSettingValue("128, 255, 255"), UserScopedSetting, DebuggerNonUserCode]
        public Color CashColor
        {
            get
            {
                return (Color) this["CashColor"];
            }
            set
            {
                this["CashColor"] = value;
            }
        }

        public static AccountsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("128, 255, 128"), SettingsDescription("Цвет для внешнего счета")]
        public Color ExternalColor
        {
            get
            {
                return (Color) this["ExternalColor"];
            }
            set
            {
                this["ExternalColor"] = value;
            }
        }

        [UserScopedSetting, DefaultSettingValue("True"), DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }
    }
}

