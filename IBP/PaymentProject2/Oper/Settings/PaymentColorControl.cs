﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Common.Component;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    internal class PaymentColorControl : UserControl
    {
        private PaymentColors colors = new PaymentColors();
        private IContainer components;
        private PropertyGrid propertyGrid1;

        public PaymentColorControl()
        {
            this.InitializeComponent();
            this.propertyGrid1.SelectedObject = this.colors;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(PaymentColorControl));
            this.propertyGrid1 = new PropertyGrid();
            base.SuspendLayout();
            manager.ApplyResources(this.propertyGrid1, "propertyGrid1");
            this.propertyGrid1.Name = "propertyGrid1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.propertyGrid1);
            base.Name = "PaymentColorControl";
            base.ResumeLayout(false);
        }

        public PaymentColors Colors
        {
            get
            {
                return this.colors;
            }
        }

        internal class PaymentColors
        {
            private Color accepted;
            private Color delayed;
            private Color finalized;
            private Color hasChild;
            private Color hasChildAndParent;
            private Color hasParent;
            private Color newColor;
            private Color processing;
            private Color rejected;
            private Color useBalance;

            [IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorAcceptedPays"), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Accepted"), Browsable(true), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorOnStatus")]
            public Color Accepted
            {
                get
                {
                    return this.accepted;
                }
                set
                {
                    this.accepted = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "SetAside"), Browsable(true), IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorSetAsidePays"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorOnStatus")]
            public Color Delayed
            {
                get
                {
                    return this.delayed;
                }
                set
                {
                    this.delayed = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorFinishedPays"), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Finished"), Browsable(true), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorPay")]
            public Color Finalized
            {
                get
                {
                    return this.finalized;
                }
                set
                {
                    this.finalized = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorPaysWithChildren"), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "HaveChild"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorsOnProperties"), Browsable(true)]
            public Color HasChild
            {
                get
                {
                    return this.hasChild;
                }
                set
                {
                    this.hasChild = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "ParentAndChild"), Browsable(true), IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "SomeDescription05"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorsOnProperties")]
            public Color HasChildAndParent
            {
                get
                {
                    return this.hasChildAndParent;
                }
                set
                {
                    this.hasChildAndParent = value;
                }
            }

            [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "HaveParent"), IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorPaysWithParents"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorsOnProperties")]
            public Color HasParent
            {
                get
                {
                    return this.hasParent;
                }
                set
                {
                    this.hasParent = value;
                }
            }

            [Browsable(true), IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorOfNewPays"), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "New"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorPay")]
            public Color New
            {
                get
                {
                    return this.newColor;
                }
                set
                {
                    this.newColor = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "InProcessing"), Browsable(true), IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorPayInProcessing"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorPay")]
            public Color Processing
            {
                get
                {
                    return this.processing;
                }
                set
                {
                    this.processing = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Rejected"), Browsable(true), IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "ColorRejectedPays"), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorOnStatus")]
            public Color Rejected
            {
                get
                {
                    return this.rejected;
                }
                set
                {
                    this.rejected = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.Description(typeof(UserStrings), "SomeDescription04"), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "ChangeUser"), Browsable(true), IBP.PaymentProject2.Common.Component.Category(typeof(UserStrings), "ColorsOnProperties")]
            public Color UseBalance
            {
                get
                {
                    return this.useBalance;
                }
                set
                {
                    this.useBalance = value;
                }
            }
        }
    }
}

