﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class WindowSettingsControl : UserControl
    {
        private CheckBox _saveLocation;
        private IContainer components;

        public WindowSettingsControl()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(WindowSettingsControl));
            this._saveLocation = new CheckBox();
            Label label = new Label();
            base.SuspendLayout();
            manager.ApplyResources(label, "label1");
            label.Name = "label1";
            manager.ApplyResources(this._saveLocation, "_saveLocation");
            this._saveLocation.Name = "_saveLocation";
            this._saveLocation.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(label);
            base.Controls.Add(this._saveLocation);
            base.Name = "WindowSettingsControl";
            base.ResumeLayout(false);
        }

        public bool SaveLocation
        {
            get
            {
                return this._saveLocation.Checked;
            }
            set
            {
                this._saveLocation.Checked = value;
            }
        }
    }
}

