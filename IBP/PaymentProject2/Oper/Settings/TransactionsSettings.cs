﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;

    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"), CompilerGenerated]
    internal sealed class TransactionsSettings : ApplicationSettingsBase
    {
        private static TransactionsSettings defaultInstance = ((TransactionsSettings) SettingsBase.Synchronized(new TransactionsSettings()));
        public readonly SettingsHelper Helper;

        public TransactionsSettings()
        {
            SettingsLoadedEventHandler handler = null;
            EventHandler handler2 = null;
            this.Helper = new SettingsHelper();
            if (handler == null)
            {
                handler = delegate (object sender, SettingsLoadedEventArgs e) {
                    this.Helper.SelectedFieldsString = this.SelectedFieldsString;
                };
            }
            base.SettingsLoaded += handler;
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
                this.Helper.SelectedFieldsString = this.SelectedFieldsString;
            }
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.SelectedFieldsString = this.Helper.SelectedFieldsString;
                };
            }
            this.Helper.SelectedFieldsChanged += handler2;
        }

        [UserScopedSetting, SettingsDescription("Цвет для кредитовых проводок"), DebuggerNonUserCode, DefaultSettingValue("128, 255, 255")]
        public Color CreditColor
        {
            get
            {
                return (Color) this["CreditColor"];
            }
            set
            {
                this["CreditColor"] = value;
            }
        }

        [DefaultSettingValue("128, 255, 128"), UserScopedSetting, SettingsDescription("Цвет для дебетовых проводок"), DebuggerNonUserCode]
        public Color DebetColor
        {
            get
            {
                return (Color) this["DebetColor"];
            }
            set
            {
                this["DebetColor"] = value;
            }
        }

        public static TransactionsSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DefaultSettingValue("True"), UserScopedSetting, DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
        public string SelectedFieldsString
        {
            get
            {
                return (string) this["SelectedFieldsString"];
            }
            set
            {
                this["SelectedFieldsString"] = value;
            }
        }
    }
}

