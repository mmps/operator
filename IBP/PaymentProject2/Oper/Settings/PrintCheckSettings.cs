﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class PrintCheckSettings : ApplicationSettingsBase
    {
        private static PrintCheckSettings defaultInstance = ((PrintCheckSettings) SettingsBase.Synchronized(new PrintCheckSettings()));

        public PrintCheckSettings()
        {
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
            }
            if (string.IsNullOrEmpty(this.PaymentFormat))
            {
                this.PaymentFormat = UserStrings.PrintFormat;
            }
        }

        [DebuggerNonUserCode, DefaultSettingValue("1"), UserScopedSetting]
        public int CopyCount
        {
            get
            {
                return (int) this["CopyCount"];
            }
            set
            {
                this["CopyCount"] = value;
            }
        }

        public static PrintCheckSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DefaultSettingValue("True"), UserScopedSetting, DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DebuggerNonUserCode, DefaultSettingValue(""), UserScopedSetting]
        public string PaymentFormat
        {
            get
            {
                return (string) this["PaymentFormat"];
            }
            set
            {
                this["PaymentFormat"] = value;
            }
        }

        [UserScopedSetting, DefaultSettingValue("True"), DebuggerNonUserCode]
        public bool PreView
        {
            get
            {
                return (bool) this["PreView"];
            }
            set
            {
                this["PreView"] = value;
            }
        }
    }
}

