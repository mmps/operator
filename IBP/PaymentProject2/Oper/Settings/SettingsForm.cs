﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class SettingsForm : Form
    {
        private Button buttonApply;
        private Button buttonCancel;
        private Button buttonOK;
        private ColumnsEditor cardOperationsEditor;
        private TreeNode cardOperationsNode;
        private TreeNode checkNode;
        private IContainer components;
        private ConnectionSettingsControl connControl;
        private TreeNode connectionNode;
        private CultureControl cultureEditor;
        private TreeNode mainNode;
        private Panel panel1;
        private PaymentColorControl paymentsColorControl;
        private TreeNode paymentsColorsNode;
        private ColumnsEditor paymentsEditor;
        private TreeNode paymentsNode;
        private SaveOptionControl paymentsOptionControl;
        private TreeNode paymentsTableNode;
        private PringCheckSettingControl printcontrol;
        private ColumnsEditor statisticEditor;
        private TreeNode statisticNode;
        private TreeNode statisticTableNode;
        private SaveOptionControl statOptionControl;
        private ColumnsEditor tranEditor;
        private TreeNode tranNode;
        private TreeView treeView1;
        private WindowSettingsControl winSetControl;
        private TreeNode winSetNode;

        public SettingsForm()
        {
            EventHandler handler = null;
            EventHandler handler2 = null;
            EventHandler handler3 = null;
            EventHandler handler4 = null;
            this.InitializeComponent();
            this.treeView1.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.OnNodeMouseClick);
            this.cultureEditor = new CultureControl();
            this.cultureEditor.Culture = MainSettings.Default.CultureInfo;
            this.mainNode = this.treeView1.Nodes.Add(UserStrings.Common);
            this.mainNode.Tag = this.cultureEditor;
            this.connControl = new ConnectionSettingsControl();
            this.connectionNode = this.treeView1.Nodes.Add(UserStrings.Connection);
            this.connectionNode.Tag = this.connControl;
            this.connControl.Host = IBP.PaymentProject2.Oper.ServerFasad.Server.Host;
            this.connControl.Port = IBP.PaymentProject2.Oper.ServerFasad.Server.Port;
            this.statisticEditor = new ColumnsEditor();
            this.statisticEditor.BindType = typeof(StatisticDataItem);
            if (!StatisticSettings.Default.Helper.IsReady)
            {
                StatisticSettings.Default.Reload();
            }
            if (!StatisticSettings.Default.Helper.IsReady)
            {
                this.statisticEditor.ColumnsCollection = StatisticSettings.Default.Helper.SelectedFields;
            }
            this.statOptionControl = new SaveOptionControl();
            this.statOptionControl.OpenFile = StatisticSettings.Default.OpenFile;
            this.statOptionControl.SelectFile = StatisticSettings.Default.SelectFile;
            this.statOptionControl.SaveToExistingFile = StatisticSettings.Default.SaveToExistingFile;
            if (handler == null)
            {
                handler = delegate (object sender, EventArgs e) {
                    this.statisticEditor.ColumnsCollection = StatisticSettings.Default.Helper.SelectedFields;
                };
            }
            StatisticSettings.Default.Helper.SelectedFieldsChanged += handler;
            this.statisticNode = this.treeView1.Nodes.Add(UserStrings.Statistics);
            this.statisticNode.Tag = this.statOptionControl;
            this.statisticTableNode = this.statisticNode.Nodes.Add(UserStrings.Table);
            this.statisticTableNode.Tag = this.statisticEditor;
            this.paymentsOptionControl = new SaveOptionControl();
            this.paymentsOptionControl.SelectFile = PaymentsSettings.Default.SelectFile;
            this.paymentsOptionControl.SaveToExistingFile = PaymentsSettings.Default.SaveToExistingFile;
            this.paymentsOptionControl.OpenFile = PaymentsSettings.Default.OpenFile;
            this.paymentsEditor = new ColumnsEditor();
            this.paymentsEditor.BindType = typeof(Payment);
            if (PaymentsSettings.Default.Helper.IsReady)
            {
                this.paymentsEditor.ColumnsCollection = PaymentsSettings.Default.Helper.SelectedFields;
            }
            this.paymentsColorControl = new PaymentColorControl();
            this.paymentsColorControl.Colors.Accepted = PaymentsSettings.Default.AcceptedColor;
            this.paymentsColorControl.Colors.New = PaymentsSettings.Default.NewColor;
            this.paymentsColorControl.Colors.Delayed = PaymentsSettings.Default.DelayedColor;
            this.paymentsColorControl.Colors.Finalized = PaymentsSettings.Default.FinalizedColor;
            this.paymentsColorControl.Colors.Processing = PaymentsSettings.Default.ProcessingColor;
            this.paymentsColorControl.Colors.HasChild = PaymentsSettings.Default.HasChildColor;
            this.paymentsColorControl.Colors.Rejected = PaymentsSettings.Default.RejectedColor;
            this.paymentsColorControl.Colors.UseBalance = PaymentsSettings.Default.UseBalanceColor;
            this.paymentsColorControl.Colors.HasChildAndParent = PaymentsSettings.Default.HasChildAndParentColor;
            this.paymentsColorControl.Colors.HasParent = PaymentsSettings.Default.HasParentColor;
            if (handler2 == null)
            {
                handler2 = delegate (object sender, EventArgs e) {
                    this.paymentsEditor.ColumnsCollection = PaymentsSettings.Default.Helper.SelectedFields;
                };
            }
            PaymentsSettings.Default.Helper.SelectedFieldsChanged += handler2;
            this.paymentsNode = this.treeView1.Nodes.Add(UserStrings.Pays02);
            this.paymentsNode.Tag = this.paymentsOptionControl;
            this.paymentsTableNode = this.paymentsNode.Nodes.Add(UserStrings.Table);
            this.paymentsTableNode.Tag = this.paymentsEditor;
            this.paymentsColorsNode = this.paymentsNode.Nodes.Add(UserStrings.PaysColor);
            this.paymentsColorsNode.Tag = this.paymentsColorControl;
            this.cardOperationsEditor = new ColumnsEditor();
            this.cardOperationsEditor.BindType = typeof(CardOperation);
            if (CardOperationsSettings.Default.Helper.IsReady)
            {
                this.cardOperationsEditor.ColumnsCollection = CardOperationsSettings.Default.Helper.SelectedFields;
            }
            this.cardOperationsNode = this.treeView1.Nodes.Add(UserStrings.CardOperation);
            this.cardOperationsNode.Tag = this.cardOperationsEditor;
            if (handler3 == null)
            {
                handler3 = delegate (object sender, EventArgs e) {
                    this.cardOperationsEditor.ColumnsCollection = CardOperationsSettings.Default.Helper.SelectedFields;
                };
            }
            CardOperationsSettings.Default.Helper.SelectedFieldsChanged += handler3;
            this.tranEditor = new ColumnsEditor();
            this.tranEditor.BindType = typeof(Transaction);
            if (TransactionsSettings.Default.Helper.IsReady)
            {
                this.tranEditor.ColumnsCollection = TransactionsSettings.Default.Helper.SelectedFields;
            }
            if (handler4 == null)
            {
                handler4 = delegate (object sender, EventArgs e) {
                    this.tranEditor.ColumnsCollection = TransactionsSettings.Default.Helper.SelectedFields;
                };
            }
            TransactionsSettings.Default.Helper.SelectedFieldsChanged += handler4;
            this.tranNode = this.treeView1.Nodes.Add(UserStrings.Transactions);
            this.tranNode.Tag = this.tranEditor;
            this.printcontrol = new PringCheckSettingControl();
            this.checkNode = this.treeView1.Nodes.Add(UserStrings.PrintCheck);
            this.checkNode.Tag = this.printcontrol;
            this.winSetControl = new WindowSettingsControl();
            this.winSetNode = this.treeView1.Nodes.Add(UserStrings.MainWindow);
            this.winSetNode.Tag = this.winSetControl;
            this.winSetControl.SaveLocation = MainSettings.Default.SaveLocation;
            this.SetControl(this.cultureEditor);
        }

        private void ApplyChanges()
        {
            MainSettings.Default.CultureInfo = this.cultureEditor.Culture;
            ServerList list = new ServerList(ConnectionSettings.Default.MemoString);
            ServerInfo srvInfo = new ServerInfo();
            srvInfo.Port = this.connControl.Port;
            srvInfo.Name = this.connControl.Host;
            list.Add(srvInfo);
            ConnectionSettings.Default.MemoString = list.ToString();
            StatisticSettings.Default.SaveToExistingFile = this.statOptionControl.SaveToExistingFile;
            StatisticSettings.Default.SelectFile = this.statOptionControl.SelectFile;
            StatisticSettings.Default.OpenFile = this.statOptionControl.OpenFile;
            StatisticSettings.Default.Helper.SelectedFields = this.statisticEditor.ColumnsCollection;
            PaymentsSettings.Default.UseBalanceColor = this.paymentsColorControl.Colors.UseBalance;
            PaymentsSettings.Default.SelectFile = this.paymentsOptionControl.SelectFile;
            PaymentsSettings.Default.SaveToExistingFile = this.paymentsOptionControl.SaveToExistingFile;
            PaymentsSettings.Default.RejectedColor = this.paymentsColorControl.Colors.Rejected;
            PaymentsSettings.Default.HasChildColor = this.paymentsColorControl.Colors.HasChild;
            PaymentsSettings.Default.ProcessingColor = this.paymentsColorControl.Colors.Processing;
            PaymentsSettings.Default.OpenFile = this.paymentsOptionControl.OpenFile;
            PaymentsSettings.Default.NewColor = this.paymentsColorControl.Colors.New;
            PaymentsSettings.Default.FinalizedColor = this.paymentsColorControl.Colors.Finalized;
            PaymentsSettings.Default.DelayedColor = this.paymentsColorControl.Colors.Delayed;
            PaymentsSettings.Default.AcceptedColor = this.paymentsColorControl.Colors.Accepted;
            PaymentsSettings.Default.Helper.SelectedFields = this.paymentsEditor.ColumnsCollection;
            PaymentsSettings.Default.HasParentColor = this.paymentsColorControl.Colors.HasParent;
            PaymentsSettings.Default.HasChildAndParentColor = this.paymentsColorControl.Colors.HasChildAndParent;
            CardOperationsSettings.Default.Helper.SelectedFields = this.cardOperationsEditor.ColumnsCollection;
            TransactionsSettings.Default.Helper.SelectedFields = this.tranEditor.ColumnsCollection;
            PrintCheckSettings.Default.PreView = this.printcontrol.PreeView;
            PrintCheckSettings.Default.PaymentFormat = this.printcontrol.Format;
            PrintCheckSettings.Default.CopyCount = this.printcontrol.CopyCount;
            MainSettings.Default.SaveLocation = this.winSetControl.SaveLocation;
            CardOperationsSettings.Default.Save();
            ConnectionSettings.Default.Save();
            MainSettings.Default.Save();
            PaymentsSettings.Default.Save();
            PrintCheckSettings.Default.Save();
            StatisticSettings.Default.Save();
            TransactionsSettings.Default.Save();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            try
            {
                this.ApplyChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
            base.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.ApplyChanges();
                base.DialogResult = DialogResult.OK;
                base.Close();
            }
            catch (ArgumentException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void ClearControls()
        {
            this.panel1.Controls.Clear();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(SettingsForm));
            this.buttonApply = new Button();
            this.buttonCancel = new Button();
            this.buttonOK = new Button();
            this.treeView1 = new TreeView();
            this.panel1 = new Panel();
            base.SuspendLayout();
            manager.ApplyResources(this.buttonApply, "buttonApply");
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new EventHandler(this.buttonApply_Click);
            manager.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
            manager.ApplyResources(this.buttonOK, "buttonOK");
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
            manager.ApplyResources(this.treeView1, "treeView1");
            this.treeView1.Name = "treeView1";
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.panel1);
            base.Controls.Add(this.treeView1);
            base.Controls.Add(this.buttonOK);
            base.Controls.Add(this.buttonCancel);
            base.Controls.Add(this.buttonApply);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "SettingsForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.ResumeLayout(false);
        }

        private void OnNodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                base.SuspendLayout();
                this.ClearControls();
                if ((e.Node != null) && (e.Node.Tag is Control))
                {
                    this.SetControl((Control) e.Node.Tag);
                }
            }
            finally
            {
                base.ResumeLayout();
            }
        }

        private void SetControl(Control control)
        {
            this.panel1.Controls.Add(control);
            control.Dock = DockStyle.Fill;
        }

        private Control CurrentControl
        {
            get
            {
                if (this.panel1.Controls.Count != 0)
                {
                    return this.panel1.Controls[0];
                }
                return null;
            }
        }
    }
}

