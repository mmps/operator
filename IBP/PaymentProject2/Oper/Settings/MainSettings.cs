﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.CodeDom.Compiler;
    using System.Configuration;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading;

    [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"), CompilerGenerated]
    internal sealed class MainSettings : ApplicationSettingsBase
    {
        private System.Globalization.CultureInfo cultureInfo;
        private static MainSettings defaultInstance = ((MainSettings) SettingsBase.Synchronized(new MainSettings()));
        private object syncRoot = new object();

        public event EventHandler CultureInfoChanged;

        public MainSettings()
        {
            if (this.NeedUpgrade)
            {
                this.Upgrade();
                this.NeedUpgrade = false;
            }
        }

        public System.Globalization.CultureInfo CultureInfo
        {
            get
            {
                if (this.cultureInfo == null)
                {
                    lock (this.syncRoot)
                    {
                        if (this.cultureInfo == null)
                        {
                            if (string.IsNullOrEmpty(this.CultureInfoString))
                            {
                                this.cultureInfo = System.Globalization.CultureInfo.CurrentCulture;
                            }
                            else
                            {
                                using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(this.CultureInfoString)))
                                {
                                    BinaryFormatter formatter = new BinaryFormatter();
                                    this.cultureInfo = (System.Globalization.CultureInfo) formatter.Deserialize(stream);
                                }
                            }
                        }
                    }
                }
                return this.cultureInfo;
            }
            set
            {
                this.cultureInfo = value;
                using (MemoryStream stream = new MemoryStream())
                {
                    new BinaryFormatter().Serialize(stream, this.cultureInfo);
                    this.CultureInfoString = Convert.ToBase64String(stream.ToArray());
                }
                if (this.CultureInfoChanged != null)
                {
                    this.CultureInfoChanged(this, new EventArgs());
                }
            }
        }

        [UserScopedSetting, DefaultSettingValue(""), DebuggerNonUserCode]
        public string CultureInfoString
        {
            get
            {
                return (string) this["CultureInfoString"];
            }
            set
            {
                this["CultureInfoString"] = value;
            }
        }

        public static MainSettings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        [DefaultSettingValue("True"), UserScopedSetting, DebuggerNonUserCode]
        public bool NeedUpgrade
        {
            get
            {
                return (bool) this["NeedUpgrade"];
            }
            set
            {
                this["NeedUpgrade"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("False")]
        public bool SaveLocation
        {
            get
            {
                return (bool) this["SaveLocation"];
            }
            set
            {
                this["SaveLocation"] = value;
            }
        }
    }
}

