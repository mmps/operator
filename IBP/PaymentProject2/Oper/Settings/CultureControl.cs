﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Windows.Forms;

    public class CultureControl : UserControl
    {
        private Button button1;
        private ComboBox comboBox1;
        private IContainer components;
        private CultureInfo currentCulture;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        [DecimalConstant(0, 0, (uint) 0, (uint) 0, (uint) 0x75bcd15)]
        private static readonly decimal SAMPLE = 123456789M;
        private bool seekComboBox = true;
        private TextBox textBoxFullDate;
        private TextBox textBoxMoney;
        private TextBox textBoxNumber;
        private TextBox textBoxShortDate;
        private TextBox textBoxTime;

        public CultureControl()
        {
            this.InitializeComponent();
            CultureTypes specificCultures = CultureTypes.SpecificCultures;
            CultureInfo[] cultures = CultureInfo.GetCultures(specificCultures);
            this.comboBox1.Items.AddRange(cultures);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (CultureTuneForm form = new CultureTuneForm(this.currentCulture))
            {
                form.ShowDialog();
            }
            this.SetCulture();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.seekComboBox)
            {
                this.currentCulture = (CultureInfo) this.comboBox1.SelectedItem;
                this.SetCulture();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FindCulture()
        {
            this.seekComboBox = false;
            this.comboBox1.SelectedText = this.currentCulture.ToString();
            this.seekComboBox = true;
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(CultureControl));
            this.label1 = new Label();
            this.label2 = new Label();
            this.textBoxNumber = new TextBox();
            this.label3 = new Label();
            this.label4 = new Label();
            this.textBoxMoney = new TextBox();
            this.label5 = new Label();
            this.textBoxTime = new TextBox();
            this.label6 = new Label();
            this.textBoxShortDate = new TextBox();
            this.label7 = new Label();
            this.textBoxFullDate = new TextBox();
            this.comboBox1 = new ComboBox();
            this.button1 = new Button();
            base.SuspendLayout();
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.textBoxNumber, "textBoxNumber");
            this.textBoxNumber.Name = "textBoxNumber";
            this.textBoxNumber.ReadOnly = true;
            manager.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            manager.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            manager.ApplyResources(this.textBoxMoney, "textBoxMoney");
            this.textBoxMoney.Name = "textBoxMoney";
            this.textBoxMoney.ReadOnly = true;
            manager.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            manager.ApplyResources(this.textBoxTime, "textBoxTime");
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.ReadOnly = true;
            manager.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            manager.ApplyResources(this.textBoxShortDate, "textBoxShortDate");
            this.textBoxShortDate.Name = "textBoxShortDate";
            this.textBoxShortDate.ReadOnly = true;
            manager.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            manager.ApplyResources(this.textBoxFullDate, "textBoxFullDate");
            this.textBoxFullDate.Name = "textBoxFullDate";
            this.textBoxFullDate.ReadOnly = true;
            manager.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Sorted = true;
            this.comboBox1.SelectedIndexChanged += new EventHandler(this.comboBox1_SelectedIndexChanged);
            manager.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new EventHandler(this.button1_Click);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.button1);
            base.Controls.Add(this.comboBox1);
            base.Controls.Add(this.textBoxFullDate);
            base.Controls.Add(this.label7);
            base.Controls.Add(this.textBoxShortDate);
            base.Controls.Add(this.label6);
            base.Controls.Add(this.textBoxTime);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.textBoxMoney);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.textBoxNumber);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Name = "CultureControl";
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void SetCulture()
        {
            DateTime now = DateTime.Now;
            this.textBoxFullDate.Text = now.ToString("F", this.currentCulture);
            this.textBoxMoney.Text = 123456789M.ToString("C", this.currentCulture);
            this.textBoxNumber.Text = 123456789M.ToString("N", this.currentCulture);
            this.textBoxShortDate.Text = now.ToString("d", this.currentCulture);
            this.textBoxTime.Text = now.ToString("t", this.currentCulture);
        }

        public CultureInfo Culture
        {
            get
            {
                return this.currentCulture;
            }
            set
            {
                this.currentCulture = (CultureInfo) value.Clone();
                this.FindCulture();
                this.SetCulture();
            }
        }
    }
}

