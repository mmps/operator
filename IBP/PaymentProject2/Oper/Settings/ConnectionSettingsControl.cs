﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.ComponentModel;
    using System.Net;
    using System.Windows.Forms;

    public class ConnectionSettingsControl : UserControl
    {
        private IContainer components;
        private string host;
        private Label label1;
        private Label label2;
        private NumericUpDown numericUpDownPort;
        private TextBox textBoxServer;

        public ConnectionSettingsControl()
        {
            this.InitializeComponent();
            this.textBoxServer.LostFocus += new EventHandler(this.OnLostFocus);
            this.textBoxServer.KeyUp += new KeyEventHandler(this.OnKeyUp);
        }

        private bool CheckHostName(string host)
        {
            try
            {
                Dns.GetHostEntry(host);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format(UserStrings.ServerNotFound, host));
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ConnectionSettingsControl));
            this.label1 = new Label();
            this.textBoxServer = new TextBox();
            this.label2 = new Label();
            this.numericUpDownPort = new NumericUpDown();
            this.numericUpDownPort.BeginInit();
            base.SuspendLayout();
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            manager.ApplyResources(this.textBoxServer, "textBoxServer");
            this.textBoxServer.Name = "textBoxServer";
            manager.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            manager.ApplyResources(this.numericUpDownPort, "numericUpDownPort");
            int[] bits = new int[4];
            bits[0] = 0xffff;
            this.numericUpDownPort.Maximum = new decimal(bits);
            int[] numArray2 = new int[4];
            numArray2[0] = 1;
            this.numericUpDownPort.Minimum = new decimal(numArray2);
            this.numericUpDownPort.Name = "numericUpDownPort";
            int[] numArray3 = new int[4];
            numArray3[0] = 1;
            this.numericUpDownPort.Value = new decimal(numArray3);
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.numericUpDownPort);
            base.Controls.Add(this.textBoxServer);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Name = "ConnectionSettingsControl";
            this.numericUpDownPort.EndInit();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.textBoxServer.Text = this.host;
            }
        }

        private void OnLostFocus(object sender, EventArgs e)
        {
            if (this.host != this.textBoxServer.Text)
            {
                if (this.CheckHostName(this.textBoxServer.Text))
                {
                    this.host = this.textBoxServer.Text;
                }
                else
                {
                    this.textBoxServer.Focus();
                    this.textBoxServer.SelectAll();
                }
            }
        }

        public string Host
        {
            get
            {
                return this.host;
            }
            set
            {
                this.host = value;
                this.textBoxServer.Text = value;
            }
        }

        public int Port
        {
            get
            {
                return (int) this.numericUpDownPort.Value;
            }
            set
            {
                this.numericUpDownPort.Value = new decimal(value);
            }
        }
    }
}

