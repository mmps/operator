﻿namespace IBP.PaymentProject2.Oper.Settings
{
    using IBP.Controls;
    using System;
    using System.Threading;

    internal class SettingsHelper
    {
        private ListViewEditColumnsCollection fields;

        public event EventHandler SelectedFieldsChanged;

        public SettingsHelper() : this("")
        {
        }

        public SettingsHelper(string selectedFieldsString)
        {
            this.fields = string.IsNullOrEmpty(selectedFieldsString) ? null : ListViewEditColumnsCollection.ConvertFromString(selectedFieldsString);
        }

        private void RaiseEvent()
        {
            if (this.SelectedFieldsChanged != null)
            {
                this.SelectedFieldsChanged(this, new EventArgs());
            }
        }

        public bool IsReady
        {
            get
            {
                return (this.fields != null);
            }
        }

        public ListViewEditColumnsCollection SelectedFields
        {
            get
            {
                return this.fields;
            }
            set
            {
                this.fields = value;
                this.RaiseEvent();
            }
        }

        public string SelectedFieldsString
        {
            get
            {
                if (this.fields != null)
                {
                    return ListViewEditColumnsCollection.ConvertToString(this.fields);
                }
                return "";
            }
            set
            {
                this.SelectedFields = string.IsNullOrEmpty(value) ? null : ListViewEditColumnsCollection.ConvertFromString(value);
            }
        }
    }
}

