﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Component;
    using System;
    using System.ComponentModel;

    public class StatisticDataItem
    {
        private object label = "";
        private StatisticInfo statinfo;

        public StatisticDataItem(object label, StatisticInfo statinfo)
        {
            this.label = label;
            this.statinfo = statinfo;
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_AvgAmount"), Browsable(true)]
        public decimal AvgAmount
        {
            get
            {
                return this.statinfo.AvgAmount;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_AvgBalance")]
        public decimal AvgBalance
        {
            get
            {
                return this.statinfo.AvgBalance;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_AvgComission")]
        public decimal AvgComission
        {
            get
            {
                return this.statinfo.AvgComission;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_AvgStartBalance"), Browsable(true)]
        public decimal AvgStartBalance
        {
            get
            {
                return this.statinfo.AvgStartBalance;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_AvgTotal"), Browsable(true)]
        public decimal AvgTotal
        {
            get
            {
                return this.statinfo.AvgTotal;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_AvgValue"), Browsable(true)]
        public decimal AvgValue
        {
            get
            {
                return this.statinfo.AvgValue;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_PaymentCount")]
        public int Count
        {
            get
            {
                return this.statinfo.Count;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_Label")]
        public object Label
        {
            get
            {
                return this.label;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MaxAmount"), Browsable(true)]
        public decimal MaxAmount
        {
            get
            {
                return this.statinfo.MaxAmount;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MaxBalance")]
        public decimal MaxBalance
        {
            get
            {
                return this.statinfo.MaxBalance;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MaxComission"), Browsable(true)]
        public decimal MaxComission
        {
            get
            {
                return this.statinfo.MaxComission;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MaxStartBalance"), Browsable(true)]
        public decimal MaxStartBalance
        {
            get
            {
                return this.statinfo.MaxStartBalance;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MaxTotal")]
        public decimal MaxTotal
        {
            get
            {
                return this.statinfo.MaxTotal;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MaxValue")]
        public decimal MaxValue
        {
            get
            {
                return this.statinfo.MaxValue;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MinAmount"), Browsable(true)]
        public decimal MinAmount
        {
            get
            {
                return this.statinfo.MinAmount;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MinBalance"), Browsable(true)]
        public decimal MinBalance
        {
            get
            {
                return this.statinfo.MinBalance;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MinComission")]
        public decimal MinComission
        {
            get
            {
                return this.statinfo.MinComission;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MinStartBalance")]
        public decimal MinStartBalance
        {
            get
            {
                return this.statinfo.MinStartBalance;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MinTotal"), Browsable(true)]
        public decimal MinTotal
        {
            get
            {
                return this.statinfo.MinTotal;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_MinValue"), Browsable(true)]
        public decimal MinValue
        {
            get
            {
                return this.statinfo.MinValue;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumAmount")]
        public decimal SumAmount
        {
            get
            {
                return this.statinfo.SumAmount;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumBalance"), Browsable(true)]
        public decimal SumBalance
        {
            get
            {
                return this.statinfo.SumBalance;
            }
        }

        [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumComission"), Browsable(true)]
        public decimal SumComission
        {
            get
            {
                return this.statinfo.SumComission;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumFromBalance")]
        public decimal SumFromBalance
        {
            get
            {
                return this.statinfo.SumFromBalance;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumStartBalance")]
        public decimal SumStartBalance
        {
            get
            {
                return this.statinfo.SumStartBalance;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumTotal")]
        public decimal SumTotal
        {
            get
            {
                return this.statinfo.SumTotal;
            }
        }

        [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Stat_SumValue")]
        public decimal SumValue
        {
            get
            {
                return this.statinfo.SumValue;
            }
        }
    }
}

