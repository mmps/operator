﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Text;

    internal class PrepaidActTemplate
    {
        private const string COM_ = "{com_";
        private decimal comission;
        private int count;
        private const string COUNT = "{count}";
        private const string COUNT_ = "{count_";
        private const string COUNT_LEFT = "{count_left}";
        private const string COUNT_LEFT_ = "{count_left_";
        private int countLeft;
        private const string DOC_DATE = "{doc_date}";
        private const string DOC_TIME = "{doc_time}";
        private DateTime docDate;
        private Encoding encoding;
        private const string END_DATE = "{end_date}";
        private DateTime endDate;
        private const string NOMINAL_ = "{nominal_";
        private const string START_DATE = "{st_date}";
        private DateTime startDate;
        private const string SUM_COM = "{sum_com}";
        private const string SUM_COM_ = "{sum_com_";
        private const string SUM_TOTAL = "{sum_total}";
        private const string SUM_TOTAL_ = "{sum_total_";
        private const string SUM_VALUE = "{sum_value}";
        private const string SUM_VALUE_ = "{sum_value_";
        private StringBuilder template;
        private decimal total;
        private const string TOTAL_ = "{total_";
        private decimal value;
        private const string VALUE_ = "{value_";

        public PrepaidActTemplate(string templateFile, PrepaidStatisticInfo[] info) : this(templateFile, Encoding.GetEncoding(0x4e3), info)
        {
        }

        public PrepaidActTemplate(string templateFile, Encoding encoding, PrepaidStatisticInfo[] info)
        {
            this.docDate = DateTime.Now;
            CultureInfo cultureInfo = MainSettings.Default.CultureInfo;
            if (info.Length == 0)
            {
                throw new ArgumentException(UserStrings.NoStatistics);
            }
            if (encoding == null)
            {
                using (StreamReader reader = new StreamReader(templateFile, true))
                {
                    this.encoding = reader.CurrentEncoding;
                    this.template = new StringBuilder(reader.ReadToEnd());
                    goto Label_0091;
                }
            }
            using (StreamReader reader2 = new StreamReader(templateFile, encoding))
            {
                this.encoding = reader2.CurrentEncoding;
                this.template = new StringBuilder(reader2.ReadToEnd());
            }
        Label_0091:
            this.count = 0;
            this.countLeft = 0;
            this.comission = 0M;
            this.total = 0M;
            this.value = 0M;
            foreach (PrepaidStatisticInfo info3 in info)
            {
                this.comission += info3.Comission;
                this.countLeft += info3.CountLeft;
                this.count += info3.Count;
                this.total += info3.Total;
                this.value += info3.Value;
                PrepaidNominal prepaidNominal = info3.PrepaidNominal;
                this["{nominal_" + prepaidNominal.Nominal + '}'] = info3.PrepaidNominal.Nominal;
                this["{total_" + prepaidNominal.Nominal + '}'] = prepaidNominal.Total.ToString("C", cultureInfo);
                this["{value_" + prepaidNominal.Nominal + '}'] = prepaidNominal.Value.ToString("C", cultureInfo);
                this["{com_" + prepaidNominal.Nominal + '}'] = prepaidNominal.Comission.ToString("C", cultureInfo);
                this["{count_" + prepaidNominal.Nominal + '}'] = info3.Count.ToString(cultureInfo);
                this["{count_left_" + prepaidNominal.Nominal + '}'] = info3.CountLeft.ToString(cultureInfo);
                this["{sum_com_" + prepaidNominal.Nominal + '}'] = info3.Comission.ToString("C", cultureInfo);
                this["{sum_total_" + prepaidNominal.Nominal + '}'] = info3.Total.ToString("C", cultureInfo);
                this["{sum_value_" + prepaidNominal.Nominal + '}'] = info3.Value.ToString("C", cultureInfo);
            }
            this["{st_date}"] = info[0].StartDate.ToString("D", cultureInfo);
            this["{end_date}"] = info[0].EndDate.ToString("D", cultureInfo);
            this["{doc_date}"] = this.docDate.ToString("D", cultureInfo);
            this["{doc_time}"] = this.docDate.ToString("T", cultureInfo);
            this["{count}"] = this.count.ToString(cultureInfo);
            this["{count_left}"] = this.countLeft.ToString(cultureInfo);
            this["{sum_total}"] = this.total.ToString("C", cultureInfo);
            this["{sum_value}"] = this.value.ToString("C", cultureInfo);
            this["{sum_com}"] = this.comission.ToString("C", cultureInfo);
        }

        public void Save(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, this.encoding))
            {
                writer.Write(this.template.ToString());
                writer.Flush();
            }
        }

        public DateTime DocDate
        {
            get
            {
                return this.docDate.Date;
            }
        }

        public TimeSpan DocTime
        {
            get
            {
                return this.docDate.TimeOfDay;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return this.endDate;
            }
            set
            {
                this.endDate = value.Date;
            }
        }

        private string this[string parameterName]
        {
            set
            {
                this.template = this.template.Replace(parameterName, value);
            }
        }

        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }
            set
            {
                this.startDate = value.Date;
            }
        }
    }
}

