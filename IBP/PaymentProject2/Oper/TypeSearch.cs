﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common.Component;
    using System;

    internal enum TypeSearch
    {
        [Description(typeof(UserStrings), "Accounts")]
        Accounts = 5,
        [Description(typeof(UserStrings), "CardOperation02")]
        CardOperations = 1,
        [Description(typeof(UserStrings), "ClientReviseActs")]
        ClientReviseAct = 10,
        [Description(typeof(UserStrings), "Clients")]
        Clients = 6,
        [Description(typeof(UserStrings), "DialerReestr")]
        ClientsRolls = 8,
        [Description(typeof(UserStrings), "TariffPlan02")]
        ClientTariffPlan = 9,
        [Description(typeof(UserStrings), "TransReceiving")]
        ExternalTransactions = 4,
        [Description(typeof(UserStrings), "Pays")]
        Payments = 0,
        [Description(typeof(UserStrings), "ReestrOfReceiver")]
        RecipientRolls = 7,
        [Description(typeof(UserStrings), "Recipients")]
        Recipients = 11,
        [Description(typeof(UserStrings), "Statistics")]
        Statistic = 3,
        [Description(typeof(UserStrings), "Transactions")]
        Transactions = 2
    }
}

