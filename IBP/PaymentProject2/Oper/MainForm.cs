﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.License;
    using IBP.PaymentProject2.Common.Login;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Forms;
    using IBP.PaymentProject2.Oper.Properties;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Reflection;
    using System.Security;
    using System.Windows.Forms;

    public class MainForm : Form
    {
        private IBP.PaymentProject2.Oper.Command _reportCommand = new ReportCommand();
        private IBP.PaymentProject2.Oper.Command aboutCmd;
        private Button buttonFilterOk;
        private CheckBox checkBoxNewWindow;
        private ComboBox comboBoxTypeSearch;
        private IContainer components;
        private IBP.PaymentProject2.Oper.Command createNewPayment;
        private IBP.PaymentProject2.Oper.Command createTransaction = new TransactionCommand(true);
        private UserControl currentFilter;
        private bool exit;
        private IBP.PaymentProject2.Oper.Command helpCommand = new HelpCommand();
        private Label label10;
        private MenuStrip mainMenuStrip;
        private ToolStrip mainWndToolStrip;
        private Panel panel1;
        private IBP.PaymentProject2.Oper.Command prepaidsReportCommand;
        private Label productVerLabel;
        private IBP.PaymentProject2.Oper.Command recountAllBalances = new RecountAllBalances();
        private IBP.PaymentProject2.Oper.Command sendCustomRoll;
        private IBP.PaymentProject2.Oper.Command sendMultipleCustomRolls;
        private IBP.PaymentProject2.Oper.Command settingsCommand;
        private IBP.PaymentProject2.Oper.Command showInfoCollect;
        private IBP.PaymentProject2.Oper.Command showPaymentsWithBalance = new ShowPaymentsWithBalance();
        private IBP.PaymentProject2.Oper.Command showTariffPlan;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusServer;

        public MainForm()
        {
            if (this.Connecting())
            {
                this.Initialize();
                base.MdiChildActivate += new EventHandler(this.Form_Activated);
                Application.CurrentCulture = MainSettings.Default.CultureInfo;
            }
        }

        private void buttonFilterOk_Click(object sender, EventArgs e)
        {
            try
            {
                base.UseWaitCursor = true;
                this.ShowForm(this.SelectedType);
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                base.UseWaitCursor = false;
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            ((IFilterControl) this.currentFilter).ResetFilter();
        }

        private void comboBoxTypeSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxTypeSearch.SelectedItem is EnumTypeInfo<TypeSearch>)
            {
                bool flag;
                if (this.currentFilter != null)
                {
                    this.currentFilter.Hide();
                }
                TypeSearch enumValue = ((EnumTypeInfo<TypeSearch>) this.comboBoxTypeSearch.SelectedItem).EnumValue;
                UserControl filter = FilterControl.GetFilter(enumValue);
                filter.Parent = this.panel1;
                filter.Show();
                this.currentFilter = filter;
                switch (enumValue)
                {
                    case TypeSearch.ClientTariffPlan:
                    case TypeSearch.ClientReviseAct:
                        flag = IBP.PaymentProject2.Oper.ServerFasad.Server.CheckFeature(FeatureType.DealerTariffPlan);
                        break;

                    default:
                        flag = true;
                        break;
                }
                this.buttonFilterOk.Enabled = flag;
            }
        }

        private bool Connecting()
        {
            ConnectionSettings settings = ConnectionSettings.Default;
            string memoString = settings.MemoString;
            string lastServer = settings.LastServer;
            string defaultServer = settings.DefaultServer;
            if (!LoginDialog.ConnectToServer(IBP.PaymentProject2.Oper.ServerFasad.Server, UserRole.oper, ref memoString, ref lastServer, ref defaultServer, settings.LastUser, settings.Localization, "Operator", this.AppVersion))
            {
                this.exit = true;
                return false;
            }
            settings.MemoString = memoString;
            settings.DefaultServer = defaultServer;
            settings.LastServer = lastServer;
            settings.LastUser = IBP.PaymentProject2.Oper.ServerFasad.Server.CurrentUser.Login;
            settings.Localization = Localization.CurrentUICulture.Name;
            settings.Save();
            IBP.PaymentProject2.Oper.ServerFasad.Server.ConnectionSettingsChanged += new EventHandler<ConnectionSettingsEventArgs>(this.OnConnectionSettingsChanged);
            return true;
        }

        internal void CreateSearchResultForm(System.Type formType, FilterOper filter)
        {
            System.Type type = formType;
            ResultForm form = type.InvokeMember(null, BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly, null, null, null) as ResultForm;
            form.MdiParent = this;
            form.Filter = filter;
            form.Show();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void Form_Activated(object sender, EventArgs e)
        {
            this.RefreshVersionInfoShow();
            if (base.ActiveMdiChild != null)
            {
                System.Type type = base.ActiveMdiChild.GetType();
                object[] customAttributes = type.GetCustomAttributes(typeof(FilterFormAttribute), false);
                if (customAttributes.Length != 1)
                {
                    if (type.GetCustomAttributes(typeof(NoFilterAtribute), false).Length == 0)
                    {
                        throw new InvalidOperationException(UserStrings.FormHaveMoreAttrib);
                    }
                }
                else
                {
                    FilterFormAttribute attribute = (FilterFormAttribute) customAttributes[0];
                    this.comboBoxTypeSearch.SelectedItem = new EnumTypeInfo<TypeSearch>(attribute.TypeSearch);
                    this.Filter = ((IFormResultSearch) base.ActiveMdiChild).Filter;
                }
            }
        }

        private System.Type GetFormType(TypeSearch typeSearch)
        {
            foreach (System.Type type in base.GetType().Assembly.GetTypes())
            {
                object[] customAttributes = type.GetCustomAttributes(typeof(FilterFormAttribute), false);
                if ((customAttributes.Length == 1) && (((FilterFormAttribute) customAttributes[0]).TypeSearch == typeSearch))
                {
                    return type;
                }
            }
            return null;
        }

        private void InitComboBoxies()
        {
            foreach (EnumTypeInfo<TypeSearch> info in EnumTypeInfo<TypeSearch>.GetInfo())
            {
                this.comboBoxTypeSearch.Items.Add(info);
            }
            this.comboBoxTypeSearch.SelectedIndex = 0;
        }

        private void InitCommands()
        {
            this.aboutCmd = new About();
            this.createNewPayment = new CreateNewPayment();
            this.showInfoCollect = new ShowInfoCollect();
            this.prepaidsReportCommand = new CreatePrepaidsReport();
            this.settingsCommand = new SettingsCommand();
            this.sendCustomRoll = new SendCustomRoll();
            this.sendMultipleCustomRolls = new SendMultipleCustomRoll();
            this.showTariffPlan = new ShowTariffPlans();
        }

        private void Initialize()
        {
            using (ConnectionDialog dialog = new ConnectionDialog(10))
            {
                dialog.Show();
                dialog.SetStage(UserStrings.InitMainForm);
                Payment.ServerSide = false;
                this.InitializeComponent();
                this.InitCommands();
                this.mainWndToolStrip = new WndToolStrip(this);
                try
                {
                    this.RefreshVersionInfoShow();
                    dialog.SetStage(UserStrings.FillPayPointsList);
                    PointsContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetPayPoints());
                    PointsContainer.Instance.AfterItemEvent += new EventHandler<ItemEventArgs<PayPoint, Guid>>(this.OnPointEvent);
                    dialog.SetStage(UserStrings.FillClientsList);
                    ClientsContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetClients());
                    ClientsContainer.Instance.AfterItemEvent += new EventHandler<ItemEventArgs<Client, Guid>>(this.OnClientEvent);
                    dialog.SetStage(UserStrings.FillServiceList);
                    ServicesContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetServices());
                    ServicesContainer.Instance.AfterItemEvent += new EventHandler<ItemEventArgs<Service, Guid>>(this.OnServiceEvent);
                    dialog.SetStage(UserStrings.FillServiceTypeList);
                    ServiceTypesContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetServiceTypes());
                    ServiceTypesContainer.Instance.AfterItemEvent += new EventHandler<ItemEventArgs<ServiceType, int>>(this.OnServiceTypeEvent);
                    dialog.SetStage(UserStrings.FillRecipientsList);
                    RecipientsContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetRecipients());
                    RecipientsContainer.Instance.AfterItemEvent += new EventHandler<ItemEventArgs<Recipient, Guid>>(this.OnRecipientEvent);
                    dialog.SetStage(UserStrings.FillAccounts);
                    AccountsContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetAccounts());
                    AccountsContainer.Instance.ResolveItem += delegate (object sender, ResolveItemEventArgs<Account, int> e) {
                        if (e.ItemId == 0)
                        {
                            throw new InvalidOperationException();
                        }
                        e.ResolvedItem = IBP.PaymentProject2.Oper.ServerFasad.Server.GetAccount(e.ItemId);
                    };
                    dialog.SetStage(UserStrings.FillTownsList);
                    TownsContainer.Instance.AddRange(IBP.PaymentProject2.Oper.ServerFasad.Server.GetTowns());
                    TownsContainer.Instance.ResolveItem += delegate (object sender, ResolveItemEventArgs<Town, int> e) {
                        if (e.ItemId == 0)
                        {
                            throw new InvalidOperationException();
                        }
                        e.ResolvedItem = IBP.PaymentProject2.Oper.ServerFasad.Server.GetTown(e.ItemId);
                    };
                    dialog.SetStage(UserStrings.FillOperatorsList);
                    OperatorsController.Initialize();
                }
                catch (SrvException exception)
                {
                    this.exit = true;
                    if (exception.InnerException is SecurityException)
                    {
                        MessageBox.Show(UserStrings.UDontHaveRigths);
                    }
                    else
                    {
                        Exception innerException = exception.InnerException;
                        MessageBox.Show(UserStrings.ServerConnectionLost);
                    }
                }
                dialog.SetStage(UserStrings.InitMenu);
                this.InitMenu();
                this.InitComboBoxies();
            }
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(MainForm));
            this.mainMenuStrip = new MenuStrip();
            this.buttonFilterOk = new Button();
            this.panel1 = new Panel();
            this.label10 = new Label();
            this.comboBoxTypeSearch = new ComboBox();
            this.checkBoxNewWindow = new CheckBox();
            this.statusStrip1 = new StatusStrip();
            this.toolStripStatusServer = new ToolStripStatusLabel();
            this.productVerLabel = new Label();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            base.SuspendLayout();
            manager.ApplyResources(this.mainMenuStrip, "mainMenuStrip");
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.RenderMode = ToolStripRenderMode.Professional;
            manager.ApplyResources(this.buttonFilterOk, "buttonFilterOk");
            this.buttonFilterOk.Name = "buttonFilterOk";
            this.buttonFilterOk.UseVisualStyleBackColor = true;
            this.buttonFilterOk.Click += new EventHandler(this.buttonFilterOk_Click);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.comboBoxTypeSearch);
            this.panel1.Controls.Add(this.checkBoxNewWindow);
            this.panel1.Controls.Add(this.buttonFilterOk);
            manager.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            manager.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            manager.ApplyResources(this.comboBoxTypeSearch, "comboBoxTypeSearch");
            this.comboBoxTypeSearch.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxTypeSearch.FormattingEnabled = true;
            this.comboBoxTypeSearch.Name = "comboBoxTypeSearch";
            this.comboBoxTypeSearch.SelectedIndexChanged += new EventHandler(this.comboBoxTypeSearch_SelectedIndexChanged);
            manager.ApplyResources(this.checkBoxNewWindow, "checkBoxNewWindow");
            this.checkBoxNewWindow.Checked = true;
            this.checkBoxNewWindow.CheckState = CheckState.Checked;
            this.checkBoxNewWindow.Name = "checkBoxNewWindow";
            this.checkBoxNewWindow.UseVisualStyleBackColor = true;
            this.statusStrip1.Items.AddRange(new ToolStripItem[] { this.toolStripStatusServer });
            manager.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            this.toolStripStatusServer.Name = "toolStripStatusServer";
            manager.ApplyResources(this.toolStripStatusServer, "toolStripStatusServer");
            manager.ApplyResources(this.productVerLabel, "productVerLabel");
            this.productVerLabel.BackColor = Color.FromArgb(190, 0xd7, 240);
            this.productVerLabel.ForeColor = Color.Black;
            this.productVerLabel.Name = "productVerLabel";
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = SystemColors.Control;
            base.Controls.Add(this.productVerLabel);
            base.Controls.Add(this.statusStrip1);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this.mainMenuStrip);
            base.IsMdiContainer = true;
            base.MainMenuStrip = this.mainMenuStrip;
            base.Name = "MainForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void InitMenu()
        {
            ToolStripMenuItem item = new ToolStripMenuItem(UserStrings.Commands, null, new ToolStripItem[] { new ToolStripMenuItem(UserStrings.CreatePay, null, this.createNewPayment.EventDelegate), new ToolStripMenuItem(UserStrings.IncassMatching, null, this.showInfoCollect.EventDelegate), new ToolStripMenuItem(UserStrings.ReportOnCardPrepay, null, this.prepaidsReportCommand.EventDelegate), new ToolStripMenuItem(UserStrings.SendReestr, null, new ToolStripItem[] { new ToolStripMenuItem(UserStrings.OneReceiver, null, this.sendCustomRoll.EventDelegate), new ToolStripMenuItem(UserStrings.OnSeveralReceivers, null, this.sendMultipleCustomRolls.EventDelegate) }), new ToolStripMenuItem(UserStrings.TariffPlans, null, this.showTariffPlan.EventDelegate), new ToolStripMenuItem(UserStrings.CreateTransaction, null, this.createTransaction.EventDelegate), new ToolStripMenuItem(UserStrings.RecountSaldo02, null, this.recountAllBalances.EventDelegate), new ToolStripMenuItem(UserStrings.MenuReport, null, this._reportCommand.EventDelegate) });
            this.mainMenuStrip.Items.Add(item);
            item = new ToolStripMenuItem(UserStrings.Filtr, null, new ToolStripItem[] { new ToolStripMenuItem(UserStrings.Set, null, new EventHandler(this.buttonFilterOk_Click)), new ToolStripMenuItem(UserStrings.DropFilter, null, new EventHandler(this.buttonReset_Click)), new ToolStripMenuItem(UserStrings.ShowPaysWithChange, null, this.showPaymentsWithBalance.EventDelegate) });
            this.mainMenuStrip.Items.Add(item);
            ToolStripMenuItem item2 = new ToolStripMenuItem(UserStrings.Receivers);
            List<Recipient> list = new List<Recipient>(RecipientsContainer.Instance.Collection);
            list.Sort(delegate (Recipient a, Recipient b) {
                string format = "{0}_{1}";
                string str2 = string.Format(format, a.Name, a.Id);
                string strB = string.Format(format, b.Name, b.Id);
                return str2.CompareTo(strB);
            });
            foreach (Recipient recipient in list)
            {
                ToolStripMenuItem item3 = new ToolStripMenuItem(recipient.Name, null, new EventHandler(this.ShowInfoRecipient));
                item3.Tag = recipient;
                item2.DropDownItems.Add(item3);
            }
            ToolStripMenuItem item4 = new ToolStripMenuItem(UserStrings.Clients02);
            List<Client> list2 = new List<Client>(ClientsContainer.Instance.Collection);
            list2.Sort(delegate (Client a, Client b) {
                string format = "{0}_{1}";
                string str2 = string.Format(format, a.Name, a.Id);
                string strB = string.Format(format, b.Name, b.Id);
                return str2.CompareTo(strB);
            });
            foreach (Client client in list2)
            {
                ToolStripMenuItem item5 = new ToolStripMenuItem(client.Name, null, new EventHandler(this.ShowInfoClient));
                item5.Tag = client;
                item4.DropDownItems.Add(item5);
            }
            item = new ToolStripMenuItem(UserStrings.Reference, null, new ToolStripItem[] { new ToolStripMenuItem(UserStrings.Help, null, this.helpCommand.EventDelegate, Keys.F1), item2, item4, new ToolStripMenuItem(UserStrings.Settings, null, this.settingsCommand.EventDelegate), new ToolStripMenuItem(UserStrings.AboutProgram, null, this.aboutCmd.EventDelegate) });
            this.mainMenuStrip.Items.Add(item);
        }

        private void OnClientEvent(object sender, ItemEventArgs<Client, Guid> e)
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new EventHandler<ItemEventArgs<Client, Guid>>(this.OnClientEvent), new object[] { sender, e });
            }
            else
            {
                this.UpdateFilters();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (IBP.PaymentProject2.Oper.ServerFasad.Server.State == ConnectionState.Open)
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.Disconnect();
            }
            PaymentsSettings.Default.Save();
            StatisticSettings.Default.Save();
            TransactionsSettings.Default.Save();
            ExtTransactionsSettings.Default.Save();
            AccountsSettings.Default.Save();
            VisibilityFormSettings.Default.Save();
            this.SaveAppLocation();
            base.OnClosing(e);
        }

        private void OnConnectionSettingsChanged(object sender, ConnectionSettingsEventArgs e)
        {
            ServerList list = new ServerList(ConnectionSettings.Default.MemoString);
            list.Add(e.ServerInfo);
            ConnectionSettings.Default.MemoString = list.ToString();
            ConnectionSettings.Default.LastUser = IBP.PaymentProject2.Oper.ServerFasad.Server.CurrentUser.Login;
            ConnectionSettings.Default.Save();
        }

        protected override void OnLoad(EventArgs e)
        {
            if (this.exit)
            {
                base.Close();
            }
            this.RestoreAppLocation();
            base.OnLoad(e);
        }

        private void OnPointEvent(object sender, ItemEventArgs<PayPoint, Guid> e)
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new EventHandler<ItemEventArgs<PayPoint, Guid>>(this.OnPointEvent), new object[] { sender, e });
            }
            else
            {
                this.UpdateFilters();
            }
        }

        private void OnRecipientEvent(object sender, ItemEventArgs<Recipient, Guid> e)
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new EventHandler<ItemEventArgs<Recipient, Guid>>(this.OnRecipientEvent), new object[] { sender, e });
            }
            else
            {
                this.UpdateFilters();
            }
        }

        private void OnServiceEvent(object sender, ItemEventArgs<Service, Guid> e)
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new EventHandler<ItemEventArgs<Service, Guid>>(this.OnServiceEvent), new object[] { sender, e });
            }
            else
            {
                this.UpdateFilters();
            }
        }

        private void OnServiceTypeEvent(object sender, ItemEventArgs<ServiceType, int> e)
        {
            if (base.InvokeRequired)
            {
                base.BeginInvoke(new EventHandler<ItemEventArgs<ServiceType, int>>(this.OnServiceTypeEvent), new object[] { sender, e });
            }
            else
            {
                this.UpdateFilters();
            }
        }

        private void RefreshVersionInfoShow()
        {
            string serverUri = IBP.PaymentProject2.Oper.ServerFasad.Server.ServerUri;
            string str2 = IBP.PaymentProject2.Oper.ServerFasad.Server.GetVersion().ToString();
            str2 = string.Format(UserStrings.ServerEKassir, serverUri, str2);
            this.toolStripStatusServer.Text = str2;
            this.productVerLabel.Text = string.Format(UserStrings.Version, this.AppVersion);
        }

        private void RestoreAppLocation()
        {
            int height = IBP.PaymentProject2.Oper.Properties.Settings.Default.Height;
            int width = IBP.PaymentProject2.Oper.Properties.Settings.Default.Width;
            if (width >= this.MinimumSize.Width)
            {
                base.Width = width;
            }
            if (height >= this.MinimumSize.Height)
            {
                base.Height = height;
            }
            if (IBP.PaymentProject2.Oper.Properties.Settings.Default.HasUserWindowPosition && MainSettings.Default.SaveLocation)
            {
                base.Location = new Point(IBP.PaymentProject2.Oper.Properties.Settings.Default.TopX, IBP.PaymentProject2.Oper.Properties.Settings.Default.TopY);
            }
        }

        private void SaveAppLocation()
        {
            IBP.PaymentProject2.Oper.Properties.Settings.Default.Width = base.Width;
            IBP.PaymentProject2.Oper.Properties.Settings.Default.Height = base.Height;
            if (MainSettings.Default.SaveLocation)
            {
                IBP.PaymentProject2.Oper.Properties.Settings.Default.TopX = base.Location.X;
                IBP.PaymentProject2.Oper.Properties.Settings.Default.TopY = base.Location.Y;
            }
            IBP.PaymentProject2.Oper.Properties.Settings.Default.Save();
        }

        private void ShowForm(EnumTypeInfo<TypeSearch> typeSearch)
        {
            if (typeSearch != null)
            {
                System.Type formType = this.GetFormType(typeSearch.EnumValue);
                if (formType == null)
                {
                    throw new InvalidOperationException(UserStrings.FormNotFound + typeSearch.Name);
                }
                if (this.checkBoxNewWindow.Checked || (base.ActiveMdiChild == null))
                {
                    this.CreateSearchResultForm(formType, this.Filter);
                }
                else if (base.ActiveMdiChild.GetType().Equals(formType))
                {
                    ResultForm activeMdiChild = base.ActiveMdiChild as ResultForm;
                    activeMdiChild.Filter = this.Filter;
                }
                else
                {
                    for (int i = base.MdiChildren.Length - 1; i >= 0; i--)
                    {
                        ResultForm form2 = base.MdiChildren[i] as ResultForm;
                        if ((form2 != null) && form2.GetType().Equals(formType))
                        {
                            form2.Filter = this.Filter;
                            return;
                        }
                    }
                    this.CreateSearchResultForm(formType, this.Filter);
                }
            }
        }

        private void ShowInfoClient(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            Client tag = item.Tag as Client;
            AvailableServiceGuide serviceGuide = null;
            try
            {
                serviceGuide = IBP.PaymentProject2.Oper.ServerFasad.Server.GetAvailableServices(tag.Id);
            }
            catch (SrvException)
            {
                if (MessageBox.Show(UserStrings.AllowedServiceNotLoadedFormat, UserStrings.Error, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    serviceGuide = new AvailableServiceGuide(0, new Guid[0]);
                }
            }
            if (serviceGuide != null)
            {
                using (ClientInfoForm form = new ClientInfoForm(tag, serviceGuide))
                {
                    form.ShowDialog();
                }
            }
        }

        private void ShowInfoRecipient(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            Recipient tag = item.Tag as Recipient;
            using (RecipientInfoForm form = new RecipientInfoForm(tag))
            {
                form.ShowDialog();
            }
        }

        private void UpdateFilters()
        {
            foreach (EnumTypeInfo<TypeSearch> info in EnumTypeInfo<TypeSearch>.GetInfo())
            {
                ((IFilterControl) FilterControl.GetFilter(info.EnumValue)).ReadContainers();
            }
            if (base.ActiveMdiChild is IFormResultSearch)
            {
                IFormResultSearch activeMdiChild = base.ActiveMdiChild as IFormResultSearch;
                this.Filter = activeMdiChild.Filter;
            }
        }

        private string AppVersion
        {
            get
            {
                return base.GetType().Assembly.GetName().Version.ToString();
            }
        }

        private FilterOper Filter
        {
            get
            {
                return ((IFilterControl) this.currentFilter).Filter;
            }
            set
            {
                ((IFilterControl) this.currentFilter).Filter = value;
            }
        }

        private EnumTypeInfo<TypeSearch> SelectedType
        {
            get
            {
                return (this.comboBoxTypeSearch.SelectedItem as EnumTypeInfo<TypeSearch>);
            }
            set
            {
                this.comboBoxTypeSearch.SelectedItem = value;
            }
        }
    }
}

