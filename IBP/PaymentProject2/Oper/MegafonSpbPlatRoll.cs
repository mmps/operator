﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.IO;

    [Roll(GatewayType.Megafon_spb)]
    internal class MegafonSpbPlatRoll : Roll
    {
        public override Stream CreateRoll(Payment[] payments)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            decimal num = 0M;
            foreach (Payment payment in payments)
            {
                writer.WriteLine(payment.Account + " " + payment.Value.ToString("n"));
                num += payment.Value;
            }
            writer.Write(payments.Length.ToString() + ":" + num.ToString("n"));
            writer.Flush();
            if (stream.CanSeek)
            {
                stream.Seek(0L, SeekOrigin.Begin);
            }
            return stream;
        }
    }
}

