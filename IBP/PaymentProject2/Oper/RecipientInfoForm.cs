﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class RecipientInfoForm : Form
    {
        private Button buttonOk;
        private ColumnHeader columnHeaderParam;
        private ColumnHeader columnHeaderValue;
        private IContainer components;
        private GroupBox groupBox1;
        private Label label1;
        private ListViewEdit listViewRecipientInfo;
        private Recipient recipient;
        private TextBox textBoxDopInfo;

        public RecipientInfoForm(Recipient recipient)
        {
            this.InitializeComponent();
            this.recipient = recipient;
        }

        private void AddParam(string param, string value)
        {
            ListViewItem item = new ListViewItem();
            item.Text = param;
            item.SubItems.Add(value);
            this.listViewRecipientInfo.Items.Add(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitDate()
        {
            this.AddParam(UserStrings.Name, this.recipient.Name);
            this.AddParam(UserStrings.Id, this.recipient.Id.ToString());
            this.AddParam(UserStrings.Account, this.recipient.Account.Name);
            this.AddParam(UserStrings.Award, this.recipient.Fee.ToString());
            this.AddParam(UserStrings.Description, this.recipient.Destination);
            this.AddParam(UserStrings.ReceiverAddress, this.recipient.Requisites.PostAddress);
            this.AddParam(UserStrings.Gateway, this.recipient.GatewayType.ToString());
            this.AddParam(UserStrings.DateOfShowing02, this.recipient.InputDate.ToString());
            this.textBoxDopInfo.Text = this.recipient.Info;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(RecipientInfoForm));
            this.groupBox1 = new GroupBox();
            this.label1 = new Label();
            this.textBoxDopInfo = new TextBox();
            this.listViewRecipientInfo = new ListViewEdit();
            this.columnHeaderParam = new ColumnHeader();
            this.columnHeaderValue = new ColumnHeader();
            this.buttonOk = new Button();
            this.groupBox1.SuspendLayout();
            base.SuspendLayout();
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxDopInfo);
            this.groupBox1.Controls.Add(this.listViewRecipientInfo);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            manager.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.textBoxDopInfo.BackColor = Color.White;
            manager.ApplyResources(this.textBoxDopInfo, "textBoxDopInfo");
            this.textBoxDopInfo.Name = "textBoxDopInfo";
            this.textBoxDopInfo.ReadOnly = true;
            this.listViewRecipientInfo.AllowSort = false;
            this.listViewRecipientInfo.BindType = null;
            this.listViewRecipientInfo.Columns.AddRange(new ColumnHeader[] { this.columnHeaderParam, this.columnHeaderValue });
            this.listViewRecipientInfo.ColumnsCollection = null;
            manager.ApplyResources(this.listViewRecipientInfo, "listViewRecipientInfo");
            this.listViewRecipientInfo.FullRowSelect = true;
            this.listViewRecipientInfo.GridLines = true;
            this.listViewRecipientInfo.Name = "listViewRecipientInfo";
            this.listViewRecipientInfo.ReadOnly = true;
            this.listViewRecipientInfo.UseCompatibleStateImageBehavior = false;
            this.listViewRecipientInfo.View = View.Details;
            manager.ApplyResources(this.columnHeaderParam, "columnHeaderParam");
            manager.ApplyResources(this.columnHeaderValue, "columnHeaderValue");
            this.buttonOk.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.buttonOk);
            base.Controls.Add(this.groupBox1);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "RecipientInfoForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            base.ResumeLayout(false);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Text = UserStrings.Receiver + this.recipient.Name;
            this.InitDate();
        }
    }
}

