﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.IO;
    using System.Text;

    [Roll(GatewayType.NoGateway)]
    internal class Roll_Default : Roll
    {
        public override Stream CreateRoll(Payment[] payments)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream, Encoding.Default);
            writer.WriteLine(UserStrings.Service + ServicesContainer.Instance[payments[0].ServiceId].Name);
            decimal num = 0M;
            foreach (Payment payment in payments)
            {
                writer.Write(payment.Account + " " + payment.Value.ToString("n") + " ");
                writer.WriteLine(payment.InputDate);
                num += payment.Value;
            }
            int length = payments.Length;
            writer.Write(string.Format(UserStrings.AllPays, length.ToString(), num.ToString("n")));
            writer.Flush();
            if (stream.CanSeek)
            {
                stream.Seek(0L, SeekOrigin.Begin);
            }
            return stream;
        }
    }
}

