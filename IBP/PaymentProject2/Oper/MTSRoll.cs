﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text;

    [Roll(GatewayType.MTS)]
    internal class MTSRoll : Roll
    {
        private static readonly string AC = "aut_cod";
        private static readonly string BANKBOOK = "bank_book";
        private static readonly string OPER_DATE = "oper_date";
        private static readonly string PROVIDER_CODE = "provider_cod";
        private static readonly string TIDMTS = "tid_mts";
        private static readonly string TIDSERV = "tid_serv";

        protected void CheckParametrs(Recipient _recipient)
        {
            StringBuilder builder = new StringBuilder();
            if (!_recipient.Contains(AC))
            {
                builder.Append(UserStrings.InSettingsNoExist + AC);
            }
            if (builder.Length > 0)
            {
                throw new RollException(builder.ToString());
            }
        }

        public override Stream CreateRoll(Payment[] payments)
        {
            if (payments.Length == 0)
            {
                throw new RollException(UserStrings.NoChoosenPays);
            }
            Recipient recipient = RecipientsContainer.Instance[payments[0].RecipientId];
            this.CheckParametrs(recipient);
            int num = 0;
            decimal num2 = 0M;
            decimal num3 = 0M;
            string str = payments[0].StateTime.ToString("yyyyMMdd");
            string str2 = str + "000000000";
            string str3 = str + "235959000";
            base.ext = "asc2";
            base.filename = "organisation" + str + "_810";
            StringBuilder builder = new StringBuilder();
            string str4 = "";
            foreach (Payment payment in payments)
            {
                DateTime time;
                PayPoint point = PointsContainer.Instance[payment.PointId];
                Client client = ClientsContainer.Instance[point.ClientId];
                if ((((payment[TIDMTS] != null) && (payment[TIDSERV] != null)) && ((payment[PROVIDER_CODE] != null) && (payment[BANKBOOK] != null))) && DateTime.TryParse(payment[OPER_DATE], out time))
                {
                    string str5 = time.ToString("yyyyMMddHHmmss") + "000";
                    decimal num4 = payment.Value - Math.Round((decimal) ((recipient.Fee / 100M) * payment.Value), 2);
                    str4 = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}", new object[] { payment[TIDMTS].PadLeft(20, '0'), str5, payment[TIDSERV].PadLeft(20, '0'), client.Serial.ToString().PadLeft(11, '0') + "." + Math.Abs(point.Id.GetHashCode()).ToString("X").PadRight(8, ' '), payment.Account.PadRight(15, ' '), payment[PROVIDER_CODE].PadRight(11, ' '), payment[BANKBOOK].PadRight(11, ' '), payment.Value.ToString("f", NumberFormatInfo.InvariantInfo).PadLeft(0x15, '0'), "810", "000001", num4.ToString("f", NumberFormatInfo.InvariantInfo).PadLeft(0x15, '0') });
                    num++;
                    num2 += payment.Value;
                    num3 += num4;
                    builder.AppendLine(str4);
                }
            }
            string s = string.Format("{0}{1}{2}{3}{4}{5}{6}", new object[] { recipient[AC].ToString().PadLeft(11, '0'), str2, str3, num.ToString().PadLeft(11, '0'), "810", num2.ToString("f", NumberFormatInfo.InvariantInfo).PadLeft(0x15, '0'), num3.ToString("f", NumberFormatInfo.InvariantInfo).PadLeft(0x15, '0') }) + Environment.NewLine + builder.ToString();
            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(Encoding.GetEncoding(0x4e3).GetBytes(s));
            }
            catch (ArgumentException exception)
            {
                throw new RollException(exception.Message);
            }
            return stream;
        }
    }
}

