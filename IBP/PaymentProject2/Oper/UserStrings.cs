﻿namespace IBP.PaymentProject2.Oper
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Resources;
    using System.Runtime.CompilerServices;

    [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
    internal class UserStrings
    {
        private static CultureInfo resourceCulture;
        private static System.Resources.ResourceManager resourceMan;

        internal UserStrings()
        {
        }

        internal static string About
        {
            get
            {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }

        internal static string AboutProgram
        {
            get
            {
                return ResourceManager.GetString("AboutProgram", resourceCulture);
            }
        }

        internal static string Accepted
        {
            get
            {
                return ResourceManager.GetString("Accepted", resourceCulture);
            }
        }

        internal static string Account
        {
            get
            {
                return ResourceManager.GetString("Account", resourceCulture);
            }
        }

        internal static string Accounts
        {
            get
            {
                return ResourceManager.GetString("Accounts", resourceCulture);
            }
        }

        internal static string Action
        {
            get
            {
                return ResourceManager.GetString("Action", resourceCulture);
            }
        }

        internal static string Active
        {
            get
            {
                return ResourceManager.GetString("Active", resourceCulture);
            }
        }

        internal static string Acts
        {
            get
            {
                return ResourceManager.GetString("Acts", resourceCulture);
            }
        }

        internal static string Add
        {
            get
            {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }

        internal static string AddIfExist
        {
            get
            {
                return ResourceManager.GetString("AddIfExist", resourceCulture);
            }
        }

        internal static string AddSubDealer
        {
            get
            {
                return ResourceManager.GetString("AddSubDealer", resourceCulture);
            }
        }

        internal static string AllowedServiceNotLoadedFormat
        {
            get
            {
                return ResourceManager.GetString("AllowedServiceNotLoadedFormat", resourceCulture);
            }
        }

        internal static string AllPays
        {
            get
            {
                return ResourceManager.GetString("AllPays", resourceCulture);
            }
        }

        internal static string AllQtyOfPages
        {
            get
            {
                return ResourceManager.GetString("AllQtyOfPages", resourceCulture);
            }
        }

        internal static string ApplyPlan
        {
            get
            {
                return ResourceManager.GetString("ApplyPlan", resourceCulture);
            }
        }

        internal static string AreUSure
        {
            get
            {
                return ResourceManager.GetString("AreUSure", resourceCulture);
            }
        }

        internal static string AreYouReallyWantDeleteAct
        {
            get
            {
                return ResourceManager.GetString("AreYouReallyWantDeleteAct", resourceCulture);
            }
        }

        internal static string AskOnReturn
        {
            get
            {
                return ResourceManager.GetString("AskOnReturn", resourceCulture);
            }
        }

        internal static string AskOnReturnNotTaken
        {
            get
            {
                return ResourceManager.GetString("AskOnReturnNotTaken", resourceCulture);
            }
        }

        internal static string AssignTariffPlanError
        {
            get
            {
                return ResourceManager.GetString("AssignTariffPlanError", resourceCulture);
            }
        }

        internal static string Attention
        {
            get
            {
                return ResourceManager.GetString("Attention", resourceCulture);
            }
        }

        internal static string Award
        {
            get
            {
                return ResourceManager.GetString("Award", resourceCulture);
            }
        }

        internal static string Back
        {
            get
            {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }

        internal static string Balance
        {
            get
            {
                return ResourceManager.GetString("Balance", resourceCulture);
            }
        }

        internal static string Basic
        {
            get
            {
                return ResourceManager.GetString("Basic", resourceCulture);
            }
        }

        internal static string Blocked
        {
            get
            {
                return ResourceManager.GetString("Blocked", resourceCulture);
            }
        }

        internal static string BlockedCheck
        {
            get
            {
                return ResourceManager.GetString("BlockedCheck", resourceCulture);
            }
        }

        internal static string ByPageWidth
        {
            get
            {
                return ResourceManager.GetString("ByPageWidth", resourceCulture);
            }
        }

        internal static string CantAssignTariffPlan
        {
            get
            {
                return ResourceManager.GetString("CantAssignTariffPlan", resourceCulture);
            }
        }

        internal static string CantChangeTariffPlan
        {
            get
            {
                return ResourceManager.GetString("CantChangeTariffPlan", resourceCulture);
            }
        }

        internal static string CantCreateTariffPlanThereIsNoServices
        {
            get
            {
                return ResourceManager.GetString("CantCreateTariffPlanThereIsNoServices", resourceCulture);
            }
        }

        internal static string CantGetClientAllowedServices
        {
            get
            {
                return ResourceManager.GetString("CantGetClientAllowedServices", resourceCulture);
            }
        }

        internal static string CantGetData
        {
            get
            {
                return ResourceManager.GetString("CantGetData", resourceCulture);
            }
        }

        internal static string CantGetList
        {
            get
            {
                return ResourceManager.GetString("CantGetList", resourceCulture);
            }
        }

        internal static string CantGetPrintersList
        {
            get
            {
                return ResourceManager.GetString("CantGetPrintersList", resourceCulture);
            }
        }

        internal static string CantGetSaldo
        {
            get
            {
                return ResourceManager.GetString("CantGetSaldo", resourceCulture);
            }
        }

        internal static string CantOpenFile
        {
            get
            {
                return ResourceManager.GetString("CantOpenFile", resourceCulture);
            }
        }

        internal static string CantRedirectPaymentFormat
        {
            get
            {
                return ResourceManager.GetString("CantRedirectPaymentFormat", resourceCulture);
            }
        }

        internal static string CantReplaceDeletedTariffPlan
        {
            get
            {
                return ResourceManager.GetString("CantReplaceDeletedTariffPlan", resourceCulture);
            }
        }

        internal static string CantSetTariffPlanDealerHasntDeal
        {
            get
            {
                return ResourceManager.GetString("CantSetTariffPlanDealerHasntDeal", resourceCulture);
            }
        }

        internal static string CardOperation
        {
            get
            {
                return ResourceManager.GetString("CardOperation", resourceCulture);
            }
        }

        internal static string CardOperation02
        {
            get
            {
                return ResourceManager.GetString("CardOperation02", resourceCulture);
            }
        }

        internal static string Change
        {
            get
            {
                return ResourceManager.GetString("Change", resourceCulture);
            }
        }

        internal static string ChangeAcc
        {
            get
            {
                return ResourceManager.GetString("ChangeAcc", resourceCulture);
            }
        }

        internal static string ChangeOfTariffPlan
        {
            get
            {
                return ResourceManager.GetString("ChangeOfTariffPlan", resourceCulture);
            }
        }

        internal static string ChangeRights
        {
            get
            {
                return ResourceManager.GetString("ChangeRights", resourceCulture);
            }
        }

        internal static string Changes
        {
            get
            {
                return ResourceManager.GetString("Changes", resourceCulture);
            }
        }

        internal static string ChangeTariffPlan02
        {
            get
            {
                return ResourceManager.GetString("ChangeTariffPlan02", resourceCulture);
            }
        }

        internal static string ChangeUser
        {
            get
            {
                return ResourceManager.GetString("ChangeUser", resourceCulture);
            }
        }

        internal static string CheckHaveNoChange
        {
            get
            {
                return ResourceManager.GetString("CheckHaveNoChange", resourceCulture);
            }
        }

        internal static string CheckId
        {
            get
            {
                return ResourceManager.GetString("CheckId", resourceCulture);
            }
        }

        internal static string CheckIncass
        {
            get
            {
                return ResourceManager.GetString("CheckIncass", resourceCulture);
            }
        }

        internal static string CheckNumber
        {
            get
            {
                return ResourceManager.GetString("CheckNumber", resourceCulture);
            }
        }

        internal static string CheckSaldo
        {
            get
            {
                return ResourceManager.GetString("CheckSaldo", resourceCulture);
            }
        }

        internal static string CheckTrans
        {
            get
            {
                return ResourceManager.GetString("CheckTrans", resourceCulture);
            }
        }

        internal static string ChooseAll
        {
            get
            {
                return ResourceManager.GetString("ChooseAll", resourceCulture);
            }
        }

        internal static string ChooseDate
        {
            get
            {
                return ResourceManager.GetString("ChooseDate", resourceCulture);
            }
        }

        internal static string ChooseDate02
        {
            get
            {
                return ResourceManager.GetString("ChooseDate02", resourceCulture);
            }
        }

        internal static string ChooseExportFile
        {
            get
            {
                return ResourceManager.GetString("ChooseExportFile", resourceCulture);
            }
        }

        internal static string ChooseFile
        {
            get
            {
                return ResourceManager.GetString("ChooseFile", resourceCulture);
            }
        }

        internal static string ChooseOneAtLeast
        {
            get
            {
                return ResourceManager.GetString("ChooseOneAtLeast", resourceCulture);
            }
        }

        internal static string ChooseOneCondition
        {
            get
            {
                return ResourceManager.GetString("ChooseOneCondition", resourceCulture);
            }
        }

        internal static string ChoosePeriod
        {
            get
            {
                return ResourceManager.GetString("ChoosePeriod", resourceCulture);
            }
        }

        internal static string ChoosePoS
        {
            get
            {
                return ResourceManager.GetString("ChoosePoS", resourceCulture);
            }
        }

        internal static string ChoosePrintSettings
        {
            get
            {
                return ResourceManager.GetString("ChoosePrintSettings", resourceCulture);
            }
        }

        internal static string ChooseReceiver
        {
            get
            {
                return ResourceManager.GetString("ChooseReceiver", resourceCulture);
            }
        }

        internal static string ChooseReceiver02
        {
            get
            {
                return ResourceManager.GetString("ChooseReceiver02", resourceCulture);
            }
        }

        internal static string ChooseRecepient
        {
            get
            {
                return ResourceManager.GetString("ChooseRecepient", resourceCulture);
            }
        }

        internal static string ChooseTmpl
        {
            get
            {
                return ResourceManager.GetString("ChooseTmpl", resourceCulture);
            }
        }

        internal static string ChooseValue
        {
            get
            {
                return ResourceManager.GetString("ChooseValue", resourceCulture);
            }
        }

        internal static string Clauses
        {
            get
            {
                return ResourceManager.GetString("Clauses", resourceCulture);
            }
        }

        internal static string Client
        {
            get
            {
                return ResourceManager.GetString("Client", resourceCulture);
            }
        }

        internal static string Client03
        {
            get
            {
                return ResourceManager.GetString("Client03", resourceCulture);
            }
        }

        internal static string ClientActFormatString
        {
            get
            {
                return ResourceManager.GetString("ClientActFormatString", resourceCulture);
            }
        }

        internal static string ClientCall
        {
            get
            {
                return ResourceManager.GetString("ClientCall", resourceCulture);
            }
        }

        internal static string ClientReestr
        {
            get
            {
                return ResourceManager.GetString("ClientReestr", resourceCulture);
            }
        }

        internal static string ClientReviseActParams
        {
            get
            {
                return ResourceManager.GetString("ClientReviseActParams", resourceCulture);
            }
        }

        internal static string ClientReviseActs
        {
            get
            {
                return ResourceManager.GetString("ClientReviseActs", resourceCulture);
            }
        }

        internal static string Clients
        {
            get
            {
                return ResourceManager.GetString("Clients", resourceCulture);
            }
        }

        internal static string Clients02
        {
            get
            {
                return ResourceManager.GetString("Clients02", resourceCulture);
            }
        }

        internal static string ClientStatus
        {
            get
            {
                return ResourceManager.GetString("ClientStatus", resourceCulture);
            }
        }

        internal static string Close
        {
            get
            {
                return ResourceManager.GetString("Close", resourceCulture);
            }
        }

        internal static string CoincidenceSearch
        {
            get
            {
                return ResourceManager.GetString("CoincidenceSearch", resourceCulture);
            }
        }

        internal static string ColorAcceptedPays
        {
            get
            {
                return ResourceManager.GetString("ColorAcceptedPays", resourceCulture);
            }
        }

        internal static string ColorFinishedPays
        {
            get
            {
                return ResourceManager.GetString("ColorFinishedPays", resourceCulture);
            }
        }

        internal static string ColorOfNewPays
        {
            get
            {
                return ResourceManager.GetString("ColorOfNewPays", resourceCulture);
            }
        }

        internal static string ColorOnStatus
        {
            get
            {
                return ResourceManager.GetString("ColorOnStatus", resourceCulture);
            }
        }

        internal static string ColorPay
        {
            get
            {
                return ResourceManager.GetString("ColorPay", resourceCulture);
            }
        }

        internal static string ColorPayInProcessing
        {
            get
            {
                return ResourceManager.GetString("ColorPayInProcessing", resourceCulture);
            }
        }

        internal static string ColorPaysWithChildren
        {
            get
            {
                return ResourceManager.GetString("ColorPaysWithChildren", resourceCulture);
            }
        }

        internal static string ColorPaysWithParents
        {
            get
            {
                return ResourceManager.GetString("ColorPaysWithParents", resourceCulture);
            }
        }

        internal static string ColorRejectedPays
        {
            get
            {
                return ResourceManager.GetString("ColorRejectedPays", resourceCulture);
            }
        }

        internal static string ColorSetAsidePays
        {
            get
            {
                return ResourceManager.GetString("ColorSetAsidePays", resourceCulture);
            }
        }

        internal static string ColorsOnProperties
        {
            get
            {
                return ResourceManager.GetString("ColorsOnProperties", resourceCulture);
            }
        }

        internal static string Commands
        {
            get
            {
                return ResourceManager.GetString("Commands", resourceCulture);
            }
        }

        internal static string CommandWasntSet
        {
            get
            {
                return ResourceManager.GetString("CommandWasntSet", resourceCulture);
            }
        }

        internal static string Comments
        {
            get
            {
                return ResourceManager.GetString("Comments", resourceCulture);
            }
        }

        internal static string CommissionLimitation
        {
            get
            {
                return ResourceManager.GetString("CommissionLimitation", resourceCulture);
            }
        }

        internal static string Common
        {
            get
            {
                return ResourceManager.GetString("Common", resourceCulture);
            }
        }

        internal static string Connection
        {
            get
            {
                return ResourceManager.GetString("Connection", resourceCulture);
            }
        }

        internal static string CopyToBuffer
        {
            get
            {
                return ResourceManager.GetString("CopyToBuffer", resourceCulture);
            }
        }

        internal static string CopyToBuffer03
        {
            get
            {
                return ResourceManager.GetString("CopyToBuffer03", resourceCulture);
            }
        }

        internal static string CreatePay
        {
            get
            {
                return ResourceManager.GetString("CreatePay", resourceCulture);
            }
        }

        internal static string CreateReviseAct
        {
            get
            {
                return ResourceManager.GetString("CreateReviseAct", resourceCulture);
            }
        }

        internal static string CreateTrans
        {
            get
            {
                return ResourceManager.GetString("CreateTrans", resourceCulture);
            }
        }

        internal static string CreateTransaction
        {
            get
            {
                return ResourceManager.GetString("CreateTransaction", resourceCulture);
            }
        }

        internal static string CreditDebet
        {
            get
            {
                return ResourceManager.GetString("CreditDebet", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        internal static string CurrentLimitation
        {
            get
            {
                return ResourceManager.GetString("CurrentLimitation", resourceCulture);
            }
        }

        internal static string CurrentPage
        {
            get
            {
                return ResourceManager.GetString("CurrentPage", resourceCulture);
            }
        }

        internal static string Date
        {
            get
            {
                return ResourceManager.GetString("Date", resourceCulture);
            }
        }

        internal static string DateOfShowing
        {
            get
            {
                return ResourceManager.GetString("DateOfShowing", resourceCulture);
            }
        }

        internal static string DateOfShowing02
        {
            get
            {
                return ResourceManager.GetString("DateOfShowing02", resourceCulture);
            }
        }

        internal static string DealerHasntDeal
        {
            get
            {
                return ResourceManager.GetString("DealerHasntDeal", resourceCulture);
            }
        }

        internal static string DealerHasntDealDoYouWantToChangeActParams
        {
            get
            {
                return ResourceManager.GetString("DealerHasntDealDoYouWantToChangeActParams", resourceCulture);
            }
        }

        internal static string DeleteAct
        {
            get
            {
                return ResourceManager.GetString("DeleteAct", resourceCulture);
            }
        }

        internal static string DeleteClientReviseAct
        {
            get
            {
                return ResourceManager.GetString("DeleteClientReviseAct", resourceCulture);
            }
        }

        internal static string DeleteTariffPlanError
        {
            get
            {
                return ResourceManager.GetString("DeleteTariffPlanError", resourceCulture);
            }
        }

        internal static string Deletion
        {
            get
            {
                return ResourceManager.GetString("Deletion", resourceCulture);
            }
        }

        internal static string DelTariffPlan
        {
            get
            {
                return ResourceManager.GetString("DelTariffPlan", resourceCulture);
            }
        }

        internal static string DelTarifPlan
        {
            get
            {
                return ResourceManager.GetString("DelTarifPlan", resourceCulture);
            }
        }

        internal static string DeltaTelecom
        {
            get
            {
                return ResourceManager.GetString("DeltaTelecom", resourceCulture);
            }
        }

        internal static string Description
        {
            get
            {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }

        internal static string Description02
        {
            get
            {
                return ResourceManager.GetString("Description02", resourceCulture);
            }
        }

        internal static string DialerReestr
        {
            get
            {
                return ResourceManager.GetString("DialerReestr", resourceCulture);
            }
        }

        internal static string DocStructure
        {
            get
            {
                return ResourceManager.GetString("DocStructure", resourceCulture);
            }
        }

        internal static string DoIgnoreOthersMessage
        {
            get
            {
                return ResourceManager.GetString("DoIgnoreOthersMessage", resourceCulture);
            }
        }

        internal static string DropFilter
        {
            get
            {
                return ResourceManager.GetString("DropFilter", resourceCulture);
            }
        }

        internal static string EditNull
        {
            get
            {
                return ResourceManager.GetString("EditNull", resourceCulture);
            }
        }

        internal static string EditTransNotSupported
        {
            get
            {
                return ResourceManager.GetString("EditTransNotSupported", resourceCulture);
            }
        }

        internal static string Empty
        {
            get
            {
                return ResourceManager.GetString("Empty", resourceCulture);
            }
        }

        internal static string Err
        {
            get
            {
                return ResourceManager.GetString("Err", resourceCulture);
            }
        }

        internal static string ErrCopingBuffer
        {
            get
            {
                return ResourceManager.GetString("ErrCopingBuffer", resourceCulture);
            }
        }

        internal static string Error
        {
            get
            {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }

        internal static string ErrorCheckPayment
        {
            get
            {
                return ResourceManager.GetString("ErrorCheckPayment", resourceCulture);
            }
        }

        internal static string ErrorOnGetTariffPlan
        {
            get
            {
                return ResourceManager.GetString("ErrorOnGetTariffPlan", resourceCulture);
            }
        }

        internal static string ErrorOnSaveTariffPlan
        {
            get
            {
                return ResourceManager.GetString("ErrorOnSaveTariffPlan", resourceCulture);
            }
        }

        internal static string ErrorOnTariffPlanAccessLost
        {
            get
            {
                return ResourceManager.GetString("ErrorOnTariffPlanAccessLost", resourceCulture);
            }
        }

        internal static string ErrorReadingReviseRoll
        {
            get
            {
                return ResourceManager.GetString("ErrorReadingReviseRoll", resourceCulture);
            }
        }

        internal static string ErrorReadingTemplate
        {
            get
            {
                return ResourceManager.GetString("ErrorReadingTemplate", resourceCulture);
            }
        }

        internal static string Execution
        {
            get
            {
                return ResourceManager.GetString("Execution", resourceCulture);
            }
        }

        internal static string Exit
        {
            get
            {
                return ResourceManager.GetString("Exit", resourceCulture);
            }
        }

        internal static string Export
        {
            get
            {
                return ResourceManager.GetString("Export", resourceCulture);
            }
        }

        internal static string ExternalNumber
        {
            get
            {
                return ResourceManager.GetString("ExternalNumber", resourceCulture);
            }
        }

        internal static string FileNotFound
        {
            get
            {
                return ResourceManager.GetString("FileNotFound", resourceCulture);
            }
        }

        internal static string FileOccupied
        {
            get
            {
                return ResourceManager.GetString("FileOccupied", resourceCulture);
            }
        }

        internal static string FillAccounts
        {
            get
            {
                return ResourceManager.GetString("FillAccounts", resourceCulture);
            }
        }

        internal static string FillClientsList
        {
            get
            {
                return ResourceManager.GetString("FillClientsList", resourceCulture);
            }
        }

        internal static string FillOperatorsList
        {
            get
            {
                return ResourceManager.GetString("FillOperatorsList", resourceCulture);
            }
        }

        internal static string FillPayPointsList
        {
            get
            {
                return ResourceManager.GetString("FillPayPointsList", resourceCulture);
            }
        }

        internal static string FillRecipientsList
        {
            get
            {
                return ResourceManager.GetString("FillRecipientsList", resourceCulture);
            }
        }

        internal static string FillServiceList
        {
            get
            {
                return ResourceManager.GetString("FillServiceList", resourceCulture);
            }
        }

        internal static string FillServiceTypeList
        {
            get
            {
                return ResourceManager.GetString("FillServiceTypeList", resourceCulture);
            }
        }

        internal static string FillTownsList
        {
            get
            {
                return ResourceManager.GetString("FillTownsList", resourceCulture);
            }
        }

        internal static string Filter_byClients
        {
            get
            {
                return ResourceManager.GetString("Filter_byClients", resourceCulture);
            }
        }

        internal static string Filter_byDays
        {
            get
            {
                return ResourceManager.GetString("Filter_byDays", resourceCulture);
            }
        }

        internal static string Filter_byDaysofPeriod
        {
            get
            {
                return ResourceManager.GetString("Filter_byDaysofPeriod", resourceCulture);
            }
        }

        internal static string Filter_byDaysofWorkState
        {
            get
            {
                return ResourceManager.GetString("Filter_byDaysofWorkState", resourceCulture);
            }
        }

        internal static string Filter_byEmpty
        {
            get
            {
                return ResourceManager.GetString("Filter_byEmpty", resourceCulture);
            }
        }

        internal static string Filter_byMonth
        {
            get
            {
                return ResourceManager.GetString("Filter_byMonth", resourceCulture);
            }
        }

        internal static string Filter_byPoints
        {
            get
            {
                return ResourceManager.GetString("Filter_byPoints", resourceCulture);
            }
        }

        internal static string Filter_byRecipient
        {
            get
            {
                return ResourceManager.GetString("Filter_byRecipient", resourceCulture);
            }
        }

        internal static string Filter_byServices
        {
            get
            {
                return ResourceManager.GetString("Filter_byServices", resourceCulture);
            }
        }

        internal static string Filtered
        {
            get
            {
                return ResourceManager.GetString("Filtered", resourceCulture);
            }
        }

        internal static string Filtr
        {
            get
            {
                return ResourceManager.GetString("Filtr", resourceCulture);
            }
        }

        internal static string Find
        {
            get
            {
                return ResourceManager.GetString("Find", resourceCulture);
            }
        }

        internal static string FindCoincidence
        {
            get
            {
                return ResourceManager.GetString("FindCoincidence", resourceCulture);
            }
        }

        internal static string Finished
        {
            get
            {
                return ResourceManager.GetString("Finished", resourceCulture);
            }
        }

        internal static string Firm
        {
            get
            {
                return ResourceManager.GetString("Firm", resourceCulture);
            }
        }

        internal static string FormHaveMoreAttrib
        {
            get
            {
                return ResourceManager.GetString("FormHaveMoreAttrib", resourceCulture);
            }
        }

        internal static string FormNotFound
        {
            get
            {
                return ResourceManager.GetString("FormNotFound", resourceCulture);
            }
        }

        internal static string From
        {
            get
            {
                return ResourceManager.GetString("From", resourceCulture);
            }
        }

        internal static string Further
        {
            get
            {
                return ResourceManager.GetString("Further", resourceCulture);
            }
        }

        internal static string Gateway
        {
            get
            {
                return ResourceManager.GetString("Gateway", resourceCulture);
            }
        }

        internal static string GetTariffPlanHistoryError
        {
            get
            {
                return ResourceManager.GetString("GetTariffPlanHistoryError", resourceCulture);
            }
        }

        internal static string GetUserRolesErrorFormat
        {
            get
            {
                return ResourceManager.GetString("GetUserRolesErrorFormat", resourceCulture);
            }
        }

        internal static string GoFirstPage
        {
            get
            {
                return ResourceManager.GetString("GoFirstPage", resourceCulture);
            }
        }

        internal static string GoNextPage
        {
            get
            {
                return ResourceManager.GetString("GoNextPage", resourceCulture);
            }
        }

        internal static string Graphic
        {
            get
            {
                return ResourceManager.GetString("Graphic", resourceCulture);
            }
        }

        internal static string GroupDeletion
        {
            get
            {
                return ResourceManager.GetString("GroupDeletion", resourceCulture);
            }
        }

        internal static string GUP
        {
            get
            {
                return ResourceManager.GetString("GUP", resourceCulture);
            }
        }

        internal static string HaveChild
        {
            get
            {
                return ResourceManager.GetString("HaveChild", resourceCulture);
            }
        }

        internal static string HaveParent
        {
            get
            {
                return ResourceManager.GetString("HaveParent", resourceCulture);
            }
        }

        internal static string Help
        {
            get
            {
                return ResourceManager.GetString("Help", resourceCulture);
            }
        }

        internal static string HistoryOfChange
        {
            get
            {
                return ResourceManager.GetString("HistoryOfChange", resourceCulture);
            }
        }

        internal static string HistoryTariffPlan
        {
            get
            {
                return ResourceManager.GetString("HistoryTariffPlan", resourceCulture);
            }
        }

        internal static string Id
        {
            get
            {
                return ResourceManager.GetString("Id", resourceCulture);
            }
        }

        internal static string IncassMatching
        {
            get
            {
                return ResourceManager.GetString("IncassMatching", resourceCulture);
            }
        }

        internal static string InitMainForm
        {
            get
            {
                return ResourceManager.GetString("InitMainForm", resourceCulture);
            }
        }

        internal static string InitMenu
        {
            get
            {
                return ResourceManager.GetString("InitMenu", resourceCulture);
            }
        }

        internal static string InProcessing
        {
            get
            {
                return ResourceManager.GetString("InProcessing", resourceCulture);
            }
        }

        internal static string InputSaldo
        {
            get
            {
                return ResourceManager.GetString("InputSaldo", resourceCulture);
            }
        }

        internal static string InSettingsNoExist
        {
            get
            {
                return ResourceManager.GetString("InSettingsNoExist", resourceCulture);
            }
        }

        internal static string Kassa
        {
            get
            {
                return ResourceManager.GetString("Kassa", resourceCulture);
            }
        }

        internal static string LastPage
        {
            get
            {
                return ResourceManager.GetString("LastPage", resourceCulture);
            }
        }

        internal static string LeftEmpty
        {
            get
            {
                return ResourceManager.GetString("LeftEmpty", resourceCulture);
            }
        }

        internal static string Lie
        {
            get
            {
                return ResourceManager.GetString("Lie", resourceCulture);
            }
        }

        internal static string MainWindow
        {
            get
            {
                return ResourceManager.GetString("MainWindow", resourceCulture);
            }
        }

        internal static string Mark
        {
            get
            {
                return ResourceManager.GetString("Mark", resourceCulture);
            }
        }

        internal static string Megaphone
        {
            get
            {
                return ResourceManager.GetString("Megaphone", resourceCulture);
            }
        }

        internal static string MenuReport
        {
            get
            {
                return ResourceManager.GetString("MenuReport", resourceCulture);
            }
        }

        internal static string MethodNotDefined
        {
            get
            {
                return ResourceManager.GetString("MethodNotDefined", resourceCulture);
            }
        }

        internal static string Month
        {
            get
            {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }

        internal static string MTC
        {
            get
            {
                return ResourceManager.GetString("MTC", resourceCulture);
            }
        }

        internal static string Name
        {
            get
            {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }

        internal static string New
        {
            get
            {
                return ResourceManager.GetString("New", resourceCulture);
            }
        }

        internal static string NewCopyPlan
        {
            get
            {
                return ResourceManager.GetString("NewCopyPlan", resourceCulture);
            }
        }

        internal static string NewLimitation
        {
            get
            {
                return ResourceManager.GetString("NewLimitation", resourceCulture);
            }
        }

        internal static string NewPlan
        {
            get
            {
                return ResourceManager.GetString("NewPlan", resourceCulture);
            }
        }

        internal static string NexpPageJust
        {
            get
            {
                return ResourceManager.GetString("NexpPageJust", resourceCulture);
            }
        }

        internal static string Next
        {
            get
            {
                return ResourceManager.GetString("Next", resourceCulture);
            }
        }

        internal static string NoAvailableServices
        {
            get
            {
                return ResourceManager.GetString("NoAvailableServices", resourceCulture);
            }
        }

        internal static string NoChoosenPays
        {
            get
            {
                return ResourceManager.GetString("NoChoosenPays", resourceCulture);
            }
        }

        internal static string NoCoincidence
        {
            get
            {
                return ResourceManager.GetString("NoCoincidence", resourceCulture);
            }
        }

        internal static string NoCommand
        {
            get
            {
                return ResourceManager.GetString("NoCommand", resourceCulture);
            }
        }

        internal static string NoCommandSign
        {
            get
            {
                return ResourceManager.GetString("NoCommandSign", resourceCulture);
            }
        }

        internal static string NoMenu
        {
            get
            {
                return ResourceManager.GetString("NoMenu", resourceCulture);
            }
        }

        internal static string NoNumber
        {
            get
            {
                return ResourceManager.GetString("NoNumber", resourceCulture);
            }
        }

        internal static string NoParentForm
        {
            get
            {
                return ResourceManager.GetString("NoParentForm", resourceCulture);
            }
        }

        internal static string NoPays
        {
            get
            {
                return ResourceManager.GetString("NoPays", resourceCulture);
            }
        }

        internal static string NoPrinterError
        {
            get
            {
                return ResourceManager.GetString("NoPrinterError", resourceCulture);
            }
        }

        internal static string NoReestr
        {
            get
            {
                return ResourceManager.GetString("NoReestr", resourceCulture);
            }
        }

        internal static string NoStatistics
        {
            get
            {
                return ResourceManager.GetString("NoStatistics", resourceCulture);
            }
        }

        internal static string NotAvailable
        {
            get
            {
                return ResourceManager.GetString("NotAvailable", resourceCulture);
            }
        }

        internal static string NotCardPay
        {
            get
            {
                return ResourceManager.GetString("NotCardPay", resourceCulture);
            }
        }

        internal static string NotChoosenPay
        {
            get
            {
                return ResourceManager.GetString("NotChoosenPay", resourceCulture);
            }
        }

        internal static string NoTieCommandMenu
        {
            get
            {
                return ResourceManager.GetString("NoTieCommandMenu", resourceCulture);
            }
        }

        internal static string NumericUpDownErrorString
        {
            get
            {
                return ResourceManager.GetString("NumericUpDownErrorString", resourceCulture);
            }
        }

        internal static string NWTelecom
        {
            get
            {
                return ResourceManager.GetString("NWTelecom", resourceCulture);
            }
        }

        internal static string Ok
        {
            get
            {
                return ResourceManager.GetString("Ok", resourceCulture);
            }
        }

        internal static string OneReceiver
        {
            get
            {
                return ResourceManager.GetString("OneReceiver", resourceCulture);
            }
        }

        internal static string OnSeveralReceivers
        {
            get
            {
                return ResourceManager.GetString("OnSeveralReceivers", resourceCulture);
            }
        }

        internal static string OnStatusTime
        {
            get
            {
                return ResourceManager.GetString("OnStatusTime", resourceCulture);
            }
        }

        internal static string OpenNotChoosen
        {
            get
            {
                return ResourceManager.GetString("OpenNotChoosen", resourceCulture);
            }
        }

        internal static string Page
        {
            get
            {
                return ResourceManager.GetString("Page", resourceCulture);
            }
        }

        internal static string ParentAndChild
        {
            get
            {
                return ResourceManager.GetString("ParentAndChild", resourceCulture);
            }
        }

        internal static string Password
        {
            get
            {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }

        internal static string PayCreated
        {
            get
            {
                return ResourceManager.GetString("PayCreated", resourceCulture);
            }
        }

        internal static string PayCreatedAndRejectedPosLimit
        {
            get
            {
                return ResourceManager.GetString("PayCreatedAndRejectedPosLimit", resourceCulture);
            }
        }

        internal static string PayInfo
        {
            get
            {
                return ResourceManager.GetString("PayInfo", resourceCulture);
            }
        }

        internal static string PayInfo02
        {
            get
            {
                return ResourceManager.GetString("PayInfo02", resourceCulture);
            }
        }

        internal static string PayNotChoosen
        {
            get
            {
                return ResourceManager.GetString("PayNotChoosen", resourceCulture);
            }
        }

        internal static string PayNotCreated
        {
            get
            {
                return ResourceManager.GetString("PayNotCreated", resourceCulture);
            }
        }

        internal static string PayNotIncluded
        {
            get
            {
                return ResourceManager.GetString("PayNotIncluded", resourceCulture);
            }
        }

        internal static string Pays
        {
            get
            {
                return ResourceManager.GetString("Pays", resourceCulture);
            }
        }

        internal static string Pays02
        {
            get
            {
                return ResourceManager.GetString("Pays02", resourceCulture);
            }
        }

        internal static string PaysColor
        {
            get
            {
                return ResourceManager.GetString("PaysColor", resourceCulture);
            }
        }

        internal static string PaysWithCheck
        {
            get
            {
                return ResourceManager.GetString("PaysWithCheck", resourceCulture);
            }
        }

        internal static string PersonalSettingsFileError
        {
            get
            {
                return ResourceManager.GetString("PersonalSettingsFileError", resourceCulture);
            }
        }

        internal static string PersonalSettingsFileFailed
        {
            get
            {
                return ResourceManager.GetString("PersonalSettingsFileFailed", resourceCulture);
            }
        }

        internal static string PhoneNumber
        {
            get
            {
                return ResourceManager.GetString("PhoneNumber", resourceCulture);
            }
        }

        internal static string PoS
        {
            get
            {
                return ResourceManager.GetString("PoS", resourceCulture);
            }
        }

        internal static string PoSNotChoosen
        {
            get
            {
                return ResourceManager.GetString("PoSNotChoosen", resourceCulture);
            }
        }

        internal static string PoSNotChoosen02
        {
            get
            {
                return ResourceManager.GetString("PoSNotChoosen02", resourceCulture);
            }
        }

        internal static string PostAddress
        {
            get
            {
                return ResourceManager.GetString("PostAddress", resourceCulture);
            }
        }

        internal static string Press
        {
            get
            {
                return ResourceManager.GetString("Press", resourceCulture);
            }
        }

        internal static string PressButton
        {
            get
            {
                return ResourceManager.GetString("PressButton", resourceCulture);
            }
        }

        internal static string Preview
        {
            get
            {
                return ResourceManager.GetString("Preview", resourceCulture);
            }
        }

        internal static string PrevPage
        {
            get
            {
                return ResourceManager.GetString("PrevPage", resourceCulture);
            }
        }

        internal static string Print
        {
            get
            {
                return ResourceManager.GetString("Print", resourceCulture);
            }
        }

        internal static string Print02
        {
            get
            {
                return ResourceManager.GetString("Print02", resourceCulture);
            }
        }

        internal static string PrintCheck
        {
            get
            {
                return ResourceManager.GetString("PrintCheck", resourceCulture);
            }
        }

        internal static string PrintCheck02
        {
            get
            {
                return ResourceManager.GetString("PrintCheck02", resourceCulture);
            }
        }

        internal static string PrintFailed
        {
            get
            {
                return ResourceManager.GetString("PrintFailed", resourceCulture);
            }
        }

        internal static string PrintFormat
        {
            get
            {
                return ResourceManager.GetString("PrintFormat", resourceCulture);
            }
        }

        internal static string PrintReport
        {
            get
            {
                return ResourceManager.GetString("PrintReport", resourceCulture);
            }
        }

        internal static string PrintSettings
        {
            get
            {
                return ResourceManager.GetString("PrintSettings", resourceCulture);
            }
        }

        internal static string Qty
        {
            get
            {
                return ResourceManager.GetString("Qty", resourceCulture);
            }
        }

        internal static string QtyOfTransactions
        {
            get
            {
                return ResourceManager.GetString("QtyOfTransactions", resourceCulture);
            }
        }

        internal static string QtyPays
        {
            get
            {
                return ResourceManager.GetString("QtyPays", resourceCulture);
            }
        }

        internal static string Ready
        {
            get
            {
                return ResourceManager.GetString("Ready", resourceCulture);
            }
        }

        internal static string Recall
        {
            get
            {
                return ResourceManager.GetString("Recall", resourceCulture);
            }
        }

        internal static string Receipt
        {
            get
            {
                return ResourceManager.GetString("Receipt", resourceCulture);
            }
        }

        internal static string Receiver
        {
            get
            {
                return ResourceManager.GetString("Receiver", resourceCulture);
            }
        }

        internal static string Receiver02
        {
            get
            {
                return ResourceManager.GetString("Receiver02", resourceCulture);
            }
        }

        internal static string ReceiverAddress
        {
            get
            {
                return ResourceManager.GetString("ReceiverAddress", resourceCulture);
            }
        }

        internal static string ReceiverNotChoosen
        {
            get
            {
                return ResourceManager.GetString("ReceiverNotChoosen", resourceCulture);
            }
        }

        internal static string ReceiverReestr
        {
            get
            {
                return ResourceManager.GetString("ReceiverReestr", resourceCulture);
            }
        }

        internal static string ReceiverReport
        {
            get
            {
                return ResourceManager.GetString("ReceiverReport", resourceCulture);
            }
        }

        internal static string Receivers
        {
            get
            {
                return ResourceManager.GetString("Receivers", resourceCulture);
            }
        }

        internal static string RecipientRollResend
        {
            get
            {
                return ResourceManager.GetString("RecipientRollResend", resourceCulture);
            }
        }

        internal static string Recipients
        {
            get
            {
                return ResourceManager.GetString("Recipients", resourceCulture);
            }
        }

        internal static string Recount
        {
            get
            {
                return ResourceManager.GetString("Recount", resourceCulture);
            }
        }

        internal static string RecountResult
        {
            get
            {
                return ResourceManager.GetString("RecountResult", resourceCulture);
            }
        }

        internal static string RecountSaldo
        {
            get
            {
                return ResourceManager.GetString("RecountSaldo", resourceCulture);
            }
        }

        internal static string RecountSaldo02
        {
            get
            {
                return ResourceManager.GetString("RecountSaldo02", resourceCulture);
            }
        }

        internal static string RedirectingPayments
        {
            get
            {
                return ResourceManager.GetString("RedirectingPayments", resourceCulture);
            }
        }

        internal static string RedirectMessageReport
        {
            get
            {
                return ResourceManager.GetString("RedirectMessageReport", resourceCulture);
            }
        }

        internal static string ReestrCreation
        {
            get
            {
                return ResourceManager.GetString("ReestrCreation", resourceCulture);
            }
        }

        internal static string ReestrDate
        {
            get
            {
                return ResourceManager.GetString("ReestrDate", resourceCulture);
            }
        }

        internal static string ReestrOfReceiver
        {
            get
            {
                return ResourceManager.GetString("ReestrOfReceiver", resourceCulture);
            }
        }

        internal static string ReestrSended
        {
            get
            {
                return ResourceManager.GetString("ReestrSended", resourceCulture);
            }
        }

        internal static string ReestrSending
        {
            get
            {
                return ResourceManager.GetString("ReestrSending", resourceCulture);
            }
        }

        internal static string ReestrToReceiver
        {
            get
            {
                return ResourceManager.GetString("ReestrToReceiver", resourceCulture);
            }
        }

        internal static string Reference
        {
            get
            {
                return ResourceManager.GetString("Reference", resourceCulture);
            }
        }

        internal static string RegestryErr
        {
            get
            {
                return ResourceManager.GetString("RegestryErr", resourceCulture);
            }
        }

        internal static string Reject
        {
            get
            {
                return ResourceManager.GetString("Reject", resourceCulture);
            }
        }

        internal static string Rejected
        {
            get
            {
                return ResourceManager.GetString("Rejected", resourceCulture);
            }
        }

        internal static string Renew
        {
            get
            {
                return ResourceManager.GetString("Renew", resourceCulture);
            }
        }

        internal static string RenewReport
        {
            get
            {
                return ResourceManager.GetString("RenewReport", resourceCulture);
            }
        }

        internal static string Report
        {
            get
            {
                return ResourceManager.GetString("Report", resourceCulture);
            }
        }

        internal static string Report02
        {
            get
            {
                return ResourceManager.GetString("Report02", resourceCulture);
            }
        }

        internal static string ReportExport
        {
            get
            {
                return ResourceManager.GetString("ReportExport", resourceCulture);
            }
        }

        internal static string ReportOnCardPrepay
        {
            get
            {
                return ResourceManager.GetString("ReportOnCardPrepay", resourceCulture);
            }
        }

        internal static string ReportOnCards
        {
            get
            {
                return ResourceManager.GetString("ReportOnCards", resourceCulture);
            }
        }

        internal static string ReportOnCards02
        {
            get
            {
                return ResourceManager.GetString("ReportOnCards02", resourceCulture);
            }
        }

        internal static string ReportSettings
        {
            get
            {
                return ResourceManager.GetString("ReportSettings", resourceCulture);
            }
        }

        internal static string Request
        {
            get
            {
                return ResourceManager.GetString("Request", resourceCulture);
            }
        }

        internal static string Resend
        {
            get
            {
                return ResourceManager.GetString("Resend", resourceCulture);
            }
        }

        internal static string ResendRoll
        {
            get
            {
                return ResourceManager.GetString("ResendRoll", resourceCulture);
            }
        }

        internal static string ResendRoll2
        {
            get
            {
                return ResourceManager.GetString("ResendRoll2", resourceCulture);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    System.Resources.ResourceManager manager = new System.Resources.ResourceManager("IBP.PaymentProject2.Oper.UserStrings", typeof(UserStrings).Assembly);
                    resourceMan = manager;
                }
                return resourceMan;
            }
        }

        internal static string RestructReestr
        {
            get
            {
                return ResourceManager.GetString("RestructReestr", resourceCulture);
            }
        }

        internal static string RestructReestr02
        {
            get
            {
                return ResourceManager.GetString("RestructReestr02", resourceCulture);
            }
        }

        internal static string Review
        {
            get
            {
                return ResourceManager.GetString("Review", resourceCulture);
            }
        }

        internal static string Review02
        {
            get
            {
                return ResourceManager.GetString("Review02", resourceCulture);
            }
        }

        internal static string ReviseActAllreadyExistsQuestion
        {
            get
            {
                return ResourceManager.GetString("ReviseActAllreadyExistsQuestion", resourceCulture);
            }
        }

        internal static string ReviseActParams
        {
            get
            {
                return ResourceManager.GetString("ReviseActParams", resourceCulture);
            }
        }

        internal static string SaldoForAccount
        {
            get
            {
                return ResourceManager.GetString("SaldoForAccount", resourceCulture);
            }
        }

        internal static string SaldoOnAcc
        {
            get
            {
                return ResourceManager.GetString("SaldoOnAcc", resourceCulture);
            }
        }

        internal static string SaldoRecounted
        {
            get
            {
                return ResourceManager.GetString("SaldoRecounted", resourceCulture);
            }
        }

        internal static string Scope
        {
            get
            {
                return ResourceManager.GetString("Scope", resourceCulture);
            }
        }

        internal static string SendReestr
        {
            get
            {
                return ResourceManager.GetString("SendReestr", resourceCulture);
            }
        }

        internal static string ServerConnectionLost
        {
            get
            {
                return ResourceManager.GetString("ServerConnectionLost", resourceCulture);
            }
        }

        internal static string ServerEKassir
        {
            get
            {
                return ResourceManager.GetString("ServerEKassir", resourceCulture);
            }
        }

        internal static string ServerErr
        {
            get
            {
                return ResourceManager.GetString("ServerErr", resourceCulture);
            }
        }

        internal static string ServerNotFound
        {
            get
            {
                return ResourceManager.GetString("ServerNotFound", resourceCulture);
            }
        }

        internal static string ServerVer
        {
            get
            {
                return ResourceManager.GetString("ServerVer", resourceCulture);
            }
        }

        internal static string Service
        {
            get
            {
                return ResourceManager.GetString("Service", resourceCulture);
            }
        }

        internal static string Service02
        {
            get
            {
                return ResourceManager.GetString("Service02", resourceCulture);
            }
        }

        internal static string ServiceNotChoosen
        {
            get
            {
                return ResourceManager.GetString("ServiceNotChoosen", resourceCulture);
            }
        }

        internal static string ServiceNotChoosen02
        {
            get
            {
                return ResourceManager.GetString("ServiceNotChoosen02", resourceCulture);
            }
        }

        internal static string ServiceNotRight
        {
            get
            {
                return ResourceManager.GetString("ServiceNotRight", resourceCulture);
            }
        }

        internal static string ServiceNotTuned
        {
            get
            {
                return ResourceManager.GetString("ServiceNotTuned", resourceCulture);
            }
        }

        internal static string Services
        {
            get
            {
                return ResourceManager.GetString("Services", resourceCulture);
            }
        }

        internal static string Set
        {
            get
            {
                return ResourceManager.GetString("Set", resourceCulture);
            }
        }

        internal static string SetAside
        {
            get
            {
                return ResourceManager.GetString("SetAside", resourceCulture);
            }
        }

        internal static string SetCommissionLimitation
        {
            get
            {
                return ResourceManager.GetString("SetCommissionLimitation", resourceCulture);
            }
        }

        internal static string Settings
        {
            get
            {
                return ResourceManager.GetString("Settings", resourceCulture);
            }
        }

        internal static string ShowAct
        {
            get
            {
                return ResourceManager.GetString("ShowAct", resourceCulture);
            }
        }

        internal static string ShowContracts
        {
            get
            {
                return ResourceManager.GetString("ShowContracts", resourceCulture);
            }
        }

        internal static string ShowCouplePays
        {
            get
            {
                return ResourceManager.GetString("ShowCouplePays", resourceCulture);
            }
        }

        internal static string ShowDocumentError
        {
            get
            {
                return ResourceManager.GetString("ShowDocumentError", resourceCulture);
            }
        }

        internal static string ShowHistorySaldo
        {
            get
            {
                return ResourceManager.GetString("ShowHistorySaldo", resourceCulture);
            }
        }

        internal static string ShowPayByCard
        {
            get
            {
                return ResourceManager.GetString("ShowPayByCard", resourceCulture);
            }
        }

        internal static string ShowPayChild
        {
            get
            {
                return ResourceManager.GetString("ShowPayChild", resourceCulture);
            }
        }

        internal static string ShowPayParent
        {
            get
            {
                return ResourceManager.GetString("ShowPayParent", resourceCulture);
            }
        }

        internal static string ShowPays
        {
            get
            {
                return ResourceManager.GetString("ShowPays", resourceCulture);
            }
        }

        internal static string ShowPays02
        {
            get
            {
                return ResourceManager.GetString("ShowPays02", resourceCulture);
            }
        }

        internal static string ShowPays03
        {
            get
            {
                return ResourceManager.GetString("ShowPays03", resourceCulture);
            }
        }

        internal static string ShowPays04
        {
            get
            {
                return ResourceManager.GetString("ShowPays04", resourceCulture);
            }
        }

        internal static string ShowPaysByTrans
        {
            get
            {
                return ResourceManager.GetString("ShowPaysByTrans", resourceCulture);
            }
        }

        internal static string ShowPaysWithChange
        {
            get
            {
                return ResourceManager.GetString("ShowPaysWithChange", resourceCulture);
            }
        }

        internal static string ShowReestrOfCllient
        {
            get
            {
                return ResourceManager.GetString("ShowReestrOfCllient", resourceCulture);
            }
        }

        internal static string ShowReestrOfReceiver
        {
            get
            {
                return ResourceManager.GetString("ShowReestrOfReceiver", resourceCulture);
            }
        }

        internal static string ShowReport
        {
            get
            {
                return ResourceManager.GetString("ShowReport", resourceCulture);
            }
        }

        internal static string ShowTrans
        {
            get
            {
                return ResourceManager.GetString("ShowTrans", resourceCulture);
            }
        }

        internal static string ShowTransByAcc
        {
            get
            {
                return ResourceManager.GetString("ShowTransByAcc", resourceCulture);
            }
        }

        internal static string SignCommand
        {
            get
            {
                return ResourceManager.GetString("SignCommand", resourceCulture);
            }
        }

        internal static string SomeDescription
        {
            get
            {
                return ResourceManager.GetString("SomeDescription", resourceCulture);
            }
        }

        internal static string SomeDescription02
        {
            get
            {
                return ResourceManager.GetString("SomeDescription02", resourceCulture);
            }
        }

        internal static string SomeDescription03
        {
            get
            {
                return ResourceManager.GetString("SomeDescription03", resourceCulture);
            }
        }

        internal static string SomeDescription04
        {
            get
            {
                return ResourceManager.GetString("SomeDescription04", resourceCulture);
            }
        }

        internal static string SomeDescription05
        {
            get
            {
                return ResourceManager.GetString("SomeDescription05", resourceCulture);
            }
        }

        internal static string Stat_AvgAmount
        {
            get
            {
                return ResourceManager.GetString("Stat_AvgAmount", resourceCulture);
            }
        }

        internal static string Stat_AvgBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_AvgBalance", resourceCulture);
            }
        }

        internal static string Stat_AvgComission
        {
            get
            {
                return ResourceManager.GetString("Stat_AvgComission", resourceCulture);
            }
        }

        internal static string Stat_AvgStartBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_AvgStartBalance", resourceCulture);
            }
        }

        internal static string Stat_AvgTotal
        {
            get
            {
                return ResourceManager.GetString("Stat_AvgTotal", resourceCulture);
            }
        }

        internal static string Stat_AvgValue
        {
            get
            {
                return ResourceManager.GetString("Stat_AvgValue", resourceCulture);
            }
        }

        internal static string Stat_Label
        {
            get
            {
                return ResourceManager.GetString("Stat_Label", resourceCulture);
            }
        }

        internal static string Stat_MaxAmount
        {
            get
            {
                return ResourceManager.GetString("Stat_MaxAmount", resourceCulture);
            }
        }

        internal static string Stat_MaxBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_MaxBalance", resourceCulture);
            }
        }

        internal static string Stat_MaxComission
        {
            get
            {
                return ResourceManager.GetString("Stat_MaxComission", resourceCulture);
            }
        }

        internal static string Stat_MaxStartBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_MaxStartBalance", resourceCulture);
            }
        }

        internal static string Stat_MaxTotal
        {
            get
            {
                return ResourceManager.GetString("Stat_MaxTotal", resourceCulture);
            }
        }

        internal static string Stat_MaxValue
        {
            get
            {
                return ResourceManager.GetString("Stat_MaxValue", resourceCulture);
            }
        }

        internal static string Stat_MinAmount
        {
            get
            {
                return ResourceManager.GetString("Stat_MinAmount", resourceCulture);
            }
        }

        internal static string Stat_MinBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_MinBalance", resourceCulture);
            }
        }

        internal static string Stat_MinComission
        {
            get
            {
                return ResourceManager.GetString("Stat_MinComission", resourceCulture);
            }
        }

        internal static string Stat_MinStartBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_MinStartBalance", resourceCulture);
            }
        }

        internal static string Stat_MinTotal
        {
            get
            {
                return ResourceManager.GetString("Stat_MinTotal", resourceCulture);
            }
        }

        internal static string Stat_MinValue
        {
            get
            {
                return ResourceManager.GetString("Stat_MinValue", resourceCulture);
            }
        }

        internal static string Stat_PaymentCount
        {
            get
            {
                return ResourceManager.GetString("Stat_PaymentCount", resourceCulture);
            }
        }

        internal static string Stat_SumAmount
        {
            get
            {
                return ResourceManager.GetString("Stat_SumAmount", resourceCulture);
            }
        }

        internal static string Stat_SumBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_SumBalance", resourceCulture);
            }
        }

        internal static string Stat_SumComission
        {
            get
            {
                return ResourceManager.GetString("Stat_SumComission", resourceCulture);
            }
        }

        internal static string Stat_SumFromBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_SumFromBalance", resourceCulture);
            }
        }

        internal static string Stat_SumStartBalance
        {
            get
            {
                return ResourceManager.GetString("Stat_SumStartBalance", resourceCulture);
            }
        }

        internal static string Stat_SumTotal
        {
            get
            {
                return ResourceManager.GetString("Stat_SumTotal", resourceCulture);
            }
        }

        internal static string Stat_SumValue
        {
            get
            {
                return ResourceManager.GetString("Stat_SumValue", resourceCulture);
            }
        }

        internal static string Statistics
        {
            get
            {
                return ResourceManager.GetString("Statistics", resourceCulture);
            }
        }

        internal static string Stop
        {
            get
            {
                return ResourceManager.GetString("Stop", resourceCulture);
            }
        }

        internal static string StopReportGeneration
        {
            get
            {
                return ResourceManager.GetString("StopReportGeneration", resourceCulture);
            }
        }

        internal static string Structure
        {
            get
            {
                return ResourceManager.GetString("Structure", resourceCulture);
            }
        }

        internal static string SuccessfulCheckPayment
        {
            get
            {
                return ResourceManager.GetString("SuccessfulCheckPayment", resourceCulture);
            }
        }

        internal static string SuccessfulCheckPaymentWithResults
        {
            get
            {
                return ResourceManager.GetString("SuccessfulCheckPaymentWithResults", resourceCulture);
            }
        }

        internal static string Sum
        {
            get
            {
                return ResourceManager.GetString("Sum", resourceCulture);
            }
        }

        internal static string SumCommission
        {
            get
            {
                return ResourceManager.GetString("SumCommission", resourceCulture);
            }
        }

        internal static string SumOfPaysTotalChange
        {
            get
            {
                return ResourceManager.GetString("SumOfPaysTotalChange", resourceCulture);
            }
        }

        internal static string SumShouldBe
        {
            get
            {
                return ResourceManager.GetString("SumShouldBe", resourceCulture);
            }
        }

        internal static string Table
        {
            get
            {
                return ResourceManager.GetString("Table", resourceCulture);
            }
        }

        internal static string TariffPlan
        {
            get
            {
                return ResourceManager.GetString("TariffPlan", resourceCulture);
            }
        }

        internal static string TariffPlan02
        {
            get
            {
                return ResourceManager.GetString("TariffPlan02", resourceCulture);
            }
        }

        internal static string TariffPlans
        {
            get
            {
                return ResourceManager.GetString("TariffPlans", resourceCulture);
            }
        }

        internal static string TariffPlanTie
        {
            get
            {
                return ResourceManager.GetString("TariffPlanTie", resourceCulture);
            }
        }

        internal static string TerminalReport
        {
            get
            {
                return ResourceManager.GetString("TerminalReport", resourceCulture);
            }
        }

        internal static string TextNotFound
        {
            get
            {
                return ResourceManager.GetString("TextNotFound", resourceCulture);
            }
        }

        internal static string ThereIsNoClientsForPaymentCreation
        {
            get
            {
                return ResourceManager.GetString("ThereIsNoClientsForPaymentCreation", resourceCulture);
            }
        }

        internal static string ThereIsNoPaymentParameter
        {
            get
            {
                return ResourceManager.GetString("ThereIsNoPaymentParameter", resourceCulture);
            }
        }

        internal static string ThereIsNoTariffPlans
        {
            get
            {
                return ResourceManager.GetString("ThereIsNoTariffPlans", resourceCulture);
            }
        }

        internal static string ThisPayNotIncluded
        {
            get
            {
                return ResourceManager.GetString("ThisPayNotIncluded", resourceCulture);
            }
        }

        internal static string TimePeriod
        {
            get
            {
                return ResourceManager.GetString("TimePeriod", resourceCulture);
            }
        }

        internal static string To
        {
            get
            {
                return ResourceManager.GetString("To", resourceCulture);
            }
        }

        internal static string TooMuchPays
        {
            get
            {
                return ResourceManager.GetString("TooMuchPays", resourceCulture);
            }
        }

        internal static string TransactionCreation
        {
            get
            {
                return ResourceManager.GetString("TransactionCreation", resourceCulture);
            }
        }

        internal static string TransactionCreationError
        {
            get
            {
                return ResourceManager.GetString("TransactionCreationError", resourceCulture);
            }
        }

        internal static string TransactionCreationForRecipient
        {
            get
            {
                return ResourceManager.GetString("TransactionCreationForRecipient", resourceCulture);
            }
        }

        internal static string Transactions
        {
            get
            {
                return ResourceManager.GetString("Transactions", resourceCulture);
            }
        }

        internal static string TransNotChoosen
        {
            get
            {
                return ResourceManager.GetString("TransNotChoosen", resourceCulture);
            }
        }

        internal static string TransReceiving
        {
            get
            {
                return ResourceManager.GetString("TransReceiving", resourceCulture);
            }
        }

        internal static string TransSumm
        {
            get
            {
                return ResourceManager.GetString("TransSumm", resourceCulture);
            }
        }

        internal static string TryAgainQuestionString
        {
            get
            {
                return ResourceManager.GetString("TryAgainQuestionString", resourceCulture);
            }
        }

        internal static string TypeNotMarked
        {
            get
            {
                return ResourceManager.GetString("TypeNotMarked", resourceCulture);
            }
        }

        internal static string UDontHaveRigths
        {
            get
            {
                return ResourceManager.GetString("UDontHaveRigths", resourceCulture);
            }
        }

        internal static string UHaveNoRights
        {
            get
            {
                return ResourceManager.GetString("UHaveNoRights", resourceCulture);
            }
        }

        internal static string UnsuccessfulCheckPayment
        {
            get
            {
                return ResourceManager.GetString("UnsuccessfulCheckPayment", resourceCulture);
            }
        }

        internal static string UploadTable
        {
            get
            {
                return ResourceManager.GetString("UploadTable", resourceCulture);
            }
        }

        internal static string UploadToXLS
        {
            get
            {
                return ResourceManager.GetString("UploadToXLS", resourceCulture);
            }
        }

        internal static string UseChange
        {
            get
            {
                return ResourceManager.GetString("UseChange", resourceCulture);
            }
        }

        internal static string UserName
        {
            get
            {
                return ResourceManager.GetString("UserName", resourceCulture);
            }
        }

        internal static string Version
        {
            get
            {
                return ResourceManager.GetString("Version", resourceCulture);
            }
        }

        internal static string Views
        {
            get
            {
                return ResourceManager.GetString("Views", resourceCulture);
            }
        }

        internal static string WantToSaveChanges
        {
            get
            {
                return ResourceManager.GetString("WantToSaveChanges", resourceCulture);
            }
        }

        internal static string WhileCreatingReviseAct
        {
            get
            {
                return ResourceManager.GetString("WhileCreatingReviseAct", resourceCulture);
            }
        }

        internal static string WholePage
        {
            get
            {
                return ResourceManager.GetString("WholePage", resourceCulture);
            }
        }

        internal static string Why
        {
            get
            {
                return ResourceManager.GetString("Why", resourceCulture);
            }
        }

        internal static string WriteComment
        {
            get
            {
                return ResourceManager.GetString("WriteComment", resourceCulture);
            }
        }
    }
}

