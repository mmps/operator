﻿namespace IBP.PaymentProject2.Oper
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class OperException : Exception
    {
        public OperException()
        {
        }

        public OperException(string message) : base(message)
        {
        }

        protected OperException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public OperException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}

