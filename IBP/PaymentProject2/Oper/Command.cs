﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Threading;
    using System.Windows.Forms;

    internal abstract class Command
    {
        protected object _tag;
        protected bool _tagIsArray;
        private EventHandler handler;

        public event EventHandler Completed;

        protected Command()
        {
        }

        protected bool ConfirmAction(string caption, string questionTextFormat, params object[] parameters)
        {
            return (MessageBox.Show(string.Format(questionTextFormat, parameters), caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
        }

        public abstract void Execute();
        private void Execute<TEventArgs>(object sender, EventArgs e) where TEventArgs: EventArgs
        {
            try
            {
                this.Execute();
                this.RaiseCompleted();
            }
            catch (SrvException exception)
            {
                this.ShowServerError(exception);
            }
        }

        private void RaiseCompleted()
        {
            if (this.Completed != null)
            {
                this.Completed(this, new EventArgs());
            }
        }

        protected void ShowError(string format, params object[] parameters)
        {
            MessageBox.Show(string.Format(format, parameters), UserStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        protected void ShowServerError(SrvException e)
        {
            MessageBox.Show(e.Message, UserStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        protected void ShowWarning(string format, params object[] parameters)
        {
            MessageBox.Show(string.Format(format, parameters), string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public virtual bool CanExecute
        {
            get
            {
                throw new NotImplementedException(UserStrings.MethodNotDefined);
            }
        }

        public EventHandler EventDelegate
        {
            get
            {
                if (this.handler == null)
                {
                    this.handler = new EventHandler(this.Execute<EventArgs>);
                }
                return this.handler;
            }
        }

        public virtual bool MenuItemIsVisible
        {
            get
            {
                return true;
            }
        }

        public virtual object Tag
        {
            get
            {
                return this._tag;
            }
            set
            {
                this._tag = value;
            }
        }

        public bool TagIsArray
        {
            get
            {
                return this._tagIsArray;
            }
        }
    }
}

