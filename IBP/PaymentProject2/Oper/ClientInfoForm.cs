﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Forms;

    public class ClientInfoForm : Form
    {
        private Client _client;
        private AvailableServiceGuide _serviceGuide;
        private Button buttonOk;
        private ColumnHeader columnHeaderParam;
        private ColumnHeader columnHeaderService;
        private ColumnHeader columnHeaderValue;
        private IContainer components;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private ListViewEdit listViewClientInfo;
        private ListViewEdit listViewEditServices;

        public ClientInfoForm(Client client, AvailableServiceGuide serviceGuide)
        {
            this.InitializeComponent();
            this._client = client;
            this._serviceGuide = serviceGuide;
        }

        private void AddParam(string param, string value)
        {
            ListViewItem item = new ListViewItem();
            item.Text = param;
            item.SubItems.Add(value);
            this.listViewClientInfo.Items.Add(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitDate()
        {
            this.AddParam(UserStrings.Name, this._client.Name);
            this.AddParam(UserStrings.Id, this._client.Id.ToString());
            this.AddParam(UserStrings.PostAddress, this._client.Email);
            this.AddParam(UserStrings.ClientStatus, this._client.IsEnabled ? UserStrings.Active : UserStrings.Blocked);
            this.AddParam(UserStrings.DateOfShowing, this._client.InputDate.ToString());
            foreach (Guid guid in (IEnumerable<Guid>) this._serviceGuide)
            {
                ListViewItem item = new ListViewItem();
                Service service = ServicesContainer.Instance[guid];
                item.Name = item.Text = service.Name;
                this.listViewEditServices.Items.Add(item);
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ComponentResourceManager manager = new ComponentResourceManager(typeof(ClientInfoForm));
            this.groupBox1 = new GroupBox();
            this.listViewClientInfo = new ListViewEdit();
            this.columnHeaderParam = new ColumnHeader();
            this.columnHeaderValue = new ColumnHeader();
            this.buttonOk = new Button();
            this.groupBox2 = new GroupBox();
            this.listViewEditServices = new ListViewEdit();
            this.columnHeaderService = new ColumnHeader();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            base.SuspendLayout();
            this.groupBox1.Controls.Add(this.listViewClientInfo);
            manager.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            this.listViewClientInfo.AllowSort = false;
            this.listViewClientInfo.BindType = null;
            this.listViewClientInfo.Columns.AddRange(new ColumnHeader[] { this.columnHeaderParam, this.columnHeaderValue });
            this.listViewClientInfo.ColumnsCollection = null;
            manager.ApplyResources(this.listViewClientInfo, "listViewClientInfo");
            this.listViewClientInfo.FullRowSelect = true;
            this.listViewClientInfo.GridLines = true;
            this.listViewClientInfo.Name = "listViewClientInfo";
            this.listViewClientInfo.ReadOnly = true;
            this.listViewClientInfo.UseCompatibleStateImageBehavior = false;
            this.listViewClientInfo.View = View.Details;
            manager.ApplyResources(this.columnHeaderParam, "columnHeaderParam");
            manager.ApplyResources(this.columnHeaderValue, "columnHeaderValue");
            this.buttonOk.DialogResult = DialogResult.OK;
            manager.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.groupBox2.Controls.Add(this.listViewEditServices);
            manager.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            this.listViewEditServices.AllowSort = false;
            this.listViewEditServices.BindType = null;
            this.listViewEditServices.Columns.AddRange(new ColumnHeader[] { this.columnHeaderService });
            this.listViewEditServices.ColumnsCollection = null;
            manager.ApplyResources(this.listViewEditServices, "listViewEditServices");
            this.listViewEditServices.FullRowSelect = true;
            this.listViewEditServices.GridLines = true;
            this.listViewEditServices.Name = "listViewEditServices";
            this.listViewEditServices.ReadOnly = true;
            this.listViewEditServices.UseCompatibleStateImageBehavior = false;
            this.listViewEditServices.View = View.Details;
            manager.ApplyResources(this.columnHeaderService, "columnHeaderService");
            manager.ApplyResources(this, "$this");
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.groupBox2);
            base.Controls.Add(this.buttonOk);
            base.Controls.Add(this.groupBox1);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ClientInfoForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            base.ResumeLayout(false);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Text = UserStrings.Client03 + this._client.Name;
            this.InitDate();
        }
    }
}

