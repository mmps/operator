﻿namespace IBP.PaymentProject2.Oper
{
    using System;

    public class StepEventArgs : EventArgs
    {
        private object sentObject;

        public StepEventArgs(object sendObject)
        {
            this.sentObject = sendObject;
        }

        public object SentObject
        {
            get
            {
                return this.sentObject;
            }
        }
    }
}

