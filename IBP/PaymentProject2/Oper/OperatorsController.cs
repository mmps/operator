﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;

    internal static class OperatorsController
    {
        public static void Initialize()
        {
            Operator[] operators = IBP.PaymentProject2.Oper.ServerFasad.Server.GetOperators();
            OperatorsContainer.Instance.AddRange(operators);
            OperatorsContainer.Instance.ResolveItem += new EventHandler<ResolveItemEventArgs<Operator, int>>(OperatorsController.OnResolveItem);
        }

        private static void OnResolveItem(object sender, ResolveItemEventArgs<Operator, int> e)
        {
            Operator @operator = IBP.PaymentProject2.Oper.ServerFasad.Server.GetOperator(e.ItemId);
            e.ResolvedItem = @operator;
        }
    }
}

