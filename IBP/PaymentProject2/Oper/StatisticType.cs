﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common.Component;
    using System;

    public enum StatisticType
    {
        [Description(typeof(UserStrings), "Filter_byClients")]
        byClients = 4,
        [Description(typeof(UserStrings), "Filter_byDays")]
        byDays = 1,
        [Description(typeof(UserStrings), "Filter_byDaysofPeriod")]
        byDaysofPeriod = 7,
        [Description(typeof(UserStrings), "Filter_byDaysofWorkState")]
        byDaysofWorkState = 8,
        [Description(typeof(UserStrings), "Filter_byEmpty")]
        byEmpty = 0,
        [Description(typeof(UserStrings), "Filter_byMonth")]
        byMonth = 2,
        [Description(typeof(UserStrings), "Filter_byPoints")]
        byPoints = 5,
        [Description(typeof(UserStrings), "Filter_byRecipient")]
        byRecipient = 6,
        [Description(typeof(UserStrings), "Filter_byServices")]
        byServices = 3
    }
}

