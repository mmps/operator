﻿namespace IBP.PaymentProject2.Oper.Properties
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed class Settings : ApplicationSettingsBase
    {
        private static Settings defaultInstance = ((Settings) SettingsBase.Synchronized(new Settings()));

        private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
        {
        }

        private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
        {
        }

        public static Settings Default
        {
            get
            {
                return defaultInstance;
            }
        }

        public bool HasUserWindowPosition
        {
            get
            {
                return (-2147483648 != this.TopX);
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("580")]
        public int Height
        {
            get
            {
                return (int) this["Height"];
            }
            set
            {
                this["Height"] = value;
            }
        }

        [DefaultSettingValue("-2147483648"), UserScopedSetting, DebuggerNonUserCode]
        public int TopX
        {
            get
            {
                return (int) this["TopX"];
            }
            set
            {
                this["TopX"] = value;
            }
        }

        [DebuggerNonUserCode, UserScopedSetting, DefaultSettingValue("-2147483648")]
        public int TopY
        {
            get
            {
                return (int) this["TopY"];
            }
            set
            {
                this["TopY"] = value;
            }
        }

        [UserScopedSetting, DebuggerNonUserCode, DefaultSettingValue("1000")]
        public int Width
        {
            get
            {
                return (int) this["Width"];
            }
            set
            {
                this["Width"] = value;
            }
        }
    }
}

