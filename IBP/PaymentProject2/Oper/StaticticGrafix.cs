﻿namespace IBP.PaymentProject2.Oper
{
    using System;
    using System.Collections.Generic;

    public class StaticticGrafix : IStatisticGrafix
    {
        private List<IStatisticItemGrafix> array;
        private string filterlabel;
        private IStatisticItemGrafix maxItem;
        private string text;
        private string xlabel;
        private string ylabel;

        public StaticticGrafix()
        {
            this.array = new List<IStatisticItemGrafix>();
            this.filterlabel = "";
            this.maxItem = new StatisticItemGrafix("", 0M);
        }

        public StaticticGrafix(string text)
        {
            this.array = new List<IStatisticItemGrafix>();
            this.filterlabel = "";
            this.maxItem = new StatisticItemGrafix("", 0M);
            this.text = text;
        }

        public StaticticGrafix(string text, IStatisticItemGrafix item)
        {
            this.array = new List<IStatisticItemGrafix>();
            this.filterlabel = "";
            this.maxItem = new StatisticItemGrafix("", 0M);
            this.text = text;
            this.array.Add(item);
        }

        public StaticticGrafix(string text, IStatisticItemGrafix[] items)
        {
            this.array = new List<IStatisticItemGrafix>();
            this.filterlabel = "";
            this.maxItem = new StatisticItemGrafix("", 0M);
            this.text = text;
            this.array.AddRange(items);
        }

        public string FilterLabel
        {
            get
            {
                return this.filterlabel;
            }
            set
            {
                this.filterlabel = value;
            }
        }

        public List<IStatisticItemGrafix> Items
        {
            get
            {
                return this.array;
            }
            set
            {
                this.array = value;
            }
        }

        public IStatisticItemGrafix MaxItem
        {
            get
            {
                if (this.maxItem.Total == 0M)
                {
                    foreach (IStatisticItemGrafix grafix in this.array)
                    {
                        if (this.maxItem.Total < grafix.Total)
                        {
                            this.maxItem.Total = grafix.Total;
                        }
                        if (this.maxItem.Label.Length < grafix.Label.Length)
                        {
                            this.maxItem.Label = grafix.Label;
                        }
                    }
                }
                return this.maxItem;
            }
        }

        public string StatisticLabel
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
            }
        }

        public string XLabel
        {
            get
            {
                return this.xlabel;
            }
            set
            {
                this.xlabel = value;
            }
        }

        public string YLabel
        {
            get
            {
                return this.ylabel;
            }
            set
            {
                this.ylabel = value;
            }
        }
    }
}

