﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Windows.Forms;

    internal class DeleteClientReviseAct : IBP.PaymentProject2.Oper.Command
    {
        private void DeleteAct(int id)
        {
            try
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.DeleteClientReviseAct(this.ClientReviseAct.Id);
            }
            catch (SrvException exception)
            {
                if (MessageBox.Show(string.Format(UserStrings.ServerErr, Environment.NewLine, exception.ToString()), UserStrings.Error, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.DeleteAct(id);
                }
            }
        }

        public override void Execute()
        {
            if (MessageBox.Show(UserStrings.AreYouReallyWantDeleteAct, UserStrings.DeleteClientReviseAct, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.DeleteAct(this.ClientReviseAct.Id);
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.ClientReviseAct != null);
            }
        }

        private IBP.PaymentProject2.Common.Reconing.ClientReviseAct ClientReviseAct
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Reconing.ClientReviseAct);
            }
        }
    }
}

