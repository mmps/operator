﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Isolation;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Threading;
    using System.Windows.Forms;

    internal class CreateNewClientTariffPlan : TariffPlanCommand
    {
        private bool _createCopy;

        public event EventHandler<TariffPlanEventArgs> TariffPlanCreated;

        public CreateNewClientTariffPlan(bool createCopy)
        {
            this._createCopy = createCopy;
        }

        public override void Execute()
        {
            ClientTariffPlan plan;
            if (this._createCopy)
            {
                plan = IBP.PaymentProject2.Oper.ServerFasad.Server.GetFullClientTariffPlan(this.SelectedPlan.Id).CreateCopy();
            }
            else
            {
                try
                {
                    plan = ClientTariffPlan.CreateNewTariffPlan();
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show(UserStrings.CantCreateTariffPlanThereIsNoServices, UserStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            ClientTariffPlanEditForm form = new ClientTariffPlanEditForm(plan, ReconingMethod.GetAvailableMethods(), true);
            form.SaveTariffPlan += new EventHandler<TariffPlanEventArgs>(this.OnFormSaveTariffPlan);
            form.ReleaseTariffPlan += new EventHandler<TariffPlanEventArgs>(this.OnFormReleaseTariffPlan);
            form.Show();
        }

        protected void OnFormSaveTariffPlan(object sender, TariffPlanEventArgs e)
        {
            ClientTariffPlanEditForm form = (ClientTariffPlanEditForm) sender;
            IsolationCookie isolationCookie = form.IsolationCookie;
            try
            {
                if (isolationCookie == null)
                {
                    ClientTariffPlan tariffPlan = IBP.PaymentProject2.Oper.ServerFasad.Server.AddClientTariffPlan(form.TariffPlan);
                    form.TariffPlan = tariffPlan;
                    TariffPlanEventArgs args = new TariffPlanEventArgs(tariffPlan);
                    this.RaiseTariffPlanCreated(args);
                    form.IsolationCookie = IsolationCookie.Create(IBP.PaymentProject2.Oper.ServerFasad.Server, tariffPlan);
                }
                else
                {
                    IBP.PaymentProject2.Oper.ServerFasad.Server.UpdateClientTariffPlan(form.TariffPlan);
                    base.RaiseTariffPlanChanged(e);
                }
                e.Successful = true;
            }
            catch (IsolationSrvException exception)
            {
                base.ShowAccessLoseError(form, exception);
                e.Successful = false;
            }
            catch (SrvException exception2)
            {
                base.ShowSaveTariffPlanError(exception2);
                e.Successful = false;
            }
        }

        private void RaiseTariffPlanCreated(TariffPlanEventArgs args)
        {
            if (this.TariffPlanCreated != null)
            {
                this.TariffPlanCreated(this, args);
            }
        }

        public override bool CanExecute
        {
            get
            {
                if (this._createCopy)
                {
                    return (this.SelectedPlan != null);
                }
                return true;
            }
        }

        private ClientTariffPlan SelectedPlan
        {
            get
            {
                return (base._tag as ClientTariffPlan);
            }
        }
    }
}

