﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Component;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    internal class SendMultipleCustomRoll : IBP.PaymentProject2.Oper.Command
    {
        private StepDialog dialog;
        private StepInfo info;

        private void _step2_BeforeLeaveStepForward(object sender, EventArgs e)
        {
            ListViewStep step = (ListViewStep) sender;
            object[] selectedItems = step.SelectedItems;
            if (selectedItems.Length == 0)
            {
                throw new StepException(UserStrings.ChooseOneAtLeast);
            }
            Recipient[] recipientArray = new Recipient[selectedItems.Length];
            for (int i = 0; i < selectedItems.Length; i++)
            {
                recipientArray[i] = ((RecipientWrapper) selectedItems[i]).Recipient;
            }
            this.info.SelectedRecipients = recipientArray;
        }

        private void _step3_BeforeEnterStepForward(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.GetStatistic), sender);
        }

        private void _step3_BeforeLeaveStepForward(object sender, EventArgs e)
        {
            if (!this.info.CanCreateRolls)
            {
                throw new StepException(UserStrings.NoReestr);
            }
        }

        private void _step4_BeforeEnterStepForward(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.CreateRolls), sender);
        }

        private void CreateRolls(object control)
        {
            ListViewStep step = (ListViewStep) control;
            step.Clear();
            this.dialog.DisableControlButtons();
            RollWrapper[] goodRollWrappers = this.info.GoodRollWrappers;
            this.dialog.ProgressBarMaximum = goodRollWrappers.Length;
            this.dialog.ProgressBarMinimum = 0;
            this.dialog.ProgressBarStep = 1;
            this.dialog.ShowProgressBar();
            foreach (RollWrapper wrapper in goodRollWrappers)
            {
                try
                {
                    IBP.PaymentProject2.Oper.ServerFasad.Server.SendCustomRoll(wrapper.Filter);
                }
                catch (SrvException exception)
                {
                    wrapper.Error = exception.Message;
                }
                step.AddItem(wrapper);
                this.dialog.PerformStep();
            }
            step.ResizeColumns();
            this.dialog.HideProgressBar();
            this.dialog.EnableControlButtons();
        }

        public override void Execute()
        {
            this.info = new StepInfo();
            using (this.dialog = new StepDialog())
            {
                this.dialog.Text = UserStrings.ReestrToReceiver;
                StartEndTimeStep step = new StartEndTimeStep(this.info);
                step.HeaderText = UserStrings.ChoosePeriod;
                step.DateTimeFormat = DateTimePickerFormat.Custom;
                step.DateTimeCustomFormat = "dd MMMM yyyy HH:mm:ss";
                this.dialog.Controls.Add(step);
                ListViewStep step2 = new ListViewStep();
                step2.HeaderText = UserStrings.Reject;
                step2.ItemObjectType = typeof(RecipientWrapper);
                step2.UseCheckBoxes = true;
                step2.AddItems(RecipientWrapper.Create());
                step2.ResizeColumns();
                step2.BeforeLeaveStepForward += new EventHandler(this._step2_BeforeLeaveStepForward);
                this.dialog.Controls.Add(step2);
                ListViewStep step3 = new ListViewStep();
                step3.HeaderText = UserStrings.Press;
                step3.ItemObjectType = typeof(RollWrapper);
                step3.Size = new Size(650, 550);
                step3.BeforeEnterStepForward += new EventHandler(this._step3_BeforeEnterStepForward);
                step3.BeforeLeaveStepForward += new EventHandler(this._step3_BeforeLeaveStepForward);
                step3.ItemAdded += new EventHandler<ItemEventArgs>(this.OnItemAdded);
                this.dialog.Controls.Add(step3);
                ListViewStep step4 = new ListViewStep();
                step4.HeaderText = UserStrings.Ready;
                step4.ItemObjectType = typeof(RollWrapper);
                step4.BeforeEnterStepForward += new EventHandler(this._step4_BeforeEnterStepForward);
                step4.ItemAdded += new EventHandler<ItemEventArgs>(this.OnItemAdded);
                this.dialog.Controls.Add(step4);
                this.dialog.ShowDialog();
            }
        }

        private void GetStatistic(object control)
        {
            ListViewStep step = (ListViewStep) control;
            step.Clear();
            this.dialog.DisableControlButtons();
            this.dialog.ProgressBarMaximum = this.info.SelectedRecipients.Length;
            this.dialog.ProgressBarMinimum = 0;
            this.dialog.ProgressBarStep = 1;
            this.dialog.ShowProgressBar();
            List<RollWrapper> list = new List<RollWrapper>();
            foreach (Recipient recipient in this.info.SelectedRecipients)
            {
                RollWrapper item = new RollWrapper(recipient, this.info);
                list.Add(item);
                try
                {
                    item.Statistic = IBP.PaymentProject2.Oper.ServerFasad.Server.GetStatisticInfo(item.Filter)[0];
                }
                catch (SrvException exception)
                {
                    item.Error = exception.Message;
                }
                step.AddItem(item);
                this.dialog.PerformStep();
            }
            step.ResizeColumns();
            this.info.RollWrappers = list.ToArray();
            this.dialog.HideProgressBar();
            this.dialog.EnableControlButtons();
        }

        private void OnItemAdded(object sender, ItemEventArgs e)
        {
            RollWrapper tag = (RollWrapper) e.Tag;
            if (tag.IsReady)
            {
                e.Item.BackColor = tag.Color;
            }
        }

        private class RecipientWrapper
        {
            private IBP.PaymentProject2.Common.Recipient recipient;

            private RecipientWrapper(IBP.PaymentProject2.Common.Recipient recipient)
            {
                this.recipient = recipient;
            }

            public static SendMultipleCustomRoll.RecipientWrapper[] Create()
            {
                List<SendMultipleCustomRoll.RecipientWrapper> list = new List<SendMultipleCustomRoll.RecipientWrapper>();
                foreach (IBP.PaymentProject2.Common.Recipient recipient in RecipientsContainer.Instance.Collection)
                {
                    if (recipient.AllowCustomRoll)
                    {
                        list.Add(new SendMultipleCustomRoll.RecipientWrapper(recipient));
                    }
                }
                return list.ToArray();
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Gateway"), Browsable(true)]
            public string GatewayName
            {
                get
                {
                    return new EnumTypeInfo<GatewayType>(this.recipient.GatewayType).Name;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Receiver02"), Browsable(true)]
            public string Name
            {
                get
                {
                    return this.recipient.Name;
                }
            }

            public IBP.PaymentProject2.Common.Recipient Recipient
            {
                get
                {
                    return this.recipient;
                }
            }
        }

        private class RollWrapper
        {
            private string error;
            public readonly PaymentsFilter Filter;
            public readonly IBP.PaymentProject2.Common.Recipient Recipient;
            private StatisticInfo statistic;

            public RollWrapper(IBP.PaymentProject2.Common.Recipient recipient, SendMultipleCustomRoll.StepInfo info)
            {
                this.Recipient = recipient;
                this.Filter = PaymentsFilter.WithStrictTime;
                this.Filter.RecipientID = this.Recipient.Id;
                this.Filter.ByStateTime = true;
                this.Filter.State = PaymentState.Finalized;
                this.Filter.StartTime = info.Start;
                this.Filter.EndTime = info.End;
            }

            [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Sum")]
            public decimal Amount
            {
                get
                {
                    return this.statistic.SumValue;
                }
            }

            public System.Drawing.Color Color
            {
                get
                {
                    if (!string.IsNullOrEmpty(this.error))
                    {
                        return System.Drawing.Color.LightPink;
                    }
                    if (this.statistic.Count > 0)
                    {
                        return System.Drawing.Color.LightGreen;
                    }
                    return System.Drawing.Color.White;
                }
            }

            [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Qty")]
            public int Count
            {
                get
                {
                    return this.statistic.Count;
                }
            }

            [Browsable(true), IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "To")]
            public DateTime EndTime
            {
                get
                {
                    return this.Filter.EndTime;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Comments"), Browsable(true)]
            public string Error
            {
                get
                {
                    if (!string.IsNullOrEmpty(this.error))
                    {
                        return this.error;
                    }
                    if (this.statistic.Count > 0)
                    {
                        return "Ок";
                    }
                    return UserStrings.NoPays;
                }
                set
                {
                    this.error = value;
                }
            }

            public bool IsReady
            {
                get
                {
                    return (this.statistic.Count > 0);
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Receiver02"), Browsable(true)]
            public string Name
            {
                get
                {
                    return this.Recipient.Name;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "From"), Browsable(true)]
            public DateTime Start
            {
                get
                {
                    return this.Filter.StartTime;
                }
            }

            public StatisticInfo Statistic
            {
                get
                {
                    return this.statistic;
                }
                set
                {
                    this.statistic = value;
                }
            }
        }

        private class StepInfo : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            private Recipient[] recipients;
            private SendMultipleCustomRoll.RollWrapper[] rollWrappers;

            public StepInfo()
            {
                base.start = base.end = DateTime.Now.Date.AddDays(-1.0);
            }

            public bool CanCreateRolls
            {
                get
                {
                    foreach (SendMultipleCustomRoll.RollWrapper wrapper in this.rollWrappers)
                    {
                        if (wrapper.IsReady)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }

            public SendMultipleCustomRoll.RollWrapper[] GoodRollWrappers
            {
                get
                {
                    List<SendMultipleCustomRoll.RollWrapper> list = new List<SendMultipleCustomRoll.RollWrapper>();
                    foreach (SendMultipleCustomRoll.RollWrapper wrapper in this.rollWrappers)
                    {
                        if (wrapper.IsReady)
                        {
                            list.Add(wrapper);
                        }
                    }
                    return list.ToArray();
                }
            }

            public SendMultipleCustomRoll.RollWrapper[] RollWrappers
            {
                get
                {
                    return this.rollWrappers;
                }
                set
                {
                    this.rollWrappers = value;
                }
            }

            public Recipient[] SelectedRecipients
            {
                get
                {
                    return this.recipients;
                }
                set
                {
                    this.recipients = value;
                }
            }
        }
    }
}

