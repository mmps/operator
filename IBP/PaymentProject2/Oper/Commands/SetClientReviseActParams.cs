﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal class SetClientReviseActParams : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            if (this.Client.HasDeal || base.ConfirmAction(UserStrings.DealerHasntDeal, UserStrings.DealerHasntDealDoYouWantToChangeActParams, new object[] { this.Client.Name }))
            {
                ClientReviseActParams actParams;
                DateTime date;
                using (SelectMonthForm form = new SelectMonthForm())
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        date = form.Date;
                        actParams = this.GetActParams(date);
                    }
                    else
                    {
                        return;
                    }
                }
                if (actParams != null)
                {
                    using (ClientReviseActParamsForm form2 = new ClientReviseActParamsForm(this.Client, actParams, date))
                    {
                        if ((form2.ShowDialog() == DialogResult.OK) && form2.IsChange)
                        {
                            this.SetActParams(form2.ActParams, this.Client.Id, date);
                        }
                    }
                }
            }
        }

        private ClientReviseActParams GetActParams(DateTime date)
        {
            Guid id = this.Client.Id;
            try
            {
                return IBP.PaymentProject2.Oper.ServerFasad.Server.GetClientReviseActParams(id, date);
            }
            catch (SrvException exception)
            {
                if (MessageBox.Show(string.Format(UserStrings.ServerErr, Environment.NewLine, exception.Message), UserStrings.Err, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return this.GetActParams(date);
                }
                return null;
            }
        }

        private void SetActParams(ClientReviseActParams param, Guid clientId, DateTime date)
        {
            try
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.SetClientReviseActParams(param, clientId, date);
            }
            catch (SrvException exception)
            {
                if (MessageBox.Show(string.Format(UserStrings.ServerErr, Environment.NewLine, exception.Message), UserStrings.Err, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.SetActParams(param, clientId, date);
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.Client != null);
            }
        }

        private IBP.PaymentProject2.Common.Client Client
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Client);
            }
        }
    }
}

