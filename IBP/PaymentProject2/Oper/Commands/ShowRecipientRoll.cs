﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowRecipientRoll : ShowWindow<RecipientRollsForm>
    {
        public override bool CanExecute
        {
            get
            {
                return ((this.Payment != null) && (this.Payment.RecipientRollId != 0));
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                IBP.PaymentProject2.Common.Payment payment = this.Payment;
                if (payment == null)
                {
                    throw new OperException(UserStrings.NotChoosenPay);
                }
                if (payment.RecipientRollId == 0)
                {
                    throw new OperException(UserStrings.ThisPayNotIncluded);
                }
                RecipientRollsFilterOper oper = new RecipientRollsFilterOper();
                oper.RollsFilter.RollID = payment.RecipientRollId;
                return oper;
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

