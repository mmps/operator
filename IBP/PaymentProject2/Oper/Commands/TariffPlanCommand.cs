﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Isolation;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;
    using System.Threading;
    using System.Windows.Forms;

    internal abstract class TariffPlanCommand : IBP.PaymentProject2.Oper.Command
    {
        public event EventHandler<TariffPlanEventArgs> TariffPlanChanged;

        protected TariffPlanCommand()
        {
        }

        protected void OnFormReleaseTariffPlan(object sender, TariffPlanEventArgs e)
        {
            ClientTariffPlanEditForm form = (ClientTariffPlanEditForm) sender;
            IsolationCookie isolationCookie = form.IsolationCookie;
            if (isolationCookie != null)
            {
                isolationCookie.Release();
            }
        }

        protected void RaiseTariffPlanChanged(TariffPlanEventArgs args)
        {
            if (this.TariffPlanChanged != null)
            {
                this.TariffPlanChanged(this, args);
            }
        }

        protected void ShowAccessLoseError(ClientTariffPlanEditForm form, IsolationSrvException ex)
        {
            DialogResult result = MessageBox.Show(string.Format(UserStrings.ErrorOnTariffPlanAccessLost, ex.CurrentIsolation.FirstName, ex.CurrentIsolation.SecondName), UserStrings.Error, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            form.CloseForce();
            if (result == DialogResult.Yes)
            {
                EditClientTariffPlan plan = new EditClientTariffPlan();
                plan.Tag = form.TariffPlan;
                plan.Execute();
            }
        }

        protected void ShowSaveTariffPlanError(SrvException ex)
        {
            base.ShowError(UserStrings.ErrorOnSaveTariffPlan, new object[] { ex.Message });
        }
    }
}

