﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows.Forms;

    internal class CreateRecipientRollTransaction : IBP.PaymentProject2.Oper.Command
    {
        public CreateRecipientRollTransaction()
        {
            base._tagIsArray = true;
        }

        private bool CanCreateTransaction(RecipientRoll roll)
        {
            return (((RollStates) roll.State.StateCode) == RollStates.Revised);
        }

        private void CreateTransaction(RecipientRoll roll)
        {
            try
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.CreateRecipientRollTransaction(roll.Id);
            }
            catch (SrvException exception)
            {
                MessageBox.Show(string.Format(UserStrings.TransactionCreationError, roll.Id, roll.Recipient, exception.Message), UserStrings.Error, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void CreateTransactions(object state)
        {
            RollsContext context = (RollsContext) state;
            try
            {
                foreach (RecipientRoll roll in context.Rolls)
                {
                    string stepName = string.Format(UserStrings.TransactionCreationForRecipient, roll.Recipient);
                    context.Dialog.SetStepName(stepName);
                    this.CreateTransaction(roll);
                    context.Dialog.PerformStep();
                }
            }
            finally
            {
                context.Dialog.Close();
            }
        }

        public override void Execute()
        {
            RecipientRoll[] availableRolls = this.AvailableRolls;
            if (availableRolls.Length == 1)
            {
                this.CreateTransaction(availableRolls[0]);
            }
            else
            {
                using (ProgressBarDialog dialog = new ProgressBarDialog(UserStrings.TransactionCreation, string.Empty, availableRolls.Length))
                {
                    RollsContext state = new RollsContext(availableRolls, dialog);
                    ThreadPool.QueueUserWorkItem(new WaitCallback(this.CreateTransactions), state);
                    dialog.ShowDialog();
                }
            }
        }

        private RecipientRoll[] AvailableRolls
        {
            get
            {
                RecipientRoll[] selectedRolls = this.SelectedRolls;
                if (selectedRolls == null)
                {
                    return null;
                }
                List<RecipientRoll> list = new List<RecipientRoll>();
                foreach (RecipientRoll roll in selectedRolls)
                {
                    if (this.CanCreateTransaction(roll))
                    {
                        list.Add(roll);
                    }
                }
                if (list.Count == 0)
                {
                    return null;
                }
                return list.ToArray();
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.AvailableRolls != null);
            }
        }

        private RecipientRoll[] SelectedRolls
        {
            get
            {
                if (base._tag == null)
                {
                    return null;
                }
                object[] sourceArray = (object[]) base._tag;
                RecipientRoll[] destinationArray = new RecipientRoll[sourceArray.Length];
                Array.Copy(sourceArray, destinationArray, sourceArray.Length);
                return destinationArray;
            }
        }

        private class RollsContext
        {
            public readonly ProgressBarDialog Dialog;
            public readonly RecipientRoll[] Rolls;

            public RollsContext(RecipientRoll[] rolls, ProgressBarDialog dialog)
            {
                this.Rolls = rolls;
                this.Dialog = dialog;
            }
        }
    }
}

