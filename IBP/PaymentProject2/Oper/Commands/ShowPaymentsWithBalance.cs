﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper.Filters;

    internal class ShowPaymentsWithBalance : ShowWindow<PaymentForm>
    {
        protected override FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                oper.Filter = PaymentsFilter.WithBalanceOnly;
                return oper;
            }
        }
    }
}

