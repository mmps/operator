﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using System;

    internal sealed class About : Command
    {
        public override void Execute()
        {
            using (AboutBox box = new AboutBox())
            {
                box.ShowDialog();
            }
        }
    }
}

