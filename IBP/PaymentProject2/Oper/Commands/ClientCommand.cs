﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;

    internal class ClientCommand : ItemCommand
    {
        public ClientCommand(bool addMode) : base(addMode)
        {
        }

        protected override object CreateItem()
        {
            return new Client();
        }

        protected override object Save(object item)
        {
            Client client = (Client) item;
            if (client.Serial == 0)
            {
                client = IBP.PaymentProject2.Oper.ServerFasad.Server.AddClient(client);
            }
            else
            {
                client = IBP.PaymentProject2.Oper.ServerFasad.Server.UpdateClient(client);
            }
            ClientsContainer.Instance[client.Id] = client;
            return client;
        }

        public override bool CanExecute
        {
            get
            {
                return ((base._tag is Client) || base._addMode);
            }
        }
    }
}

