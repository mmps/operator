﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.License;
    using IBP.PaymentProject2.Oper;
    using System;

    internal class AddSubDealer : ClientCommand
    {
        public AddSubDealer() : base(true)
        {
        }

        protected override object CreateItem()
        {
            if (base._tag == null)
            {
                throw new InvalidOperationException("tag can not be null");
            }
            Client client = new Client();
            Client client2 = base._tag as Client;
            client.ParentId = client2.Id;
            return client;
        }

        public override void Execute()
        {
            object obj2 = base._tag;
            try
            {
                base.Execute();
            }
            finally
            {
                base._tag = obj2;
            }
        }

        public override bool CanExecute
        {
            get
            {
                if (!IBP.PaymentProject2.Oper.ServerFasad.Server.CheckFeature(FeatureType.SubDealerSchema))
                {
                    return false;
                }
                Client client = base._tag as Client;
                return ((client != null) && client.HasAccount);
            }
        }
    }
}

