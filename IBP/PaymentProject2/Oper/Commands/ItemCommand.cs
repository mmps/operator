﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common.Forms;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Windows.Forms;

    internal abstract class ItemCommand : IBP.PaymentProject2.Oper.Command
    {
        protected bool _addMode;

        public ItemCommand(bool addMode)
        {
            this._addMode = addMode;
        }

        protected abstract object CreateItem();
        public override void Execute()
        {
            object item = null;
            try
            {
                item = this.Item;
                if (item == null)
                {
                    return;
                }
            }
            catch (ArgumentException exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }
            EventHandler handler = null;
            using (ItemForm itemForm = new ItemForm())
            {
                itemForm.Item = item;
                if (handler == null)
                {
                    handler = delegate (object sender, EventArgs e) {
                        object obj2 = this.Save(itemForm.Item);
                        if (obj2 is ICloneable)
                        {
                            obj2 = ((ICloneable) obj2).Clone();
                        }
                        itemForm.Item = obj2;
                        this._tag = obj2;
                    };
                }
                itemForm.Save += handler;
                itemForm.ShowDialog();
                if (this._addMode)
                {
                    base._tag = null;
                }
            }
        }

        protected abstract object Save(object item);

        public override bool CanExecute
        {
            get
            {
                if (!this._addMode)
                {
                    return (base._tag != null);
                }
                return true;
            }
        }

        private object Item
        {
            get
            {
                if (this._addMode)
                {
                    return this.CreateItem();
                }
                if (base._tag == null)
                {
                    throw new InvalidOperationException("tag can not be null in add mode");
                }
                if (base._tag is ICloneable)
                {
                    return ((ICloneable) base._tag).Clone();
                }
                return base._tag;
            }
        }
    }
}

