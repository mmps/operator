﻿using IBP.PaymentProject2.Common.Reconing;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;

    internal class ShowClientTariffPlanHistory : Command
    {
        public override void Execute()
        {
            Client currentClient = this.CurrentClient;
            try
            {
                List<ClientTariffPlanHistoryItem> clientTariffPlanHistory = IBP.PaymentProject2.Oper.ServerFasad.Server.GetClientTariffPlanHistory(currentClient.Id);
                new ClientTariffPlanHistoryForm(currentClient.Name, clientTariffPlanHistory.ToArray()).Show();
            }
            catch (SrvException exception)
            {
                base.ShowError(UserStrings.GetTariffPlanHistoryError, new object[] { currentClient.Name, exception.Message });
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.CurrentClient != null);
            }
        }

        private Client CurrentClient
        {
            get
            {
                return (base._tag as Client);
            }
        }
    }
}

