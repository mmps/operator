﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Windows.Forms;

    internal class ResendRecipientRoll : InterfaceCommand
    {
        public override void Execute()
        {
            base._tag = ServerFasad.Server.ResendRecipientRoll(this.RecipientRoll.Id);
            base.Execute();
            MessageBox.Show(UserStrings.RecipientRollResend, UserStrings.ResendRoll2);
        }

        public override bool CanExecute
        {
            get
            {
                return ((this.RecipientRoll != null) && (((RollStates) this.RecipientRoll.State.StateCode) != RollStates.Closed));
            }
        }

        private IBP.PaymentProject2.Common.Rolls.RecipientRoll RecipientRoll
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Rolls.RecipientRoll);
            }
        }
    }
}

