﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal sealed class ShowSrcPayment : ShowWindow<PaymentForm>
    {
        public override bool CanExecute
        {
            get
            {
                return ((this.Payment != null) && this.Payment.HasParent);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                PaymentsFilter filter = new PaymentsFilter();
                filter.ByTime = false;
                filter.ChildPaymentID = this.Payment.Serial;
                oper.Filter = filter;
                return oper;
            }
        }

        public override bool MenuItemIsVisible
        {
            get
            {
                return false;
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

