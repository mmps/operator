﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using Ibp;
    using IBP.PaymentProject2.Common.Files;
    using IBP.PaymentProject2.Common.Reports;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using ICSharpCode.SharpZipLib.Zip;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security;
    using System.Security.Policy;
    using System.Windows.Forms;

    internal class ReportCommand : IBP.PaymentProject2.Oper.Command
    {
        private void CatchExceptionFromDomain(object sender, UnhandledExceptionEventArgs e)
        {
            using (ReportErrorForm form = new ReportErrorForm((Exception) e.ExceptionObject))
            {
                form.ShowDialog();
            }
        }

        private Dictionary<string, byte[]> DecompressFiles(byte[] data)
        {
            Dictionary<string, byte[]> dictionary2;
            Dictionary<string, byte[]> dictionary = new Dictionary<string, byte[]>();
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (ZipInputStream stream2 = new ZipInputStream(stream))
                {
                    ZipEntry entry;
                    while ((entry = stream2.GetNextEntry()) != null)
                    {
                        byte[] buffer = new byte[0x400];
                        using (MemoryStream stream3 = new MemoryStream())
                        {
                            int num;
                        Label_0029:
                            num = stream2.Read(buffer, 0, buffer.Length);
                            if (num != 0)
                            {
                                stream3.Write(buffer, 0, num);
                                goto Label_0029;
                            }
                            stream3.Flush();
                            dictionary.Add(entry.Name, stream3.ToArray());
                            continue;
                        }
                    }
                    dictionary2 = dictionary;
                }
            }
            return dictionary2;
        }

        public override void Execute()
        {
            using (ReportSelectForm form = new ReportSelectForm(ServerFasad.Server.LoadReport(new ReportFilter())))
            {
                AppDomainSetup setup;
                Evidence evidence;
                string str;
                string str3;
                DomainRunner runner;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    AbsFile file = new AbsFile(new byte[0]);
                    file.Serial = form.SelectedReport.FileId;
                    file = ServerFasad.Server.LoadFile(file);
                    setup = new AppDomainSetup();
                    evidence = new Evidence();
                    evidence.AddHost(new Zone(SecurityZone.MyComputer));
                    string path = Path.Combine(Path.GetTempPath(), "eKassirReports");
                    Directory.CreateDirectory(path);
                    str3 = Path.Combine(path, Path.GetRandomFileName());
                    Directory.CreateDirectory(str3);
                    setup.ApplicationBase = str3;
                    try
                    {
                        string extension = Path.GetExtension(file.Name);
                        if (extension == null)
                        {
                            goto Label_018E;
                        }
                        if (!(extension == ".exe"))
                        {
                            if (extension == ".zip")
                            {
                                goto Label_00F6;
                            }
                            goto Label_018E;
                        }
                        str = Path.Combine(str3, Path.GetRandomFileName());
                        File.WriteAllBytes(str, file.GetMemoryStream().ToArray());
                        goto Label_01BE;
                    Label_00F6:
                        foreach (KeyValuePair<string, byte[]> pair in this.DecompressFiles(file.GetMemoryStream().ToArray()))
                        {
                            string str4 = Path.Combine(str3, pair.Key);
                            if (pair.Value.Length > 0)
                            {
                                File.WriteAllBytes(str4, pair.Value);
                            }
                            else
                            {
                                Directory.CreateDirectory(str4);
                            }
                        }
                        str = Path.ChangeExtension(Path.Combine(str3, Path.GetFileName(file.Name)), ".exe");
                        goto Label_01BE;
                    Label_018E:
                        MessageBox.Show("Данный формат не поддерживается.");
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show("Ошибка запуска отчета:" + exception.Message);
                    }
                }
                return;
            Label_01BE:
                setup.ConfigurationFile = str + ".config";
                UnhandledExceptionEventRepeater repeater = new UnhandledExceptionEventRepeater();
                repeater.UnhandledException += new UnhandledExceptionEventHandler(this.CatchExceptionFromDomain);
                AppDomain domain = AppDomain.CreateDomain("newDomain", evidence, setup);
                try
                {
                    runner = DomainRunner.CreateInstanceInDomain(domain);
                    runner.AddUnhandledExceptionRepeater(repeater);
                }
                catch (Exception exception2)
                {
                    MessageBox.Show("Ошибка запуска отчета: " + exception2.Message);
                    return;
                }
                try
                {
                    runner.ExecuteAssembly = str;
                    runner.Run();
                }
                catch (Exception exception3)
                {
                    using (ReportErrorForm form2 = new ReportErrorForm(exception3))
                    {
                        form2.ShowDialog();
                    }
                }
                finally
                {
                    runner.RemoveUnhandledExceptionRepeater(repeater);
                    runner = null;
                    GC.Collect();
                    AppDomain.Unload(domain);
                    Directory.Delete(str3, true);
                }
            }
        }
    }
}

