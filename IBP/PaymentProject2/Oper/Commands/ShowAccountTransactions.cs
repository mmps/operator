﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal sealed class ShowAccountTransactions : ShowWindow<TransactionForm>
    {
        private IBP.PaymentProject2.Common.Account Account
        {
            get
            {
                if (base._tag == null)
                {
                    return null;
                }
                if (base._tag is Client)
                {
                    Client client = (Client) base._tag;
                    if (client != null)
                    {
                        return client.Account;
                    }
                    return null;
                }
                if (!(base._tag is IBP.PaymentProject2.Common.Account))
                {
                    throw new InvalidOperationException("Tag type is unknown");
                }
                return (IBP.PaymentProject2.Common.Account) base._tag;
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.Account != null);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                TransactionsFilter filter = new TransactionsFilter();
                filter.SourceAccountId = this.Account.Id;
                filter.EndTime = filter.StartTime = DateTime.Now.Date;
                oper.Filter = filter;
                return oper;
            }
        }
    }
}

