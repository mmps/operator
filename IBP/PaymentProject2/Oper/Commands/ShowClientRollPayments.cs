﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowClientRollPayments : ShowWindow<PaymentForm>
    {
        public override bool CanExecute
        {
            get
            {
                return (this.ClientRoll != null);
            }
        }

        private IBP.PaymentProject2.Common.Rolls.ClientRoll ClientRoll
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Rolls.ClientRoll);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                PaymentsFilter withStrictTime = PaymentsFilter.WithStrictTime;
                withStrictTime.DealerOnly = true;
                withStrictTime.ClientRollID = this.ClientRoll.Id;
                withStrictTime.ByTime = false;
                oper.Filter = withStrictTime;
                return oper;
            }
        }
    }
}

