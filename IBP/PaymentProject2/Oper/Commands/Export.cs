﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using Microsoft.Win32;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows.Forms;

    internal abstract class Export : IBP.PaymentProject2.Oper.Command
    {
        private SaveFileDialog _saveDialog;

        protected Export()
        {
        }

        public override void Execute()
        {
            ListView listview = base._tag as ListView;
            if (listview != null)
            {
                string fileName = this.FileName;
                if (!string.IsNullOrEmpty(fileName))
                {
                    ListViewXLSExporter exporter = new ListViewXLSExporter(listview, fileName, this.TabName);
                    exporter.TableExists += delegate (object sender, TableExistsEventArgs e) {
                        e.TableName = e.TableName + "_1";
                    };
                    try
                    {
                        exporter.ExportAsText = false;
                        exporter.CheckedOnly = false;
                        exporter.Export();
                    }
                    catch (SrvException exception)
                    {
                        MessageBox.Show(exception.Message);
                        return;
                    }
                    if (this.OpenFile)
                    {
                        if (this.XlsIsRegistred())
                        {
                            try
                            {
                                Process.Start(fileName);
                                return;
                            }
                            catch (Exception)
                            {
                                this.ShowErrorSaveFile(fileName);
                                return;
                            }
                        }
                        this.ShowErrorSaveFile(fileName);
                    }
                }
            }
        }

        protected string SelectFileName(bool useExistingFile)
        {
            if (this._saveDialog == null)
            {
                this._saveDialog = new SaveFileDialog();
                this._saveDialog.AddExtension = true;
                this._saveDialog.DefaultExt = "xls";
                this._saveDialog.Filter = "Excel files (*.xls)|*.xls";
                this._saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                this._saveDialog.ValidateNames = true;
            }
            this._saveDialog.OverwritePrompt = !useExistingFile;
            this._saveDialog.Title = this.Title;
            if (this._saveDialog.ShowDialog() != DialogResult.OK)
            {
                return null;
            }
            string fileName = this._saveDialog.FileName;
            if (File.Exists(fileName))
            {
                bool flag = true;
                while (flag)
                {
                    Stream stream = null;
                    try
                    {
                        try
                        {
                            stream = File.Open(fileName, FileMode.Open, FileAccess.Write);
                            flag = false;
                        }
                        catch (Exception)
                        {
                            if (MessageBox.Show(UserStrings.FileOccupied, string.Empty, MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                return null;
                            }
                        }
                        continue;
                    }
                    finally
                    {
                        if (stream != null)
                        {
                            stream.Close();
                            stream.Dispose();
                        }
                    }
                }
                if (!useExistingFile)
                {
                    File.Delete(fileName);
                }
            }
            return fileName;
        }

        private void ShowErrorSaveFile(string file)
        {
            base.ShowError(UserStrings.CantOpenFile, new object[] { file, Environment.NewLine });
            if (this.UseTempFile)
            {
                string str = this.SelectFileName(false);
                if (!string.IsNullOrEmpty(str))
                {
                    File.Copy(file, str, true);
                    File.Delete(file);
                }
            }
        }

        protected bool XlsIsRegistred()
        {
            bool flag;
            try
            {
                using (RegistryKey key = Registry.ClassesRoot)
                {
                    key.OpenSubKey(".xls", false);
                    flag = true;
                }
            }
            catch
            {
                flag = false;
            }
            return flag;
        }

        protected abstract bool CanUseExistingFile { get; }

        private string FileName
        {
            get
            {
                if (this.UseTempFile)
                {
                    return (Path.GetTempPath() + Guid.NewGuid().ToString() + ".xls");
                }
                return this.SelectFileName(this.CanUseExistingFile);
            }
        }

        protected abstract bool OpenFile { get; }

        protected abstract string TabName { get; }

        protected abstract string Title { get; }

        protected abstract bool UseTempFile { get; }
    }
}

