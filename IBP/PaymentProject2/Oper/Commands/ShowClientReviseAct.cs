﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Files;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal class ShowClientReviseAct : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            ShowReviseAct(this.ClientReviseAct.Serial);
        }

        public static void ShowReviseAct(int actId)
        {
            IBP.PaymentProject2.Common.Reconing.ClientReviseAct dataSetObject = null;
            Template fullTemplate = null;
            try
            {
                dataSetObject = IBP.PaymentProject2.Oper.ServerFasad.Server.GetFullClientReviseAct(actId);
            }
            catch (SrvException exception)
            {
                if (MessageBox.Show(string.Format(UserStrings.TryAgainQuestionString, exception.ToString(), Environment.NewLine), UserStrings.ErrorReadingReviseRoll, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ShowReviseAct(actId);
                }
                else
                {
                    return;
                }
            }
            try
            {
                TemplateFilter filter = new TemplateFilter();
                filter.ByTime = true;
                filter.StartTime = dataSetObject.StartTime;
                filter.EndTime = dataSetObject.EndTime;
                filter.Type = TemplateType.ClientReviseAct;
                filter.AbsolutlyFind = true;
                fullTemplate = IBP.PaymentProject2.Oper.ServerFasad.Server.GetFullTemplate(filter);
            }
            catch (SrvException exception2)
            {
                if (MessageBox.Show(string.Format(UserStrings.TryAgainQuestionString, exception2.ToString(), Environment.NewLine), UserStrings.ErrorReadingTemplate, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ShowReviseAct(actId);
                }
                else
                {
                    return;
                }
            }
            if ((dataSetObject != null) && (fullTemplate != null))
            {
                ReportForm form = new ReportForm(dataSetObject, fullTemplate);
                form.Text = string.Format(UserStrings.ClientActFormatString, dataSetObject.ClientName, dataSetObject.StartTime, dataSetObject.EndTime);
                form.Show();
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.ClientReviseAct != null);
            }
        }

        private IBP.PaymentProject2.Common.Reconing.ClientReviseAct ClientReviseAct
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Reconing.ClientReviseAct);
            }
        }
    }
}

