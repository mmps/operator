﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowRecipientRollPayments : ShowWindow<PaymentForm>
    {
        public override bool CanExecute
        {
            get
            {
                return (this.RecipientRoll != null);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                FilterOper oper = new FilterOper();
                PaymentsFilter withStrictTime = PaymentsFilter.WithStrictTime;
                withStrictTime.DealerOnly = false;
                withStrictTime.RecipientRollID = this.RecipientRoll.Id;
                withStrictTime.ByTime = false;
                oper.Filter = withStrictTime;
                return oper;
            }
        }

        private IBP.PaymentProject2.Common.Rolls.RecipientRoll RecipientRoll
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Rolls.RecipientRoll);
            }
        }
    }
}

