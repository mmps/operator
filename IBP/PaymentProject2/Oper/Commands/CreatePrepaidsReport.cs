﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;

    internal class CreatePrepaidsReport : Command
    {
        public override void Execute()
        {
            using (StepDialog dialog = new StepDialog())
            {
                StepInfo info = new StepInfo();
                RecipientStartEndTimeStep step = new RecipientStartEndTimeStep(info);
                step.HeaderText = UserStrings.ChooseReceiver;
                dialog.Controls.Add(step);
                dialog.Controls.Add(new PrepaidsReportStep2(info));
                dialog.Text = UserStrings.ReportOnCards;
                dialog.ShowDialog();
            }
        }

        private class StepInfo : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            public StepInfo() : base(null)
            {
                List<Recipient> list = new List<Recipient>();
                foreach (Recipient recipient in RecipientsContainer.Instance.Collection)
                {
                    if (recipient.Contains("key"))
                    {
                        list.Add(recipient);
                    }
                }
                base.allowedRecipients = list.ToArray();
                DateTime time = DateTime.Now.Date.AddMonths(-1);
                base.start = new DateTime(time.Year, time.Month, 1);
                base.end = this.start.AddMonths(1).AddDays(-1.0);
            }
        }
    }
}

