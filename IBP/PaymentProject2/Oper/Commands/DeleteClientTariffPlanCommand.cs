﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using System;

    internal class DeleteClientTariffPlanCommand : Command
    {
        public override void Execute()
        {
            IBP.PaymentProject2.Common.Reconing.ClientTariffPlan clientTariffPlan = this.ClientTariffPlan;
            if (base.ConfirmAction(UserStrings.Deletion, UserStrings.DelTarifPlan, new object[] { clientTariffPlan.Name }))
            {
                try
                {
                    IBP.PaymentProject2.Oper.ServerFasad.Server.DeleteTariffPlan(clientTariffPlan.Id);
                }
                catch (SrvException exception)
                {
                    base.ShowError(UserStrings.DeleteTariffPlanError, new object[] { exception.Message });
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Reconing.ClientTariffPlan clientTariffPlan = this.ClientTariffPlan;
                return ((clientTariffPlan != null) && clientTariffPlan.IsActive);
            }
        }

        private IBP.PaymentProject2.Common.Reconing.ClientTariffPlan ClientTariffPlan
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Reconing.ClientTariffPlan);
            }
        }
    }
}

