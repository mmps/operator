﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;

    internal class RecallPayment : Command
    {
        public override void Execute()
        {
            IBP.PaymentProject2.Common.Payment payment = this.Payment;
            if (payment != null)
            {
                using (RecallPaymentDialog dialog = new RecallPaymentDialog(payment))
                {
                    dialog.ShowDialog();
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Payment payment = this.Payment;
                if (payment == null)
                {
                    return false;
                }
                return ((payment.WorkState != PaymentState.Rejected) && !payment.IsArchieved);
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

