﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Isolation;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms.TariffPlan;
    using System;

    internal class EditClientTariffPlan : TariffPlanCommand
    {
        public override void Execute()
        {
            if (!ClientTariffPlanEditForm.FindAndShowTariffPlanWindow(this.SelectedPlan.Id))
            {
                ClientTariffPlan clientTariffPlan = this.GetClientTariffPlan(this.SelectedPlan.Id);
                if (clientTariffPlan != null)
                {
                    ClientTariffPlanEditForm form = new ClientTariffPlanEditForm(clientTariffPlan, ReconingMethod.GetAvailableMethods(), false);
                    form.LockTariffPlan += new EventHandler<TariffPlanEventArgs>(this.OnFormLockTariffPlan);
                    form.SaveTariffPlan += new EventHandler<TariffPlanEventArgs>(this.OnFormSaveTariffPlan);
                    form.ReleaseTariffPlan += new EventHandler<TariffPlanEventArgs>(this.OnFormReleaseTariffPlan);
                    form.Show();
                }
            }
        }

        private ClientTariffPlan GetClientTariffPlan(int planId)
        {
            try
            {
                return IBP.PaymentProject2.Oper.ServerFasad.Server.GetFullClientTariffPlan(planId);
            }
            catch (SrvException exception)
            {
                base.ShowError(UserStrings.ErrorOnGetTariffPlan, new object[] { exception.Message });
            }
            return null;
        }

        private void OnFormLockTariffPlan(object sender, TariffPlanEventArgs e)
        {
            ClientTariffPlanEditForm form = (ClientTariffPlanEditForm) sender;
            ClientTariffPlan clientTariffPlan = this.GetClientTariffPlan(form.TariffPlan.Id);
            if (clientTariffPlan != null)
            {
                form.TariffPlan = clientTariffPlan;
                form.IsolationCookie = IsolationCookie.Create(IBP.PaymentProject2.Oper.ServerFasad.Server, form.TariffPlan);
            }
            e.Successful = form.IsolationCookie != null;
        }

        private void OnFormSaveTariffPlan(object sender, TariffPlanEventArgs e)
        {
            ClientTariffPlanEditForm form = (ClientTariffPlanEditForm) sender;
            try
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.UpdateClientTariffPlan(form.TariffPlan);
                base.RaiseTariffPlanChanged(e);
                e.Successful = true;
            }
            catch (IsolationSrvException exception)
            {
                base.ShowAccessLoseError(form, exception);
                e.Successful = false;
            }
            catch (SrvException exception2)
            {
                base.ShowSaveTariffPlanError(exception2);
                e.Successful = false;
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.SelectedPlan != null);
            }
        }

        private ClientTariffPlan SelectedPlan
        {
            get
            {
                return (base._tag as ClientTariffPlan);
            }
        }
    }
}

