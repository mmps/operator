﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Threading;
    using System.Windows.Forms;

    internal sealed class RedirectPayment : IBP.PaymentProject2.Oper.Command
    {
        public RedirectPayment()
        {
            base._tagIsArray = true;
        }

        public override void Execute()
        {
            Payment[] selectedPayment = this.SelectedPayment;
            if (selectedPayment.Length == 1)
            {
                this.RedirectSinglePayment(selectedPayment[0]);
            }
            else if (this.IsSingleService(selectedPayment))
            {
                this.RedirectMultyPaymentForOneService(selectedPayment);
            }
            else
            {
                this.RedirectMultiPayments(selectedPayment);
            }
        }

        private bool IsSingleService(Payment[] payments)
        {
            int length = payments.Length;
            Guid serviceId = payments[0].ServiceId;
            for (int i = 1; i < length; i++)
            {
                if (serviceId != payments[i].ServiceId)
                {
                    return false;
                }
            }
            return true;
        }

        private void RedirectCycle(Payment[] pays, string reason, Guid srvId)
        {
            using (ProgressBarDialog dialog = new ProgressBarDialog(UserStrings.RedirectingPayments, string.Empty, pays.Length))
            {
                RedirectPaymentsContext state = new RedirectPaymentsContext(pays, reason, srvId, dialog);
                ThreadPool.QueueUserWorkItem(new WaitCallback(this.RedirectPayments), state);
                dialog.ShowDialog();
            }
        }

        private void RedirectMultiPayments(Payment[] pays)
        {
            using (RedirectMultiPaymentsDialog dialog = new RedirectMultiPaymentsDialog(pays.Length))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string redirectReason = dialog.RedirectReason;
                    this.RedirectCycle(pays, redirectReason, Guid.Empty);
                }
            }
        }

        private void RedirectMultyPaymentForOneService(Payment[] pays)
        {
            int length = pays.Length;
            using (RedirectPaymentsForServiceDialog dialog = new RedirectPaymentsForServiceDialog(length, pays[0].Service))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Guid id = dialog.SelectedService.Id;
                    string redirectReason = dialog.RedirectReason;
                    this.RedirectCycle(pays, redirectReason, id);
                }
            }
        }

        private void RedirectPayments(object state)
        {
            int num = 0;
            RedirectPaymentsContext context = (RedirectPaymentsContext) state;
            try
            {
                foreach (Payment payment in context.Payments)
                {
                    context.Dialog.PerformStep();
                    try
                    {
                        IBP.PaymentProject2.Oper.ServerFasad.Server.RedirectPayment(payment.Serial, (context.ServiceId == Guid.Empty) ? payment.ServiceId : context.ServiceId, payment.Account, context.Reason);
                    }
                    catch (SrvException)
                    {
                        num++;
                    }
                }
                MessageBox.Show(string.Format(UserStrings.RedirectMessageReport, context.Payments.Length, context.Payments.Length - num, num), UserStrings.RedirectingPayments, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            finally
            {
                context.Dialog.Close();
            }
        }

        private void RedirectSinglePayment(Payment pay)
        {
            try
            {
                AvailableServiceGuide availableServices = IBP.PaymentProject2.Oper.ServerFasad.Server.GetAvailableServices(pay.ClientId);
                using (RedirectPaymentDialog dialog = new RedirectPaymentDialog(pay, availableServices))
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            IBP.PaymentProject2.Oper.ServerFasad.Server.RedirectPayment(pay.Serial, dialog.SelectedService.Id, dialog.SelectedAccount, dialog.RedirectReason);
                        }
                        catch (SrvException exception)
                        {
                            base.ShowError(UserStrings.CantRedirectPaymentFormat, new object[] { pay.Serial, exception.Message });
                        }
                    }
                }
            }
            catch (SrvException exception2)
            {
                base.ShowError(UserStrings.CantGetClientAllowedServices, new object[] { pay.Client.Name, exception2.Message });
            }
        }

        public override bool CanExecute
        {
            get
            {
                Payment[] selectedPayment = this.SelectedPayment;
                int length = selectedPayment.Length;
                if (length <= 0)
                {
                    return false;
                }
                bool flag = true;
                for (int i = 0; i < length; i++)
                {
                    flag = flag && (selectedPayment[i].WorkState == PaymentState.Rejected);
                }
                return flag;
            }
        }

        public Payment[] SelectedPayment
        {
            get
            {
                if (base._tag is object[])
                {
                    object[] objArray = (object[]) base._tag;
                    if (objArray[0] is Payment)
                    {
                        int length = objArray.Length;
                        Payment[] paymentArray = new Payment[length];
                        for (int i = 0; i < length; i++)
                        {
                            paymentArray[i] = (Payment) objArray[i];
                        }
                        return paymentArray;
                    }
                    return new Payment[0];
                }
                return new Payment[0];
            }
        }

        private class RedirectPaymentsContext
        {
            public readonly ProgressBarDialog Dialog;
            public readonly Payment[] Payments;
            public readonly string Reason;
            public readonly Guid ServiceId;

            public RedirectPaymentsContext(Payment[] payments, string reason, Guid serviceId, ProgressBarDialog dialog)
            {
                this.Payments = payments;
                this.Dialog = dialog;
                this.Reason = reason;
                this.ServiceId = serviceId;
            }
        }
    }
}

