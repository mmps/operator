﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal sealed class ShowAccountBalance : IBP.PaymentProject2.Oper.Command
    {
        private Info info;

        public override void Execute()
        {
            IBP.PaymentProject2.Common.Account account = this.Account;
            if (account != null)
            {
                this.info = new Info(account.Id);
                using (StepDialog dialog = new StepDialog())
                {
                    dialog.Text = UserStrings.SaldoOnAcc + account.Name;
                    StartEndTimeStep step = new StartEndTimeStep(this.info);
                    step.DateTimeCustomFormat = Application.CurrentCulture.DateTimeFormat.ShortDatePattern;
                    step.DateTimeFormat = DateTimePickerFormat.Custom;
                    step.HeaderText = UserStrings.ChooseDate02 + account.Name;
                    step.BeforeLeaveStepForward += new EventHandler(this.ReceiveData);
                    dialog.Controls.Add(step);
                    ListViewStep step2 = new ListViewStep();
                    step2.ItemObjectType = typeof(BalanceInfo);
                    step2.HeaderText = UserStrings.HistoryOfChange + account.Name;
                    step2.BeforeEnterStepForward += new EventHandler(this.FillData);
                    dialog.Controls.Add(step2);
                    dialog.ShowDialog();
                }
                this.info = null;
            }
        }

        private void FillData(object sender, EventArgs e)
        {
            ListViewStep step = (ListViewStep) sender;
            step.Clear();
            step.AddItems(this.info.History);
        }

        private void ReceiveData(object sender, EventArgs e)
        {
            TransactionsFilter filter = new TransactionsFilter();
            filter.StartTime = this.info.Start.Date;
            filter.EndTime = this.info.End.Date;
            filter.SourceAccountId = this.info.AccountId;
            try
            {
                this.info.History = IBP.PaymentProject2.Oper.ServerFasad.Server.GetBalanceHistory(filter).ToArray();
            }
            catch (SrvException exception)
            {
                throw new StepException(exception.Message);
            }
        }

        private IBP.PaymentProject2.Common.Account Account
        {
            get
            {
                if (base._tag == null)
                {
                    return null;
                }
                if (base._tag is Client)
                {
                    Client client = (Client) base._tag;
                    if (client != null)
                    {
                        return client.Account;
                    }
                    return null;
                }
                if (!(base._tag is IBP.PaymentProject2.Common.Account))
                {
                    throw new Exception("Tag type is unknown");
                }
                return (IBP.PaymentProject2.Common.Account) base._tag;
            }
        }

        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Account account = this.Account;
                return ((account != null) && !account.IsExternal);
            }
        }

        private class Info : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            private int accountId;
            private BalanceInfo[] history;

            public Info(int accountId)
            {
                this.accountId = accountId;
                base.end = DateTime.Now.Date;
            }

            public int AccountId
            {
                get
                {
                    return this.accountId;
                }
            }

            public BalanceInfo[] History
            {
                get
                {
                    return this.history;
                }
                set
                {
                    this.history = value;
                }
            }
        }
    }
}

