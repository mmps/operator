﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Windows.Forms;

    internal class SavePayments : Export
    {
        public override bool CanExecute
        {
            get
            {
                return (((this.Tag != null) && (this.Tag is ListView)) && (((ListView) this.Tag).Items.Count > 0));
            }
        }

        protected override bool CanUseExistingFile
        {
            get
            {
                return PaymentsSettings.Default.SaveToExistingFile;
            }
        }

        protected override bool OpenFile
        {
            get
            {
                return PaymentsSettings.Default.OpenFile;
            }
        }

        protected override string TabName
        {
            get
            {
                return UserStrings.Pays;
            }
        }

        public override object Tag
        {
            get
            {
                return base.Tag;
            }
            set
            {
                if (value is ListView)
                {
                    base.Tag = value;
                }
            }
        }

        protected override string Title
        {
            get
            {
                return UserStrings.ChooseExportFile;
            }
        }

        protected override bool UseTempFile
        {
            get
            {
                return !PaymentsSettings.Default.SelectFile;
            }
        }
    }
}

