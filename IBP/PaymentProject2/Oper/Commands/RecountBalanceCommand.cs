﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Windows.Forms;

    internal class RecountBalanceCommand : IBP.PaymentProject2.Oper.Command
    {
        private Info info;

        public override void Execute()
        {
            IBP.PaymentProject2.Common.Account account = this.Account;
            if (account != null)
            {
                using (StepDialog dialog = new StepDialog())
                {
                    dialog.Text = UserStrings.Recount + account.Name;
                    this.info = new Info();
                    this.info.AccountId = account.Id;
                    StartEndTimeStep step = new StartEndTimeStep(this.info);
                    step.DateTimeCustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
                    step.DateTimeFormat = DateTimePickerFormat.Custom;
                    step.HeaderText = UserStrings.ChooseDate + account.Name;
                    step.BeforeLeaveStepForward += new EventHandler(this.RecountBalance);
                    dialog.Controls.Add(step);
                    InfoStep step2 = new InfoStep(this.info);
                    step2.HeaderText = UserStrings.SaldoRecounted;
                    dialog.Controls.Add(step2);
                    dialog.ShowDialog();
                }
            }
        }

        private void RecountBalance(object sender, EventArgs e)
        {
            try
            {
                IBP.PaymentProject2.Common.Account account = IBP.PaymentProject2.Oper.ServerFasad.Server.RecountInBalance((TransactionsFilter) this.info.Filter);
                AccountsContainer.Instance[account.Id] = account;
            }
            catch (SrvException exception)
            {
                throw new StepException(exception.Message);
            }
        }

        private IBP.PaymentProject2.Common.Account Account
        {
            get
            {
                if (base._tag == null)
                {
                    return null;
                }
                if (base._tag is Client)
                {
                    Client client = (Client) base._tag;
                    if (client != null)
                    {
                        return client.Account;
                    }
                    return null;
                }
                if (!(base._tag is IBP.PaymentProject2.Common.Account))
                {
                    throw new InvalidOperationException("Tag type is unknown");
                }
                return (IBP.PaymentProject2.Common.Account) base._tag;
            }
        }

        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Account account = this.Account;
                return ((account != null) && !account.IsExternal);
            }
        }

        private class Info : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            private int accountId;

            public override string ToString()
            {
                Account account = AccountsContainer.Instance[((TransactionsFilter) this.Filter).SourceAccountId];
                return string.Format(UserStrings.SaldoForAccount, account.Name, account.InBalance);
            }

            public int AccountId
            {
                get
                {
                    return this.accountId;
                }
                set
                {
                    this.accountId = value;
                }
            }

            public override FilterBase Filter
            {
                get
                {
                    TransactionsFilter filter = new TransactionsFilter();
                    filter.StartTime = this.start.Date;
                    filter.SourceAccountId = this.accountId;
                    return filter;
                }
            }
        }
    }
}

