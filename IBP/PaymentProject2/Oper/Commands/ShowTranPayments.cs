﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal sealed class ShowTranPayments : ShowWindow<PaymentForm>
    {
        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Transactions.Transaction tag = this.Tag as IBP.PaymentProject2.Common.Transactions.Transaction;
                return ((tag != null) && (tag.RelationType == RelationTypes.Payment));
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                PaymentsFilter filter = new PaymentsFilter();
                filter.ByTime = false;
                filter.PaymentSerial = this.Transaction.RelationObjectId;
                FilterOper oper = new FilterOper();
                oper.Filter = filter;
                return oper;
            }
        }

        private IBP.PaymentProject2.Common.Transactions.Transaction Transaction
        {
            get
            {
                return (IBP.PaymentProject2.Common.Transactions.Transaction) base._tag;
            }
        }
    }
}

