﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowCardOperation : ShowWindow<CardOperationsForm>
    {
        public override bool CanExecute
        {
            get
            {
                return ((this.Payment != null) && this.Payment.Type.IsCardPayment);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                if (this.Payment == null)
                {
                    throw new OperException(UserStrings.PayNotChoosen);
                }
                if (!this.Payment.Type.IsCardPayment)
                {
                    throw new OperException(UserStrings.NotCardPay);
                }
                CardFilterOper oper = new CardFilterOper();
                oper.CardFilter.PaymentId = this.Payment.Serial;
                return oper;
            }
        }

        public override bool MenuItemIsVisible
        {
            get
            {
                return this.CanExecute;
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

