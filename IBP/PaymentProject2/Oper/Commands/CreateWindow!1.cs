﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Reflection;
    using System.Windows.Forms;

    internal sealed class CreateWindow<TForm> : IBP.PaymentProject2.Oper.Command where TForm: Form
    {
        private bool newWindow;
        private MainForm parent;

        public CreateWindow(MainForm parent, bool newWindow)
        {
            this.parent = parent;
            this.newWindow = newWindow;
        }

        private void CreateNewWindow()
        {
            System.Type type = typeof(TForm);
            TForm local = type.InvokeMember(null, BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly, null, null, null) as TForm;
            local.MdiParent = this.parent;
            local.Tag = this.Tag;
            local.Show();
        }

        public override void Execute()
        {
            if (this.newWindow)
            {
                this.CreateNewWindow();
            }
            else
            {
                foreach (Form form in this.parent.MdiChildren)
                {
                    if (form is TForm)
                    {
                        form.Activate();
                        return;
                    }
                }
                this.CreateNewWindow();
            }
        }
    }
}

