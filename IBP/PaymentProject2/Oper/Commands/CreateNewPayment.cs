﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;

    internal class CreateNewPayment : Command
    {
        public override void Execute()
        {
            using (CreateNewPaymentForm form = new CreateNewPaymentForm())
            {
                form.GetServiceGuide += new CreateNewPaymentForm.GetGuide(this.GetServiceGuide);
                try
                {
                    form.ShowDialog();
                }
                catch (ObjectDisposedException)
                {
                }
            }
        }

        private AvailableServiceGuide GetServiceGuide(Guid clientId)
        {
            try
            {
                return IBP.PaymentProject2.Oper.ServerFasad.Server.GetAvailableServices(clientId);
            }
            catch (SrvException exception)
            {
                base.ShowError(UserStrings.CantGetClientAllowedServices, new object[] { clientId, exception.Message });
                return new AvailableServiceGuide(0, new Guid[0]);
            }
        }
    }
}

