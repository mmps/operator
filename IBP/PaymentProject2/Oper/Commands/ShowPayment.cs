﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Windows.Forms;

    internal class ShowPayment : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            IBP.PaymentProject2.Common.Payment payment = this.Payment;
            if (payment != null)
            {
                if (payment.Count == 0)
                {
                    PaymentsFilter filter = new PaymentsFilter();
                    filter.ByTime = false;
                    filter.PaymentSerial = payment.Serial;
                    payment = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPayments(filter)[0];
                }
                try
                {
                    using (ShowPaymentForm form = new ShowPaymentForm(payment))
                    {
                        form.ShowDialog();
                    }
                }
                catch (SrvException exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.Payment != null);
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

