﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Globalization;
    using System.IO;
    using System.Windows.Forms;

    internal class PrintCommand : IBP.PaymentProject2.Oper.Command
    {
        private string _format = PrintCheckSettings.Default.PaymentFormat;
        private PageSettings _pageSettings;

        public override void Execute()
        {
            this._format = PrintCheckSettings.Default.PaymentFormat;
            try
            {
                if (PrinterSettings.InstalledPrinters.Count == 0)
                {
                    base.ShowError(UserStrings.NoPrinterError, new object[] { string.Empty });
                    return;
                }
            }
            catch (Exception exception)
            {
                base.ShowError(UserStrings.CantGetPrintersList, new object[] { exception.Message });
                return;
            }
            IBP.PaymentProject2.Common.Payment payment = this.Payment;
            try
            {
                payment = this.GetPaymentFromServer(payment.Serial);
            }
            catch (SrvException exception2)
            {
                base.ShowError(exception2.Message, new object[] { string.Empty });
                return;
            }
            this._pageSettings = new PageSettings();
            PrintDocument document = new PrintDocument();
            document.DefaultPageSettings = this._pageSettings;
            document.PrintPage += delegate (object sender, PrintPageEventArgs e) {
                StringReader reader = new StringReader(payment.ToString(this._format, CultureInfo.InvariantCulture));
                int num = 0;
                float num2 = 0f;
                Font font = new Font("Arial", 10f);
                double height = font.GetHeight();
                string text = null;
                int copyCount = PrintCheckSettings.Default.CopyCount;
                int num5 = (e.MarginBounds.Height / 2) + e.MarginBounds.Top;
                while (true)
                {
                    text = reader.ReadLine();
                    if (text == null)
                    {
                        break;
                    }
                    float width = e.Graphics.MeasureString(text, font).Width;
                    num2 = (num2 >= width) ? num2 : width;
                    e.Graphics.DrawString(text, font, Brushes.Black, (float) e.MarginBounds.Left, (float) (e.MarginBounds.Top + (height * num)));
                    if (copyCount == 2)
                    {
                        e.Graphics.DrawString(text, font, Brushes.Black, (float) e.MarginBounds.Left, (float) ((e.MarginBounds.Top + num5) + (height * num)));
                    }
                    num++;
                }
                e.Graphics.DrawRectangle(Pens.Black, (float) (e.MarginBounds.Left - 10), (float) (e.MarginBounds.Top - height), num2 + 10f, (float) (height * (num + 2)));
                if (copyCount == 2)
                {
                    e.Graphics.DrawLine(Pens.Black, e.MarginBounds.Left, num5, e.MarginBounds.Right, num5);
                    e.Graphics.DrawRectangle(Pens.Black, (float) (e.MarginBounds.Left - 10), (float) ((e.MarginBounds.Top - height) + num5), num2 + 10f, (float) (height * (num + 2)));
                }
                reader.Close();
            };
            document.DocumentName = UserStrings.PrintCheck;
            if (PrintCheckSettings.Default.PreView)
            {
                using (PrintPreviewDialog dialog = new PrintPreviewDialog())
                {
                    dialog.Document = document;
                    dialog.Text = UserStrings.PrintCheck;
                    dialog.ShowDialog();
                    dialog.Close();
                    return;
                }
            }
            using (PrintDialog dialog2 = new PrintDialog())
            {
                dialog2.Document = document;
                dialog2.UseEXDialog = true;
                dialog2.ShowDialog();
            }
        }

        private IBP.PaymentProject2.Common.Payment GetPaymentFromServer(int paymentId)
        {
            PaymentsFilter filter = new PaymentsFilter();
            filter.ByTime = false;
            filter.PaymentSerial = paymentId;
            List<IBP.PaymentProject2.Common.Payment> payments = IBP.PaymentProject2.Oper.ServerFasad.Server.GetPayments(filter);
            if (payments.Count != 1)
            {
                throw new InvalidOperationException(string.Format("Count of payments doesn't match. Server has returned {0} payments.", payments.Count));
            }
            IBP.PaymentProject2.Common.Payment payment = payments[0];
            if (payment.Serial != paymentId)
            {
                throw new InvalidOperationException(string.Format("Server return payment this id={0}. Requested id={1}", payment.Serial, paymentId));
            }
            if (payment.Type.IsCardPayment)
            {
                CardOperationsFilter filter2 = new CardOperationsFilter();
                filter2.PaymentId = paymentId;
                List<CardOperation> cardOperations = IBP.PaymentProject2.Oper.ServerFasad.Server.GetCardOperations(filter2);
                if (cardOperations.Count != 1)
                {
                    throw new InvalidOperationException(string.Format("Count of CardOperations doesn't match. Server has returned {0} CardOperations.", cardOperations.Count));
                }
                payment.CardOperation = cardOperations[0];
            }
            return payment;
        }

        public override bool CanExecute
        {
            get
            {
                return (this.Payment != null);
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

