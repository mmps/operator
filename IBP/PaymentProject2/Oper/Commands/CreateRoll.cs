﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;

    internal class CreateRoll : Command
    {
        public override void Execute()
        {
            using (StepDialog dialog = new StepDialog())
            {
                IBP.PaymentProject2.Oper.Forms.StepInfo info = new IBP.PaymentProject2.Oper.Forms.StepInfo();
                dialog.Text = UserStrings.ReestrCreation;
                RecipientStartEndTimeStep step = new RecipientStartEndTimeStep(info);
                step.HeaderText = UserStrings.ChooseReceiver02;
                dialog.Controls.Add(step);
                dialog.Controls.Add(new CreateRollStep2(info));
                dialog.ShowDialog();
            }
        }
    }
}

