﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal sealed class CreatePaymentForm : IBP.PaymentProject2.Oper.Command
    {
        private FilterOper filter;
        private bool newWindow;
        private MainForm parent;

        public CreatePaymentForm(MainForm parent, FilterOper filter, bool newWindow)
        {
            this.parent = parent;
            this.newWindow = newWindow;
            this.filter = filter;
        }

        private void CreateNewWindow()
        {
            PaymentForm form = new PaymentForm();
            form.MdiParent = this.parent;
            form.Tag = this.Tag;
            form.Filter = this.filter;
            form.Show();
        }

        public override void Execute()
        {
            if (this.newWindow)
            {
                this.CreateNewWindow();
            }
            else
            {
                foreach (Form form in this.parent.MdiChildren)
                {
                    if (form is PaymentForm)
                    {
                        form.Activate();
                        return;
                    }
                }
                this.CreateNewWindow();
            }
        }
    }
}

