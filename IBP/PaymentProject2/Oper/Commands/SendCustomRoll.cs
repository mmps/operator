﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class SendCustomRoll : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            using (StepDialog dialog = new StepDialog())
            {
                dialog.Text = UserStrings.ReestrSending;
                StepInfo _info = new StepInfo();
                RecipientStartEndTimeStep step = new RecipientStartEndTimeStep(_info);
                step.HeaderText = UserStrings.ChooseRecepient;
                step.DateTimeCustomFormat = Application.CurrentCulture.DateTimeFormat.FullDateTimePattern;
                step.DateTimeFormat = DateTimePickerFormat.Custom;
                dialog.Controls.Add(step);
                PaymentsStatisticStep step2 = new PaymentsStatisticStep(_info);
                step2.HeaderText = UserStrings.PayInfo;
                dialog.Controls.Add(step2);
                InfoStep step3 = new InfoStep(_info);
                step3.HeaderText = UserStrings.Ready;
                step3.BeforeEnterStepForward += delegate (object sender, EventArgs e) {
                    try
                    {
                        IBP.PaymentProject2.Oper.ServerFasad.Server.SendCustomRoll((PaymentsFilter) _info.Filter);
                    }
                    catch (SrvException exception)
                    {
                        throw new StepException(exception.Message);
                    }
                };
                dialog.Controls.Add(step3);
                dialog.ShowDialog();
            }
        }

        private class StepInfo : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            public StepInfo() : base(null)
            {
                List<Recipient> list = new List<Recipient>();
                foreach (Recipient recipient in RecipientsContainer.Instance.Collection)
                {
                    if (recipient.AllowCustomRoll)
                    {
                        list.Add(recipient);
                    }
                }
                base.allowedRecipients = list.ToArray();
                base.start = this.start.Date;
                base.end = this.end.Date.AddDays(1.0).AddSeconds(-1.0);
            }

            public override string ToString()
            {
                return string.Format(UserStrings.ReestrSended, base.Recipient, this.start.ToString("D"), this.end.ToString("D"));
            }

            public override FilterBase Filter
            {
                get
                {
                    PaymentsFilter withStrictTime = PaymentsFilter.WithStrictTime;
                    withStrictTime.ByStateTime = true;
                    withStrictTime.RecipientID = base.Recipient.Id;
                    withStrictTime.State = PaymentState.Finalized;
                    withStrictTime.StartTime = base.start;
                    withStrictTime.EndTime = base.end;
                    return withStrictTime;
                }
            }

            public override decimal Value
            {
                get
                {
                    return base.StatisticInfo.SumValue;
                }
            }
        }
    }
}

