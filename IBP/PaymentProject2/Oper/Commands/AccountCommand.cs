﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Threading;

    internal class AccountCommand : ItemCommand
    {
        public event EventHandler Update;

        public AccountCommand(bool addMode) : base(addMode)
        {
        }

        protected override object CreateItem()
        {
            return new Account();
        }

        public override void Execute()
        {
            base.Execute();
            if (this.Update != null)
            {
                this.Update(base._tag, null);
            }
        }

        private Account GetAccount(object o)
        {
            if (o == null)
            {
                return null;
            }
            if (o is Client)
            {
                Client client = (Client) o;
                if (client != null)
                {
                    return client.Account;
                }
                return null;
            }
            if (!(o is Account))
            {
                throw new InvalidOperationException("Tag/item type is unknown");
            }
            return (Account) o;
        }

        protected override object Save(object item)
        {
            Account account = this.GetAccount(item);
            if (account == null)
            {
                throw new NullReferenceException("Saving item is null");
            }
            account = (account.Id == 0) ? IBP.PaymentProject2.Oper.ServerFasad.Server.AddAccount(account) : IBP.PaymentProject2.Oper.ServerFasad.Server.UpdateAccount(account);
            AccountsContainer.Instance[account.Id] = account;
            return account;
        }

        public override bool CanExecute
        {
            get
            {
                return (this.GetAccount(base._tag) != null);
            }
        }

        public override object Tag
        {
            get
            {
                return base._tag;
            }
            set
            {
                base._tag = this.GetAccount(value);
            }
        }
    }
}

