﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal class UseBalance : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            IBP.PaymentProject2.Common.Payment payment = this.Payment;
            try
            {
                SourceInfo info;
                Form.ActiveForm.UseWaitCursor = true;
                IBP.PaymentProject2.Oper.ServerFasad.Server.GetSource(payment.Number, true, out info);
                if (info.State == PaymentState.SourceNotExists)
                {
                    throw new SrvException(UserStrings.PaysWithCheck);
                }
                if (info.State == PaymentState.SourceBlocked)
                {
                    throw new SrvException(UserStrings.BlockedCheck);
                }
                if (info.Payment.Balance == 0M)
                {
                    throw new SrvException(UserStrings.CheckHaveNoChange);
                }
                using (CreateNewPaymentForm form = new CreateNewPaymentForm(info.Payment))
                {
                    form.GetServiceGuide += new CreateNewPaymentForm.GetGuide(this.GetServiceGuide);
                    form.ShowDialog();
                }
            }
            catch (SrvException exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                Form.ActiveForm.UseWaitCursor = false;
            }
        }

        private AvailableServiceGuide GetServiceGuide(Guid clientId)
        {
            try
            {
                return IBP.PaymentProject2.Oper.ServerFasad.Server.GetAvailableServices(clientId);
            }
            catch (SrvException exception)
            {
                base.ShowError(UserStrings.CantGetClientAllowedServices, new object[] { clientId, exception.Message });
                return new AvailableServiceGuide(0, new Guid[0]);
            }
        }

        public override bool CanExecute
        {
            get
            {
                return ((this.Payment != null) && (this.Payment.Balance > 0M));
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

