﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowClientRoll : ShowWindow<ClientRollsForm>
    {
        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Payment payment = this.Payment;
                return ((payment != null) && (payment.ClientRollId != 0));
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                IBP.PaymentProject2.Common.Payment payment = this.Payment;
                if (payment == null)
                {
                    throw new OperException(UserStrings.PayNotChoosen);
                }
                if (payment.ClientRollId == 0)
                {
                    throw new OperException(UserStrings.PayNotIncluded);
                }
                ClientRollsFilterOper oper = new ClientRollsFilterOper();
                oper.RollsFilter.RollID = payment.ClientRollId;
                return oper;
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

