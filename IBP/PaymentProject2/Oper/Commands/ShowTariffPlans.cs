﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper.Filters;

    internal class ShowTariffPlans : ShowWindow<ClientTariffPlansForm>
    {
        protected override FilterOper Filter
        {
            get
            {
                ClientTariffPlanFilterOper oper = new ClientTariffPlanFilterOper();
                oper.PlanFilter.ActiveOnly = true;
                oper.PlanFilter.ByTime = false;
                oper.PlanFilter.NamePart = "";
                return oper;
            }
        }
    }
}

