﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowRecipientRollTransaction : ShowWindow<TransactionForm>
    {
        public override bool CanExecute
        {
            get
            {
                return (this.RecipientRoll != null);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                if (this.RecipientRoll == null)
                {
                    throw new OperException(UserStrings.TransNotChoosen);
                }
                TransactionsFilter filter = new TransactionsFilter();
                filter.RelationType = RelationTypes.RecipientRoll;
                filter.RelationObjectId = this.RecipientRoll.Id;
                filter.ByTime = false;
                FilterOper oper = new FilterOper();
                oper.Filter = filter;
                return oper;
            }
        }

        private IBP.PaymentProject2.Common.Rolls.RecipientRoll RecipientRoll
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Rolls.RecipientRoll);
            }
        }
    }
}

