﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using IBP.PaymentProject2.Oper.Properties;
    using System;
    using System.IO;

    internal sealed class HelpCommand : Command
    {
        private HelpForm _helpForm;
        private string fileName;

        public override void Execute()
        {
            if ((this.fileName == null) || !File.Exists(this.fileName))
            {
                this.fileName = Path.Combine(Environment.CurrentDirectory, "PaySystem Operator.mht");
                File.WriteAllBytes(this.fileName, Resources.PaySystem_Operator);
            }
            if ((this._helpForm == null) || this._helpForm.IsDisposed)
            {
                this._helpForm = new HelpForm(this.fileName);
            }
            if (this._helpForm.Visible)
            {
                this._helpForm.WindowState = this._helpForm.WinState;
                this._helpForm.Select();
            }
            else
            {
                this._helpForm.Show();
            }
        }
    }
}

