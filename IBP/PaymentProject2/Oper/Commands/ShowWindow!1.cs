﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;
    using System.Windows.Forms;

    internal abstract class ShowWindow<T> : IBP.PaymentProject2.Oper.Command where T: ResultForm, new()
    {
        private Form mainForm;

        protected ShowWindow()
        {
        }

        public override void Execute()
        {
            try
            {
                this.MainForm.UseWaitCursor = true;
                T local = Activator.CreateInstance<T>();
                local.MdiParent = this.MainForm;
                local.WindowState = FormWindowState.Normal;
                local.Filter = this.Filter;
                local.Show();
            }
            finally
            {
                this.MainForm.UseWaitCursor = false;
            }
        }

        protected abstract FilterOper Filter { get; }

        private Form MainForm
        {
            get
            {
                bool flag = false;
                if (this.mainForm == null)
                {
                    foreach (Form form in Application.OpenForms)
                    {
                        if (form.MdiChildren.Length > 0)
                        {
                            this.mainForm = form;
                            flag = true;
                            break;
                        }
                    }
                }
                if (!flag && (Application.OpenForms.Count == 1))
                {
                    this.mainForm = Application.OpenForms[0];
                }
                return this.mainForm;
            }
        }
    }
}

