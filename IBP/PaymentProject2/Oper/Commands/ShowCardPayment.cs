﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowCardPayment : ShowWindow<PaymentForm>
    {
        public override bool CanExecute
        {
            get
            {
                return (this.CardOperation != null);
            }
        }

        private IBP.PaymentProject2.Common.CardOperation CardOperation
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.CardOperation);
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                if (this.CardOperation == null)
                {
                    throw new OperException(UserStrings.OpenNotChoosen);
                }
                FilterOper oper = new FilterOper();
                PaymentsFilter filter = new PaymentsFilter();
                filter.ByTime = false;
                filter.PaymentSerial = this.CardOperation.PaymentId;
                oper.Filter = filter;
                return oper;
            }
        }
    }
}

