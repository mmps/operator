﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.CommissionLimitation;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Threading;
    using System.Windows.Forms;

    internal class ShowCommissionLimitation : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            if (this.Recipient != null)
            {
                LimitationCollection limitations;
                try
                {
                    LimitationFilter filter = new LimitationFilter();
                    filter.RecipientId = this.Recipient.Id;
                    filter.ByTime = false;
                    limitations = IBP.PaymentProject2.Oper.ServerFasad.Server.LoadLimitation(filter);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    return;
                }
                using (CommissionLimitationForm form = new CommissionLimitationForm(this.Recipient.Name, this.Recipient.Id, limitations))
                {
                    form.ShowDialog();
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                bool flag = Thread.CurrentPrincipal.IsInRole(UserRole.commissionLimitation.ToString());
                return ((this.Recipient != null) && flag);
            }
        }

        private IBP.PaymentProject2.Common.Recipient Recipient
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Recipient);
            }
        }
    }
}

