﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.Controls;
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Component;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using IBP.PaymentProject2.Oper.Settings;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;

    internal class RecountAllBalances : IBP.PaymentProject2.Oper.Command
    {
        private StepDialog dialog;
        private Info info;

        public override void Execute()
        {
            using (StepDialog dialog = new StepDialog())
            {
                this.dialog = dialog;
                dialog.Text = UserStrings.Recount;
                this.info = new Info();
                StartEndTimeStep step = new StartEndTimeStep(this.info);
                step.DateTimeCustomFormat = MainSettings.Default.CultureInfo.DateTimeFormat.ShortDatePattern;
                step.DateTimeFormat = DateTimePickerFormat.Custom;
                step.HeaderText = UserStrings.ChooseDate;
                dialog.Controls.Add(step);
                ListViewStep step2 = new ListViewStep();
                step2.HeaderText = UserStrings.RecountResult;
                step2.ItemObjectType = typeof(AccountWrapper);
                step2.BeforeEnterStepForward += new EventHandler(this.StartRecount);
                step2.ItemAdded += new EventHandler<ItemEventArgs>(this.ItemAdded);
                step2.Size = new Size(650, 550);
                dialog.Controls.Add(step2);
                dialog.ShowDialog();
            }
        }

        private void ItemAdded(object sender, ItemEventArgs e)
        {
            e.Item.BackColor = ((AccountWrapper) e.Tag).Color;
        }

        private void RecountBalances(object control)
        {
            ListViewStep step = (ListViewStep) control;
            Account[] accountArray = AccountsContainer.Instance.ToArray();
            step.Clear();
            this.dialog.DisableControlButtons();
            this.dialog.ProgressBarMaximum = accountArray.Length;
            this.dialog.ProgressBarMinimum = 0;
            this.dialog.ProgressBarStep = 1;
            this.dialog.ShowProgressBar();
            for (int i = 0; i < accountArray.Length; i++)
            {
                Account account = accountArray[i];
                if (!account.IsExternal)
                {
                    this.info.AccountId = account.Id;
                    AccountWrapper wrapper = null;
                    try
                    {
                        account = IBP.PaymentProject2.Oper.ServerFasad.Server.RecountInBalance((TransactionsFilter) this.info.Filter);
                        AccountsContainer.Instance[account.Id] = account;
                        wrapper = new AccountWrapper(account);
                    }
                    catch (SrvException exception)
                    {
                        wrapper = new AccountWrapper(account);
                        wrapper.Error = exception.Message;
                    }
                    step.AddItem(wrapper);
                }
                this.dialog.PerformStep();
            }
            step.ResizeColumns();
            this.dialog.HideProgressBar();
            this.dialog.EnableControlButtons();
        }

        private void StartRecount(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.RecountBalances), sender);
        }

        public override bool CanExecute
        {
            get
            {
                return true;
            }
        }

        private class AccountWrapper
        {
            private Account account;
            private string error = "";

            public AccountWrapper(Account account)
            {
                this.account = account;
            }

            public System.Drawing.Color Color
            {
                get
                {
                    if (!string.IsNullOrEmpty(this.error) && !(this.error == "Ок"))
                    {
                        return System.Drawing.Color.Pink;
                    }
                    return System.Drawing.Color.LightGreen;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Error"), Browsable(true)]
            public string Error
            {
                get
                {
                    return this.error;
                }
                set
                {
                    this.error = value;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Balance"), Browsable(true)]
            public decimal InBalance
            {
                get
                {
                    return this.account.InBalance;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Date"), Browsable(true)]
            public DateTime InBalanceDate
            {
                get
                {
                    return this.account.InBalanceDate;
                }
            }

            [IBP.PaymentProject2.Common.Component.DisplayName(typeof(UserStrings), "Description02"), Browsable(true)]
            public string Name
            {
                get
                {
                    return this.account.Name;
                }
            }
        }

        private class Info : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            private int accountId;

            public override string ToString()
            {
                return UserStrings.Ready;
            }

            public int AccountId
            {
                get
                {
                    return this.accountId;
                }
                set
                {
                    this.accountId = value;
                }
            }

            public override FilterBase Filter
            {
                get
                {
                    TransactionsFilter filter = new TransactionsFilter();
                    filter.StartTime = this.start.Date;
                    filter.SourceAccountId = this.accountId;
                    return filter;
                }
            }
        }
    }
}

