﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using IBP.PaymentProject2.Oper.Settings;
    using System;

    internal class ShowInfoCollect : Command
    {
        private void _step2_BeforeEnterStepForward(object sender, EventArgs e)
        {
            CashCheckStep step = (CashCheckStep) sender;
            string fullDateTimePattern = MainSettings.Default.CultureInfo.DateTimeFormat.FullDateTimePattern;
            step.HeaderText = string.Format(UserStrings.TerminalReport, new object[] { Environment.NewLine, step.StepInfo.Point.Name, step.StepInfo.Filter.StartTime.ToString(fullDateTimePattern), step.StepInfo.Filter.EndTime.ToString(fullDateTimePattern) });
        }

        public override void Execute()
        {
            using (StepDialog dialog = new StepDialog())
            {
                StepInfo info = new StepInfo();
                dialog.Controls.Add(new CollectDialogStep1(info));
                CashCheckStep step = new CashCheckStep(info);
                step.BeforeEnterStepForward += new EventHandler(this._step2_BeforeEnterStepForward);
                step.HeaderText = UserStrings.Report;
                dialog.Controls.Add(step);
                dialog.Text = UserStrings.CheckIncass;
                dialog.ShowDialog();
            }
        }

        private class StepInfo : IBP.PaymentProject2.Oper.Forms.StepInfo
        {
            public override FilterBase Filter
            {
                get
                {
                    PaymentsFilter withStrictTime = PaymentsFilter.WithStrictTime;
                    withStrictTime.StartTime = base.start;
                    withStrictTime.EndTime = base.end;
                    withStrictTime.PointID = base.Point.Id;
                    withStrictTime.ByStateTime = false;
                    return withStrictTime;
                }
            }

            public override decimal Value
            {
                get
                {
                    return base.StatisticInfo.SumAmount;
                }
            }
        }
    }
}

