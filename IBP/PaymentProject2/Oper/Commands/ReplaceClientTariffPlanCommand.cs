﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class ReplaceClientTariffPlanCommand : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            ClientTariffPlan selectedTariffPlan = this.SelectedTariffPlan;
            if (!selectedTariffPlan.IsActive)
            {
                base.ShowWarning(UserStrings.CantReplaceDeletedTariffPlan, new object[] { selectedTariffPlan.Name });
            }
            else
            {
                ClientTariffPlan[] activeTariffPlans;
                try
                {
                    activeTariffPlans = GetActiveTariffPlans(this.SelectedTariffPlan);
                }
                catch (SrvException exception)
                {
                    base.ShowError(UserStrings.CantGetList, new object[] { exception.ToString() });
                    return;
                }
                if (activeTariffPlans.Length != 0)
                {
                    using (AssignPlanForm form = new AssignPlanForm(activeTariffPlans))
                    {
                        form.Tag = UserStrings.ChangeOfTariffPlan;
                        if (form.ShowDialog() == DialogResult.OK)
                        {
                            DateTime assignedTime = form.AssignedTime;
                            ClientTariffPlan assignedTariffPlan = form.AssignedTariffPlan;
                            try
                            {
                                IBP.PaymentProject2.Oper.ServerFasad.Server.ReplaceTariffPlan(selectedTariffPlan.Id, assignedTariffPlan.Id, assignedTime);
                            }
                            catch (SrvException exception2)
                            {
                                base.ShowError(UserStrings.CantChangeTariffPlan, new object[] { exception2.Message });
                            }
                        }
                    }
                }
                else
                {
                    base.ShowError(UserStrings.ThereIsNoTariffPlans, new object[] { string.Empty });
                }
            }
        }

        internal static ClientTariffPlan[] GetActiveTariffPlans(ClientTariffPlan excludablePlan)
        {
            ClientTariffPlanFilter filter = new ClientTariffPlanFilter();
            filter.ActiveOnly = true;
            filter.NamePart = string.Empty;
            filter.ByTime = false;
            List<ClientTariffPlan> clientTariffPlans = IBP.PaymentProject2.Oper.ServerFasad.Server.GetClientTariffPlans(filter);
            if (excludablePlan != null)
            {
                foreach (ClientTariffPlan plan in clientTariffPlans.ToArray())
                {
                    if (plan.Id == excludablePlan.Id)
                    {
                        clientTariffPlans.Remove(plan);
                        break;
                    }
                }
            }
            return clientTariffPlans.ToArray();
        }

        public override bool CanExecute
        {
            get
            {
                return (this.SelectedTariffPlan != null);
            }
        }

        private ClientTariffPlan SelectedTariffPlan
        {
            get
            {
                return (base._tag as ClientTariffPlan);
            }
        }
    }
}

