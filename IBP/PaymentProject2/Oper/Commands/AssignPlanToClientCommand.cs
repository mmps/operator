﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Reconing;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    internal class AssignPlanToClientCommand : IBP.PaymentProject2.Oper.Command
    {
        public AssignPlanToClientCommand()
        {
            base._tagIsArray = true;
        }

        private void AssignPlanToClients(Client[] clients, ClientTariffPlan plan, DateTime assignDate)
        {
            List<Guid> list = new List<Guid>(clients.Length);
            foreach (Client client in clients)
            {
                list.Add(client.Id);
            }
            try
            {
                IBP.PaymentProject2.Oper.ServerFasad.Server.AssignClientTariffPlan(list.ToArray(), plan.Id, assignDate);
            }
            catch (SrvException exception)
            {
                base.ShowError(UserStrings.AssignTariffPlanError, new object[] { plan.Name, exception.Message });
            }
        }

        public override void Execute()
        {
            ClientTariffPlan[] activeTariffPlans;
            Client[] selectedClients = this.SelectedClients;
            ClientTariffPlan currentTariffPlan = null;
            foreach (Client client in selectedClients)
            {
                if (!client.HasDeal)
                {
                    base.ShowError(UserStrings.CantSetTariffPlanDealerHasntDeal, new object[] { client.Name });
                    return;
                }
            }
            try
            {
                activeTariffPlans = ReplaceClientTariffPlanCommand.GetActiveTariffPlans(null);
                if (selectedClients.Length == 1)
                {
                    currentTariffPlan = this.GetCurrentTariffPlan(selectedClients[0], activeTariffPlans);
                }
            }
            catch (SrvException exception)
            {
                base.ShowError(UserStrings.CantGetList, new object[] { exception.Message });
                return;
            }
            if (activeTariffPlans.Length != 0)
            {
                using (AssignPlanForm form = new AssignPlanForm(activeTariffPlans))
                {
                    form.Text = UserStrings.TariffPlan;
                    form.AssignedTariffPlan = currentTariffPlan;
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        this.AssignPlanToClients(selectedClients, form.AssignedTariffPlan, form.AssignedTime);
                    }
                }
            }
            else
            {
                base.ShowError(UserStrings.CantAssignTariffPlan, new object[] { string.Empty });
            }
        }

        private ClientTariffPlan GetCurrentTariffPlan(Client client, ClientTariffPlan[] activePlans)
        {
            List<ClientTariffPlanHistoryItem> clientTariffPlanHistory = IBP.PaymentProject2.Oper.ServerFasad.Server.GetClientTariffPlanHistory(client.Id);
            Comparison<ClientTariffPlanHistoryItem> comparison = delegate (ClientTariffPlanHistoryItem x, ClientTariffPlanHistoryItem y) {
                return x.AssignTime.CompareTo(y.AssignTime);
            };
            clientTariffPlanHistory.Sort(comparison);
            int id = -1;
            foreach (ClientTariffPlanHistoryItem item in clientTariffPlanHistory)
            {
                if (item.AssignTime <= DateTime.Today)
                {
                    id = item.Id;
                }
            }
            foreach (ClientTariffPlan plan in activePlans)
            {
                if (plan.Id == id)
                {
                    return plan;
                }
            }
            return null;
        }

        public override bool CanExecute
        {
            get
            {
                if (this.SelectedClients.Length == 0)
                {
                    return false;
                }
                return true;
            }
        }

        public Client[] SelectedClients
        {
            get
            {
                List<Client> list = new List<Client>();
                object[] objArray = base._tag as object[];
                if (objArray != null)
                {
                    foreach (Client client in objArray)
                    {
                        list.Add(client);
                    }
                }
                return list.ToArray();
            }
        }
    }
}

