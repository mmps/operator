﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal class CreateClientReviseAct : IBP.PaymentProject2.Oper.Command
    {
        protected virtual void CreateAct(IBP.PaymentProject2.Common.Client client, DateTime date, bool showAct)
        {
            int actId = 0;
            try
            {
                actId = IBP.PaymentProject2.Oper.ServerFasad.Server.CreateClientReviseAct(client.Id, date);
                IBP.PaymentProject2.Common.Client[] clientArray = ClientsContainer.Instance.ToArray();
                int length = clientArray.Length;
                for (int i = 0; i < length; i++)
                {
                    if (clientArray[i].ParentId == client.Id)
                    {
                        this.CreateAct(clientArray[i], date, false);
                    }
                }
            }
            catch (ClientReviseActAlreadyCreatedException exception)
            {
                if (MessageBox.Show(string.Format(UserStrings.ReviseActAllreadyExistsQuestion, client.Name), UserStrings.Err, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    actId = exception.ActID;
                }
                else
                {
                    actId = -1;
                }
            }
            catch (SrvException exception2)
            {
                if ((actId == 0) && (MessageBox.Show(string.Format(UserStrings.TryAgainQuestionString, string.Format(UserStrings.WhileCreatingReviseAct, client.Name, exception2.Message), Environment.NewLine), UserStrings.Err, MessageBoxButtons.YesNo) == DialogResult.Yes))
                {
                    this.CreateAct(client, date, showAct);
                }
            }
            if ((actId > 0) && showAct)
            {
                ShowClientReviseAct.ShowReviseAct(actId);
            }
        }

        public override void Execute()
        {
            using (SelectReviseActDateForm form = new SelectReviseActDateForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    this.CreateAct(this.Client, form.StartDate, true);
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                return (this.Client != null);
            }
        }

        private IBP.PaymentProject2.Common.Client Client
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Client);
            }
        }
    }
}

