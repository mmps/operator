﻿using IBP.PaymentProject2.Oper.Forms;

namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Filters;
    using System;

    internal class ShowPaymentTransaction : ShowWindow<TransactionForm>
    {
        public override bool CanExecute
        {
            get
            {
                return ((this.Payment != null) && (this.Payment.Id != Guid.Empty));
            }
        }

        protected override FilterOper Filter
        {
            get
            {
                if (this.Payment == null)
                {
                    throw new OperException(UserStrings.NotChoosenPay);
                }
                TransactionsFilter filter = new TransactionsFilter();
                filter.ByTime = false;
                filter.Origination = TransactionOrigination.eKassir;
                filter.TransactionState = TransactionState.All;
                filter.RelationType = RelationTypes.Payment;
                filter.RelationObjectId = this.Payment.Serial;
                FilterOper oper = new FilterOper();
                oper.Filter = filter;
                return oper;
            }
        }

        private IBP.PaymentProject2.Common.Payment Payment
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Payment);
            }
        }
    }
}

