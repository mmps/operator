﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal class RebuildClientRoll : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            IBP.PaymentProject2.Common.Rolls.ClientRoll clientRoll = this.ClientRoll;
            if (clientRoll != null)
            {
                using (CommentForm form = new CommentForm(UserStrings.Why))
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        ServerFasad.Server.RebuildClientRoll(clientRoll.Id, form.Comment);
                    }
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                IBP.PaymentProject2.Common.Rolls.ClientRoll clientRoll = this.ClientRoll;
                return ((clientRoll != null) && (((RollStates) clientRoll.State.StateCode) == RollStates.NotRevised));
            }
        }

        private IBP.PaymentProject2.Common.Rolls.ClientRoll ClientRoll
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Rolls.ClientRoll);
            }
        }
    }
}

