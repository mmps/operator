﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Oper;
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading;

    internal abstract class InterfaceCommand : Command
    {
        public event UpdateFormEventHandler UpdateForm;

        protected InterfaceCommand()
        {
        }

        public override void Execute()
        {
            if (this.UpdateForm != null)
            {
                this.UpdateForm(this);
            }
        }

        public delegate void UpdateFormEventHandler(InterfaceCommand sender);
    }
}

