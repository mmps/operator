﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common.Rolls;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Forms;
    using System;
    using System.Windows.Forms;

    internal class RebuildRecipientRoll : IBP.PaymentProject2.Oper.Command
    {
        public override void Execute()
        {
            IBP.PaymentProject2.Common.Rolls.RecipientRoll recipientRoll = this.RecipientRoll;
            if (recipientRoll != null)
            {
                using (CommentForm form = new CommentForm(UserStrings.Why))
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        ServerFasad.Server.RebuildRecipientRoll(recipientRoll.Id, form.Comment);
                    }
                }
            }
        }

        public override bool CanExecute
        {
            get
            {
                return ((this.RecipientRoll != null) && (((RollStates) this.RecipientRoll.State.StateCode) == RollStates.NotRevised));
            }
        }

        private IBP.PaymentProject2.Common.Rolls.RecipientRoll RecipientRoll
        {
            get
            {
                return (base._tag as IBP.PaymentProject2.Common.Rolls.RecipientRoll);
            }
        }
    }
}

