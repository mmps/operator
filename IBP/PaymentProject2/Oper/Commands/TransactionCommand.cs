﻿namespace IBP.PaymentProject2.Oper.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Common.Transactions;
    using IBP.PaymentProject2.Oper;
    using System;

    internal class TransactionCommand : ItemCommand
    {
        public TransactionCommand(bool addMode) : base(addMode)
        {
        }

        protected override object CreateItem()
        {
            Transaction transaction = new Transaction();
            transaction.State = TransactionState.ReadyToExport;
            return transaction;
        }

        protected override object Save(object item)
        {
            Transaction transaction = (Transaction) item;
            if (transaction.Id != 0)
            {
                throw new SrvException(UserStrings.EditTransNotSupported);
            }
            transaction = IBP.PaymentProject2.Oper.ServerFasad.Server.AddTransaction(transaction);
            Account account = IBP.PaymentProject2.Oper.ServerFasad.Server.GetAccount(transaction.SourceAccountId);
            AccountsContainer.Instance[account.Id] = account;
            account = IBP.PaymentProject2.Oper.ServerFasad.Server.GetAccount(transaction.DestAccountId);
            AccountsContainer.Instance[account.Id] = account;
            return transaction;
        }
    }
}

