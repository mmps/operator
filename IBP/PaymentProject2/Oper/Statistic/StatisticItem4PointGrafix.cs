﻿namespace IBP.PaymentProject2.Oper.Statistic
{
    using IBP.PaymentProject2.Oper;
    using System;

    public class StatisticItem4PointGrafix : StatisticItemGrafix, IStatisticItem4Point, IStatisticItemGrafix
    {
        private decimal max;
        private decimal middle;
        private decimal min;

        public StatisticItem4PointGrafix()
        {
        }

        public StatisticItem4PointGrafix(string label, decimal total) : base(label, total)
        {
        }

        public StatisticItem4PointGrafix(string label, decimal min, decimal middle, decimal max, decimal total) : base(label, total)
        {
            this.min = min;
            this.middle = middle;
            this.max = max;
        }

        public decimal Max
        {
            get
            {
                return this.max;
            }
            set
            {
                this.max = value;
            }
        }

        public decimal Middle
        {
            get
            {
                return this.middle;
            }
            set
            {
                this.middle = value;
            }
        }

        public decimal Min
        {
            get
            {
                return this.min;
            }
            set
            {
                this.min = value;
            }
        }
    }
}

