﻿namespace IBP.PaymentProject2.Oper.Statistic.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using System;

    [StatisticCommand(IBP.PaymentProject2.Oper.StatisticType.byPoints)]
    internal class byPoints : StatisticCommand
    {
        public byPoints(PaymentsFilter filter)
        {
            base.filter = filter;
            base.filter.PointID = Guid.Empty;
            base.filter.StatisticType = IBP.PaymentProject2.Common.StatisticType.ByPoints;
        }

        protected override StaticticGrafix GetStaticGrafix()
        {
            StaticticGrafix grafix = new StaticticGrafix(base.Discription + " c " + base.filter.StartTime.ToString("dd.MM.yyyy") + " по " + base.filter.EndTime.ToString("dd.MM.yyyy"));
            grafix.FilterLabel = base.GetFiltelLabel();
            grafix.XLabel = UserStrings.Sum;
            grafix.YLabel = UserStrings.PoS;
            foreach (StatisticDataItem item in base.StatisticData.Items)
            {
                grafix.Items.Add(new StatisticItemGrafix(item.Label.ToString(), item.SumTotal));
            }
            return grafix;
        }
    }
}

