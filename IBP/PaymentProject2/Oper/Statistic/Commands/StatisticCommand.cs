﻿namespace IBP.PaymentProject2.Oper.Statistic.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Statistic;
    using System;
    using System.ComponentModel;
    using System.Reflection;
    using System.Text;

    public abstract class StatisticCommand
    {
        protected StatisticInfo[] _statInfoArr;
        protected string description = "";
        protected PaymentsFilter filter;
        protected IBP.PaymentProject2.Oper.Statistic.StatisticData st_data;
        protected IBP.PaymentProject2.Oper.StaticticGrafix st_grfx;

        protected StatisticCommand()
        {
        }

        public static StatisticCommand GetCommand(IBP.PaymentProject2.Oper.StatisticType type, PaymentsFilter filter)
        {
            string description = "";
            object[] customAttributes = type.GetType().GetField(type.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (customAttributes.Length != 0)
            {
                description = (customAttributes[0] as DescriptionAttribute).Description;
            }
            Type type2 = null;
            foreach (Type type3 in typeof(StatisticCommand).Assembly.GetTypes())
            {
                object[] objArray2 = type3.GetCustomAttributes(typeof(StatisticCommandAttribute), false);
                if ((objArray2.Length != 0) && (((StatisticCommandAttribute) objArray2[0]).StatisticType == type))
                {
                    type2 = type3;
                    break;
                }
            }
            StatisticCommand command = type2.InvokeMember(null, BindingFlags.CreateInstance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly, null, null, new object[] { filter }) as StatisticCommand;
            command.description = description;
            return command;
        }

        protected string GetFiltelLabel()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Сервис = ");
            if (this.filter.ServiceID != Guid.Empty)
            {
                builder.Append(ServicesContainer.Instance[this.filter.ServiceID]);
            }
            else
            {
                builder.Append("Все");
            }
            builder.Append(", Клиент = ");
            if (this.filter.ClientID != Guid.Empty)
            {
                builder.Append(ClientsContainer.Instance[this.filter.ClientID]);
            }
            else
            {
                builder.Append("Все");
            }
            builder.Append(", Точка = ");
            if (this.filter.PointID != Guid.Empty)
            {
                builder.Append(PointsContainer.Instance[this.filter.PointID]);
            }
            else
            {
                builder.Append("Все");
            }
            builder.Append(", Статус = ");
            if (this.filter.State != PaymentState.All)
            {
                builder.Append(new EnumTypeInfo<PaymentState>(this.filter.State).ToString());
            }
            else
            {
                builder.Append("Все");
            }
            builder.Append(", Получатель = ");
            if (this.filter.RecipientID != Guid.Empty)
            {
                builder.Append(RecipientsContainer.Instance[this.filter.RecipientID]);
            }
            else
            {
                builder.Append("Все");
            }
            return builder.ToString();
        }

        protected abstract IBP.PaymentProject2.Oper.StaticticGrafix GetStaticGrafix();
        protected virtual IBP.PaymentProject2.Oper.Statistic.StatisticData GetStatisticDate()
        {
            StatisticDataItem item = null;
            IBP.PaymentProject2.Oper.Statistic.StatisticData data = new IBP.PaymentProject2.Oper.Statistic.StatisticData(this.Discription);
            foreach (StatisticInfo info in this.GetStatisticInfo())
            {
                item = new StatisticDataItem(info.Label, info);
                data.Items.Add(item);
            }
            return data;
        }

        protected StatisticInfo[] GetStatisticInfo()
        {
            return IBP.PaymentProject2.Oper.ServerFasad.Server.GetStatisticInfo(this.filter);
        }

        public string Discription
        {
            get
            {
                return this.description;
            }
        }

        public string Period
        {
            get
            {
                return (this.filter.StartTime.ToString("dd MM") + " - " + this.filter.EndTime.ToString("dd MM"));
            }
        }

        public IBP.PaymentProject2.Oper.StaticticGrafix StaticticGrafix
        {
            get
            {
                if (this.st_data == null)
                {
                    this.st_data = this.GetStatisticDate();
                }
                if (this.st_grfx == null)
                {
                    this.st_grfx = this.GetStaticGrafix();
                }
                return this.st_grfx;
            }
        }

        public IBP.PaymentProject2.Oper.Statistic.StatisticData StatisticData
        {
            get
            {
                if (this.st_data == null)
                {
                    this.st_data = this.GetStatisticDate();
                }
                return this.st_data;
            }
        }
    }
}

