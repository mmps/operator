﻿namespace IBP.PaymentProject2.Oper.Statistic.Commands
{
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Commands;
    using IBP.PaymentProject2.Oper.Settings;
    using System;

    internal class StatisticExportCommand : Export
    {
        private string tabName = UserStrings.Statistics;

        public void SetTableName(string tableName)
        {
            this.tabName = tableName;
        }

        protected override bool CanUseExistingFile
        {
            get
            {
                return StatisticSettings.Default.SaveToExistingFile;
            }
        }

        protected override bool OpenFile
        {
            get
            {
                return StatisticSettings.Default.OpenFile;
            }
        }

        protected override string TabName
        {
            get
            {
                return this.tabName;
            }
        }

        protected override string Title
        {
            get
            {
                return UserStrings.ChooseFile;
            }
        }

        protected override bool UseTempFile
        {
            get
            {
                return !StatisticSettings.Default.SelectFile;
            }
        }
    }
}

