﻿namespace IBP.PaymentProject2.Oper.Statistic.Commands
{
    using IBP.PaymentProject2.Oper;
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited=false, AllowMultiple=true)]
    internal sealed class StatisticCommandAttribute : Attribute
    {
        private IBP.PaymentProject2.Oper.StatisticType statistictype;

        public StatisticCommandAttribute(IBP.PaymentProject2.Oper.StatisticType type)
        {
            this.statistictype = type;
        }

        public IBP.PaymentProject2.Oper.StatisticType StatisticType
        {
            get
            {
                return this.statistictype;
            }
        }
    }
}

