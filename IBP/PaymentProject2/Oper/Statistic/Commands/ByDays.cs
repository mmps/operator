﻿namespace IBP.PaymentProject2.Oper.Statistic.Commands
{
    using IBP.PaymentProject2.Common;
    using IBP.PaymentProject2.Oper;
    using IBP.PaymentProject2.Oper.Statistic;
    using System;

    [StatisticCommand(IBP.PaymentProject2.Oper.StatisticType.byDays)]
    internal class ByDays : StatisticCommand
    {
        public ByDays(PaymentsFilter filter)
        {
            base.filter = filter;
            base.filter.StatisticType = IBP.PaymentProject2.Common.StatisticType.Daily;
        }

        protected override StaticticGrafix GetStaticGrafix()
        {
            StaticticGrafix grafix = new StaticticGrafix(base.Discription + " c " + base.filter.StartTime.ToString("dd.MM.yyyy") + " по " + base.filter.EndTime.ToString("dd.MM.yyyy"));
            grafix.FilterLabel = base.GetFiltelLabel();
            grafix.XLabel = UserStrings.Sum;
            grafix.YLabel = UserStrings.Date;
            foreach (StatisticDataItem item in base.StatisticData.Items)
            {
                grafix.Items.Add(new StatisticItemGrafix(item.Label.ToString(), item.SumTotal));
            }
            return grafix;
        }

        protected override StatisticData GetStatisticDate()
        {
            DateTime time = base.filter.StartTime.AddDays((double) (1 - base.filter.StartTime.Day));
            DateTime time2 = time.AddDays(-1.0).AddMonths(1);
            base.filter.StartTime = time;
            base.filter.EndTime = time2;
            return base.GetStatisticDate();
        }
    }
}

