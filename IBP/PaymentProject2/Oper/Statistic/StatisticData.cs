﻿namespace IBP.PaymentProject2.Oper.Statistic
{
    using System;
    using System.Collections.Generic;

    public class StatisticData
    {
        private List<StatisticDataItem> items = new List<StatisticDataItem>();
        private string text = "";

        public StatisticData(string text)
        {
            this.text = text;
        }

        public List<StatisticDataItem> Items
        {
            get
            {
                return this.items;
            }
        }

        public string Text
        {
            get
            {
                return this.text;
            }
        }
    }
}

