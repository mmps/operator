﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.OleDb;
    using System.IO;
    using System.Windows.Forms;

    [Roll(GatewayType.GUPVCKP)]
    public class VckpRoll : Roll
    {
        private OleDbCommand command;

        public VckpRoll()
        {
            base.ext = "b18";
        }

        public override Stream CreateRoll(Payment[] payments)
        {
            string str = Path.GetDirectoryName(base.GetType().Assembly.Location) + @"\";
            OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str + ";Extended Properties=dBASE IV;User ID=Admin;Password=");
            string str3 = "CREATE TABLE test2.dbf (ID_COMP CHAR(2), REC_TYP NUMERIC(1), NP NUMERIC(2), PMTYP NUMERIC(1), VERS DATE, KKC CHAR(9), 'UNKNOWN' NUMERIC(1), S NUMERIC(10,2), SE NUMERIC(10,2), PCH NUMERIC(5),STOT NUMERIC(18,2), PE NUMERIC(9,2), D DATE, NDOC NUMERIC(18), N_REC NUMERIC(6), PK CHAR(1), DUM1 CHAR(10), DUM2 CHAR(10), COD_128 CHAR(50))";
            this.command = new OleDbCommand();
            this.command.CommandText = str3;
            this.command.Connection = connection;
            try
            {
                connection.Open();
            }
            catch (OleDbException exception)
            {
                MessageBox.Show(exception.Message);
                throw new RollException(UserStrings.RegestryErr);
            }
            string str4 = "temp.dbf";
            string str5 = "vckp.dbf";
            if (!File.Exists(str + str5))
            {
                throw new RollException(UserStrings.FileNotFound + str5);
            }
            File.Copy(str + str5, str + str4, true);
            Dictionary<DateTime, List<Payment>> dictionary = new Dictionary<DateTime, List<Payment>>();
            Dictionary<DateTime, decimal> dictionary2 = new Dictionary<DateTime, decimal>();
            List<DateTime> list = new List<DateTime>();
            decimal num = 0M;
            foreach (Payment payment in payments)
            {
                if (dictionary.ContainsKey(payment.InputDate.Date))
                {
                    Dictionary<DateTime, decimal> dictionary3;
                    DateTime time6;
                    dictionary[payment.InputDate.Date].Add(payment);
                    (dictionary3 = dictionary2)[time6 = payment.InputDate.Date] = dictionary3[time6] + payment.Value;
                }
                else
                {
                    List<Payment> list2 = new List<Payment>();
                    list2.Add(payment);
                    dictionary.Add(payment.InputDate.Date, list2);
                    dictionary2.Add(payment.InputDate.Date, payment.Value);
                    list.Add(payment.InputDate.Date);
                }
                num += payment.Value;
            }
            this.Insert("INSERT INTO " + str4 + " (REC_TYP, NP, PMTYP, VERS, KKC, [UNKNOWN], S, SE, PCH, STOT, PE, D, NDOC, N_REC) VALUES(1,0,0,'01/01/2001',18,0,0,0,0," + num.ToString() + ",0,'" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + "',0," + dictionary.Count.ToString() + ")");
            int num2 = 1;
            list.Sort();
            foreach (DateTime time in list)
            {
                List<Payment> list3 = dictionary[time];
                decimal num3 = dictionary2[time];
                this.Insert("INSERT INTO " + str4 + " (REC_TYP, NP, PMTYP, VERS, KKC, [UNKNOWN], S, SE, PCH, STOT, PE, D, NDOC, N_REC) VALUES(2," + num2.ToString() + ",1,'01/01/2001',181,0,0,0,0," + num3.ToString() + ",0,'" + list3[0].InputDate.Day.ToString() + "/" + list3[0].InputDate.Month.ToString() + "/" + list3[0].InputDate.Year.ToString() + "',0," + list3.Count.ToString() + ")");
                int num4 = 1;
                foreach (Payment payment2 in list3)
                {
                    this.Insert("INSERT INTO " + str4 + " (REC_TYP, NP, PMTYP, VERS, [UNKNOWN], S, SE, PCH, STOT, PE, NDOC, N_REC, COD_128) VALUES(3," + num2.ToString() + ",1,'01/01/2001',0,0,0,0,0,0,0," + num4.ToString() + ",'" + payment2.Account + "')");
                    num4++;
                }
                num2++;
            }
            connection.Close();
            FileStream stream = null;
            if (File.Exists(str + str4))
            {
                stream = new FileStream(str + str4, FileMode.Open);
            }
            DateTime time2 = list[list.Count - 1];
            base.filename = "P00" + ((time2.Month < 10) ? ("0" + time2.Month.ToString()) : time2.Month.ToString()) + ((time2.Day < 10) ? ("0" + time2.Day.ToString()) : time2.Day.ToString());
            return stream;
        }

        private void Insert(string query)
        {
            this.command.CommandText = query;
            try
            {
                this.command.ExecuteNonQuery();
            }
            catch (OleDbException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}

