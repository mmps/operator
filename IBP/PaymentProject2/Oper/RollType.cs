﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common.Component;
    using System;

    internal enum RollType
    {
        [Description(typeof(UserStrings), "Basic")]
        Default = 0,
        [Description(typeof(UserStrings), "GUP")]
        GUPVCKP = 1,
        [Description(typeof(UserStrings), "Megaphone")]
        Megafon_spb = 5,
        [Description(typeof(UserStrings), "MTC")]
        MTS = 4,
        [Description(typeof(UserStrings), "NWTelecom")]
        NWTelecom = 3,
        [Description(typeof(UserStrings), "DeltaTelecom")]
        SkyLink_spb = 2
    }
}

