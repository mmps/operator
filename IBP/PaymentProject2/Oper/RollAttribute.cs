﻿namespace IBP.PaymentProject2.Oper
{
    using IBP.PaymentProject2.Common;
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited=false)]
    internal class RollAttribute : Attribute
    {
        private GatewayType type;

        public RollAttribute(GatewayType type)
        {
            this.type = type;
        }

        public GatewayType Type
        {
            get
            {
                return this.type;
            }
        }
    }
}

